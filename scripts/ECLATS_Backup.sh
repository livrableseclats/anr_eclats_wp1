#!/bin/bash                                                                                                                    
# Script permettant de sauvegarder les donnees du repository ECLATS4 sur http://steamerlod.imag.fr/                            

# Récupération des noms des fichiers de backup les plus récents
# option -r pour pouvoir avoir la liste des fichiers du plus récent au plus ancien                                                                                                                             
alfLastExport=`ls -r /data/steamer/ECLATS/backups/graph/alf_*.trig | head -n 1`    
interpLastExport=`ls -r /data/steamer/ECLATS/backups/graph/interp_*.trig | head -n 1` 
repsLastExport=`ls -r /data/steamer/ECLATS/backups/graph/reps_*.trig | head -n 1`                                                          
# Construction des noms des nouveaux fichiers de backup créés par ce script                                                                                                                     
alfNewExport=/data/steamer/ECLATS/backups/graph/alf_$(date +"%Y_%m_%d_%H_%M_%S"_export.trig)
interpNewExport=/data/steamer/ECLATS/backups/graph/interp_$(date +"%Y_%m_%d_%H_%M_%S"_export.trig)  
repsNewExport=/data/steamer/ECLATS/backups/graph/reps_$(date +"%Y_%m_%d_%H_%M_%S"_export.trig)                                           
#echo $alfNewExport " new export"                                                                                               
#                                                                                                  
# Récupération des fichiers backups de la version courante des graphes du repository ECLATS4																								  
# curl -X GET -H "Accept:application/x-trig" "http://steamerlod.imag.fr/repositories/ECLATS4/statements?infer=false" > $newExport
curl -s -X GET -H "Accept:application/x-trig" "http://steamerlod.imag.fr/repositories/ECLATS4/rdf-graphs/service?graph=http:%2F%2Fpurl.org%2Ffr%2Feclats%2Fgraph%23ALF" > $alfNewExport
curl -s -X GET -H "Accept:application/x-trig" "http://steamerlod.imag.fr/repositories/ECLATS4/rdf-graphs/service?graph=http:%2F%2Fpurl.org%2Ffr%2Feclats%2Fgraph%23ALF_interpretations" > $interpNewExport
curl -s -X GET -H "Accept:application/x-trig" "http://steamerlod.imag.fr/repositories/ECLATS4/rdf-graphs/service?graph=http:%2F%2Fpurl.org%2Ffr%2Feclats%2Fgraph%23ALF_USER_PHONETIC_RESPONSES" > $repsNewExport             
#
# Test si les nouveau fichiers sont différents des dernières sauvegarde.
# Dans ce cas ils sont conservés, dans le cas contraire ils sont effacés.
#			  
diff -s $alfNewExport $alfLastExport > /dev/null                                                                                     
if [ $? -eq 0 ]; then                                                                                                          
#     echo "Les fichiers sont identiques"                                                                                      
         rm $alfNewExport                                                                                                         
#else                                                                                                                          
#     echo "Les fichiers sont différents"                                                                                      
fi  

diff -s $interpNewExport $interpLastExport > /dev/null                                                                                     
if [ $? -eq 0 ]; then                                                                                                          
#     echo "Les fichiers sont identiques"                                                                                      
         rm $interpNewExport                                                                                                         
#else                                                                                                                          
#     echo "Les fichiers sont différents"                                                                                      
fi   

diff -s $repsNewExport $repsLastExport > /dev/null                                                                                     
if [ $? -eq 0 ]; then                                                                                                          
#     echo "Les fichiers sont identiques"                                                                                      
         rm $repsNewExport                                                                                                         
#else                                                                                                                          
#     echo "Les fichiers sont différents"                                                                                      
fi                                                                                                                         