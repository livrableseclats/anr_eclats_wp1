# Entrées d'inventaire

Ce repertoire contient différent fichiers de données associées aux entrées d'inventaire des différentes cartes:

* des fichiers csv `mapXXXX_yyyyy_invEntries.csv` contenant les URI et les coordonnées viewport des entrées d'inventaire de la carte 
de n° `XXXX` et d'intitulé `yyyyy`. Ces fichiers ont été utilisé par Ontorefine pour créer les triplets

* des fichiers `mapXXXX_invEntries.ttl` contenant les triplets définissant les entrées d'inventaire et les réponses associées de la carte de n° `XXXX`. 
Ces fichiers ont été générés par l'outil `InventoryEntriesGenerator` à partir des données de boits englobantes fournies par Jordan Drapeau (La Rochelle) et
des données des fichiers excel fournies par les dialectologues (voir projet Data).

Pour chaque entrée d'inventaire nous avons les triplets suivants:

```
@prefix eclats: <http://purl.org/fr/eclats/resource/> .
@prefix eclatsonto: <http://purl.org/fr/eclats/ontology#> .

eclats:map0378 eclatsonto:hasInventoryEntry eclats:inventoryEntry_SP635_map0378.
eclats:inventoryEntry_SP635_map0378 a eclatsonto:InventoryEntry;
              eclatsonto:forSurveyPoint eclats:surveyPoint635;
              eclatsonto:xViewport "0.2693023565487959"^^xsd:double;
              eclatsonto:yViewport "0.8883317771732585"^^xsd:double;
              eclatsonto:hasResponse eclats:response_map0378_635.
eclats:response_map0378_635 a eclatsonto:Response;
	      eclatsonto:rousselotPhoneticForm "ălŭeːt".
```

