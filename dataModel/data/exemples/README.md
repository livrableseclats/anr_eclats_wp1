* **exempleInstanciationModel2.ttl** : un exemple manuel d'instanciation du modèle
* **eclat2.ttl** : un jeu de données contenant toutes les images, les cartes et les intitulés et contextes des intitulés. 
Ces données ont été extraites de la base cartodialect et complétées/corrigées par des données issue de la Digitale Bibliotek d'Insbruck
