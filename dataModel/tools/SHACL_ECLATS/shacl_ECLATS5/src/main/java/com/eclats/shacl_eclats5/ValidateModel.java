package com.eclats.shacl_eclats5;


import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStream;
import java.io.StringWriter;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.Resource;
import org.apache.jena.riot.RDFDataMgr;
import org.apache.jena.riot.RDFFormat;
import org.apache.jena.util.FileUtils;
import org.topbraid.jenax.util.JenaUtil;
import org.topbraid.shacl.validation.ValidationUtil;
import org.topbraid.shacl.vocabulary.SH;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */

/**
 *
 * @author seffarm
 */
public class ValidateModel {
    
    public static void validateModel(InputStream graph, InputStream rules, File reportFile) throws IOException{
    
     // ---------------------- Vérification du graph ALF ------------------- //
        Model dataModel = JenaUtil.createDefaultModel();
        dataModel.read(graph, null, FileUtils.langTurtle);

        Model shapeModel = JenaUtil.createDefaultModel();
        shapeModel.read(rules, null, FileUtils.langTurtle);

        Resource reportResource = ValidationUtil.validateModel(dataModel, shapeModel, true);

        boolean conforms = reportResource.getProperty(SH.conforms).getBoolean();
        if (!conforms) {
            StringWriter stringWriter = new StringWriter();
            RDFDataMgr.write(stringWriter, reportResource.getModel(), RDFFormat.TTL);            
            try (FileWriter writer = new FileWriter(reportFile)) {
                writer.write(stringWriter.toString());
                writer.close();
            }
        } else {
            System.out.println("CONFORMS");
        }
    }    
}
