/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eclats.shacl_eclats5;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import org.eclipse.rdf4j.query.QueryLanguage;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.rio.turtle.TurtleWriter;

/**
 *
 * @author seffarm
 */
public class GenerateALFShaclReport {

    public static void main(String[] args) throws IOException {

        // ---------------------- Requête sur le graph ALF ------------------- //
        String query = String.format(Queries.GRAPH_QUERY, "graph:ALF");
        try (RepositoryConnection connection = Queries.REPOSITORY.getConnection()) {
            TurtleWriter turtleWriter = new TurtleWriter(new FileWriter(new File("src/main/resources/ALF.ttl")));
            connection.prepareGraphQuery(QueryLanguage.SPARQL, query).evaluate(turtleWriter);
        }

        // ---------------------- Validation du graph ALF ------------------- //
        ValidateModel.validateModel(
                GenerateALFShaclReport.class.getClassLoader().getResourceAsStream("ALF.ttl"),
                GenerateALFShaclReport.class.getClassLoader().getResourceAsStream("shapeALF.ttl"),
                new File("shaclReports/reportALF.ttl"));
    }
}
