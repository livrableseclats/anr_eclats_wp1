/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package com.eclats.shacl_eclats5;

import org.eclipse.rdf4j.repository.http.HTTPRepository;

/**
 *
 * @author seffarm
 */
public class Queries {
    /**
     * Lien vers le reposiory ECLATS5
     */
    public static final HTTPRepository REPOSITORY = new HTTPRepository("http://steamerlod.imag.fr/repositories/ECLATS5");

    public static final String GRAPH_QUERY
            = "PREFIX graph: <http://purl.org/fr/eclats/graph#>"
            + "CONSTRUCT { ?s ?p ?o }\n"
            + "WHERE { \n"
            + "    GRAPH %s {\n"
            + "?s ?p ?o .\n"
            + "    }\n"
            + "} ";

      static {
        REPOSITORY.initialize();
        REPOSITORY.setUsernameAndPassword("eclats5", "eclats5$@*");
      }

}
