/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lig.steamer.eclatsdatautils.skos;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import joptsimple.OptionParser;
import joptsimple.OptionSet;
import joptsimple.OptionSpec;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.vocabulary.DCTerms;
import org.apache.jena.vocabulary.OWL2;
import org.apache.jena.vocabulary.RDFS;
import org.apache.jena.vocabulary.SKOS;
import org.apache.jena.vocabulary.SKOSXL;
import org.apache.jena.vocabulary.XSD;
import lig.steamer.eclatsdatautils.vocabulary.ECLATSGramCat;

/**
 *
 * @author natete
 */
public class Util {

    public static void checkFormat(String format) {
        switch (format) {
            case "TURTLE":
            case "TTL":
            case "TRIG":
            case "N3":
            case "JSON-LD":
            case "NT":
            case "RDF∕JSON":
            case "RDF/XML":
            case "N-TRIPLES":
            case "N-TRIPLE":
                break;
            default:
                System.out.println(format + " unknown format");
                System.exit(0);
        }
    }

    public static void helpOption(OptionSet options, OptionParser parser) throws IOException {
        if (options.has("?")) {
            parser.printHelpOn(System.out);
            System.exit(0);
        }
    }

    public static InputStream fileOption(OptionSet options, OptionSpec<File> file) throws FileNotFoundException {
        if (file.values(options).size() != 1) {
            System.out.println("missing input file " + file.values(options).size());
            System.exit(0);
        }
        InputStream in = new FileInputStream(file.value(options));
        if (in == null) {
            throw new IllegalArgumentException("File not found");
        }
        return in;
    }

    public static OptionParser createParser() {
        OptionParser parser = new OptionParser() {
            {
                acceptsAll(Arrays.asList("input", "i"), "inputput type (Turtle, N3, RDF/XML ....)").withRequiredArg().defaultsTo("RDF/XML");
                acceptsAll(Arrays.asList("out", "o"), "output file").withRequiredArg().ofType(File.class);
                acceptsAll(Arrays.asList("h", "?", "help"), "show help").forHelp();

            }
        };

        return parser;
    }

    public static void setPrefix(Model model) {
        // define prefixes used for output
        model.setNsPrefix("eclatsgramcat", ECLATSGramCat.getURI());
        model.setNsPrefix("skos", SKOS.getURI());
        model.setNsPrefix("dct", DCTerms.getURI());
        model.setNsPrefix("owl", OWL2.getURI());
        model.setNsPrefix("xsd", XSD.getURI());
        model.setNsPrefix("rdfs", RDFS.getURI());
        model.setNsPrefix("skosXL", SKOSXL.getURI());
    }

    public static OutputStream outPut(OptionSet options, OptionSpec<File> file) throws FileNotFoundException {
        OutputStream out = System.out;
        if (options.has("out")) {
            out = new FileOutputStream((File) options.valueOf("out"));
        }
        return out;
    }
}
