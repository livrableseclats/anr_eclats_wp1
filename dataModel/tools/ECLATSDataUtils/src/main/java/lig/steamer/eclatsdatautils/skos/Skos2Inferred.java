package lig.steamer.eclatsdatautils.skos;

import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.OutputStream;
import java.util.Arrays;
import java.util.List;
import joptsimple.OptionParser;
import joptsimple.OptionSet;
import joptsimple.OptionSpec;
import org.apache.jena.rdf.model.InfModel;
import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.reasoner.Reasoner;
import org.apache.jena.reasoner.rulesys.GenericRuleReasoner;
import org.apache.jena.reasoner.rulesys.Rule;
import org.apache.jena.query.Dataset;
import org.apache.jena.query.DatasetFactory;
import org.apache.jena.riot.RDFWriter;
import org.apache.jena.riot.Lang;

/**
 * To create a version of ECLATS Gramatical Categories containing inferences
 * usage : Skos2Inferred --rules data/ECLATSRules.rules --formatted=TRIG --out data/inferredGramCat.ttl data/gramCat.rdf
 *
 * @author Philippe Genoud - Marie-Jeanne Natete
 */
public class Skos2Inferred {

    public static void main(String[] args) throws IOException {

        OptionParser parser = Util.createParser();
        parser.acceptsAll(Arrays.asList("formatted", "f"), "output type (Turtle, N3, RDF/XML, TRIG)").withRequiredArg().defaultsTo("TURTLE");
        parser.acceptsAll(Arrays.asList("rules", "r"), "rules for inferrences").withRequiredArg().ofType(File.class);
        OptionSpec<File> file = parser.nonOptions("input file").ofType(File.class);
        parser.posixlyCorrect(true);
        OptionSet options = parser.parse(args);
        Util.helpOption(options, parser);
        InputStream in = Util.fileOption(options, file);
        String outputformat = options.valueOf("formatted").toString().toUpperCase();
        String inputformat = options.valueOf("input").toString().toUpperCase();
        Util.checkFormat(outputformat);
        Util.checkFormat(inputformat);
        Model model = ModelFactory.createDefaultModel();
        model.read(in, null, inputformat);
        List<Rule> rules = Rule.rulesFromURL(options.valueOf("rules").toString());
        Reasoner reasoner = new GenericRuleReasoner(rules);
        InfModel inf = ModelFactory.createInfModel(reasoner, model);
        Util.setPrefix(inf);
        OutputStream out = Util.outPut(options, file);
        Dataset ds = DatasetFactory.create();
        Model defaultModel = ds.getDefaultModel();
        Util.setPrefix(defaultModel);
        ds.addNamedModel("http://purl.org/fr/eclats/graph#VOC_catgram", inf);
       // inf.write(out, outputformat);
        RDFWriter.create().source(ds).lang(Lang.TRIG).output(out);
    }

}
