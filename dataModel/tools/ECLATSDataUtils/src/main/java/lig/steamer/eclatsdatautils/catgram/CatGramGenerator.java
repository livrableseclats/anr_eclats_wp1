package lig.steamer.eclatsdatautils.catgram;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Arrays;
import java.util.Date;
import java.util.HashMap;
import java.util.Map;
import java.util.UUID;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Permet de générer le graphe pour les catégories grammaticales des intitulés de
 * cartes.
 *
 * @author Philippe Genoud - LIG Steamer - Université Grenoble Aples
 */
public class CatGramGenerator {

    private static String CATGRAM_FILENAME = "data/Categories_grammaticales_28-11-19_articles_PhDG.csv";

    private static final String REUSE_TITLE_ELT_TRIPLE = "eclats:%s eclatsonto:hasTitleElement eclats:%s .";

    private static final String LEMME_RDF = "eclats:%s a skos:Concept;\n"
            + "     skos:label \"%s\" .";

    /**
     * clé : mot valeur: URI du titleElement correspondant à ce mot
     */
    private static final Map<String, String> titleElementsMap = new HashMap<>();
    private static final Map<String, String> lemmasMap = new HashMap<>();

    // colonne C --> 3    mapTitleURI
    private final static int MAP_URI = 2;
    // colonne D --> 3    mapTitleURI
    private final static int MAP_TITLE_URI = 3;
    // colonne E --> 4    title element URI
    private final static int TITLE_ELT_URI = 4;
    // colonne F --> 5    mot
    private final static int MOT = 5;
    // colonne G --> 6    lemme
    private final static int LEMME = 6;
    // colonne J --> 9    ressourceCatGram URI
    private final static int CAT_GRAM_URI = 9;

    private final static int GENRE = 10;
    private final static int NOMBRE = 11;
    private final static int MODE = 12;
    private final static int TEMPS = 13;
    private final static int PERSONNE = 14;
    private final static int GROUPE = 15;

    private static final String[] PREDICATE = {
        "catgram:genre",
        "catgram:nombre",
        "catgram:mode",
        "catgram:temps",
        "catgram:personne",
        "catgram:groupe"
    };

    private static final Map<String, String> PROP_GRAM_MAP = Stream.of(new String[][]{
        // GENRE
        {"feminin", "catgram:genreFeminin"},
        {"masculin", "catgram:genreMasculin"},
        // NOMBRE
        {"singulier", "catgram:nombreSingulier"},
        {"pluriel", "catgram:nombrePluriel"},
        // MODE
        {"conditionnel", "catgram:modeConditionnel"},
        {"imperatif", "catgram:modeImperatif"},
        {"indicatif", "catgram:modeIndicatif"},
        {"inf", "catgram:modeInfinitif"}, 
        {"participe", "catgram:modeParticipe"},
        {"subjonctif", "catgram:modeSubjonctif"},
        // TEMPS
        {"futur", "catgram:tempsFutur"},
        {"imparfait", "catgram:tempsImparfait"},
        {"passe", "catgram:tempsPasse"},
        // PERSONNE
        {"present", "catgram:tempsPresent"},
        {"personne1", "catgram:personne1"},
        {"personne2", "catgram:personne2"},
        {"personne3", "catgram:personne3"},
        {"personne4", "catgram:personne4"},
        {"personne5", "catgram:personne5"},
        {"personne6", "catgram:personne6"},
        // GROUPE
        {"auxiliaire", "catgram:groupeAuxiliaire"},
        {"groupe1", "catgram:groupe1"},
        {"groupe2", "catgram:groupe2"},
        {"groupe3", "catgram:groupe3"},}).collect(Collectors.toMap(data -> data[0], data -> data[1]));

    // colonne P --> 15
    /**
     *
     * @param path chemin relatif pour accéder au fichier texte
     * @return String contenant le contenu du fichier texte
     * @throws IOException
     */
    private static String loadFromFile(String path) throws IOException {
        return new String(Files.readAllBytes(Paths.get(path)),StandardCharsets.UTF_8);
    }

    private static String toLocalName(String uri) {
        int index = uri.lastIndexOf('/');
        return uri.substring(index + 1);
    }

    private static void println(BufferedWriter bw, String s) throws IOException {
        bw.write(s);
        bw.write("\n");
    }

    private static void print(BufferedWriter bw, String s) throws IOException {
        bw.write(s);
    }

    public static void main(String[] args) throws IOException {
        // lecture du template TURTLE
        String template = loadFromFile("src/main/resources/CatGramTemplate.ttl");
        String outputFileHeader = loadFromFile("src/main/resources/OutputFileHeader.ttl");
        System.out.println(template);

        File file = new File(System.getProperty("user.dir") + "/" + CATGRAM_FILENAME);
        BufferedReader br = new BufferedReader(new InputStreamReader(
                      new FileInputStream(file), StandardCharsets.UTF_8));

        try ( BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(new File(System.getProperty("user.dir") + "/data/titlesData.trig")), StandardCharsets.UTF_8))) {
            // on écrit en tête du fichier de sortie
            println(bw, String.format(outputFileHeader, new Date()));
            // on saute la première ligne
            br.readLine();
            String line;
            int ligne = 1;
            while ((line = br.readLine()) != null) {
                ligne++;
                String[] tokens = line.split(",");
                if (ligne == 63) {
                    System.out.println(Arrays.toString(tokens));
                }

                println(bw, "####### ligne " + ligne + " " + tokens[MAP_URI]);
                String titleElementURI = titleElementsMap.get(tokens[MOT]);
                if (titleElementURI == null) {
                    // il n'existe pas encore de titleElement pour ce mot
                    // il faut le créer
                    titleElementURI = UUID.randomUUID().toString();

                    String lemmeURI = lemmasMap.get(tokens[LEMME]);
                    if (lemmeURI == null) {
                        lemmeURI = UUID.randomUUID().toString();
                        lemmasMap.put(tokens[LEMME], lemmeURI);
                        println(bw, String.format(LEMME_RDF, lemmeURI, tokens[LEMME]));

                    }
                    print(bw, String.format(template, titleElementURI, tokens[MOT], lemmeURI,
                            tokens[CAT_GRAM_URI]));

                    for (int i = GENRE; i <= GROUPE && i < tokens.length; i++) {
                        if (tokens[i] != null && !"".equals(tokens[i])) {
                            
                            print(bw,";\n        " + PREDICATE[i - GENRE] + " " + PROP_GRAM_MAP.get(tokens[i]));
                            if (PROP_GRAM_MAP.get(tokens[i]) == null) {
                                throw new IllegalArgumentException("ligne " + ligne + "propriété inconnue " + tokens[i]);
                            }
                        }
                    }
                    println(bw, ".");
                }
                println(bw, String.format(REUSE_TITLE_ELT_TRIPLE, toLocalName(tokens[MAP_TITLE_URI]), titleElementURI));
            }
            println(bw,"}");
        }
    }

}
