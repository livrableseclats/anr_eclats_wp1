package lig.steamer.eclatsdatautils.inventorpoints;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Méthodes utilitaires pour charger les fichiers de données
 *
 * @author Philippe Genoud - LIG Steamer - Université Grenoble Aples
 */
public class InventoryPointsLoader {

    private static final int IP_PREFIX_LENGTH = "http://purl.org/fr/eclats/resource/surveyPoint".length();

    /**
     * taille de l'image
     */
    private final double imageWidth;
    private final double imageHeight;

    public InventoryPointsLoader(double imageWidth, double imageHeight) {
        this.imageWidth = imageWidth;
        this.imageHeight = imageHeight;
    }
    
    public InventoryPointsLoader() {
        this(-1,-1); // la taille de l'image n'a pas d'importance
    }

    /**
     * Création d'une entrée d'inventaire à partir de sa boite englobante
     *
     * @param noLigne le numéro de ligne où se trouve la définition de l'entrée
     * d'inventaire dans le fichier de données servant à créer l'objet.
     * @param upperLeftX abscisse (en pixels) du coin supérieur gauche de la
     * boite englobante
     * @param upperLeftY ordonnée (en pixels) du coin supérieur gauche de la
     * boite englobante
     * @param width largeur (en pixels) de la boite englobante
     * @param height hauteur (en pixels) de la boite englobante
     * @return
     */
    public InventoryPoint calculateIventoryPoint(int noLigne, int upperLeftX, int upperLeftY, int width, int height) {
        double x = (upperLeftX + (width / 2.0)) / imageWidth;
        double y = (upperLeftY + (height / 2.0)) / imageWidth;  // attention les coordonnées viewport sont fixées par rapport
        // à la largeur de l'image
        return new InventoryPoint(noLigne, x, y);
    }

    /**
     * Crée une liste des entrées d'inventaire définies dans un fichier de
     * données csv au format de ligne suivant. Pour le moment les entrées
     * d'inventaire sont associées au point d'enquête -1.
     *
     * @param fileName le nom du fichier de données à charger
     * @return la liste des entrées d'inventaire.
     * @throws FileNotFoundException
     * @throws IOException
     */
    public List<InventoryPoint> loadInventoryPoints(String fileName) throws FileNotFoundException, IOException {
        File file = new File(System.getProperty("user.dir") + "/" + fileName);
        BufferedReader br = new BufferedReader(new FileReader(file));
        String line;
        List<InventoryPoint> res = new ArrayList<>();
        int noLigne = 1;
        while ((line = br.readLine()) != null) {
            if (line.startsWith("#")) {
                // ligne de commentaire on la saute
            } else if (line.startsWith("M")) {
               // c'est un point d'enquête manquant pour lequel on a rajouté les coordonnées viewport manuellement
               String[] tokens = line.split(",|;");
               res.add(new InventoryPoint(noLigne, Double.parseDouble(tokens[1]), Double.parseDouble(tokens[2])));
            } else {

                String[] tokens = line.split(",|;");
                res.add(calculateIventoryPoint(
                        noLigne,
                        Integer.parseInt(tokens[0]),
                        Integer.parseInt(tokens[1]),
                        Integer.parseInt(tokens[2]),
                        Integer.parseInt(tokens[3])
                )
                );
            }
            noLigne++;
        }
        return res;
    }

//    public List<InventoryPoint> loadInventoryPoints4Points(String fileName) throws FileNotFoundException, IOException {
//        File file = new File(System.getProperty("user.dir") + "/data/" + fileName);
//        BufferedReader br = new BufferedReader(new FileReader(file));
//
//        String line;
//        List<InventoryPoint> res = new ArrayList<>();
//        int noLigne = 1;
//        while ((line = br.readLine()) != null) {
//            if (line.startsWith("#")) {
//                // on la saute
//            } else {
//                String[] tokens = line.split(";");
//                String[] pt1Tokens = tokens[0].split(",");
//                int x1 = Integer.parseInt(pt1Tokens[0]);
//                int y1 = Integer.parseInt(pt1Tokens[1]);
//                String[] pt2Tokens = tokens[1].split(",");
//                int x2 = Integer.parseInt(pt2Tokens[0]);
//                int y2 = Integer.parseInt(pt2Tokens[1]);
//                String[] pt3Tokens = tokens[2].split(",");
//                int x3 = Integer.parseInt(pt1Tokens[0]);
//                int y3 = Integer.parseInt(pt1Tokens[1]);
//                res.add(calculateIventoryPoint(noLigne, x1, y1, x2 - x1, y3 - y2));
//            }
//            noLigne++;
//        }
//        return res;
//    }
    /**
     *
     * @param fileName
     * @return
     * @throws FileNotFoundException
     * @throws IOException
     */
    public List<InventoryPoint> loadReferenceInventoryPoints(String fileName) throws FileNotFoundException, IOException {
        File file = new File(System.getProperty("user.dir") + "/data/" + fileName);
        //      BufferedReader br = new BufferedReader(new FileReader(file));
        BufferedReader br = new BufferedReader(new InputStreamReader(
                new FileInputStream(file), "UTF-8"));
        String line;
        List<InventoryPoint> res = new ArrayList<>();
        br.readLine(); // on saute la première ligne qui contient les noms de colonnes
        int noLigne = 1;
        while ((line = br.readLine()) != null) {
            if (line.startsWith("#")) {
                // on la saute
            } else {
                String[] tokens = line.split(",");
                res.add(new InventoryPoint(
                        noLigne,
                        Double.parseDouble(tokens[2]),
                        Double.parseDouble(tokens[3]),
                        Integer.parseInt(tokens[1].substring(IP_PREFIX_LENGTH))
                )
                );
            }
            noLigne++;
        }
        return res;
    }

    /**
     *
     * @param fileName le nom du fichier contenant les réponses phonétiques
     * @param numColNoPE le numérode la colonne contenant le numéro du PE
     * @param numColRepPhonetique le numéro de la colonne contenant le réponse phonétique
     * @return
     * @throws FileNotFoundException
     * @throws IOException
     */
    public Map<Integer, String> loadResponses(String fileName, int numColNoPE, int numColRepPhonetique) throws FileNotFoundException, IOException {
        File file = new File(System.getProperty("user.dir") + "/" + fileName);
        //BufferedReader br = new BufferedReader(new FileReader(file));
        BufferedReader br = new BufferedReader(new InputStreamReader(
                new FileInputStream(file), "UTF-8"));
        String line;
        Map<Integer, String> res = new HashMap<>();
        br.readLine(); // on saute la première ligne qui contient les noms de colonnes
        while ((line = br.readLine()) != null) {
            if (line.startsWith("#")) {
                // on la saute
            } else {
                String[] tokens = line.split(",|;");
                res.put(Integer.parseInt(tokens[numColNoPE]), tokens[numColRepPhonetique]);
            }
        }
        return res;
    }

}
