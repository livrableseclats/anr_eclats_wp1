package lig.steamer.eclatsdatautils.inventorpoints;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Génère les coordonnées viewport des PE à partir des données fournies par
 * Jordan Drapeau (univ. La Rochelle) et les associe à un numéro de PE.
 *
 * @author Philippe Genoud - LIG Steamer - Université Grenoble Aples
 */
public class FakeInventoryEntriesGenerator {

    private static final int POINT_OUEST = 493;
    private static final int POINT_EST = 987;
    private static final int POINT_NORD = 297;
    private static final int POINT_SUD = 796;

    /**
     * le numero de la carte que l'on veut évaluer
     */
    private static final String[] MAP_NUM = {
        "1022", // carte 1022 Pissenlit
        "0872", // carte 0872 Molène
        "0274", // carte 0274 Chèvrefeuille
        "0278", // carte 278 Chiendent
        "0888", // carte 888 Muflier
        "0969", // carte 969 Paquerette
        "0112", // carte 112 Bardanne
        "0139", // carte 139 Bleuet
        "0747", // carte 747 Laiteron
        "1027", // Carte 1027 Plantain
        "1092", // Carte 1092 Primevère 
        "0828", // Carte 828 Mauve
        "0837", // Carte 837 Menthe
        // Carte 1705 Sainfoin  1/2 carte
        "1222", // Carte 1222 Serpolet
    };

    // x du point 493 dans la carte cible
    private static double[] xOuestCible = {
        0.040836230860905945, // carte 1022 Pissenlit
        0.033852604041281764, // carte 0872 Molène
        0.05322047291777911, // carte 0274 Chèvrefeuille
        0.026698933794841305, // carte 278 Chiendent
        0.029706174785592468, // carte 888 Muflier
        0.05900375309450329, // carte 969 Paquerette
        0.05615294536564678, // carte 112 Bardanne
        0.055356730322581996, // carte 139 Bleuet
        0.03710508709724562, // carte 747 Laiteron
        0.034345318886143644 , // Carte 1027 Plantain
        0.04547487225959945, // Carte 1092 Primevère
        0.03658835637781827, // Carte 828 Mauve
        0.044396838448491946, // Carte 837 Menthe
        // Carte 1705 Sainfoin  1/2 carte
        0.047244021493950863, // Carte 1222 Serpolet
    };

    // x du point 987 dans la carte cible
    private static final double[] xEstCible = {
        0.9255976870653845, // 1022
        0.9193877376599966, // 0872
        0.9266488170121624, // carte 0274 Chèvrefeuille
        0.9151885858516168, // carte 278 Chiendent
        0.9191112972865233, // carte 888 Muflier
        0.9271439279843916, // carte 969 Paquerette
        0.938471675789356, // carte 112 Bardanne
        0.9257508091225393, // carte 139 Bleuet
        0.9237016150780896, // carte 747 Laiteron
        0.9194628265628751, // Carte 1027 Plantain
        0.9292712111356012, // Carte 1092 Primevère 
        0.9211894019425764, // Carte 828 Mauve
        0.932501407010679, // Carte 837 Menthe
       // Carte 1705 Sainfoin  1/2 carte
        0.9269160731427597, // Carte 1222 Serpolet
    };

    // y du point 297 dans la carte cible
    private static final double[] yNordCible = {
        0.07217534879497921, // 1022 
        0.0657965076710459, // 0872
        0.0806124124158623, // carte 0274 Chèvrefeuille
        0.07989047865866786, // carte 278 Chiendent
        0.07300305396578388, // carte 888 Muflier
        0.08668874200351855, // carte 969 Paquerette
        0.05946028941155923, // carte 112 Bardanne
        0.08160809581138677, // carte 139 Bleuet
        0.11196896615107212, // carte 747 Laiteron
        0.07368309998616612, // Carte 1027 Plantain
        0.07086552269985698, // Carte 1092 Primevère
        0.07073921576629008, // Carte 828 Mauve
        0.06869530632603554, // Carte 837 Menthe
        // Carte 1705 Sainfoin  1/2 carte
        0.08465755779363135, // Carte 1222 Serpolet
        
    };
    
    // y du point 796 dans la carte cible
    private static final double[] ySudCible = {
        1.1688444852703164, // 1022
        1.1626331316112903, // 0872
        1.1175238776221406, // carte 0274 Chèvrefeuille
        1.1758868136669811, // carte 278 Chiendent
        1.1698251127763573, // carte 888 Muflier
        1.11686824927738, // carte 969 Paquerette
        1.1512834917218115, // carte 112 Bardanne
        1.1137478566251846, // carte 139 Bleuet
        1.2084044310225786, // carte 747 Laiteron carte coupée en bas !!
        1.1698879051218787, // Carte 1027 Plantain
        1.1683246832362666, // Carte 1092 Primevère
        1.1696930709461177, // Carte 828 Mauve
        1.1667911880808126, // Carte 837 Menthe
        1.1272888918619666, // Carte 1222 Serpolet
    };

    /**
     * le fichier ttl qui contient les SurveyPoints créés pour la carte
     */
    private static final String OUTPUT_FILE_NAME = "/data/fakefullSurveyEntriesPlantes.ttl";

    /**
     * le fichier qui contient les points de référence que l'on va alligner la
     * carte cible (on prend la carte Dauphinelle)
     */
    private static final String REF_FILE_NAME = "Image0378Data.csv";

    private static final String RDF_STRING = ""
            + "<http://purl.org/fr/eclats/resource/map%s> eclatsonto:hasInventoryEntry <http://purl.org/fr/eclats/resource/inventoryEntry_SP%s_map%s>.\n"
            + "    <http://purl.org/fr/eclats/resource/inventoryEntry_SP%s_map%s> a eclatsonto:InventoryEntry;\n"
            + "              eclatsonto:forSurveyPoint <http://purl.org/fr/eclats/resource/surveyPoint%s>;\n"
            + "              eclatsonto:xViewport \"%s\"^^xsd:double;\n"
            + "              eclatsonto:yViewport \"%s\"^^xsd:double;\n"
            + "              eclatsonto:hasResponse <http://purl.org/fr/eclats/resource/%s>.\n"
            + "	 <http://purl.org/fr/eclats/resource/%s> a eclatsonto:Response;\n"
            + "				  eclatsonto:rousselotPhoneticForm \"%s\".\n";

    public static final String PREFIX = ""
            + "@prefix eclats: <http://purl.org/fr/eclats/resource/> .\n"
            + "@prefix eclatsonto: <http://purl.org/fr/eclats/ontology#> .\n";

    /**
     * renvoie la chaine RDF en turtle correspondant à un point d'inventaire
     *
     * @param mapNum le numéro de la carte
     * @param pt le point d'inventaire
     * @return
     */
    private static String printRDF(String mapNum, InventoryPoint pt) {
        String responseId = UUID.randomUUID().toString();
        return String.format(RDF_STRING, mapNum, pt.getNum(), mapNum,
                pt.getNum(), mapNum, pt.getNum(), pt.getX(), pt.getY(), responseId, responseId, "forme phonétique non encore renseignée");
    }

    public static void main(String[] args) throws FileNotFoundException, IOException {

        InventoryPointsLoader loader = new InventoryPointsLoader();

        // charge les Entrées d'inventaire de référence
        List<InventoryPoint> refInventoryPoints = loader.loadReferenceInventoryPoints(REF_FILE_NAME);

        // recherche des points de référence pour allignement
        double xOuestRef = find(refInventoryPoints, POINT_OUEST).getX();
        double xEstRef = find(refInventoryPoints, POINT_EST).getX();
        double yNordRef = find(refInventoryPoints, POINT_NORD).getY();
        double ySudRef = find(refInventoryPoints, POINT_SUD).getY();

        try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(new File(System.getProperty("user.dir") + String.format(OUTPUT_FILE_NAME, MAP_NUM))), "UTF8"))) {

            bw.write(PREFIX);

            for (int mapNum = 0; mapNum < MAP_NUM.length; mapNum++) {
                // calcul des fonctions d'interpolation pour la carte mapNum
                LinearInterpolator xInterpolator = new LinearInterpolator(xOuestRef, xOuestCible[mapNum], xEstRef, xEstCible[mapNum]);
                LinearInterpolator yInterpolator = new LinearInterpolator(yNordRef, yNordCible[mapNum], ySudRef, ySudCible[mapNum]);

                // calcule par interpolation des coordonnées des points d'inventaire sur la carte mapNum
                List<InventoryPoint> newInventoryPoints = new ArrayList<>();
                for (InventoryPoint pt : refInventoryPoints) {
                    //  InventoryPoint newPt = new InventoryPoint(pt.getNoLigne(), xInterpolator.interpolate(pt.getX()), yInterpolator.interpolate(pt.getY()));
                    InventoryPoint newPt = new InventoryPoint(pt.getNoLigne(), xInterpolator.interpolate(pt.getX()), yInterpolator.interpolate(pt.getY()));
                    newPt.setNum(pt.getNum());
                    newInventoryPoints.add(newPt);
                }

                // écriture des points d'inventaire de la carte mapNum dans le fichier turtle
                System.out.println("");
                bw.write("#\n#Carte n° " + MAP_NUM[mapNum] + "\n#\n");
                for (InventoryPoint pt : newInventoryPoints) {
                    bw.write(printRDF(MAP_NUM[mapNum], pt));
                }
            }
        }
    }

    /**
     * cherche dans une liste de points d'inventaire celui correspondant à un
     * numéro donné.
     *
     * @param inventoryPoints la liste des points d'inventaires.
     * @param num le numéro de point recherché.
     * @return le point d'inventaire de numéro num ou null si il n'existe pas
     * dans la liste.
     */
    private static InventoryPoint find(List<InventoryPoint> inventoryPoints, int num) {
        for (InventoryPoint pt : inventoryPoints) {
            if (pt.getNum() == num) {
                return pt;
            }
        }
        return null;
    }
}
