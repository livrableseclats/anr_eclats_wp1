package lig.steamer.eclatsdatautils.vocabulary;

import org.apache.jena.rdf.model.Model;
import org.apache.jena.rdf.model.ModelFactory;
import org.apache.jena.rdf.model.Resource;

/**
 * Vocabulary definition for the
* <a href="tools/ECLATSDataUtils/data/gramCat.rdf>ECLATS Grammatical Categories SKOS vocabulary
 *
 * @author Philippe GENOUD - Université Grenoble Alpes - Lab LIG-Steamer
 */
public class ECLATSGramCat {

    /**
     * The RDF model that holds the vocabulary terms</p
     */
    private static final Model m_model = ModelFactory.createDefaultModel();

    /**
     * The namespace of the vocabulary as a string ({@value})
     */
    public static final String NS = "http://purl.org/fr/eclats/ontologies/catgram#";



     
    /**
     * The namespace of the vocabulary as a string</p
     *
     * @return name space of ECLATS Grammatical Categories SKOS vocabulary
     * @see #NS
     */
    public static String getURI() {
        return NS;
    }

    /**
     * The namespace of the vocabalary as a resource
     */
    public static final Resource NAMESPACE = m_model.createResource(NS);

}
