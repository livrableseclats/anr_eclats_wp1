package lig.steamer.eclatsdatautils.inventorpoints;

import static java.lang.System.out;
import static java.util.Arrays.asList;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import java.util.Map;
import java.util.UUID;
import joptsimple.OptionException;
import joptsimple.OptionParser;
import joptsimple.OptionSet;

/**
 * Pour invoquer ce programme
 * java CreateResponses --map=1022 --output=data/reponsesPissenlit2.ttl --ptcol=3 --transcol=11  data/Transcriptions_Pissenlit_ALF_1022_15_11_2019.csv
 * @author Philippe Genoud - LIG Steamer - Université Grenoble Aples
 */
public class CreateResponses {

    /**
     * le fichier ttl qui contient les réponses SurveyPoints créés pour la carte
     */
    private static final String OUTPUT_FILE_NAME = "/data/reponsesPissenlit.ttl";

    /**
     * le fichier csv qui contient les réponses phonétiques pour la carte
     * pissenlit
     */
    private static final String REP_FILE_NAME = "Transcriptions_Pissenlit_ALF_1022_15_11_2019.csv";
    
    
    public static String MAP_NUM = "1022";
    
    

    private static final String RDF_STRING = ""
            + "eclats:inventoryEntry_SP%s_map%s> a eclatsonto:InventoryEntry;\n"
            + "          eclatsonto:hasResponse eclats:%s.\n"
            + "eclats:%s a eclatsonto:Response;\n"
            + "		 eclatsonto:rousselotPhoneticForm \"%s\".\n";

    public static final String PREFIX = ""
            + "@prefix : <http://purl.org/fr/eclats/graph#> ."
            + "@prefix eclats: <http://purl.org/fr/eclats/resource/> .\n"
            + "@prefix eclatsonto: <http://purl.org/fr/eclats/ontology#> .\n\n";


    /**
     * renvoie la chaine RDF en turtle correspondant à un point d'inventaire
     *
     * @param mapNum le numéro de la carte
     * @param pt le point d'inventaire
     * @return
     */
    private static String printRDF(String mapNum, int numPt, String formePhonetique) {
        String responseId = UUID.randomUUID().toString();
        return String.format(RDF_STRING, numPt, mapNum, responseId, responseId, formePhonetique);
    }

    public static void main(String[] args) throws FileNotFoundException, IOException {

        final OptionParser optionParser = new OptionParser();

        optionParser.acceptsAll(asList("o", "output"), "Path and name of output file.").withRequiredArg().required();
        optionParser.acceptsAll(asList("m", "map"), "Map id (with padding 0).").withRequiredArg().required();
        optionParser.acceptsAll(asList("p", "ptcol"), "col number of survey points ids").withRequiredArg().ofType(Integer.class).defaultsTo(3);
        optionParser.acceptsAll(asList("t", "transcol"), "col number of phonetic transcription").withRequiredArg().ofType(Integer.class).defaultsTo(11);
        optionParser.acceptsAll(asList("h", "help"), "Display help/usage information").forHelp();
        optionParser.nonOptions("Path and name of the csv file").ofType(String.class);

        try {
            final OptionSet options = optionParser.parse(args);

            if (options.has("help")) {
                printHelp(optionParser);
            }

            String csvFileName;
            List<?> fileNames = options.nonOptionArguments();
            if (fileNames.isEmpty()) {
                System.out.println("missing csv file path and name");
                printHelp(optionParser);
            } else {

                String mapNum = options.valueOf("map").toString();
                String outputfilename = options.valueOf("output").toString();
                int ptCol = (options.has("ptcol") ? ((Integer) options.valueOf("ptcol")) : 3);
                int transCol = (options.has("ptcol") ? ((Integer) options.valueOf("transcol")) : 11);

                csvFileName = (String) fileNames.get(0);

                InventoryPointsLoader loader = new InventoryPointsLoader();
                Map<Integer, String> reponsesMap = loader.loadResponses(csvFileName, ptCol, transCol);

                try ( BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(
                        new FileOutputStream(new File(System.getProperty("user.dir") + "/" + outputfilename)), "UTF8"))) {

                    bw.write("#------------------------------------------------------------\n");
                    bw.write("# Réponses phonétiques de la Carte n° " + mapNum + "\n#\n");
                    DateTimeFormatter dtf = DateTimeFormatter.ofPattern("yyyy/MM/dd HH:mm:ss");
                    LocalDateTime now = LocalDateTime.now();
                    bw.write("# Généré le  " + dtf.format(now) + "\n");
                    bw.write("#------------------------------------------------------------\n\n");
                    
                    bw.write(PREFIX);
                    
                    bw.write(":ALF {\n\n");  // pour graphe nommé
                    
                    // écriture des réponses pour points d'inventaire de la carte mapNum dans le fichier turtle
                    for (Map.Entry<Integer, String> mapEntry : reponsesMap.entrySet()) {
                        bw.write(printRDF(mapNum, mapEntry.getKey(), mapEntry.getValue()));
                    }
                    
                    bw.write("\n}\n"); // ferme graphe nommé
                    
                    System.out.println("Fichier " + csvFileName + " créé");
                }
            }

        } catch (OptionException ex) {
            System.out.println(ex.getMessage());
            printHelp(optionParser);
        }

    }

    public static void printHelp(OptionParser optionParser) throws IOException {
        System.out.println("Usage :   CreateResponses [options] cvsFilename");
        optionParser.printHelpOn(out);
        System.exit(0);
    }
}
