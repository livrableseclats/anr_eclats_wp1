package lig.steamer.eclatsdatautils.inventorpoints;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.List;

/**
 * Génère les coordonnées viewport des PE à partir des données fournies par
 * Jordan Drapeau (univ. La Rochelle).
 *
 * @author Philippe Genoud - LIG Steamer - Université Grenoble Aples
 */
public class CreateAllPoints {

    private static final String INPUT_FILE_NAME = "CarteALF1319-Investigations.csv"; // "CarteALF0378-NumPtEnq.csv";
    private static final String OUTPUT_FILE_NAME = "/data/rawSurveyEntries_1319.ttl"; //"/data/rawSurveyEntries_0378.ttl";
    private static final String MAP_NUM = "1319"; // "0378";
    // pour avoir la taile de l'image sur le serveur dans /data/steamer/www/eclats/cartesALF/TIFF
    // faire commande  exiv2 CarteALF0378.tif
    private static final int MAP_WIDTH = 6202; // 9760; // taille de l'image 0378,  carte 101 x = 9808
    private static final int MAP_HEIGHT = 7378; // 11808; // a vérifier
    
    private static final String RDF_STRING = ""
            + "<http://purl.org/fr/eclats/resource/map%s> eclatsonto:hasInventoryEntry <http://purl.org/fr/eclats/resource/inventoryEntry_SP%s_map%s>.\n"
            + "    <http://purl.org/fr/eclats/resource/inventoryEntry_SP%s_map%s> a eclatsonto:InventoryEntry;\n"
            + "              eclatsonto:forSurveyPoint <http://purl.org/fr/eclats/resource/surveyPoint%s>;\n"
            + "              eclatsonto:xViewport \"%s\"^^xsd:double;\n"
            + "              eclatsonto:yViewport \"%s\"^^xsd:double;\n"
            + "              eclatsonto:hasResponse <http://purl.org/fr/eclats/resource/fakeResponse_map%s_%s>.\n"
            + "	 <http://purl.org/fr/eclats/resource/fakeResponse_map%s_%s> a eclatsonto:Response;\n"
            + "				  eclatsonto:rousselotPhoneticForm \"ligne %s \".\n";

    public static final String PREFIX = ""
            + "@prefix eclats: <http://purl.org/fr/eclats/resource/> .\n"
            + "@prefix eclatsonto: <http://purl.org/fr/eclats/ontology#> .\n";

    private static String printRDF(InventoryPoint pt) {
        return String.format(RDF_STRING, MAP_NUM, pt.getNoLigne(), MAP_NUM,
                pt.getNoLigne(), MAP_NUM, pt.getNoLigne(), pt.getX(), pt.getY(), MAP_NUM, pt.getNoLigne(),  MAP_NUM, pt.getNoLigne(), pt.getNoLigne());
    }

    public static void main(String[] args) throws FileNotFoundException, IOException {

        InventoryPointsLoader loader = new InventoryPointsLoader(MAP_WIDTH, MAP_HEIGHT); // 11824); // taille de l'image 0101
        List<InventoryPoint> newInventoryPoints1 = loader.loadInventoryPoints(INPUT_FILE_NAME);

        try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(new File(System.getProperty("user.dir") + OUTPUT_FILE_NAME)), "UTF8"));) {
            bw.write(PREFIX);
            for (InventoryPoint pt : newInventoryPoints1) {
                bw.write(printRDF(pt));
            }
        }
    }
}
