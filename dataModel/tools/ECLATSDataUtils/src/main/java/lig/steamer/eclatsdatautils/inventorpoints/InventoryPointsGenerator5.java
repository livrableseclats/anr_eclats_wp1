package lig.steamer.eclatsdatautils.inventorpoints;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;

/**
 * Génère les coordonnées viewport des PE à partir des données fournies par
 * Jordan Drapeau (univ. La Rochelle) et les associe à un numéro de PE.
 *
 * @author Philippe Genoud - LIG Steamer - Université Grenoble Aples
 */
public class InventoryPointsGenerator5 {
    
    private static final String INPUT_FILE_NAME = "CarteALF0378-NumPtEnq.csv";
    private static final String OUTPUT_FILE_NAME = "/data/fullSurveyEntries_0378.ttl";
    private static final String MAP_NUM = "0378";
    // pour avoir la taile de l'image sur le serveur dans /data/steamer/www/eclats/cartesALF/TIFF
    // faire commande  exiv2 CarteALF0378.tif
    private static final int MAP_WIDTH = 9760; // taille de l'image 0378,  carte 101 x = 9808
    private static final int MAP_HEIGHT = 11808; // a vérifier

        private static final String RDF_STRING = ""
            + "<http://purl.org/fr/eclats/resource/map%s> eclatsonto:hasInventoryEntry <http://purl.org/fr/eclats/resource/inventoryEntry_SP%s_map%s>.\n"
            + "    <http://purl.org/fr/eclats/resource/inventoryEntry_SP%s_map%s> a eclatsonto:InventoryEntry;\n"
            + "              eclatsonto:forSurveyPoint <http://purl.org/fr/eclats/resource/surveyPoint%s>;\n"
            + "              eclatsonto:xViewport \"%s\"^^xsd:double;\n"
            + "              eclatsonto:yViewport \"%s\"^^xsd:double;\n"
            + "              eclatsonto:hasResponse <http://purl.org/fr/eclats/resource/response_map%s_%s>.\n"
            + "	 <http://purl.org/fr/eclats/resource/response_map%s_%s> a eclatsonto:Response;\n"
            + "				  eclatsonto:rousselotPhoneticForm \"ligne %s \".\n";

    public static final String PREFIX = ""
            + "@prefix eclats: <http://purl.org/fr/eclats/resource/> .\n"
            + "@prefix eclatsonto: <http://purl.org/fr/eclats/ontology#> .\n";

    private static String printRDF(InventoryPoint pt) {
        return String.format(RDF_STRING, MAP_NUM, pt.getNum(), MAP_NUM,
                pt.getNum(), MAP_NUM, pt.getNum(), pt.getX(), pt.getY(), MAP_NUM, pt.getNum(),  MAP_NUM, pt.getNum(), pt.getNoLigne());
    }


    public static void main(String[] args) throws FileNotFoundException, IOException {

        InventoryPointsLoader loader = new InventoryPointsLoader(MAP_WIDTH, MAP_HEIGHT); 
        List<InventoryPoint> newInventoryPoints1 = loader.loadInventoryPoints(INPUT_FILE_NAME);
        List<InventoryPoint> refInventoryPoints1 = loader.loadReferenceInventoryPoints("Image0859Data.csv");
        // trie des points de référence
        // on prend le point le plus à l'ouest (le plus à gauche) puis le plus loin
        List<InventoryPoint> refInventoryPoints = trier(refInventoryPoints1);
        List<InventoryPoint> newInventoryPoints = trier2(newInventoryPoints1, refInventoryPoints);

        for (int i = 0; i < refInventoryPoints.size(); i++) {
            newInventoryPoints.get(i).setNum(refInventoryPoints.get(i).getNum());
        };

        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(new File(System.getProperty("user.dir") + OUTPUT_FILE_NAME)), "UTF8"));
        System.out.println("");
        bw.write(PREFIX);
        for (InventoryPoint pt : newInventoryPoints) {
            bw.write(printRDF(pt));
        }
        bw.close();

        BufferedWriter bw859 = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(new File(System.getProperty("user.dir") + "/data/pts101_859.csv")), "UTF8"));
        System.out.println("");
        for (int i = 0; i < newInventoryPoints.size(); i++) {
            bw859.write(refInventoryPoints.get(i).toString());
            bw859.write(newInventoryPoints.get(i).toString());
            bw859.write("\n");
        }
        bw859.close();
    }

    private static int findPos(List<InventoryPoint> pts, InventoryPoint pt) {
        int i;
        for (i = 0; i < pts.size(); i++) {
            if (Math.abs(pts.get(i).getX() - pt.getX()) > 0.01) {
                return i;
            }
        }
        return i;
    }

    private static List<InventoryPoint> trier(List<InventoryPoint> pts) {
        // tri selon les x
        Collections.sort(pts);
        List<InventoryPoint> resultat = new ArrayList<>();
        InventoryPoint ptRef = pts.remove(0);
        resultat.add(ptRef);
        while (!pts.isEmpty()) {
            ptRef = chercherSuiv(ptRef, pts);
            pts.remove(ptRef);
            resultat.add(ptRef);
        }
        return resultat;
    }

    /**
     * 
     * @param ptsATrier
     * @param ptsRefTries
     * @return 
     */
    private static List<InventoryPoint> trier2(List<InventoryPoint> ptsATrier, List<InventoryPoint> ptsRefTries) {
        // tri selon les x
        Collections.sort(ptsATrier);
        List<InventoryPoint> resultat = new ArrayList<>();
        InventoryPoint ptRef = ptsATrier.remove(0);
        resultat.add(ptRef);
        int numpPt = 1;
        while (!ptsATrier.isEmpty()) {
            // on cherche dans ptsAtrier les 5 points suivants les plus loins de ptRef
            // et calcule de l'angle qu'ils font avec celui-ci
            List<InventoryPoint> ptsSuiv = chercher5Suiv(ptRef, ptsATrier);
            // dans cette liste on calcule le delataAngle
            for (InventoryPoint ptSuiv : ptsSuiv) {
                ptSuiv.calculerDeltaAngle(ptsRefTries.get(numpPt));
            }
            // tri selon deltaAngle croissant
            Collections.sort(ptsSuiv, new Comparator<InventoryPoint>() {
                @Override
                public int compare(InventoryPoint o1, InventoryPoint o2) {
                    if (o1.getDeltaAngle() < o2.getDeltaAngle()) {
                        return -1;
                    } else if (o1.getDeltaAngle() > o2.getDeltaAngle()) {
                        return 1;
                    } else {
                        return 0;
                    }
                }
            });
            System.out.println("point trouvé " + ptsRefTries.get(numpPt));
//            if (ptsRefTries.get(numpPt).getNum() == 164) {
//
//                System.out.println("point ref " + ptsRefTries.get(numpPt));
//
//                for (InventoryPoint p : ptsSuiv) {
//                    System.out.println(p);
//                }
//            }
            // on prend celui qui a la plus petit delta angle
//            ptRef = ptsSuiv.get(0);
//            if ( ptsSuiv.size() >= 2) {
//                ptRef.setDeltaDistance(Double.MAX_VALUE);
//                for (int i = 1; i < ptsSuiv.size(); i++) {
//                    if (Math.abs(ptRef.getDeltaAngle() - ptsSuiv.get(1).getDeltaAngle()) < 3.5) {
//                       ptsSuiv.get(i).setDeltaDistance(Math.abs(ptRef.getDistance() - ptsRefTries.get(numpPt).getDistance()));
//                    } else {
//                        ptsSuiv.get(i).setDeltaDistance(Double.MAX_VALUE);
//                    }
//                }
//                // on trie selon les delta dist
//                Collections.sort(ptsSuiv, new Comparator<InventoryPoint>() {
//                    @Override
//                    public int compare(InventoryPoint o1, InventoryPoint o2) {
//                        double d =  o1.getDeltaDistance() - o2.getDeltaDistance();
//                        if (d < 0) {
//                            return -1;
//                        } else if (d > 0) {
//                            return 1;
//                        } else {
//                            return 0;
//                        }
//                    }
//                    
//                });
//                ptRef = ptsSuiv.get(0);
//            }
// v11            
//                        ptRef = ptsSuiv.get(0);
//            if ( (ptsSuiv.size() >= 2) &&
//                   (Math.abs(ptRef.getDeltaAngle() - ptsSuiv.get(1).getDeltaAngle()) < 3.5) &&
//                 (Math.abs(ptsSuiv.get(1).getDistance() - ptsRefTries.get(numpPt).getDistance()) < (Math.abs(ptRef.getDistance() - ptsRefTries.get(numpPt).getDistance())))) {
//                ptRef = ptsSuiv.get(1);
//            }
// v12

            ptRef = ptsSuiv.get(0);   // le plus loin
            if ((ptsSuiv.size() >= 2)) {
                double variationAngle = 3.5;
                if (ptsRefTries.get(numpPt).getDistance() > 0.432) {
                    variationAngle = 1.5;
                }
                if ((Math.abs(ptRef.getDeltaAngle() - ptsSuiv.get(1).getDeltaAngle()) < variationAngle)
                        && (Math.abs(ptsSuiv.get(1).getDistance() - ptsRefTries.get(numpPt).getDistance()) < (Math.abs(ptRef.getDistance() - ptsRefTries.get(numpPt).getDistance())))) {
                    ptRef = ptsSuiv.get(1);
                }

            }
            ptsATrier.remove(ptRef);
            resultat.add(ptRef);
            numpPt++;
        }
        return resultat;
    }

    /**
     * recherche les 5 points les plus loins du point de référence et calcule de l'angle qu'ils font avec celui-ci
     * @param ptRef
     * @param newsPointsATrier
     * @return 
     */
    private static List<InventoryPoint> chercher5Suiv(InventoryPoint ptRef, List<InventoryPoint> newsPointsATrier) {
        // calcul des distances des points de newsPointsATrier par rapport à ptRef
        for (InventoryPoint pt : newsPointsATrier) {
            pt.setDistance(pt.distance(ptRef));
        }
        // tri selon les distances décroissantes
        Collections.sort(newsPointsATrier, new Comparator<InventoryPoint>() {
            @Override
            public int compare(InventoryPoint o1, InventoryPoint o2) {
                if (o1.getDistance() < o2.getDistance()) {
                    return 1;
                } else if (o1.getDistance() > o2.getDistance()) {
                    return -1;
                } else {
                    return 0;
                }
            }
        });
        // sous liste des 5 points les plus loins
        List<InventoryPoint> plusProches = newsPointsATrier.subList(0, Math.min(5, newsPointsATrier.size()));
        // calcule de l'angle qu'ils font avec le point de référence
        for (InventoryPoint pt : plusProches) {
            pt.setAngle(ptRef.angle(pt));
        }
        return plusProches;
    }

    /**
     * recherche dans une liste de points celui qui est le plus loin de ptRef
     * @param ptRef le point de référence par rapport auquel on fait la recherche
     * @param pts la liste des points dans laquelle on fait la recherche
     * @return le point le plus loin avec le calcule de l'angle par rapport à l'horizontale que fait ce point par rapport au points de référence
     */
    private static InventoryPoint chercherSuiv(InventoryPoint ptRef, List<InventoryPoint> pts) {
        // calcul des distances des pointds de pts par rapport à ptRef
        for (InventoryPoint pt : pts) {
            pt.setDistance(pt.distance(ptRef));
        }
        // tri selon les distances decroissantes
        Collections.sort(pts, new Comparator<InventoryPoint>() {
            @Override
            public int compare(InventoryPoint o1, InventoryPoint o2) {
                if (o1.getDistance() < o2.getDistance()) {
                    return 1;
                } else if (o1.getDistance() > o2.getDistance()) {
                    return -1;
                } else {
                    return 0;
                }
            }
        });
        InventoryPoint res = pts.get(0);
        res.setAngle(ptRef.angle(res));
        return res;
//        // sous liste des 5 points les plus proches
//        List<InventoryPoint> plusProches = pts.subList(0, Math.min(5, pts.size()));
//        for (InventoryPoint pt : plusProches) {
//            pt.setAngle(ptRef.angle(pt));
//        }
//        // tri de la sous selon l'angle à l'origine
//        Collections.sort(plusProches, new Comparator<InventoryPoint>() {
//            @Override
//            public int compare(InventoryPoint o1, InventoryPoint o2) {
//                if (o1.getAngle() < o2.getAngle()) {
//                    return -1;
//                } else if (o1.getAngle() > o2.getAngle()) {
//                    return 1;
//                } else {
//                    return 0;
//                }
//            }
//        });
//        // on retourne le plus loin de l'origine
//        return plusProches.get(0);
    }
}
