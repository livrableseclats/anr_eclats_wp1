/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lig.steamer.eclatsdatautils.inventorpoints;

/**
 *
 * @author Philippe Genoud - LIG Steamer - Université Grenoble Aples
 */
public class LinearPointInterpolator {

    private LinearInterpolator xInterpolator ;
    private LinearInterpolator yInterpolator ;
    
    public LinearPointInterpolator(InventoryPoint p1ref, InventoryPoint p1Cible, InventoryPoint p2ref, InventoryPoint p2Cible ) {
        this.xInterpolator = new LinearInterpolator(p1ref.getX(), p1Cible.getX(), p2ref.getX(), p2Cible.getX());
        this.yInterpolator = new LinearInterpolator(p1ref.getY(), p1Cible.getY(), p2ref.getY(), p2Cible.getY());
    }
    
    InventoryPoint interpolate(InventoryPoint pt) {
        pt.setX(xInterpolator.interpolate(pt.getX()));
        pt.setY(yInterpolator.interpolate(pt.getY()));
        return pt;
    }

}
