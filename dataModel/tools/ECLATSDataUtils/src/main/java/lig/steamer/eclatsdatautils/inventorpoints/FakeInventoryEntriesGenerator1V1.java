package lig.steamer.eclatsdatautils.inventorpoints;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Génère les coordonnées viewport des PE à partir des données fournies par
 * Jordan Drapeau (univ. La Rochelle) et les associe à un numéro de PE.
 *
 * @author Philippe Genoud - LIG Steamer - Université Grenoble Aples
 */
public class FakeInventoryEntriesGenerator1V1 {

    private static final int POINT_OUEST = 493;
    private static final int POINT_EST = 987;
    private static final int POINT_NORD = 297;
    private static final int POINT_SUD = 796;

    /**
     * le numero de la carte que l'on veut évaluer
     */
    private static final String[] MAP_NUM = {
                "1022", // carte 1022 Pissenlit
                "0872", // carte 0872 Molène
                "0274", // carte 0274 Chèvrefeuille
                "0278", // carte 278 Chiendent
                "0888", // carte 888 Muflier
        "0969", // carte 969 Paquerette
        "0112", // carte 112 Bardanne
        "0139", // carte 139 Bleuet
        "0747", // carte 747 Laiteron
        "1027", // Carte 1027 Plantain
        "1092", // Carte 1092 Primevère 
        "0828", // Carte 828 Mauve
        "0837", // Carte 837 Menthe
        //        // Carte 1705 Sainfoin  1/2 carte
        "1222", // Carte 1222 Serpolet
    };

    // x du point 493 dans la carte cible
    private static final InventoryPoint[] ptOuestCible = {
        new InventoryPoint(-1, 0.04069385705726098, 0.3846874157578499), // carte 1022 Pissenlit
        new InventoryPoint(-1, 0.0335579402606611, 0.37844160885136185), // carte 0872 Molène
        new InventoryPoint(-1, 0.05247277542549626, 0.374584465994219), // carte 0274 Chèvrefeuille
        new InventoryPoint(-1, 0.026686252288790305, 0.3961666022800879), // carte 278 Chiendent
        new InventoryPoint(-1, 0.029504933607471626, 0.39090011876360437), // carte 888 Muflier
        new InventoryPoint(-1, 0.058733358245764075, 0.3794217937450082), // carte 969 Paquerette
        new InventoryPoint(-1, 0.05615294536564678, 0.3722821998221945),// carte 112 Bardanne
        new InventoryPoint(-1, 0.05499489670730254, 0.3749712442944587), // carte 139 Bleuet
        new InventoryPoint(-1, 0.03674168681247253, 0.42557996980415347), // carte 747 Laiteron
        new InventoryPoint(-1, 0.03400415210297647, 0.3857696560365205), // Carte 1027 Plantain
        new InventoryPoint(-1, 0.04515938601547338, 0.38229701239977376), // Carte 1092 Primevère
        new InventoryPoint(-1, 0.03658835637781827, 0.3838981156481965), // Carte 828 Mauve
        new InventoryPoint(-1, 0.044396838448491946, 0.3846273931289118),// Carte 837 Menthe
        //        // Carte 1705 Sainfoin  1/2 carte
        new InventoryPoint(-1, 0.047244021493950863, 0.38084480231694273), // Carte 1222 Serpolet
    };

    // x du point 987 dans la carte cible
    private static final InventoryPoint[] ptEstCible = {
        new InventoryPoint(-1, 0.925430836725416, 0.7184865743437522), // 1022 Pissenlit
        new InventoryPoint(-1, 0.9193333814672768, 0.7120483153161797), // 0872 Molène
        new InventoryPoint(-1, 0.9265483208764876, 0.6932149591665278), // carte 0274 Chèvrefeuille
        new InventoryPoint(-1, 0.9152362743413562, 0.7214505787614924), // carte 278 Chiendent
        new InventoryPoint(-1, 0.9190192413743226, 0.7132170622779749), // carte 888 Muflier
        new InventoryPoint(-1, 0.9270985695283458, 0.6933599196167418), // carte 969 Paquerette
        new InventoryPoint(-1, 0.938471675789356, 0.7013153133372081), // carte 112 Bardanne
        new InventoryPoint(-1, 0.9256601075339558, 0.689609457660904), // carte 139 Bleuet
        new InventoryPoint(-1, 0.9236814975952463, 0.7583744218755423), // carte 747 Laiteron
        new InventoryPoint(-1, 0.9195568633261785, 0.7201772588915503), // Carte 1027 Plantain
        new InventoryPoint(-1, 0.9293486796500469, 0.719227605831215), // Carte 1092 Primevère 
        new InventoryPoint(-1, 0.9211894019425764, 0.7195633210476593), // Carte 828 Mauve
        new InventoryPoint(-1, 0.932501407010679, 0.7124430703812039), // Carte 837 Menthe   ??? à verifier
        //        // Carte 1705 Sainfoin  1/2 carte
        new InventoryPoint(-1, 0.9269160731427597, 0.6999565299418707), // Carte 1222 Serpolet
    };

    // y du point 297 dans la carte cible
    private static final InventoryPoint[] ptNordCible = {
        new InventoryPoint(-1, 0.468308370440995, 0.0720423921833473), // 1022 Pissentlit
        new InventoryPoint(-1, 0.46130678017145677, 0.0658266606283012), // 0872 Molène
        new InventoryPoint(-1, 0.4764502743040874, 0.08043909985587802), // carte 0274 Chèvrefeuille
        new InventoryPoint(-1, 0.45160137320518506, 0.07972357399689646), // carte 278 Chiendent
        new InventoryPoint(-1, 0.4537675097820047, 0.07313696332096911), // carte 888 Muflier
        new InventoryPoint(-1, 0.47883531103975346, 0.08647022256820439), // carte 969 Paquerette
        new InventoryPoint(-1, 0.4856032688562604, 0.05946028941155923), // carte 112 Bardanne
        new InventoryPoint(-1, 0.4762833121176363, 0.08130053181312144), // carte 139 Bleuet
        new InventoryPoint(-1, 0.4644896539386193, 0.11196768372957223), // carte 747 Laiteron
        new InventoryPoint(-1, 0.46255409091487215, 0.07394016150617806), // Carte 1027 Plantain
        new InventoryPoint(-1, 0.47418445512386825, 0.07097333105003283), // Carte 1092 Primevère
        new InventoryPoint(-1, 0.4646433202264727, 0.07073921576629008), // Carte 828 Mauve
        new InventoryPoint(-1, 0.4703817855628969, 0.06869530632603554), // Carte 837 Menthe
        //        // Carte 1705 Sainfoin  1/2 carte
        new InventoryPoint(-1, 0.08465755779363135, 0.4736836343555935),// Carte 1222 Serpolet
    };

    // y du point 796 dans la carte cible
    private static final InventoryPoint[] ptSudCible = {
        new InventoryPoint(-1, 0.4879359010505776, 1.168769827128277), // 1022 Pissenlit
        new InventoryPoint(-1, 0.4820760109406875, 1.162835761194211), // 0872 Molène
        new InventoryPoint(-1, 0.4950684061722192, 1.1171253872768874), // carte 0274 Chèvrefeuille
        new InventoryPoint(-1, 0.4817909336447455, 1.1757984641999646), // carte 278 Chiendent
        new InventoryPoint(-1, 0.486745676188803, 1.1698052601174442), // carte 888 Muflier
        new InventoryPoint(-1, 0.5008686529480695, 1.1164744515816347), // carte 969 Paquerette
        new InventoryPoint(-1, 0.5091979226669969, 1.1512834917218115), // carte 112 Bardanne
        new InventoryPoint(-1, 0.49791298244730664, 1.1135790286876086), // carte 139 Bleuet
        new InventoryPoint(-1, 0.48429430112862537, 1.2088207869293683), // carte 747 Laiteron carte coupée en bas !!
        new InventoryPoint(-1, 0.4818989433490405, 1.1699112055554013), // Carte 1027 Plantain
        new InventoryPoint(-1, 0.49062202027211743, 1.1683980187422145), // Carte 1092 Primevère
        new InventoryPoint(-1, 0.48298932844070225, 1.1696930709461177),// Carte 828 Mauve
        new InventoryPoint(-1, 0.4976689205116074, 1.1667911880808126), // Carte 837 Menthe
        new InventoryPoint(-1, 0.49393354327392447, 1.1272888918619666), // Carte 1222 Serpolet
    };

    /**
     * le fichier ttl qui contient les SurveyPoints créés pour la carte
     */
    private static final String OUTPUT_FILE_NAME = "/data/fakefullSurveyEntriesPlantes-v1.ttl";

    /**
     * le fichier qui contient les points de référence que l'on va alligner la
     * carte cible (on prend la carte Dauphinelle)
     */
    private static final String REF_FILE_NAME = "Image0378Data.csv";

    private static final String RDF_STRING = ""
            + "<http://purl.org/fr/eclats/resource/map%s> eclatsonto:hasInventoryEntry <http://purl.org/fr/eclats/resource/inventoryEntry_SP%s_map%s>.\n"
            + "    <http://purl.org/fr/eclats/resource/inventoryEntry_SP%s_map%s> a eclatsonto:InventoryEntry;\n"
            + "              eclatsonto:forSurveyPoint <http://purl.org/fr/eclats/resource/surveyPoint%s>;\n"
            + "              eclatsonto:xViewport \"%s\"^^xsd:double;\n"
            + "              eclatsonto:yViewport \"%s\"^^xsd:double;\n"
            + "              eclatsonto:hasResponse <http://purl.org/fr/eclats/resource/%s>.\n"
            + "	 <http://purl.org/fr/eclats/resource/%s> a eclatsonto:Response;\n"
            + "				  eclatsonto:rousselotPhoneticForm \"%s\".\n";

    public static final String PREFIX = ""
            + "@prefix eclats: <http://purl.org/fr/eclats/resource/> .\n"
            + "@prefix eclatsonto: <http://purl.org/fr/eclats/ontology#> .\n";

    /**
     * renvoie la chaine RDF en turtle correspondant à un point d'inventaire
     *
     * @param mapNum le numéro de la carte
     * @param pt le point d'inventaire
     * @return
     */
    private static String printRDF(String mapNum, InventoryPoint pt) {
        String responseId = UUID.randomUUID().toString();
        return String.format(RDF_STRING, mapNum, pt.getNum(), mapNum,
                pt.getNum(), mapNum, pt.getNum(), pt.getX(), pt.getY(), responseId, responseId, "forme phonétique non encore renseignée");
    }

    public static void main(String[] args) throws FileNotFoundException, IOException {

        InventoryPointsLoader loader = new InventoryPointsLoader();

        // charge les Entrées d'inventaire de référence
        List<InventoryPoint> refInventoryPoints = loader.loadReferenceInventoryPoints(REF_FILE_NAME);

        // recherche des points de référence pour allignement
        InventoryPoint ptOuestRef = find(refInventoryPoints, POINT_OUEST);
        InventoryPoint ptEstRef = find(refInventoryPoints, POINT_EST);
        InventoryPoint ptNordRef = find(refInventoryPoints, POINT_NORD);
        InventoryPoint ptSudRef = find(refInventoryPoints, POINT_SUD);

        try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(new File(System.getProperty("user.dir") + String.format(OUTPUT_FILE_NAME, MAP_NUM))), "UTF8"))) {

            bw.write(PREFIX);

            for (int mapNum = 0; mapNum < MAP_NUM.length; mapNum++) {
                // calcul des fonctions d'interpolation pour la carte mapNum
                LinearPointInterpolator nordOuestInterpolator = new LinearPointInterpolator(ptOuestRef, ptOuestCible[mapNum], ptNordRef, ptNordCible[mapNum]);
                LinearPointInterpolator nordEstInterpolator = new LinearPointInterpolator(ptNordRef, ptNordCible[mapNum], ptEstRef, ptEstCible[mapNum]);
                LinearPointInterpolator sudEstInterpolator = new LinearPointInterpolator(ptSudRef, ptSudCible[mapNum], ptEstRef, ptEstCible[mapNum]);

                LinearPointInterpolator sudOuestInterpolator = new LinearPointInterpolator(ptOuestRef, ptOuestCible[mapNum], ptSudRef, ptSudCible[mapNum]);
                // calcule par interpolation des coordonnées des points d'inventaire sur la carte mapNum
                List<InventoryPoint> newInventoryPoints = new ArrayList<>();
                for (InventoryPoint pt : refInventoryPoints) {
                    //  InventoryPoint newPt = new InventoryPoint(pt.getNoLigne(), xInterpolator.interpolate(pt.getX()), yInterpolator.interpolate(pt.getY()));
                    InventoryPoint newPt = new InventoryPoint(pt.getNoLigne(), pt.getX(), pt.getY());
                    LinearPointInterpolator interpolator = null;
                    if (newPt.getX() <= ptNordRef.getX()) {
                        if (newPt.getY() <= ptOuestRef.getY()) {
                            interpolator = nordOuestInterpolator;
                        } else {
                            interpolator = sudOuestInterpolator;
                        }
                    } else {
                        if (newPt.getY() <= ptOuestRef.getY()) {
                            interpolator = nordEstInterpolator;
                        } else {
                            interpolator = sudEstInterpolator;
                        }
                    }
                    interpolator.interpolate(newPt);
                    newPt.setNum(pt.getNum());
                    newInventoryPoints.add(newPt);
                }

                // écriture des points d'inventaire de la carte mapNum dans le fichier turtle
                System.out.println("");
                bw.write("#\n#Carte n° " + MAP_NUM[mapNum] + "\n#\n");
                for (InventoryPoint pt : newInventoryPoints) {
                    bw.write(printRDF(MAP_NUM[mapNum], pt));
                }
            }
        }
    }

    /**
     * cherche dans une liste de points d'inventaire celui correspondant à un
     * numéro donné.
     *
     * @param inventoryPoints la liste des points d'inventaires.
     * @param num le numéro de point recherché.
     * @return le point d'inventaire de numéro num ou null si il n'existe pas
     * dans la liste.
     */
    private static InventoryPoint find(List<InventoryPoint> inventoryPoints, int num) {
        for (InventoryPoint pt : inventoryPoints) {
            if (pt.getNum() == num) {
                return pt;
            }
        }
        return null;
    }
}
