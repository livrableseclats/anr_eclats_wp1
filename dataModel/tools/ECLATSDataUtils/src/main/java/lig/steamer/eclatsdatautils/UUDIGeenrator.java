package lig.steamer.eclatsdatautils;

import java.util.Scanner;
import java.util.UUID;

/**
 * Permet de générer manuellement des UUIDs
 * @author Philippe Genoud - LIG Steamer - Université Grenoble Aples
 */
public class UUDIGeenrator {
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.print("entrez le nbre d'uuids à générer : ");
        int nbUUID = sc.nextInt();
        for (int i = 1; i <= nbUUID; i++) {
                System.out.println("uudi " + i  + UUID.randomUUID().toString());
        }      
        
    }

    
}
