package lig.steamer.eclatsdatautils.inventorpoints;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Map;
import java.util.UUID;

/**
 * Génère les coordonnées viewport des PE à partir des données fournies par
 * Jordan Drapeau (univ. La Rochelle) et les associe à un numéro de PE.
 *
 * @author Philippe Genoud - LIG Steamer - Université Grenoble Aples
 */
public class InventoryEntriesGenerator {

    // donnée pour générer points de la carte 378
//    private static final String INPUT_FILE_NAME = "CarteALF0378-NumPtEnq_2.csv";
//    private static final String OUTPUT_FILE_NAME = "/data/fullSurveyEntries_0378_2.ttl";
//    private static final String REF_FILE_NAME = "Image0859Data_2.csv";
//    private static final String MAP_NUM = "0378";
//    private static final String PHONETIC_FORMS_FILE_NAME = "dauphinelle.csv";
//        // pour avoir la taile de l'image sur le serveur dans /data/steamer/www/eclats/cartesALF/TIFF
//    // faire commande  exiv2 CarteALF0378.tif
//    private static final int MAP_WIDTH = 9760; // taille de l'image 0378,  carte 101 x = 9808
//    private static final int MAP_HEIGHT = 11808; // a vérifier
//    // données pour générer points de la carte 101
//    private static final String INPUT_FILE_NAME = "listePoints0101.csv";
//    private static final String OUTPUT_FILE_NAME = "/data/fullSurveyEntries_0101.ttl";
//    private static final String REF_FILE_NAME = "Image0859Data_pour_101.csv";
//    private static final String MAP_NUM = "0101";
//    private static final String PHONETIC_FORMS_FILE_NAME = "paspeur.csv";
//
//    // pour avoir la taile de l'image sur le serveur dans /data/steamer/www/eclats/cartesALF/TIFF
//    // faire commande  exiv2 CarteALF0101.tif
//    private static final int MAP_WIDTH = 9808; 
//    private static final int MAP_HEIGHT = 11824; 
    // données pour générer points de la carte 1319
    /**
     * le fichier csv des boites englobantes calculées par Jordan
     */
    private static final String INPUT_FILE_NAME = "CarteALF1319-rawData.csv";
    /**
     * le fichier ttl qui contient les SurveyPoints créés pour la carte
     */
    private static final String OUTPUT_FILE_NAME = "/data/fullSurveyEntries_from0378_1319.ttl";
    /**
     * le fichier qui contient les points de référence sur lesquels on s'alligne
     */
    private static final String REF_FILE_NAME = "Image0378Data.csv";
    /**
     * le numero de la carte que l'on veut évaluer
     */
    private static final String MAP_NUM = "1319";
    /**
     * le fichier contenant les formes phonétiques de la carte
     */
    private static final String PHONETIC_FORMS_FILE_NAME = "data/ALF_Toupie_1319_Carole_24_9_2019.csv"; 

    // pour avoir la taile de l'image de la carte 101 sur le serveur dans /data/steamer/www/eclats/cartesALF/TIFF
    // faire commande  exiv2 CarteALF0101.tif
    private static final int MAP_WIDTH = 6202;
    private static final int MAP_HEIGHT = 7378;

    private static final String RDF_STRING = ""
            + "<http://purl.org/fr/eclats/resource/map%s> eclatsonto:hasInventoryEntry <http://purl.org/fr/eclats/resource/inventoryEntry_SP%s_map%s>.\n"
            + "    <http://purl.org/fr/eclats/resource/inventoryEntry_SP%s_map%s> a eclatsonto:InventoryEntry;\n"
            + "              eclatsonto:forSurveyPoint <http://purl.org/fr/eclats/resource/surveyPoint%s>;\n"
            + "              eclatsonto:xViewport \"%s\"^^xsd:double;\n"
            + "              eclatsonto:yViewport \"%s\"^^xsd:double;\n"
            + "              eclatsonto:hasResponse <http://purl.org/fr/eclats/resource/%s>.\n"
            + "	 <http://purl.org/fr/eclats/resource/%s> a eclatsonto:Response;\n"
            + "				  eclatsonto:rousselotPhoneticForm \"%s\".\n";

    public static final String PREFIX = ""
            + "@prefix eclats: <http://purl.org/fr/eclats/resource/> .\n"
            + "@prefix eclatsonto: <http://purl.org/fr/eclats/ontology#> .\n";

    private static Comparator<InventoryPoint> distancesComparator = new Comparator<InventoryPoint>() {
        @Override
        public int compare(InventoryPoint o1, InventoryPoint o2) {
            if (o1.getDistance() < o2.getDistance()) {
                return -1;
            } else if (o1.getDistance() > o2.getDistance()) {
                return 1;
            } else {
                return 0;
            }
        }
    };

    /**
     * renvoie la chaine RDF en turtle correspodant à un point d'inventaire
     *
     * @param pt
     * @return
     */
    private static String printRDF(InventoryPoint pt) {
        String responseId = UUID.randomUUID().toString();
        return String.format(RDF_STRING, MAP_NUM, pt.getNum(), MAP_NUM,
                pt.getNum(), MAP_NUM, pt.getNum(), pt.getX(), pt.getY(), responseId, responseId, pt.getPhoneticForm());
    }

    public static void main(String[] args) throws FileNotFoundException, IOException {

        InventoryPointsLoader loader = new InventoryPointsLoader(MAP_WIDTH, MAP_HEIGHT);

        // Entrées d'inventaire de référence triées en partant du point le plus à l'ouest puis ensuite avec ensuite l'entrée d'inventaire associée au
        // point le plus le plus proche du point précédent
        List<InventoryPoint> refInventoryPoints = trierDeProcheEnProche(loader.loadReferenceInventoryPoints(REF_FILE_NAME));

        // nouvelles entrées d'inventaires. On part de celle associée au point le plus à l'ouest, puis on cherche parmi les 5 plus proches
        // 
        List<InventoryPoint> newInventoryPoints = trier2(loader.loadInventoryPoints(INPUT_FILE_NAME), refInventoryPoints);

        // lecture des formes phonétiques associées aux nouvelles entrées d'inventaire
        Map<Integer, String> phoneticForms = loader.loadResponses(PHONETIC_FORMS_FILE_NAME, 3, 10);

        // association du numéro de point d'enquête et de la forme phonétique aux nouvelles entrées d'inventaires
        for (int i = 0; i < refInventoryPoints.size(); i++) {
            newInventoryPoints.get(i).setNum(refInventoryPoints.get(i).getNum());
            String phoneticForm = phoneticForms.get(refInventoryPoints.get(i).getNum());
            if (phoneticForm != null) {
                newInventoryPoints.get(i).setPhoneticForm(phoneticForm);
            }
        };

        // ecriture des nouvelles entrées d'inventaire dans un fichier turtle
        BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(
                new FileOutputStream(new File(System.getProperty("user.dir") + OUTPUT_FILE_NAME)), "UTF8"));
        System.out.println("");
        bw.write(PREFIX);
        for (InventoryPoint pt : newInventoryPoints) {
            bw.write(printRDF(pt));
        }
        bw.close();

//        BufferedWriter bw859 = new BufferedWriter(new OutputStreamWriter(
//                new FileOutputStream(new File(System.getProperty("user.dir") + "/data/pts101_859.csv")), "UTF8"));
//        System.out.println("");
//        for (int i = 0; i < newInventoryPoints.size(); i++) {
//            bw859.write(refInventoryPoints.get(i).toString());
//            bw859.write(newInventoryPoints.get(i).toString());
//            bw859.write("\n");
//        }
//        bw859.close();
    }

    private static int findPos(List<InventoryPoint> pts, InventoryPoint pt) {
        int i;
        for (i = 0; i < pts.size(); i++) {
            if (Math.abs(pts.get(i).getX() - pt.getX()) > 0.01) {
                return i;
            }
        }
        return i;
    }

    /**
     * trie une liste de points d'inventaire en partant du point le plus à
     * l'ouest (plus petit x) puis en cherchant pour chaque point le point
     * suivant le plus proche.
     *
     * @param pts La liste des points d'inventaire à trierDeProcheEnProche
     * @return la liste triée des points d'inventaire, pour chaque point de la
     * liste on a sa distance et son angle par rapport au point précédant.
     */
    private static List<InventoryPoint> trierDeProcheEnProche(List<InventoryPoint> pts) {
        // tri selon les x
        Collections.sort(pts);
        List<InventoryPoint> resultat = new ArrayList<>();
        InventoryPoint ptRef = pts.remove(0);
        resultat.add(ptRef);

        while (!pts.isEmpty()) {

            ptRef = chercherSuiv(ptRef, pts);
            pts.remove(ptRef);
            resultat.add(ptRef);
        }
        return resultat;
    }

    /**
     *
     * @param newInvEntries les nouvelles entrées d'inventaire à trier
     * @param refInventoryEntries les entrées d'inventaire triées
     * @return les nouvelles entrées d'inventaire allignées sur les entrées
     * d'inventaire de référence
     */
    private static List<InventoryPoint> trier2(List<InventoryPoint> newInvEntries, List<InventoryPoint> refInventoryEntries) {
        List<InventoryPoint> resultat = new ArrayList<>();
        // trie les nouvlles entrées d'inventaire selon les x
        Collections.sort(newInvEntries);

        // le point de départ est le premier de la liste des nouveaux points d'inventaires triée selon les x
        // on le sort de la liste des nouveaux point d'inventaire et on le met dans la liste résultat
        InventoryPoint ptRef = newInvEntries.remove(0);
        resultat.add(ptRef);

        int numpPt = 1;
        while (!newInvEntries.isEmpty()) {
            // on cherche les 5 points suivants les plus proches
            List<InventoryPoint> ptsSuiv = chercher5Suiv(ptRef, newInvEntries);
            // dans cette liste on calcule le delataAngle
            for (InventoryPoint ptSuiv : ptsSuiv) {
                ptSuiv.calculerDeltaAngle(refInventoryEntries.get(numpPt));
            }
            // tri selon deltaAngle
            Collections.sort(ptsSuiv, (InventoryPoint o1, InventoryPoint o2) -> {
                if (o1.getDeltaAngle() < o2.getDeltaAngle()) {
                    return -1;
                } else if (o1.getDeltaAngle() > o2.getDeltaAngle()) {
                    return 1;
                } else {
                    return 0;
                }
            });
            System.out.println("point de référence " + refInventoryEntries.get(numpPt));
            List<Integer> list = Arrays.asList(new Integer[]{162,703,199,155,156,178,154,88,705,144,143,153});
            if (list.contains(refInventoryEntries.get(numpPt).getNum())) {
                System.out.println("****************** ALERTE **********************");
                for (InventoryPoint p : ptsSuiv) {
                    System.out.println(p);
                }
                System.out.println("------------------------------------------------");
            }
//            if (refInventoryEntries.get(numpPt).getNum() == 164) {
//
//                System.out.println("point ref " + refInventoryEntries.get(numpPt));
//
//                for (InventoryPoint p : ptsSuiv) {
//                    System.out.println(p);
//                }
//            }
            // on prend celui qui a la plus petit delta angle
//            ptRef = ptsSuiv.get(0);
//            if ( ptsSuiv.size() >= 2) {
//                ptRef.setDeltaDistance(Double.MAX_VALUE);
//                for (int i = 1; i < ptsSuiv.size(); i++) {
//                    if (Math.abs(ptRef.getDeltaAngle() - ptsSuiv.get(1).getDeltaAngle()) < 3.5) {
//                       ptsSuiv.get(i).setDeltaDistance(Math.abs(ptRef.getDistance() - refInventoryEntries.get(numpPt).getDistance()));
//                    } else {
//                        ptsSuiv.get(i).setDeltaDistance(Double.MAX_VALUE);
//                    }
//                }
//                // on trie selon les delta dist
//                Collections.sort(ptsSuiv, new Comparator<InventoryPoint>() {
//                    @Override
//                    public int compare(InventoryPoint o1, InventoryPoint o2) {
//                        double d =  o1.getDeltaDistance() - o2.getDeltaDistance();
//                        if (d < 0) {
//                            return -1;
//                        } else if (d > 0) {
//                            return 1;
//                        } else {
//                            return 0;
//                        }
//                    }
//                    
//                });
//                ptRef = ptsSuiv.get(0);
//            }
// v11            
//                        ptRef = ptsSuiv.get(0);
//            if ( (ptsSuiv.size() >= 2) &&
//                   (Math.abs(ptRef.getDeltaAngle() - ptsSuiv.get(1).getDeltaAngle()) < 3.5) &&
//                 (Math.abs(ptsSuiv.get(1).getDistance() - refInventoryEntries.get(numpPt).getDistance()) < (Math.abs(ptRef.getDistance() - refInventoryEntries.get(numpPt).getDistance())))) {
//                ptRef = ptsSuiv.get(1);
//            }
// v12
            // on prend celui qui a la plus petit delta angle
            ptRef = ptsSuiv.get(0);
            if ((ptsSuiv.size() >= 2)) {
                double variationAngle = 9;
                if (refInventoryEntries.get(numpPt).getDistance() > 0.432) {
                    variationAngle = 3;
                }
                // parmi les points dont le delta angle est inférieur au seuil de variation (variation angle), on prend celui qui a la plus petite distance
                int j = 1;
                while (j < ptsSuiv.size() && ptsSuiv.get(j).getDeltaAngle() < variationAngle) {
                    if (ptsSuiv.get(j).getDistance() < ptRef.getDistance()) {
                        ptRef = ptsSuiv.get(j);
                    }
                    j++;
                }
//                double variationAngle = 3.5;
//                if (refInventoryEntries.get(numpPt).getDistance() > 0.432) {
//                    variationAngle = 1.5;
//                }
//                if ((Math.abs(ptRef.getDeltaAngle() - ptsSuiv.get(1).getDeltaAngle()) < variationAngle)
//                        && (Math.abs(ptsSuiv.get(1).getDistance() - refInventoryEntries.get(numpPt).getDistance()) < (Math.abs(ptRef.getDistance() - refInventoryEntries.get(numpPt).getDistance())))) {
//                    ptRef = ptsSuiv.get(1);
//                }
            }
            newInvEntries.remove(ptRef);
            resultat.add(ptRef);
            System.out.println("Point choisi " + ptRef);
            numpPt++;
        }
        return resultat;
    }

    /**
     * cherche dans un liste de points d'inventaires, les 5 (au maximum) plus
     * proches d'un point donné
     *
     * @param ptRef le point d'inventaire de référence
     * @param pts la liste des points d'inventaires dans laquelle on effectue la
     * recherche
     * @return une liste contenant les 5 points les plus proches de ptRef, pour
     * ces points on a la distance au point de référence et l'angle par rapport
     * au point de référence.
     */
    private static List<InventoryPoint> chercher5Suiv(InventoryPoint ptRef, List<InventoryPoint> pts) {
        // calcul des distances des points de pts par rapport à ptRef
        for (InventoryPoint pt : pts) {
            pt.setDistance(pt.distance(ptRef));
        }
        // tri selon les distances croissantes
        Collections.sort(pts, distancesComparator);
        // sous liste des 5 points les plus proches
        List<InventoryPoint> plusProches = pts.subList(0, Math.min(5, pts.size()));
        // calcul de l'angle entre ces points et le point de référence
        for (InventoryPoint pt : plusProches) {
            pt.setAngle(ptRef.angle(pt));
        }
        return plusProches;
    }

    /**
     * Recherche dans une liste de points d'inventaire, celui qui est à
     * la plus petite distance d'un point d'inventaire donnée
     *
     * @param ptRef l'entrée d'inventaire dont on cherche l'entrée d'inventaire
     * la plus proche
     * @param pts la liste des points d'inventaire dans laquelle on cherche le
     * point le plus proche
     * @return le point le plus proche
     */
    private static InventoryPoint chercherSuiv(InventoryPoint ptRef, List<InventoryPoint> pts) {
        // calcul des distances des pointds de pts par rapport à ptRef
        for (InventoryPoint pt : pts) {
            pt.setDistance(pt.distance(ptRef));
        }
        // tri selon les distances croissantes
        Collections.sort(pts, distancesComparator);
        // le résultat c'est le  premier de la liste triée par distances croissantes
        InventoryPoint res = pts.get(0);
        // on calcule l'angle entre le point trouvé et le point de référence
        res.setAngle(ptRef.angle(res));
        // on retourne le point trouvé avec sa distance et l'angle par rapport au point de référence
        return res;
    }
}
