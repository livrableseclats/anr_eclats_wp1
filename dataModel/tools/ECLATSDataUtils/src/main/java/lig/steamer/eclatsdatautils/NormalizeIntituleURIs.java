/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package lig.steamer.eclatsdatautils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileOutputStream;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;

/**
 * Ce programme charge une copie de l'export du repository en format turtle
 * il créer un nouveau fichier permettant de modifier les intitululés des cartes
 * 
 * par exemple: eclat:intitule_abeille sera remplacé par eclat:intitule0001
 * 
 *
 * @author Philippe GENOUD - Université Grenoble Alpes - LIG STeamer
 * @author Maeva SEFFAR
 */
public class NormalizeIntituleURIs {

    final static Charset ENCODING = StandardCharsets.UTF_8;

    public static void main(String[] args) throws IOException {

        // read the file 'example-data-artists.ttl' as an InputStream.
        Path pathModel = Paths.get(System.getProperty("user.dir") + "/../../data/test.ttl");
        BufferedReader modelReader = Files.newBufferedReader(pathModel, ENCODING);
// Rio also accepts a java.io.Reader as input for the parser.
        Model model = Rio.parse(modelReader, "", RDFFormat.TURTLE);

        ValueFactory vf = SimpleValueFactory.getInstance();

// We want to find all information about the artist `ex:VanGogh` in our model
        IRI mapClassIRI = vf.createIRI("http://purl.org/fr/eclat/ontology#Map");
        IRI hasIntituleIRI = vf.createIRI("http://purl.org/fr/eclat/ontology#hasIntitule");
        Map<String, String> intitulesURISMap = new HashMap<>();
        Map<String, String> intitulesURISMap2 = new HashMap<>();

        for (Resource map : model.filter(null, RDF.TYPE, mapClassIRI).subjects()) {
            for (Value intitule : model.filter(map, hasIntituleIRI, null).objects()) {

                String URIIntitule_abrege = "eclat:intitule" + intitule.stringValue().substring(42);
                String URIIntitule_complet = "<" + intitule.stringValue() + ">";
                System.out.println(URIIntitule_complet);
                String NewURIIntitule = "eclat:intitule" + map.stringValue().substring(map.stringValue().length() - 4);

                intitulesURISMap.put(URIIntitule_abrege, NewURIIntitule);
                intitulesURISMap2.put(URIIntitule_complet, NewURIIntitule);

            }

        }

        BufferedWriter out = new BufferedWriter(new FileWriter(System.getProperty("user.dir") + "/../../data/testout.ttl"));

        try (BufferedReader reader = Files.newBufferedReader(pathModel, ENCODING)) {
            String line = null;
            while ((line = reader.readLine()) != null) {
                String[] tokens = line.split("\\s+");
                for (int i = 0; i < tokens.length; i++) {

                    if (intitulesURISMap.containsKey(tokens[i])) {
                        tokens[i] = intitulesURISMap.get(tokens[i]);
                    } else if (intitulesURISMap2.containsKey(tokens[i])) {
                        tokens[i] = intitulesURISMap2.get(tokens[i]);

                    }
                    out.write(tokens[i] + " ");
                }
                out.newLine();
            }
            // Fin de fichier - Fermeture du flux
            out.close();
        }
    }
}
