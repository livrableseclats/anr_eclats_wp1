package lig.steamer.eclatsdatautils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;

/**
 * Ce programme répond à l'ISSUE #1
 * normaliser les URIs de ressources correspondant aux images et aux cartes.
 * Numéro sur 4 chiffres avec padding de 0 devant.
 *
 * Exemples d'URIs actuelles  *
 *
 * cartes :
 *
 * 'http://purl.org/fr/eclat/resource/carte_ALF_308'
 * 'http://purl.org/fr/eclat/resource/carte_ALF_14'
 *
 * images :
 *
 * 'http://purl.org/fr/eclat/resource/image_ALF_308A'
 * 'http://purl.org/fr/eclat/resource/image_ALF_308B'
 * 'http://purl.org/fr/eclat/resource/image_ALF_364'
 *
 *
 * URI souhaitées
 *
 * cartes :
 *
 * 'http://purl.org/fr/eclat/resource/carteALF0308'
 * 'http://purl.org/fr/eclat/resource/carteALF0014'
 *
 * images:
 *
 * 'http://purl.org/fr/eclat/resource/imageALF0308A'
 * 'http://purl.org/fr/eclat/resource/imageALF0308B'
 * 'http://purl.org/fr/eclat/resource/imageALF0014'
 *
 * @author Philippe GENOUD - Université Grenoble Alpes - LIG STeamer
 * @author Maeva SEFFAR
 */
public class NormalizeMapImagesIds {

    final static Charset ENCODING = StandardCharsets.UTF_8;

    public static void main(String[] args) throws IOException {
        System.out.println(System.getProperty("user.dir"));
        Path path = Paths.get(System.getProperty("user.dir") + "/../../data/statements_11_07_2017.ttl");
        BufferedWriter out = new BufferedWriter(new FileWriter(System.getProperty("user.dir") + "/../../data/statements_25_05_2018.ttl"));

        try (BufferedReader reader = Files.newBufferedReader(path, ENCODING)) {
            String line = null;
            while ((line = reader.readLine()) != null) {
                String[] tokens = line.split("\\s+");
                for (int i = 0; i < tokens.length; i++) {

                    if (tokens[i].contains("image_ALF")) {
                        // tokens[i] est une URI d'image
                        tokens[i] = normalizeURI(tokens[i], "eclat:imageALF");
                    } else if (tokens[i].contains("carte_ALF")) {
                        // tokens[i] est une URI de carte
                        tokens[i] = normalizeURI(tokens[i], "eclat:carteALF");
                    }
                    // Ecriture du mot courant et ajout d'un espace
                    out.write(tokens[i] + " ");
                    System.out.print(tokens[i] + " ");
                }
                // Fin de ligne - Saut de ligne
                out.newLine();
                System.out.println();
            }
            // Fin de fichier - Fermeture du flux
            out.close();
        }
    }

    /**
     * Donne la forme normalisée de l'uri d'une resource de type Carte ou Image
     *
     * @param resourceURI l'URI de la resource à normaliser.
     * @param resourcePrefix le prefixe à utiliser ("eclat:carteALF" pour les
     * cartes, "eclat:imageALF" pour les images.
     * @return la forme normalisée de l'uri.
     */
    private static final String normalizeURI(String resourceURI, String resourcePrefix) {
        char lettre = ' ';
        String numResource = resourceURI.substring(16);
        if (numResource.charAt(0) == 'I') {
            // le numéro de la resource est un chiffre romain
            return resourcePrefix + numResource;
        } else {
            // le numéro de la resource est un entier
            if (numResource.endsWith("A") || numResource.endsWith("B")) {
                // cas où les noms finissent par A ou B
                lettre = numResource.charAt(numResource.length() - 1);
                numResource = numResource.substring(0, numResource.length() - 1);
            }
            return resourcePrefix + String.format("%04d", Integer.parseInt(numResource)) + lettre;
        }
    }
}
