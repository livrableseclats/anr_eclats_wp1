package lig.steamer.eclatsdatautils;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.HashMap;
import java.util.Map;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Resource;
import org.eclipse.rdf4j.model.Value;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.Rio;

/**
 * Ce programme crée un fichier turtle qui contient les labesl des intitulés
 * construit à partir d'un export de la base postgres de cartodialect v2
 *
 * @author Philippe GENOUD - Université Grenoble Alpes - LIG STeamer
 * @author Maeva SEFFAR
 */
public class ReplaceIntitule {

    final static Charset ENCODING = StandardCharsets.UTF_8;

    final static String NEWMAP = "eclat:carteALF%s  a  <http://purl.org/fr/eclat/ontology#Map> ;\n"
            + "        <http://purl.org/dc/terms/identifier>\n"
            + "                \"%s\" ;\n"
            + "        <http://purl.org/fr/eclat/ontology#hasIntitule>\n"
            + "                eclat:intitule%s ;\n"
            + "        <http://purl.org/fr/eclat/ontology#isMapOf>\n"
            + "                eclat:ALF ;\n"
            + "        <http://purl.org/fr/eclat/ontology#isRepresentedIn>\n"
            + "                eclat:imageALF%s .\n\n"
            + "eclat:intitule%s\n"
            + "                a   <http://purl.org/fr/eclat/ontology#intitule> ;\n"
            + "        <http://purl.org/fr/eclat/ontology#hasMap>\n"
            + "                eclat:carteALF%s ;\n"
            + "        <http://www.w3.org/2004/02/skos/core#prefLabel>\n"
            + "                \"%s\" .";

    public static void main(String[] args) throws IOException {
        System.out.println(System.getProperty("user.dir"));

        // read the file 'example-data-artists.ttl' as an InputStream.
        Path pathModel = Paths.get(System.getProperty("user.dir") + "/../../data/statementsEclats.ttl");
        BufferedReader modelReader = Files.newBufferedReader(pathModel, ENCODING);
// Rio also accepts a java.io.Reader as input for the parser.
        Model model = Rio.parse(modelReader, "", RDFFormat.TURTLE);

        ValueFactory vf = SimpleValueFactory.getInstance();

// We want to find all information about the artist `ex:VanGogh` in our model
        IRI mapClassIRI = vf.createIRI("http://purl.org/fr/eclat/ontology#Map");
        IRI hasIntituleIRI = vf.createIRI("http://purl.org/fr/eclat/ontology#hasIntitule");

        Map<String, Value> intitulesURISMap = new HashMap<>();

        for (Resource map : model.filter(null, RDF.TYPE, mapClassIRI).subjects()) {
            for (Value intitule : model.filter(map, hasIntituleIRI, null).objects()) {
                intitulesURISMap.put("C" + map.toString().substring(35), intitule);

            }

        }

        Path path = Paths.get(System.getProperty("user.dir") + "/../../data/listeThemes.csv");
        BufferedWriter out = new BufferedWriter(new FileWriter(System.getProperty("user.dir") + "/../../data/listeThemes.ttl"));

        try (BufferedReader reader = Files.newBufferedReader(path, ENCODING)) {
            String line = null;
            while ((line = reader.readLine()) != null) {
                String carteName = line.substring(0, 12);
                if (!carteName.endsWith("B")) {
                    if (carteName.endsWith("A")) {
                        carteName = carteName.substring(0, carteName.length() - 1);
                    }
                    String intitule = line.substring(13);
                    if (intitule.charAt(0) == '"') {
                        intitule = intitule.substring(1, intitule.length() - 1);
                    }
                    if (intitulesURISMap.get(carteName) == null) {
                        String numALF = carteName.substring(8);
                        String turtleNewMap = String.format(NEWMAP, numALF, numALF.replace("^(0)*", ""), numALF, numALF, numALF, numALF, intitule);
                        out.write(turtleNewMap);
                        out.newLine();
                        System.out.println(turtleNewMap);
                    } else {
                        System.out.println();
                        System.out.println(intitulesURISMap.get(carteName)+ " skos:prefLabel " + intitule);
                        out.write("<" + intitulesURISMap.get(carteName) + ">"  + " skos:prefLabel " + "\"" + intitule + "\".");
                        out.newLine();
                    }
                }
                out.newLine();
            }
            // Fin de fichier - Fermeture du flux
            out.close();
        }
    }

}
