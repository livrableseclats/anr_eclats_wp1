package lig.steamer.eclatsdatautils.inventorpoints;

/**
 * Boite englobante d'une entrée d'inventaire
 * @author Philippe Genoud - LIG Steamer - Université Grenoble Aples
 */
public class SPBoundingBox {
    
    /**
     * abscisse (en pixels) du coin supérieur gauche de la boite englobante
     */
    private final int x;
    
    /**
     * ordonnée (en pixels) du coin supérieur gauche de la boite englobante
     */
    private final int y;
    
    /**
     * largeur en pixels
     */
    private final int width;
    
    /**
     * hauteur en pixels
     */
    private final int height;
    
    /**
     * no de la ligne du fichier de données où cette boite englobante est définie
     */
    private final int noLigne;

    /**
     * 
     * @param x abscisse (en pixels) du coin supérieur gauche de la boite englobante
     * @param y ordonnée (en pixels) du coin supérieur gauche de la boite englobante
     * @param width largeur en pixels
     * @param height  hauteur en pixels
     * @param noLigne  no de la ligne du fichier de données où cette boite englobante est définie
     */
    public SPBoundingBox(int x, int y, int width, int height, int noLigne) {
        this.x = x;
        this.y = y;
        this.width = width;
        this.height = height;
        this.noLigne = noLigne;
    }

    public int getNoLigne() {
        return noLigne;
    }
    
    public boolean contient(double x, double y) {
        return ( this.x <= x && x <= (this.x + this.width) &&  this.y <= y && x <= (this.y + this.height));
    }
    
}
