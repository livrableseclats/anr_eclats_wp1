package lig.steamer.eclatsdatautils.inventorpoints;

/**
 *
 * Les entrées d'inventaire sont comparables. L'ordre de comparaison des 
 * entrées d'inventaire et basé sur les abscisses croissants et ensuite
 * sur les ordonnées croissantes.
 * 
 * @author Philippe Genoud - LIG Steamer - Université Grenoble Aples
 */
public class InventoryPoint implements Comparable<InventoryPoint> {

    /**
     * abscisse viewport de l'entrée d'inventaire
     */
    private double x;

    /**
     * ordonnée viewport de l'entrée d'inventaire
     */
    private double y;

    /**
     * le numéro du point d'enquête associé au point d'inventaire
     */
    private int num;
    private boolean allreadyUsed = false;
    private int usedBy = -1;

    /**
     * le numéro de ligne où se trouve la définition de l'entrée d'inventaire
     * dans le fichier de données servant à créer l'objet
     */
    private int noLigne;

    private double distance = -1;
    
    /**
     * l'angle en degrés que fait le segment (point de référence, ce point) avec l'horizontale.
     * point de référence: le point précédemment traité
     */
    private double angle = 0;
    private double deltaAngle;
    private double deltaDistance;
    
    private String phoneticForm = "Pas encore de données disponibles";

    /**
     * Création d'une entrée d'inventaire
     * @param noLigne numéro de ligne où se trouve la définition de l'entrée d'inventaire
     * dans le fichier de données servant à créer l'objet.
     * @param x abscisse viewport de l'entrée d'inventaire
     * @param y ordonnée viewport de l'entrée d'inventaire
     */
    public InventoryPoint(int noLigne, double x, double y) {
        this(noLigne, x, y, -1);
    }

    /**
     * Création d'une entrée d'inventaire
     * @param noLigne numéro de ligne où se trouve la définition de l'entrée d'inventaire
     * dans le fichier de données servant à créer l'objet.
     * @param x abscisse viewport de l'entrée d'inventaire.
     * @param y ordonnée viewport de l'entrée d'inventaire.
     * @param num numéro du point d'enquête associé à cette entrée d'inventaire
     */
    public InventoryPoint(int noLigne, double x, double y, int num) {
        this.noLigne = noLigne;
        this.x = x;
        this.y = y;
        this.num = num;
    }

  
    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

    public int getNum() {
        return num;
    }

    public int getNoLigne() {
        return noLigne;
    }

    public void setNum(int num) {
        this.num = num;
    }

    public boolean isAllreadyUsed() {
        return allreadyUsed;
    }

    public void setAllreadyUsed(boolean allreadyUsed) {
        this.allreadyUsed = allreadyUsed;
    }

    public int getUsedBy() {
        return usedBy;
    }

    public void setUsedBy(int usedBy) {
        this.usedBy = usedBy;
    }

    public double getDistance() {
        return distance;
    }

    public void setDistance(double distance) {
        this.distance = distance;
    }

    public double getAngle() {
        return angle;
    }

    public void setAngle(double angle) {
        this.angle = angle;
    }

    public double angle(InventoryPoint target) {
        double angle = Math.toDegrees(Math.atan2(target.y - y, target.x - x));

        if (angle < 0) {
            angle += 360;
        }

        return angle;
    }

    public double getDeltaAngle() {
        return deltaAngle;
    }

    public void setDeltaAngle(double deltaAngle) {
        this.deltaAngle = deltaAngle;
    }

    void calculerDeltaAngle(InventoryPoint pt) {
        double delta = Math.abs(pt.angle - this.angle);
        if (delta > 180) {
            delta = 360 - delta;
        }
        this.deltaAngle = delta;
    }

    public double getDeltaDistance() {
        return deltaDistance;
    }

    public void setDeltaDistance(double d) {
        this.deltaDistance = d;
    }

    public double distance(InventoryPoint pt) {
        return Math.sqrt((this.x - pt.x) * (this.x - pt.x) + (this.y - pt.y) * (this.y - pt.y));
    }

    public final double distance() {
        return Math.sqrt(this.x * this.x + this.y * this.y);
    }

    public String getPhoneticForm() {
        return phoneticForm;
    }

    public void setPhoneticForm(String phoneticForm) {
        this.phoneticForm = phoneticForm;
    }

//    @Override
//    public String toString() {
//        // return "" + noLigne + ';' + num + ";" + x + ';' + normalizedX + ";" + y + ';' + normalizedY + ";" + distance() + ";" + distanceOrigine + ";";
//        return "" + num + ";" + noLigne + ';' + x + ';' + y + ";" + angle + ";" + deltaAngle + ";" + distance + ";";
//    }

    @Override
    public String toString() {
        return "InventoryPoint{num=" + num + ", x=" + x + "  y=" + y  + ", distance=" + distance + ", angle=" + angle + ", deltaDistance=" + deltaDistance + 
                 ", deltaAngle=" + deltaAngle + '}';
    }

    public void setX(double x) {
        this.x = x;
    }

    public void setY(double y) {
        this.y = y;
    }
    
    
    @Override
    public int compareTo(InventoryPoint o) {

        if (this.x < o.x) {
            return -1;
        } else if (this.x > o.x) {
            return 1;
        } else {
            return 0;
        }
    }

}
