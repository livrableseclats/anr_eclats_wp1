# MODELE DE DONNEES ECLATS

Ce projet défini le modèle de données pour le projet ECLATS (voir le wiki) et son implémentation 
sous la forme d'une ontologie OWL2.

- Le répertoire `ontology` contient les fichiers UML et OWL décrivant le modèle de données ECLATS.
- Le répertoire `data` contient des fichiers RDF (sérialisés en TURTLE) correspondant à une instanciation du modèle ECLATS pour l'ALF.
- Le répertoire `tools` contient des applications JAVA pour manipuler les données RDF du répertoire `data`.


