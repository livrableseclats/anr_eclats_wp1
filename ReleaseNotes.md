# Release Notes - CartoDialect 4.1

```
Release Note TAG 4.1.1, 29/04/19
```

# 1. Release utilisateurs

```
Section concernant les utilisateurs de l'application
```

# 1.1 AMÉLIORATIONS

## VISUALISEUR

- Mise en valeur des points d'enquête comportant une réponse phonétique identique au point sélectionné  #16 (Au clic sur un point, les points dont une réponse est phonétiquement identique à celui sélectionné sont différenciés de tous les autres points).

- Suppression de l'affichage des des numéros de volumes dans la barre de navigation du visualiseur  #10

- Mise en place de l'éditeur de texte FROALA pour les commentaires de cartes et d'annotations #2

  - Personnalisation de la barre d'outils FROALA pour correspondre aux besoins de l'utilisateur  #2

  - Ajout de la possibilité de créer des liens hypertextes dans la barre d'outils de l'éditeur de commentaires  #2

## ANNOTATIONS

- Affichage des effectifs en face des réposnes phonétiques proposées par Levenshtein  #6

## AUTRES

- Ajout d'un attribut nature grammaticale à l'échelle de la réponse  #14 (en attendant les modifications à apporter au modèle depuis conversation avec Carole du 29/04/19)

# 1.2 STYLE

- Mise en place d'un spinner (barre de chargement) pour le rechargement des données à la création d'annotations

- Amélioration du style pour l'édition des annotations (apparaît désormais sous forme d'un tableau).

- Modification du style pour les colonnes de consultation d'annotations pour une meilleure lisibilité (retour à la ligne automatique, largeur de colonnes fixes)  #1

- Rétrécissement de la largeur des barres latérales du visualiseur pour une meilleure lisibilité de la carte  #16
  
# 1.3 BUGS

## VISUALISEUR

- Correction du bug d'affichage du bandeau latéral d'aide à l'utilisation du visualiseur lorsque l'utilisateur change de page et revient sur le visualiseur.

## ANNOTATIONS

- Correction du bug sur la suppression/edition d'annotations (l'édition provoquait la suppression de l'annotation aux réponses intialement intégrées à la propagation).

- Ajout de la possibilité de mettre un champ 'vide' après avoir choisi une première option dans les listes déroulantes de création et/ou édition des annotations (Exemple: l'utilisateur renseigne d'abord un genre puis souhaite finalement ne rien renseigner dans ce champs).

- Modification de l'odre d'apprition des attributs d'un composant lors de la consultation, création ou édition d'annotations #3

- Correction du bug à l'affichage du type de la réponse à la consultation d'une annotation (l'attribut ne s'affichait pas)  #13
  
--------------------

--------------------

--------------------

# 2. Release développeurs

```
Section concernant les développeurs de l'application
```

# 2.1 BUGS

## REQUÊTES SPARQL

- Correction d'un bug suite à des modifications sur les requêtes de recherches de réponses

- Correction d'un bug sur les requêtes SPARQL

- Correction bug recherche de tous les survey points (manquait \n dans requête SPARQL)

# 2.2 FONCTIONNALITÉS PROVISOIRES

- Ajout de la possibilité de renseigner plusieurs réponses à la création d'une entrée d'inventaire  #18

- Ajout de la possibilité d'ajouter des entrées d'inventaire et ler réponse associée au double clic sur un point  #18

# 2.3 AMÉLIORATIONS

## VISUALISEUR

- Ajout de l'url permettant d'effectuer la requête HTTP qui récupère la liste de toutes les réponses contenues dans une carte pour la mise en valeur des points d'enquête dont la réponse phonétique est identiques à celle du point cliqué par l'utilisateur  #16

- Ajout d'un attribut spIdentifier à la classe réponse pour accéder aux numéros de points d'enquête associés à une réponse, ajout de la requête HTTP HTTP permettant d'accéder aux réponses contenues dans une carte ALF  #16
  
- Editeur de texte FROALA:
  - Suppression de l'ancien code permettant de commenter une carte ou une annotation  #2

  - Affichage du contenu HTML du commentaire à la place de la variable brute  #2

## AUTRES

- Requêtes SPARQL: Modification de la requête SPARQL FIND_INVENTORY qui renvoie un booléen  #7

- Export: correction d'un petit bug et ajout de l'en tête de requête définissant le type de contenu en UTF-8  #4

# 2.4 PERFORMANCES

- Modification du code pour améliorer les performances à la création et édition d'annotations

- Amélioration du code pour gain de performance  #16

# 2.5 REFACTOR

## REQUÊTES SPARQL

- Renommage de l'attribut mapId

- Suppression d'espaces nuisant à la lisibilité du code dans les queries

## DOCS

- Correction commentaires documentants

- Correction d'un commentaire d'attribut de la classe Component

## CODE MORT

- Suppression de la ligne de code mort pour le changement d'URL vers le serveur au moment du déploiement de l'application

- Suppression de code mort et ajout de commentaires HTML

- Suppression de code mort et mise en forme de l'édition d'annotations sous forme de tableau

## AUTRES

- tests SELENIUM: modification de l'utilisateur choisi pour ces tests. Utilisation d'admin plutôt que du profil de Guylaine

- gestion du lien dbepdia de manière optionnelle
