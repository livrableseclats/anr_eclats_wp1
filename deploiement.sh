#!/bin/bash
# Script permettant de déployer la partie front-end de l'application Cartodialect V5
# Il assure le remplacement du lien vers la partie Backend et le repository de l'application
# Concernant la partie Back-end, le script assure la création du fichier WAR et sa copie
# A la racine du dossier de contenant le projet cartodialect (BASEDIR)

# ./deploiement.sh ~/ECLATS/développement/

# Fonction qui permet de vérifier si la commande précédente a correctement été réalisée
# Si ce n'est pas le cas, le script s'arrête et renvoie l'erreur en paramètre ($1)
errorChecking () {
    if [ $? -eq 0 ]; then
        echo -e "ETAPE REALISÉE AVEC SUCCÈS"
    else
        echo -e "$1"
        exit
    fi
}

# Argument donné par l'utilisateur pour déterminer le chemin vers le projet.
BASEDIR=$1

if [ ! -d ${BASEDIR}/deploiement_cartodialect ]; then
    echo "Création du dosser de déploiment";
    mkdir ${BASEDIR}/deploiement_cartodialect
fi
##################################################################
##################################################################

echo -e "ETAPE 1 - MODIFICATION DES L'URL VERS LE REPOSITORY ECLATS5"

# Remplacement de la ligne contenant l'url vers le repository dans le fichier Queries.java
cd ${BASEDIR}/eclatsApps/eclatsApps-back-end/src/main/java/fr/eclatsgeoling/eclatsAppsBackEnd/dao/
errorChecking "Problème d'accès au chemin ${BASEDIR}/cartodialect_dialectoLOD/eclatsApps-back-end/src/main/java/fr/eclatsgeoling/eclatsApps/dao/"

# Remplacement de la ligne contenant l'url vers le serveur
sed -i "/HTTPRepository REPOSITORY/c\    public static final HTTPRepository REPOSITORY = new HTTPRepository(\"http://steamerlod.imag.fr/repositories/ECLATS5\");" Queries.java
errorChecking " => Erreur lors du remplacement de la ligne correspondant à l'url vers le repository dans le fichier Queries.java"

##################################################################
##################################################################

echo -e "ETAPE 2  - BUILD ET DEPLACEMENT DU FICHIER WAR"
# Racine du projet backend
cd ${BASEDIR}/eclatsApps/eclatsApps-back-end
#Commande permettant de générer le fichier WAR
mvn clean package
errorChecking " => Erreur lors de la création du fichier WAR"

# Suppression du fichier .war s'il existe
if [ -f ${BASEDIR}/deploiement_cartodialect/eclats5AppsBackEnd.war ]; then
    rm ${BASEDIR}/deploiement_cartodialect/eclats5AppsBackEnd.war
    errorChecking " => Erreur lors de la suppression du fichier war pré existant"
fi

mv target/eclatsAppsBackEnd-0.0.1-SNAPSHOT.war ${BASEDIR}/deploiement_cartodialect/eclats5AppsBackEnd.war
errorChecking " => Erreur lors de la copie du fichier war dans le dossier ${BASEDIR}"

##################################################################
##################################################################

echo -e "ETAPE 3  - MODIFICATION DES L'URL VERS LE BACKEND"

# Chemin vers le fichier store.js
cd ${BASEDIR}/eclatsApps/cartodialect-front-end/src/components/store/
errorChecking "Problème d'accès au chemin ${BASEDIR}/cartodialect/cartodialect-front-end/src/components/store/"

# Remplacement de la ligne contenant l'url vers le serveur dans le cas où ce serait le backend local qui serait pointé
sed -i "/eclats5AppsBackEnd/c\  url_serveur: 'http://lig-tdcge.imag.fr/eclats5AppsBackEnd'," store.js
errorChecking " => Erreur lors du remplacement de la ligne correspondant à l'url vers le backEnd dans le fichier store.js"


##################################################################
##################################################################

echo -e "ETAPE 4 - BUILD DU FRONT-END"
# Répertoire racine de la partie front-End
cd ${BASEDIR}/eclatsApps/cartodialect-front-end/
sed -i "/publicPath/c\   publicPath: \"/cartodialect5/\"," webpack.config.js

# Génération du dossier dist - Build de la partie front-end
npm run build
errorChecking " => Erreur lors de la compilation  npm run build"

##################################################################
##################################################################

echo -e "ETAPE 5 - COMPRESSION DU FRONT-END"
# Repertoire contenant le projet généré par la commande build
cd dist/
errorChecking  " => Problème d'accès au chemin dist/front-end/"

# Remplacement de la ligne contenant l'url vers le fichier build.js
sed -i "/build.js/c\  <!-- <script src=\"/dist/build.js\"></script>-->" index.html

# Suppression du fichier .zip s'il existe
if [ -f ${BASEDIR}/deploiement_cartodialect/front-end.zip ]; then
    rm ${BASEDIR}/deploiement_cartodialect/front-end.zip
    errorChecking " => Erreur lors de la suppression du fichier zip pré existant"
    echo "Suppression du fichier zip car il existe déjà"
fi

# Compression du projet dans le répertoire dist
zip -r ${BASEDIR}/deploiement_cartodialect/front-end.zip *
errorChecking " => Erreur lors de la compression du dossier front-end"

##################################################################
##################################################################

echo -e "ETAPE 6 - Réinitialisation des url back-end et front-end"

cd ${BASEDIR}/eclatsApps/eclatsApps-back-end/src/main/java/fr/eclatsgeoling/eclatsAppsBackEnd/dao/
sed -i "/HTTPRepository REPOSITORY/c\    public static final HTTPRepository REPOSITORY = new HTTPRepository(\"http://localhost:7200/repositories/ECLATS5\"); " Queries.java
cd ${BASEDIR}/eclatsApps/cartodialect-front-end/src/components/store/
sed -i "/eclats5AppsBackEnd/c\  url_serveur: 'http://localhost:8080/eclats5AppsBackEnd'," store.js
cd ${BASEDIR}/eclatsApps/cartodialect-front-end/
sed -i "/publicPath/c\   publicPath: \"\"," webpack.config.js

echo "---------------------------|-------------------------------"
echo "                     FIN DU SCRIPT                           "
