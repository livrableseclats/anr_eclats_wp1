# -*- coding: utf-8 -*-
# Script python réalisé par Maeva SEFFAR.
# Test de vérification du fonctionnement des
# pages liste carte et liste point d'enquête

########## -------- IMPORTS -------- ##########

from selenium import webdriver
#Permet d'envoyer les touches du clavier au navigateur
from selenium.webdriver.common.keys import Keys
#Permet d'inserer des temps d'attente avec sleep(sec)
from time import sleep
print("|")
print("----------------- NOUVEAU TEST -----------------")
print("|")
print("LANCEMENT TEST CHARGEMENT PAGE LISTE DES CARTES")
print("|")

############################
# -------- DRIVER -------- #
############################

# Driver permettant d'ouvrir le navigateur
driver = webdriver.Chrome('/home/steamer/seffarm/chromedriver')

## methode qui mène vers la page en attribut
print("CHARGEMENT DE LA PAGE")
print("|")
driver.get("http://localhost:4200/")


###########################
# -------- Cartes ------- #
###########################


driver.find_element_by_id("listeCartes").click()
sleep(2)
assert "ABEILLE" in driver.find_element_by_xpath("/html/body/app-root/app-navigation/app-maplist/mat-table/mat-row[1]/mat-cell[2]/span").text
print("CHARGEMENT CARTES ALF ------> OK ")
print("|")


driver.find_element_by_id("researchMapInput").click()

print("CHAMPS DE RECHERCHE ------> OK ")
print("|")
driver.find_element_by_id("researchMapInput").send_keys("champ")
assert "CHAMP" in driver.find_element_by_xpath("/html/body/app-root/app-navigation/app-maplist/mat-table/mat-row[1]/mat-cell[2]/span").text
assert "GARDE" in driver.find_element_by_xpath("/html/body/app-root/app-navigation/app-maplist/mat-table/mat-row[4]/mat-cell[2]/span").text

print("FONCTIONNALITÉS DE RECHERCHE ------> OK ")
print("|")
driver.find_element_by_id("researchMapInput").send_keys(Keys.BACK_SPACE)
driver.find_element_by_id("researchMapInput").send_keys(Keys.BACK_SPACE)
driver.find_element_by_id("researchMapInput").send_keys(Keys.BACK_SPACE)
driver.find_element_by_id("researchMapInput").send_keys(Keys.BACK_SPACE)
driver.find_element_by_id("researchMapInput").send_keys(Keys.BACK_SPACE)

#Fermeture du navigateur
driver.close()

print("FIN DU TEST")
