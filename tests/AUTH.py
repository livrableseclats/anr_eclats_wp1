# -*- coding: utf-8 -*-
# Script python realisé par Maeva SEFFAR.
# Réalise différents tests relatifs à la connexion et
# déconnexion de l'utilisateur

########## -------- IMPORTS -------- ##########

from selenium import webdriver
#Permet d'envoyer les touches du clavier au navigateur
from selenium.webdriver.common.keys import Keys
#Permet d'inserer des temps d'attente avec sleep(sec)
from time import sleep

print("----------------- NOUVEAU TEST -----------------")
print("LANCEMENT TEST D'AUTHENTIFICATION UTILISATEUR")
print("|")

############################
# -------- DRIVER -------- #
############################

# Driver permettant d'ouvrir le navigateur
driver = webdriver.Chrome('/home/steamer/seffarm/chromedriver')

## methode qui mene vers la page en attribut
print("CHARGEMENT DE LA PAGE")
print("|")
driver.get("http://localhost:4200/")


###########################
# -------- LOGIN -------- #
###########################

driver.find_element_by_id("loginRouter").click()
driver.find_element_by_name("username").send_keys("admin")
driver.find_element_by_name("password").send_keys("9kWN3Sjb5")
driver.find_element_by_id("login").click()

cookies = driver.get_cookies()

for cookie in cookies:
	if cookie['value'] == "admin":
		print("1. CONNEXION UTILISATEUR ----------> OK")
		print("|")

###########################
# -------- LOGOUT -------- #
###########################
sleep(2)
driver.find_element_by_id("logoutButton").click()

cookies = driver.get_cookies()

for cookie in cookies:
	if cookie['value'] == "bruntrigaudg":
		print("2. DECONNEXION ECHOUEE")
		print("|")


print("2. DECONNEXION ----------> OK")
print("|")

#Fermeture du navigateur
driver.close()

print("FIN TEST D'AUTHENTIFICATION UTILISATEUR")
