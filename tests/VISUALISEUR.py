# -*- coding: utf-8 -*-
# Script python realisé par Maeva SEFFAR.
#  Test de vérification du fonctionnement 
# du visualiseur

########## -------- IMPORTS -------- ##########

from selenium import webdriver
#Permet d'envoyer les touches du clavier au navigateur
from selenium.webdriver.common.keys import Keys
#Permet d'inserer des temps d'attente avec sleep(sec)
from time import sleep

print("LANCEMENT TEST VISUALISEUR")
print("|")

############################
# -------- DRIVER -------- #
############################

# Driver permettant d'ouvrir le navigateur
driver = webdriver.Chrome('/home/steamer/seffarm/chromedriver')

## methode qui mene vers la page en attribut
print("CHARGEMENT DE LA PAGE")
print("|")
driver.get("http://localhost:4200/")


#################################
# -------- VISUALISEUR -------- #
#################################

driver.find_element_by_id("visualiseur").click()
driver.find_element_by_id("searchInput").click()
driver.find_element_by_id("searchInput").send_keys("227")
driver.find_element_by_id("searchInput").send_keys(Keys.RETURN)
print("1. RECHERCHE DANS LE VISUALISEUR --------> OK")
print("|")

#Verification de l'intitule de la carte
assert "CHAMPIGNON" in driver.find_element_by_id("mapTitleSpan").text
print("2. VISUALISATION DES CARTES ALF OPENSEADRAGON --------> OK")
print("|")

#Verification de l'intitule de la carte
driver.find_element_by_xpath("//*[@id='openseadragon1']/div/div[2]/div[1]/div/div[1]").click()
print("3. ZOOM OPENSEADRAGON --------> OK")
print("|")

#################################
# -------- ANNOTATIONS -------- #
#################################

#Activation du mode d'annotation
driver.find_element_by_class_name("mat-button-toggle-label-content").click()
print("4. ACTIVATION DU MODE ANNOTATIONS --------> OK")
print("|")

sleep(2)

#Verification de l'affichage des PE
for circle in driver.find_elements_by_tag_name("circle"):
	prefix,id = circle.id.split("-")
	if id == "307":
		print("5. AFFICHAGE DES POINTS D'ENQUETE -------> OK")
		#driver.find_element_by_id(id).click()	
				
#Fermeture du navigateur
driver.close()
						
print("FIN TEST VISUALISEUR")
