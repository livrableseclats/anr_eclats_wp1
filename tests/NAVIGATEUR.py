 #-*- coding: utf-8 -*-
# Script python realisé par Maeva SEFFAR.
#  Test de vérification du navigateur

########## -------- IMPORTS -------- ##########

from selenium import webdriver
#Permet d'envoyer les touches du clavier au navigateur
from selenium.webdriver.common.keys import Keys
#Permet d'inserer des temps d'attente avec sleep(sec)
from time import sleep
print("|")
print("----------------- NOUVEAU TEST -----------------")
print("|")
print("LANCEMENT TEST ROUTAGE")
print("|")

############################
# -------- DRIVER -------- #
############################

# Driver permettant d'ouvrir le navigateur
driver = webdriver.Chrome('/home/steamer/seffarm/chromedriver')

## methode qui mene vers la page en attribut
print("CHARGEMENT DE LA PAGE")
print("|")
driver.get("http://localhost:4200/")

########################################################
# --------------- TESTS SUR LE ROUTAGE --------------- #
########################################################

driver.find_element_by_id("accueil").click()
assert "Extraction d'informations" in driver.find_element_by_id("accueilTitle").text
print("PAGE D'ACCUEIL ---------------------> OK")
print("|")
driver.find_element_by_id("listeCartes").click()
assert "Liste des cartes ALF" in driver.find_element_by_id("listeCartesTitle").text
print("PAGE DE CARTE ---------------------> OK")
print("|")
driver.find_element_by_id("listePointsEnquete").click()
assert u"Liste des points d'enquête" in driver.find_element_by_id("surveyPointTitle").text
print("PAGE DE POINT ENQUETE ---------------------> OK")
print("|")
driver.find_element_by_id("visualiseur").click()
assert "ABEILLE" in driver.find_element_by_id("mapTitleSpan").text
print("PAGE DE VISUALISEUR ---------------------> OK")
print("|")
driver.find_element_by_id("loginRouter").click()
assert "Identifiez-vous" in driver.find_element_by_id("loginTitle").text
print("PAGE DE LOGIN ---------------------> OK")
print("|")


#Fermeture du navigateur
driver.close()

print("FIN TEST ROUTAGE")
