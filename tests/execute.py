# -*- coding: utf-8 -*-

print("----------------- LANCEMENT DE L'EXECUTION DES DIFFERENTS TESTS -----------------")

print("----------------- TEST 1 : NAVIGATION -----------------")
execfile("NAVIGATEUR.py")

print("----------------- TEST 2 : LISTE DES POINTS D'ENQUETE -----------------")
execfile("LISTE_PE.py")

print("----------------- TEST 3 : LISTE DES CARTES ALF -----------------")
execfile("LISTE_CARTES.py")

print("----------------- TEST 4 : AUTHENTIFICATION -----------------")
execfile("AUTH.py")

print("----------------- TEST 5 : ANNOTATIONS -----------------")
execfile("ANNOTATIONS.py")

print("----------------- TESTS RÉUSSIS -----------------")
