# -*- coding: utf-8 -*-
# Script python réalisé par Maeva SEFFAR.
# Réalise differents tests de fonctionnement de l'application Cartodialect.
# A la fin du test, l'utilisateur est connecté sous l'identité Maeva et
# se situe sur la carte ALF numéro 227 (Champignon) du visualiseur avec
# le mode annotations activé.

########## -------- IMPORTS -------- ##########

from selenium import webdriver
#Permet d'envoyer les touches du clavier au navigateur
from selenium.webdriver.common.keys import Keys
#Permet d'insérer des temps d'attente avec sleep(sec)
from time import sleep

#############################################################
# ------------------------ DRIVER ------------------------ #
#############################################################

# Driver permettant d'ouvrir le navigateur
driver = webdriver.Chrome('/home/steamer/seffarm/chromedriver')

## méthode qui mène vers la page en attribut
print("----------------- NOUVEAU TEST -----------------")
print("CHARGEMENT DE LA PAGE")
print("|")
driver.get("http://localhost:4200/")


############################################################
# ------------------------ LOGIN ------------------------ #
############################################################

## Click sur le lien de navigation vers la connexion utilisateur
driver.find_element_by_id("loginRouter").click()
driver.find_element_by_name("username").send_keys("bruntrigaudg")
driver.find_element_by_name("password").send_keys("NptG28wU3")
driver.find_element_by_id("login").click()

## Vérification de la connexion grâce aux cookies
cookies = driver.get_cookies()
for cookie in cookies:
	if cookie['value'] == "bruntrigaudg":
		print("3. CONNEXION UTILISATEUR ----------> OK")
		print("|")

##################################################################
# ------------------------ VISUALISEUR ------------------------ #
##################################################################

## Click sur le lien de navigation vers le visualiseur
driver.find_element_by_id("visualiseur").click()
driver.find_element_by_id("searchInput").click()
driver.find_element_by_id("searchInput").send_keys("227")
driver.find_element_by_id("searchInput").send_keys(Keys.RETURN)
print("4. RECHERCHE DANS LE VISUALISEUR --------> OK")
print("|")

#Verification de l'intitule de la carte
assert "CHAMPIGNON" in driver.find_element_by_id("mapTitleSpan").text
print("5. VISUALISATION DES CARTES ALF OPENSEADRAGON --------> OK")
print("|")

#Zoom
driver.find_element_by_xpath("//*[@id='openseadragon1']/div/div[2]/div[1]/div/div[1]").click()
print("6. ZOOM OPENSEADRAGON --------> OK")
print("|")

##################################################################
# ------------------------ ANNOTATIONS ------------------------ #
##################################################################

#Activation du mode d'annotation
driver.find_element_by_id("annotationsButton").click()
print("7. ACTIVATION DU MODE ANNOTATIONS --------> OK")
print("|")

sleep(2)

#Verification de l'affichage des PE
for circle in driver.find_elements_by_tag_name("circle"):
	prefix,id = circle.id.split("-")
	if id == "307":
		# Click sur un point d'enquête
		circle.click()
		print("8. AFFICHAGE DES POINTS D'ENQUETE -------> OK")
		print("|")

		sleep(2)
		#Vérification de l'affichage du bandeau latéral
		annotation = driver.find_element_by_tag_name("mat-sidenav-content")
		if annotation is not None:
			print("9. AFFICHAGE BANDEAU LATERAL -------> OK")
			print("|")

##################################################################
# ------------------------ ANNOTATIONS ------------------------ #
##################################################################

# Verification du contenu du bandeau lateral vierge
assert "377" in driver.find_element_by_id("sideBar_spIdentifier").text
assert u"ʃˈãpiɲɔ̃" in driver.find_element_by_id("sideBar_rousselotPhoneticForm").text
print("10. AFFICHAGE DES DONNEES D'ENTREES D'INVENTAIRE -------> OK")
print("|")
