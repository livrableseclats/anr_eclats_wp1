package fr.eclatsgeoling.eclatsAppsBackEnd.controller;

import fr.eclatsgeoling.eclatsAppsBackEnd.classes.Map;
import fr.eclatsgeoling.eclatsAppsBackEnd.dao.MapDAO;

import org.springframework.web.bind.annotation.*;

import java.util.HashMap;
import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class MapController {

	/**
	 * Fonction qui permet de récupérer la liste de toutes les cartes contenues dans le repository
	 * @return la liste de toutes les cartes contenues dans le repository
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/findAllMaps")

	public List<Map> findAllMaps() {
		return MapDAO.findAllMaps();
	}
	
	/**
	 * permet de connaître le "statut" d'une carte:
	 * 
	 * Carte vierge: C'est une carte pour laquelle nous n'avons pas les entrées d'inventaires (si la requête HAS_MAP_INVENTRIES retourne TRUE )
	 * Carte transcriptable: c'est à dire sur laquelle on peut utiliser les outils CRUD pour les transcriptions phonétiques (si la requête MISS_MAP_PHONETIC_TRANSCRIPTIONS retourne TRUE)
	 * Carte annotable: c'est à dire sur laquelle on peut utiiser les outils CRUD pour les annotations (si la requête MISS_MAP_PHONETIC_TRANSCRIPTIONS retourne FALSE)
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/getMapStatus")

	public HashMap<String, Boolean> getMapStatus(String mapIdentifier) {
		return MapDAO.getMapStatus(mapIdentifier);
	}
	
}
