package fr.eclatsgeoling.eclatsAppsBackEnd.controller;

import java.io.FileNotFoundException;
import java.io.IOException;

import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import fr.eclatsgeoling.eclatsAppsBackEnd.dao.ExportInterpretationsListDAO;
import fr.eclatsgeoling.eclatsAppsBackEnd.dao.ExportMapListDAO;
import fr.eclatsgeoling.eclatsAppsBackEnd.dao.ExportPhoneticTranscriptionsDAO;

import org.springframework.http.HttpHeaders;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class ExportController {

    /**
     * Cette fonction permet à l'utilisateur d'exporter les données
     * d'interprétation d'une carte ALF.
     *
     * @param map l'id de la carte pour laquelle il faut exporter les données
     * @return un fichier csv comportant les données de la carte
     * @throws FileNotFoundException
     */
    @RequestMapping(method = RequestMethod.GET, value = "/exportAllInterpretationsInMap")
    public ResponseEntity<String> exportAllInterpretationsInMap(String mapId, String username) throws FileNotFoundException {
        // -----------------------------------------------------
        // définition de l'en tête de la réponse HTTP
        // -----------------------------------------------------
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "text/csv; charset=utf-8");

        // ResponseEntity représente la réponse HTTP entière. Vous pouvez contrôler tout
        // ce qui s'y passe: code d'état, en-têtes et corps.
        return ResponseEntity.ok().headers(headers)
                // placement de la string dans le corps de la requête, 
                .body(ExportInterpretationsListDAO.exportAllInterpretationsInMap(mapId, username));
    }
    
    /**
     * Export des transcriptions phonétiques associées aux entrées d'inventaires de la carte passée en paramètre
     * @param mapId l'id de la carte pour laquelle il faut exporter les transcriptions phonétiques
     * @return un fichier csv comportant les données de la carte
     * @throws FileNotFoundException
     */
    @RequestMapping(method = RequestMethod.GET, value = "/exportAllPhoneticTranscriptionInMap")
    public ResponseEntity<String> exportAllPhoneticTranscriptionInMap(String mapId) throws FileNotFoundException {
        // -----------------------------------------------------
        // définition de l'en tête de la réponse HTTP
        // -----------------------------------------------------
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "text/csv; charset=utf-8");

        // ResponseEntity représente la réponse HTTP entière. Vous pouvez contrôler tout
        // ce qui s'y passe: code d'état, en-têtes et corps.
        return ResponseEntity.ok().headers(headers)
                // placement de la string dans le corps de la requête, 
                .body(ExportPhoneticTranscriptionsDAO.exportAllPhoneticTranscriptionInMap(mapId));
    }
	/**
	 * Permet la mise en forme des cartes issues des recherches de cartes par intitulé, numéro, ou par catégories grammaticales
	 * pour l'export de ces données en format csv
	 * @param mapsList la liste des cartes issue de la recherche
	 * @return Retourne une liste de cartes de type string
	 */
    @RequestMapping(method = RequestMethod.POST, value = "/exportMapSearchResult")
    public ResponseEntity<String> exportMapSearchResult(@RequestBody String mapList) throws JsonParseException, JsonMappingException, IOException {
        // -----------------------------------------------------
        // définition de l'en tête de la réponse HTTP
        // -----------------------------------------------------
        HttpHeaders headers = new HttpHeaders();
        headers.add("Content-Type", "text/csv; charset=utf-8");

        // ResponseEntity représente la réponse HTTP entière. Vous pouvez contrôler tout
        // ce qui s'y passe: code d'état, en-têtes et corps.
        return ResponseEntity.ok().headers(headers)
                // placement de la string dans le corps de la requête, 
                .body(ExportMapListDAO.exportMapSearchResult(mapList));         
    }
}
