package fr.eclatsgeoling.eclatsAppsBackEnd.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.http.HttpHeaders;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import fr.eclatsgeoling.eclatsAppsBackEnd.classes.Map;
import fr.eclatsgeoling.eclatsAppsBackEnd.dao.GrammaticalCategoriesDAO;
import org.springframework.web.bind.annotation.CrossOrigin;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class GrammaticalCategoriesController {

    /**
     * @param concept l'URI du concept (catégorie grammaticale) renseigné par
     * l'utilisateur
     * @return une liste de propriétés associées à la catégorie grammaticale
     * (concept) passé en paramètre
     */
    @RequestMapping(method = RequestMethod.GET, value = "/findProps")
    public ResponseEntity<String> findProps(String concept) {
        HttpHeaders headers = new HttpHeaders();
        headers.add(HttpHeaders.CONTENT_TYPE, "application/json");

        return ResponseEntity.ok()
                .headers(headers)
                .body(GrammaticalCategoriesDAO.findProps(concept).toString());
    }

    /**
     * @param selectedProperties
     * @return une liste de cartes dont l'intitulé contient un mot dont la
     * catégorie grammaticale correspond au concept en paramètre
     * @throws IOException 
     * @throws JsonMappingException 
     * @throws JsonParseException 
     */
    @RequestMapping(method = RequestMethod.GET, value = "/findMapsWithGramCat")
    public List<Map> findMapsWithGramCat(String selectedProperties) throws JsonParseException, JsonMappingException, IOException {
        return GrammaticalCategoriesDAO.findMapsWithGramCat(selectedProperties);
    }

    /**
     * @param selectedProperties
     * @return une liste de cartes dont l'intitulé contient des mots dont la
     * catégorie grammaticale correspond aux concepts en paramètres
     * @throws IOException 
     * @throws JsonMappingException 
     * @throws JsonParseException 
     */
    @RequestMapping(method = RequestMethod.GET, value = "/findMapsWithGramCatWithOperator")
    public List<Map> findMapsWithGramCatWithOperator(String selectedProperties) throws JsonParseException, JsonMappingException, IOException {
        return GrammaticalCategoriesDAO.findMapsWithGramCatWithOperator(selectedProperties);
    }

}
