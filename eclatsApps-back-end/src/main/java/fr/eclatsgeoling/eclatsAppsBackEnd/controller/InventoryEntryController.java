package fr.eclatsgeoling.eclatsAppsBackEnd.controller;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.eclatsgeoling.eclatsAppsBackEnd.classes.InventoryEntry;
import fr.eclatsgeoling.eclatsAppsBackEnd.dao.InventoryEntryDAO;
import fr.eclatsgeoling.eclatsAppsBackEnd.dao.SurveyPointDAO;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class InventoryEntryController {
	
	/**
	 * Envoie la requête qui permet de récupérer les entrées d'inventaires contenues dans une
	 * carte, la liste est utilisée pour le mode transcription phonétique
	 * @param mapId la carte courante
	 * @return la liste de toutes les entrées d'inventaires
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/findAllInventoryEntries")

	public ArrayList<InventoryEntry> findAllInventoryEntries(String mapId) {
		return InventoryEntryDAO.findAllInventoryEntries(mapId);
	}
	/**
	 * Envoie la requête qui permet de récupérer les entrées d'inventaires contenues dans une
	 * carte, la liste permet d'afficher les disques d3
	 * 
	 * @param mapId l'id de la carte pour laquelle il faut trouver les entrées d'inventaires
	 * @param username le nom de l'utilisateur courant
	 * @return un ArrayList d'entrées d'inventaires
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/findAllInventoryEntriesForD3")

	public ArrayList<InventoryEntry> findAllInventoryEntriesForD3(String mapId, String username) {
		return InventoryEntryDAO.findAllInventoryEntriesForD3(mapId, username);
	}

	//--------------------------------------------------------------------------------
	// Partie crétation d'entrées d'inventaire (sera obsolète quand les données sur
	// les point d'enquêtes (coordonnées dans l'image, rousselotPhoneticForm) seront
	// récupérées.
	//--------------------------------------------------------------------------------

	

	/**
	 * Envoie la requête qui permet d'ajouter une entrée d'inventaire à une carte
	 * 
	 * @param inventoryEntry l'entrée d'inventaire à ajouter
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/addInventoryEntry")
	public void addInventoryEntry(@RequestBody LinkedHashMap<String, Object> inventoryEntry) {
		InventoryEntryDAO.addInventoryEntry(inventoryEntry);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/addInventoryEntry")
	public void addSurveyPoint() {
	}

	/**
	 * Envoie la requête appelée lorsque l'utilisateur souhaite visualiser la localisation d'un point d'enquête dans la "Carte N° I :  NOMS FRANÇAIS DES LOCALITÉS"
	 * Retourne l'entrée d'inventaire correspondant à l'id passé en paramètre
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/findInventoryEntry")

	public InventoryEntry findInventoryEntry(String inventoryEntry) {
		return InventoryEntryDAO.findInventoryEntry(inventoryEntry);
	}
	
	
	/**
	 * Envoie la requête qui permet de récupérer les entrées d'inventaires associées à une interprétation
	 * 
	* @param interpretationId l'id de l'interprétation
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/findRelatedInventoryEntries")

	public ArrayList<InventoryEntry> findRelatedInventoryEntries(String interpretationId) {
		return InventoryEntryDAO.findRelatedInventoryEntries(interpretationId);
	}

}
