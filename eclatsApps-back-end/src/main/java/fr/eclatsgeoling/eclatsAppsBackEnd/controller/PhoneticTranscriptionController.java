package fr.eclatsgeoling.eclatsAppsBackEnd.controller;

import java.io.IOException;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import fr.eclatsgeoling.eclatsAppsBackEnd.dao.PhoneticTranscriptionDAO;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class PhoneticTranscriptionController {
	
	/**
	 * Permet d'ajouter une transcription phonétiques à une liste d'entrées d'inventaires
	 * @param params contient l'id de la carte, les ids des entrées d'inventaire et la 
	 * transcription phonétique à associer
	 * @throws IOException 
	 * @throws JsonMappingException 
	 * @throws JsonParseException 
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/addPhoneticTranscription")
	public void addPhoneticTranscription(
			@RequestParam("mapId") String mapId,
			@RequestParam("phoneticTranscription") String phoneticTranscription,
			@RequestParam("phoneticTranscriptionInfo") String phoneticTranscriptionInfo,
			@RequestParam("circleList") String circleList) throws JsonParseException, JsonMappingException, IOException {
		PhoneticTranscriptionDAO.addPhoneticTranscription(mapId, phoneticTranscription, phoneticTranscriptionInfo, circleList);
	}
	
	/**
	 * Supprime la transcription phonétique associées aux entrées d'inventaires contenues dans la liste des disques
	 * @param mapId
	 * @param phoneticTranscriptionToDelete
	 * @param circleList
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/deletePhoneticTranscription")
	public void deletePhoneticTranscription(
			@RequestParam("mapId") String mapId,
			@RequestParam("phoneticTranscriptionToDelete") String phoneticTranscriptionToDelete,
			@RequestParam("complementaryInfoToDelete") String complementaryInfoToDelete,
			@RequestParam("circleList") String circleList) throws JsonParseException, JsonMappingException, IOException {
		PhoneticTranscriptionDAO.deletePhoneticTranscription(mapId, phoneticTranscriptionToDelete, complementaryInfoToDelete, circleList);
	}
}
