package fr.eclatsgeoling.eclatsAppsBackEnd.controller;

import fr.eclatsgeoling.eclatsAppsBackEnd.classes.SurveyPoint;
import fr.eclatsgeoling.eclatsAppsBackEnd.dao.SurveyPointDAO;

import org.springframework.web.bind.annotation.*;

import java.util.List;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class SurveyPointController {

	/**
	 * Fonction qui permet de récupérer la liste de toutes lespoints d'enquête contenues dans le repository
	 * @return la liste de toutes les cartes contenues dans le repository
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/findAllSurveyPoints")

	public List<SurveyPoint> findAllSurveyPoints() {
		return SurveyPointDAO.findAllSurveyPoints();
	}
	
	// Requête qui permet de récupérer le point d'enquête en attribut
	@RequestMapping(method = RequestMethod.GET, value = "/findSurveyPoint")

	public SurveyPoint findSurveyPoint(String spIdentifier) {
		return SurveyPointDAO.findSurveyPoint(spIdentifier);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/getSurveyPointsCoords", produces={"application/json"})

	public String getSurveyPointsCoords() {
		return SurveyPointDAO.getSurveyPointsCoords();
	}


}
