package fr.eclatsgeoling.eclatsAppsBackEnd.controller;

import java.util.ArrayList;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.eclatsgeoling.eclatsAppsBackEnd.dao.FilterDAO;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class FilterController {

	/**
	 * Fonction qui permet de récupérer la liste des différentes valeurs associées aux attributs des interprétations publiques 
	 * OU appartenant à l'utilisateur connecté déjà renseignées
	 * @param username le nom de l'utilisateur connecté
	 * @return une liste de valeurs
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/findAllAttributes")
	public ArrayList<ArrayList<String>> findAllAttributes(String username){
		return FilterDAO.findAllAttributes(username);
	}
	
}
