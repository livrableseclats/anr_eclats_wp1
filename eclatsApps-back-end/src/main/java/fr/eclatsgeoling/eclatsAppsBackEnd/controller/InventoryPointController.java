package fr.eclatsgeoling.eclatsAppsBackEnd.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.eclatsgeoling.eclatsAppsBackEnd.utils.inventoryentries.InventoryEntriesCoordinateGenerator;
import fr.eclatsgeoling.eclatsAppsBackEnd.classes.InventoryPoint;
import fr.eclatsgeoling.eclatsAppsBackEnd.dao.InventoryPointDAO;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.springframework.web.bind.annotation.RequestParam;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class InventoryPointController {

    /**
     * Fonction qui permet de récupérer les entrées d'inventaires contenues dans
     * une carte
     *
     * @param mapId l'id de la carte pour laquelle il faut trouver les entrées
     * d'inventaires
     * @param username le nom de l'utilisateur courant
     * @return un ArrayList d'entrées d'inventaires
     */
    @RequestMapping(method = RequestMethod.GET, value = "/generateInventries")
    public List<InventoryPoint> generateInventries(@RequestParam("mapId") String mapId, @RequestParam("referencePoints") String referencePoints) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            List<InventoryPoint> referencePointsList = objectMapper.readValue(referencePoints, new TypeReference<List<InventoryPoint>>() {
            });
//            InventoryPoint ptOuestCible = new InventoryPoint(493, 0.0531436044406855, 0.38198213796496794);
//            InventoryPoint ptEstCible = new InventoryPoint(987, 0.9281341445265345, 0.6985015494451419);
//            InventoryPoint ptNordCible = new InventoryPoint(297, 0.4772670305592545, 0.08685767676378049);
//            InventoryPoint ptSudCible = new InventoryPoint(796, 0.4980327175947318, 1.1241981336724476);
//            InventoryEntriesCoordinateGenerator generator = new InventoryEntriesCoordinateGenerator("0042",
//                    ptOuestCible, ptEstCible, ptNordCible, ptSudCible);
            InventoryEntriesCoordinateGenerator generator = new InventoryEntriesCoordinateGenerator(mapId, referencePointsList);
            return generator.generate();
        } catch (IOException ex) {
            return new ArrayList<>();
        }
    }

    @RequestMapping(method = RequestMethod.POST, value = "/generateInventries")
    public void addInventoryEntry(@RequestParam("mapId") String mapId, @RequestParam("referencePoints") String referencePoints) {
        try {
            ObjectMapper objectMapper = new ObjectMapper();
            List<InventoryPoint> referencePointsList = objectMapper.readValue(referencePoints, new TypeReference<List<InventoryPoint>>() {
            });
            InventoryEntriesCoordinateGenerator generator = new InventoryEntriesCoordinateGenerator(mapId, referencePointsList);
            InventoryPointDAO.createInventoryPoints(mapId, generator.generate());
        } catch (IOException ex) {
            throw new IllegalArgumentException("pb creation des points d'inventaires ", ex);
        }
    }

//    //--------------------------------------------------------------------------------
//    // Partie crétation d'entrées d'inventaire (sera obsolète quand les données sur
//    // les point d'enquêtes (coordonnées dans l'image, rousselotPhoneticForm) seront
//    // récupérées.
//    //--------------------------------------------------------------------------------
//    /**
//     * Fonction qui permet d'ajouter une entrée d'inventaire à une carte
//     *
//     * @param inventoryEntry l'entrée d'inventaire à ajouter
//     */
//    @RequestMapping(method = RequestMethod.POST, value = "/addInventoryEntry")
//    public void addInventoryEntry(@RequestBody LinkedHashMap<String, Object> inventoryEntry) {
//        InventoryEntryDAO.addInventoryEntry(inventoryEntry);
//    }
//
//    @RequestMapping(method = RequestMethod.GET, value = "/addInventoryEntry")
//    public void addSurveyPoint() {
//    }
//
//    /**
//     * Fonction appelée lorsque l'utilisateur souhaite visualiser la
//     * localisation d'un point d'enquête dans la "Carte N° I : NOMS FRANÇAIS DES
//     * LOCALITÉS" Retourne l'entrée d'inventaire correspondant à l'id passé en
//     * paramètre
//     */
//    @RequestMapping(method = RequestMethod.GET, value = "/findInventoryEntry")
//
//    public InventoryEntry findInventoryEntry(String inventoryEntry) {
//        return InventoryEntryDAO.findInventoryEntry(inventoryEntry);
//    }
//
//    /**
//     * Fonction qui permet de récupérer les entrées d'inventaires associées à
//     * une interprétation
//     *
//     * @param interpretationId l'id de l'interprétation
//     */
//    @RequestMapping(method = RequestMethod.GET, value = "/findRelatedInventoryEntries")
//
//    public ArrayList<InventoryEntry> findRelatedInventoryEntries(String interpretationId) {
//        return InventoryEntryDAO.findRelatedInventoryEntries(interpretationId);
//    }
}
