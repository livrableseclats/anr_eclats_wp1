/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.eclatsgeoling.eclatsAppsBackEnd.controller;

import fr.eclatsgeoling.eclatsAppsBackEnd.classes.EuropeanaResource;
import fr.eclatsgeoling.eclatsAppsBackEnd.dao.EuropeanaResourcesDAO;
import java.util.LinkedHashMap;
import java.util.List;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

/**
 *
 * @author seffarm
 */
@RestController
public class EuropeanaResourceController {

    /**
     * Ajoute la ressource Europeana au tripleStore
     * @param europeanaResourceForm les éléments caractérisant la ressource Europeana a ajouter au tripleStore
     */
    @RequestMapping(method = RequestMethod.POST, value = "/addEuropeanaResourceForMap")
    public void addEuropeanaResourceForMap(@RequestBody LinkedHashMap<String, String> europeanaResourceForm) {
        EuropeanaResourcesDAO.addEuropeanaResourceForMap(europeanaResourceForm);
    }

    @RequestMapping(method = RequestMethod.GET, value = "/addEuropeanaResourceForMap")
    public void addEuropeanaResourceForMap() {
    }

    /**
     * Recherche la liste des ressources Europeana associées à la carte dont l'identifiant est passé en paramètre
     * @param mapIdentifier l'identifiant de la carte à laquelle il faut récupérer les ressources Europeana
     * @return une liste de ressources Europeana associées à la carte dont l'idientiant est passé
     * en paramètre
     */
    @RequestMapping(method = RequestMethod.GET, value = "/findEuropeanaResourcesForMap")
    public List<EuropeanaResource> findEuropeanaResourcesForMap(String mapIdentifier) {
        return EuropeanaResourcesDAO.findEuropeanaResourcesForMap(mapIdentifier);
    }
}
