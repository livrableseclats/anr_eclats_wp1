package fr.eclatsgeoling.eclatsAppsBackEnd.controller;

import java.io.IOException;
import java.util.List;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.databind.JsonMappingException;

import fr.eclatsgeoling.eclatsAppsBackEnd.classes.Map;
import fr.eclatsgeoling.eclatsAppsBackEnd.dao.ThematicalCategoriesDAO;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class ThematicalCategoriesController {

	/**
	 * Méthode permettant de récupérer la liste des cartes associées aux thèmes passés en paramètre
	 * @param thematicCat la liste des thèmes renseignés par l'utilisateur
	 * @return la liste des cartes associées aux thèmes passés en paramètre
	 */
    @RequestMapping(method = RequestMethod.GET, value = "/findMapsWithThematicCat")
    public List<Map> findMapsWithThematicCat(String thematicCat) throws JsonParseException, JsonMappingException, IOException {
        return ThematicalCategoriesDAO.findMapsWithThematicCat(thematicCat);
    }
}
