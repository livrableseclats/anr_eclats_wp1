package fr.eclatsgeoling.eclatsAppsBackEnd.controller;

import java.util.ArrayList;
import java.util.LinkedHashMap;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.eclatsgeoling.eclatsAppsBackEnd.classes.MapComment;
import fr.eclatsgeoling.eclatsAppsBackEnd.dao.MapCommentDAO;

@CrossOrigin(origins = "http://localhost:4200")
@RestController

public class MapCommentController {

	/**
	 * Requête qui permet d'ajouter un commentaire à une carte
	 * @param mapCommentForm le formulaire renseigné par l'utilisateur dans la partie client de l'application
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/addMapComment")
	public void addMapComment(@RequestBody LinkedHashMap<String, String> mapCommentForm) {
			MapCommentDAO.addMapComment(mapCommentForm);
	}

	@RequestMapping(method = RequestMethod.GET, value = "/addMapComment")
	public void addMapComment() {
	}
	
	/**
	 * Requête qui permet de récupérer la liste des commentaires associés à une carte
	 * @param mapId l'id de la carte
	 * @return une liste de commentaires
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/findAllMapComment")
	public ArrayList<MapComment> findAllMapComment(String mapId) {
		return MapCommentDAO.findAllMapComment(mapId);
	}
	
	/**
	 * Requête qui permet de récupérer le commentaires à éditer
	 * @param mapCommentIdToEdit l'id du commentaire à éditer
	 * @return le commentaire de carte à éditer
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/findMapCommentToEdit")
	public MapComment findMapCommentToEdit(String mapCommentIdToEdit) {
		return MapCommentDAO.findMapCommentToEdit(mapCommentIdToEdit);
	}

	
	/**
	 * Requête qui permet de supprimer une interprétation assopciée à une réponse dans une carte 
	 * @param interpretationId l'id de l'interprétation à supprimer
	 */
	@RequestMapping(method = RequestMethod.DELETE, value = "/deleteMapComment")
	public void deleteMapComment(String mapCommentId) {
		 MapCommentDAO.deleteMapComment(mapCommentId);
	}

}
