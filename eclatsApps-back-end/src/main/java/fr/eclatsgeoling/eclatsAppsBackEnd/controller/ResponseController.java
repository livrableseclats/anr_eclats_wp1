package fr.eclatsgeoling.eclatsAppsBackEnd.controller;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedHashMap;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import fr.eclatsgeoling.eclatsAppsBackEnd.classes.Response;
import fr.eclatsgeoling.eclatsAppsBackEnd.dao.InterpretationDAO;
import fr.eclatsgeoling.eclatsAppsBackEnd.dao.ResponseDAO;

@CrossOrigin(origins = "http://localhost:4200")
@RestController
public class ResponseController {
	
	// -----------------------------
	// Requêtes génériques
	// ------------------------------
	
	/**
	 * Fonction qui permet de récupérer la liste des réponses associées à une entrée
	 * d'inventaire. Cette fonction est appelée notamment pour la consutation
	 * d'interprétations. 
	 * 
	 * @param mapId l'id de la carte courante
	 * @return la liste des réponses associées à une entrée d'inventaire
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/findAllResponses")

	public ArrayList<Response> findAllResponses(String inventoryEntry) {
		return ResponseDAO.findAllResponses(inventoryEntry);
	}

	/**
	 * Fontion qui permet de récupérer la liste des réponses contenues dans une
	 * carte Elle est appelée notamment lors de la diffusion d'une interprétation aux réponses
	 * d'une carte pour identifier celles dont la forme phonétique en alphabet Rousselot est identique.
	 * 
	 * @param mapId l'id de la carte courante
	 * @return la liste des réponses contenues dans une carte
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/findAllResponsesInMap")

	public ArrayList<Response> findAllResponsesInMap(String mapId) {
		return ResponseDAO.findAllResponsesInMap(mapId);
	}
	
	/**
	 * Fonction qui permet de trouver toutes les réposnes et interprétations
	 * associées à une entrée d'inventaire Elle est appelée au moment où
	 * l'utilisateur souhaite consulter les annotations d'un point d'enquête
	 * 
	 * @param inventoryEntry l'entrée d'inventaire cliquée dans le visualiseur
	 * @return une liste contenant la ou les réponses associées à l'entrée
	 *         d'inventaire et les interprétations qui lui sont associées
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/findAllInterpretations")

	public ArrayList<Response> findAllInterpretations(String inventoryEntry) {
		return ResponseDAO.findAllInterpretations(inventoryEntry);
	}
	
	/**
	 * Requête qui permet d'ajouter une interprétation à une entrée d'inventaire et à la diffuser à toutes les
	 * entrées d'inventaires dont une réponse associée contient la même forme phonétique en alphabet Rousselot
	 * @param responsesForm le formulaire renseigné par l'utilisateur dans la partie client de l'application
	 */
	@RequestMapping(method = RequestMethod.POST, value = "/addInterpretation")
	public void addInterpretation(@RequestBody LinkedHashMap<String, Object> creationForm) {
		ResponseDAO.addInterpretation(creationForm);
	}

	/**
	 * Requête qui permet de suppromer une interprétation assopciée à une réponse dans une carte 
	 * @param interpretationId l'id de l'interprétation à supprimer
	 */
	@RequestMapping(method = RequestMethod.DELETE, value = "/deleteInterpretation")
	public void deleteInterpretation(String interpretationId) {
		InterpretationDAO.deleteInterpretation(interpretationId);
	}

	/**
	 * Requête qui permet d'éditer une interprétation
	 * @param interpretationForm le formulaire permettant d'éditer une interprétation
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/editInterpretation")
	public void editInterpretation() {}
	
	/**
	 * Requête qui permet de trouver les formes phonétiques proches de celle renseignée en paramètre de la fonction
	 * @param rousselotPhoneticForm_mapId_levenshteinIndice la forme phonétique à comparer, l'id de la carte et
	 * l'indice de levenshtein renseigné par l'utilisateur
	 * @return Une liste de formes phonétiques qui sont phonétiquement proches
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/findLevenshteinCorrespondance")

	public HashMap<String, Integer[]>  findLevenshteinCorrespondance(String rousselotPhoneticForm, String mapId, String levenshteinDistance, String username) {
		return ResponseDAO.findLevenshteinCorrespondance(rousselotPhoneticForm, mapId, levenshteinDistance, username);
	}
	
	/**
	 * Fonction qui permet de récupérer le nombre de réponses phonétiques identiques à une réponse phonétique
	 * données dans une carte
	 * @param mapId l'id de la carte courante
	 * @param rousselotPhoneticForm la réponse phonétique à trouver
	 * @return le nombre de réponses phonétiques identiques
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/findCountOfIdenticalResponses")

	public int findCountOfIdenticalResponses(String mapId, String rousselotPhoneticForm) {
		return ResponseDAO.findCountOfIdenticalResponses(mapId, rousselotPhoneticForm);
	}
	
	/**
	 * Fonction qui permet de récupérer une liste de réponses phonétiques associées à une interprétation
	 */
	@RequestMapping(method = RequestMethod.GET, value = "/findAssociatedResponses")

	public ArrayList<Response> findAssociatedResponses(String interpretationId, String rousselotPhoneticForm) {
		return ResponseDAO.findAssociatedResponses(interpretationId, rousselotPhoneticForm);
	}
	
//	/**
//	 * CODE MORT
//	 */
//	@RequestMapping(method = RequestMethod.POST, value = "/addPhoneticResponse")
//	public void addPhoneticResponse(@RequestBody LinkedHashMap<String, Object> responseForm) {
//		ResponseDAO.addPhoneticResponse(responseForm);
//	}

	@RequestMapping(method = RequestMethod.GET, value = "/addPhoneticResponse")
	public void addPhoneticResponse() {}
	
	
	
}
