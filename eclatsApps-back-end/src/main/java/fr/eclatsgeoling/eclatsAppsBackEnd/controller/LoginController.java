package fr.eclatsgeoling.eclatsAppsBackEnd.controller;

import java.security.Principal;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

@CrossOrigin(origins = "http://localhost:4200")
@RestController

public class LoginController {
	@RequestMapping(method = RequestMethod.GET, value="/login")
	public Principal user(Principal user) {
		return user;
	}

}
