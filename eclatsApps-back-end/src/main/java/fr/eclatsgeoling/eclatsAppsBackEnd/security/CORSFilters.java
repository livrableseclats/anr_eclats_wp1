package fr.eclatsgeoling.eclatsAppsBackEnd.security;

import java.io.IOException;

import javax.servlet.Filter;
import javax.servlet.FilterChain;
import javax.servlet.FilterConfig;
import javax.servlet.ServletException;
import javax.servlet.ServletRequest;
import javax.servlet.ServletResponse;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import org.apache.catalina.connector.Connector;
import org.springframework.boot.web.embedded.tomcat.TomcatConnectorCustomizer;
import org.springframework.boot.web.embedded.tomcat.TomcatServletWebServerFactory;
import org.springframework.boot.web.servlet.server.ConfigurableServletWebServerFactory;
import org.springframework.context.annotation.Bean;
import org.springframework.core.Ordered;
import org.springframework.core.annotation.Order;
import org.springframework.stereotype.Component;

@Component
@Order(Ordered.HIGHEST_PRECEDENCE)
public class CORSFilters implements Filter {

    @Override
    public void init(FilterConfig fc) throws ServletException {
    }

    @Bean
    public ConfigurableServletWebServerFactory webServerFactory() {
        TomcatServletWebServerFactory factory = new TomcatServletWebServerFactory();
        factory.addConnectorCustomizers(new TomcatConnectorCustomizer() {
            @Override
            public void customize(Connector connector) {
                connector.setProperty("relaxedQueryChars", "|{}[]");
            }
        });
    return factory;
    }
    
    @Override
    public void doFilter(ServletRequest req, ServletResponse res, FilterChain filterChain)
            throws ServletException, IOException {

        HttpServletRequest request = (HttpServletRequest) req;
        HttpServletResponse response = (HttpServletResponse) res;
        
        response.setHeader("Access-Control-Allow-Origin", "*");   // la ressource ne peut être demandée que par une ressource issue de localhost:4200
        response.setHeader("Access-Control-Allow-Credentials", "true");               // lorsque utilisé pour la réponse à la requête préliminaire (preflight request)
                                                                                      // indique que la requête principale peut être effecuée avec des informations d'authentification
        response.setHeader("Access-Control-Allow-Methods", "GET, POST, OPTIONS, DELETE");  // indique la où les methodes autorisées pour accéder à la ressource
        response.setHeader("Access-Control-Allow-Headers", "X-Requested-With, application/json, text/plain, auth, "
        		+ "Authorization, Accept, Origin, authorization, withCredentials, X-XSRF-TOKEN, Content-Type"); // indique 
                                                                                      // les en-têtes autorisés pour accès à la ressource lorsque la requête principale est envoyée
        response.setHeader("Access-Control-Max-Age", "3600");                         // durée pendant lequel le résultat de la requête préliminaire peut être conservé en cache

        if ("OPTIONS".equalsIgnoreCase(request.getMethod())) {
            response.setStatus(HttpServletResponse.SC_OK);
        } else {
            filterChain.doFilter(request, response);
        }

    }

    @Override
    public void destroy() {
    }

}
