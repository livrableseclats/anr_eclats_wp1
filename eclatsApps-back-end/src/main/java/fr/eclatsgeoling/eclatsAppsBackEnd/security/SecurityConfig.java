package fr.eclatsgeoling.eclatsAppsBackEnd.security;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

@EnableWebSecurity
@Configuration

public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        http
                .csrf().disable()
                .authorizeRequests()
                // Requêtes autorisée sans identification
                .antMatchers(
                        "/findAllMaps",
                        "/findAllSurveyPoints",
                        "/findMapsWithGramCat",
                        "/findMapsWithGramCatWithOperator",
                        "/findProps",
                        "/findMapsWithThematicCat",
                        "/login",
                        "/findAllResponses",
                        "/findAllResponsesInMap",
                        "/findAllInterpretations",
                        "/findAllUserComment",
                        "/exportAllInterpretationsInMap",
                        "/findAllInventoryEntriesForD3",
                        "/findAllInventoryEntries",
                        "/findInventoryEntry",
                        "/findAllMapComment",
                        "/findSurveyPoint",
                        "/findInterpretationsHistoric",
                        "/findRelatedInventoryEntries",
                        "/findRelatedResponses",
                        "/findAllAttributes",
                        "/getSurveyPointsCoords",
                        "/findCountOfIdenticalResponses",
                        "/findLevenshteinCorrespondance",
                        "/findAssociatedResponses",
                        "/getMapStatus",
                        "/exportMapSearchResult",
                        "/exportAllPhoneticTranscriptionInMap",
                        "/findEuropeanaResourcesForMap")
                .permitAll()
                // Les autres nécessitent une authentification
                .anyRequest().authenticated()
                .and()
                .httpBasic();
    }

    @Autowired
    public void configureGlobal(AuthenticationManagerBuilder auth)
            throws Exception {
        auth.inMemoryAuthentication()
                .withUser("admin").password("{noop}admin38.").roles("USER")
                .and()
                .withUser("chauvinc").password("{noop}V9CmkA56f").roles("USER")
                .and()
                .withUser("sauzetm").password("{noop}kE6Qz9pPa").roles("USER")
                .and()
                .withUser("bruntrigaudg").password("{noop}NptG28wU3").roles("USER")
                .and()
                .withUser("delgiudicep").password("{noop}3EszzQS63").roles("USER")
                .and()
                .withUser("carpitellie").password("{noop}6NiF3t4Sf").roles("USER")
                .and()
                .withUser("chagnaudc").password("{noop}ZS4zZw69p").roles("USER")
                .and()
                .withUser("davoinepa").password("{noop}gEMS423ee").roles("USER")
                .and()
                .withUser("villanovam").password("{noop}iu6R39MBf").roles("USER")
                .and()
                .withUser("deparisA").password("{noop}8rDQ2w6fU").roles("USER")
                .and()
                .withUser("test1").password("{noop}5Q7wveZ9F").roles("USER")
                .and()
                .withUser("test2").password("{noop}5Dd59NnhK").roles("USER");
    }
}
