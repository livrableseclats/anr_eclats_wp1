package fr.eclatsgeoling.eclatsAppsBackEnd.classes;

public class Etymon {

	/**
	 * Racine du lemme selon le FEW. Par exemple pour pertuis : Etymon : PERTUSIARE
	 * qui signifie faire un trou en latin
	 *
	 */
	private final String fewEtymon;

	/**
	 * Origine linguistique de l'éthymon du composant. Par exemple pour pertuis :
	 * Ethymon : PERTUSIARE qui sinifie faire un trou en latin
	 *
	 */
	private final String fewEtymonLanguage;

	/**
	 * Signification de l'étymon.
	 * 
	 * Par exemple pour pertuis : Ethymon : PERTUSIARE qui sinifie faire un trou en
	 * latin
	 *
	 */
	private final String fewEtymonSignified;

	/**
	 * Référence FEW de l'étymon.
	 */
	private final String fewEtymonReference;

	/**
	 * Commentaires associés à l'étymon
	 */
	private final String etymonComment;

	/**
	 * Niveau d'incertitude de l'interprétation
	 */
	private final String uncertainty;
	

	public Etymon(String fewEtymon, String fewEtymonLanguage, String fewEtymonSignified, String fewEtymonReference,
			String etymonComment, String uncertainty) {
		this.fewEtymon = fewEtymon;
		this.fewEtymonLanguage = fewEtymonLanguage;
		this.fewEtymonSignified = fewEtymonSignified;
		this.fewEtymonReference = fewEtymonReference;
		this.etymonComment = etymonComment;
		this.uncertainty = uncertainty;
	}

	public String getFewEtymon() {
		return fewEtymon;
	}

	public String getFewEtymonLanguage() {
		return fewEtymonLanguage;
	}

	public String getFewEtymonSignified() {
		return fewEtymonSignified;
	}

	public String getFewEtymonReference() {
		return fewEtymonReference;
	}

	public String getEtymonComment() {
		return etymonComment;
	}

	public String getUncertainty() {
		return uncertainty;
	}
	
}
