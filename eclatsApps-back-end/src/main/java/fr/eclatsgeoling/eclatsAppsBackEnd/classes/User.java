package fr.eclatsgeoling.eclatsAppsBackEnd.classes;

public class User {
	
	/**
	 * Le nom d'utilisateur, constitué du nom de famille et de la
	 * première lettre du prénom de l'utilisateur
	 */
	private final String username;
	
	/**
	 * L'organisme de rattachement de l'utilisateur (gipsaLab, LIG...)
	 */
	private final String organization;
	
	public User(String username, String organization) {
		this.username = username;
		this.organization = organization;
	}	

	public String getUsername() {
		return username;
	}

	public String getOrganization() {
		return organization.substring(35);
	}
}
