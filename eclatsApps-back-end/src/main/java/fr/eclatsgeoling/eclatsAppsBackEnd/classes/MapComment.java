package fr.eclatsgeoling.eclatsAppsBackEnd.classes;

public class MapComment {

	/**
	 * L'id du commentaire de carte
	 */
	String mapCommentId;

	/**
	 * L'utilisateur qui a publié le commentaire de carte
	 */
	User user;

	/**
	 * le contenu du commentaire
	 */
	String mapComment;

	/**
	 * La date à laquelle le commentaire a été publié
	 */
	long date;

	public MapComment(String mapCommentId, long date, User user, String mapComment) {
		this.mapCommentId = mapCommentId;
		this.user = user;
		this.mapComment = mapComment;
		this.date = date;
	}

	public String getMapCommentId() {
		return mapCommentId;
	}

	public User getUser() {
		return user;
	}

	public String getMapComment() {
		return mapComment;
	}

	public long getDate() {
		return date;
	}

}
