package fr.eclatsgeoling.eclatsAppsBackEnd.classes;

import java.util.ArrayList;
import java.util.List;

public class SurveyPoint {

    /**
     * L'identifiant du point d'enquête. Exemple: 621 pour le point d'enquête
     * 621 correspondant à la commune d'Yviers
     */
    private final int spIdentifier;

    /**
     * Le nom de l'entité géographique associée au point d'enquête. Peut être
     * une commune, un lieu-dit, un village Exemple: pour le point 621 ->
     * commune d'Yviers
     */
    private final String placeName;

    /**
     * Le lien DBPedia correspondant au point d'enquête.
     *
     * Exemple: Pour le point 621 à Yviers :
     * http://fr.dbpedia.org/resource/Yviers
     *
     */
    private final String dbPediaLink;

    /**
     * Le lien DBPedia correspondant au point d'enquête.
     *
     * Exemple: Pour le point 988 à Evolène :
     * http://data.bnf.fr/ark:/12148/cb15288646r
     *
     */
    private final String bnfLink;
    /**
     * Le lien Gallica vers le carnet de note correspondant au point d'enquête.
     *
     * Exemple: Pour le point 798 à Collioure :
     * http://data.bnf.fr/ark:/12148/cb15270964c
     *
     */
    private final String gallicaNoteBookLink;
    /**
     * Informations relatives au pays
     */
    private final String country;

    /**
     * Informations relatives au pays
     */
    private final String latitude;

    /**
     * Informations relatives au pays
     */
    private final String longitude;
    /**
     * La date de l'enquête
     */
    private final String dateEnquete;
    /**
     * Le nombre de témoins
     */
    private final int nbTemoins;
    /**
     * Le sexe du témoin principal
     */
    private final String sexeTemoin1;
    /**
     * L'âge du témoin principal
     */
    private final String ageTemoin1;
    /**
     * Les informations complémentaires sur les témoins
     */
    private final List<String> infosTemoins;

    public SurveyPoint(int spIdentifier, String placeName, String dbPediaLink, String bnfLink, String gallicaNoteBookLink, String country, String latitude,
            String longitude, String dateEnquete, int nbTemoins, String sexeTemoin1,
            String ageTemoin1) {
        this.spIdentifier = spIdentifier;
        this.placeName = placeName;
        this.dbPediaLink = dbPediaLink;
        this.bnfLink = bnfLink;
        this.gallicaNoteBookLink = gallicaNoteBookLink;
        this.country = country;
        this.latitude = latitude;
        this.longitude = longitude;
        this.dateEnquete = dateEnquete;
        this.nbTemoins = nbTemoins;
        this.sexeTemoin1 = sexeTemoin1;
        this.ageTemoin1 = ageTemoin1;
        this.infosTemoins = new ArrayList();
    }

    public int getSpIdentifier() {
        return this.spIdentifier;
    }

    public String getPlaceName() {
        return placeName;
    }

    public String getDbPediaLink() {
        return dbPediaLink;
    }

    public String getBnfLink() {
        return bnfLink;
    }
    
    public String getGallicaNoteBookLink() {
        return gallicaNoteBookLink;
    }
    
    public String getCountry() {
        return country;
    }

    public String getLatitude() {
        return latitude;
    }

    public String getLongitude() {
        return longitude;
    }

    /**
     * @return the dateEnquete
     */
    public String getDateEnquete() {
        return dateEnquete;
    }

    /**
     * @return the nbTemoins
     */
    public int getNbTemoins() {
        return nbTemoins;
    }

    /**
     * @return the sexeTemoin1
     */
    public String getSexeTemoin1() {
        return sexeTemoin1;
    }

    /**
     * @return the ageTemoin1
     */
    public String getAgeTemoin1() {
        return ageTemoin1;
    }

    /**
     * @return the infosTemoins
     */
    public List<String> getInfosTemoins() {
        return infosTemoins;
    }
    
    
    public void addInfosTemoins(String info){
        this.infosTemoins.add(info);
    }
}
