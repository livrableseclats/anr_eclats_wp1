package fr.eclatsgeoling.eclatsAppsBackEnd.classes;

import java.util.ArrayList;
import java.util.List;

public class Response {
	
	public final String responseId;
	
	public String rousselotPhoneticForm;
	
	public String complementaryInformation;

	/**
	 * L'identifiant du point d'enquête associé à une entrée d'inventaire comportant une réponse
	 * EXEMPLE: Réponse 'Mille-poires-cuites' de la carte MillePertuis (ALF n°859) sur l'entrée
	 * d'inventaire inventoryEntry_SP407_map0859 sur le PE n°407.
	 * Permet de retrouver rapidement le point d'enquête correspondant.
	 */
	public String spIdentifier;
	
	public final List<Interpretation> interpretationList;


	public Response(String responseId, String rousselotPhoneticForm, String complementaryInformation, String spIdentifier) {
		this.responseId = responseId;
		this.rousselotPhoneticForm = rousselotPhoneticForm;
		this.complementaryInformation = complementaryInformation;
		this.spIdentifier = spIdentifier;
		this.interpretationList = new ArrayList<>();
	}
	
    public String getResponseId() {
		return responseId.substring(35);
	}


	public String getRousselotPhoneticForm() {
		return rousselotPhoneticForm;
	}
	
	public String getComplementaryInformation() {
		return complementaryInformation;
	}
	
	public String getSpIdentifier(){
		return spIdentifier;
	}

	public List<Interpretation> getInterpretationList() {
		return interpretationList;
	}

	/**
     * @param interpretration <Interpretation>
     * Une interprétation à ajouter à la liste des interprétations qui correspondent à une
     * réponse
     */
    public void addInterpretation(Interpretation interpretration) {
        this.interpretationList.add(interpretration);
    }
    
    /**
     * Calcul du cout minimal pour transformer la première chaîne en la seconde chaine en effectuant les opérations élémentaires suivantes:
     * substitution d'un caractère de lhs par un caractère différent de rhs ;
	 * insertion (ou ajout) dans lhs d'un caractère de rhs ;
	 * suppression (ou effacement) d'un caractère de lhs.
     * @param lhs la première chaine
     * @param rhs la seconde chaine
     * @return le cout correspondant à la distance entre les deux chaines
     */
    public static int levenshteinDistance (CharSequence lhs, CharSequence rhs) {                          
    	int len0 = lhs.length() + 1;                                                     
        int len1 = rhs.length() + 1;                                                     
                                                                                        
        // the array of distances                                                       
        int[] cost = new int[len0];                                                     
        int[] newcost = new int[len0];                                                  
                                                                                        
        // initial cost of skipping prefix in String s0                                 
        for (int i = 0; i < len0; i++) cost[i] = i;                                     
                                                                                        
        // dynamically computing the array of distances                                  
                                                                                        
        // transformation cost for each letter in s1                                    
        for (int j = 1; j < len1; j++) {                                                
            // initial cost of skipping prefix in String s1                             
            newcost[0] = j;                                                             
                                                                                        
            // transformation cost for each letter in s0                                
            for(int i = 1; i < len0; i++) {                                             
                // matching current letters in both strings                             
                int match = (lhs.charAt(i - 1) == rhs.charAt(j - 1)) ? 0 : 1;             
                                                                                        
                // computing cost for each transformation                               
                int cost_replace = cost[i - 1] + match;                                 
                int cost_insert  = cost[i] + 1;                                         
                int cost_delete  = newcost[i - 1] + 1;                                  
                                                                                        
                // keep minimum cost                                                    
                newcost[i] = Math.min(Math.min(cost_insert, cost_delete), cost_replace);
            }                                                                           
                                                                                        
            // swap cost/newcost arrays                                                 
            int[] swap = cost; cost = newcost; newcost = swap;                          
        }                                                                               
                                                                                        
        // the distance is the cost for transforming all letters in both strings        
        return cost[len0 - 1];                                                          
    }


}
