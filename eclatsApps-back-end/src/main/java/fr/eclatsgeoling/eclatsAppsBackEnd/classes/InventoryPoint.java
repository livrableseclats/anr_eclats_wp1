package fr.eclatsgeoling.eclatsAppsBackEnd.classes;

/**
 * Représente un Point d'inventaire, c'est à dire le point donnant la position
 * d'un point d'enquête dans l'image d'une carte.
 *
 * @author Philippe Genoud - LIG Steamer - Université Grenoble Aples
 */
public class InventoryPoint {

    /**
     * abscisse viewport de l'entrée d'inventaire
     */
    private double xViewport;

    /**
     * ordonnée viewport de l'entrée d'inventaire
     */
    private double yViewport;

    /**
     * le numéro du point d'enquête associé au point d'inventaire
     */
    private int id;

    /**
     * est-ce utile d'avoir le label ? NON à mon avis (PhG)
     */
    private String label;

    /**
     * Création d'une entrée d'inventaire
     *
     * @param noLigne numéro de ligne où se trouve la définition de l'entrée
     * d'inventaire dans le fichier de données servant à créer l'objet.
     * @param x abscisse viewport de l'entrée d'inventaire.
     * @param y ordonnée viewport de l'entrée d'inventaire.
     * @param id numéro du point d'enquête associé à cette entrée d'inventaire
     */
    public InventoryPoint(int id, double x, double y) {
        this.xViewport = x;
        this.yViewport = y;
        this.id = id;
        label = "";
    }

    public InventoryPoint(int id, double x, double y, String label) {
        this.xViewport = x;
        this.yViewport = y;
        this.id = id;
        this.label = label;
    }
    
    public InventoryPoint() {
        
    }

    public String getLabel() {
        return label;
    }

    public void setLabel(String label) {
        this.label = label;
    }
    

    public double getxViewport() {
        return xViewport;
    }

    public double getyViewport() {
        return yViewport;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    @Override
    public String toString() {
        return "InventoryPoint{id=" + id + ", x=" + xViewport + "  y=" + yViewport + "}";
    }

    public void setxViewport(double xViewport) {
        this.xViewport = xViewport;
    }

    public void setyViewport(double yViewport) {
        this.yViewport = yViewport;
    }

}
