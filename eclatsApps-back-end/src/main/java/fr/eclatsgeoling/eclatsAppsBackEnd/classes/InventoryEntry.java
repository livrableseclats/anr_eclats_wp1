package fr.eclatsgeoling.eclatsAppsBackEnd.classes;

public class InventoryEntry {

	/**
	 * URI du PE et de la carte dans laquelle il est situé. Exemple pour le PE n°33
	 * dans l'image n°0859:
	 *
	 * http://purl.org/fr/eclat/resource/invEntry_SP33_map0859
	 *
	 */
	private final String inventoryEntry;

	/**
	 * Coordonnées X du PE dans une carte dans le viewer OSD. Exemple pour le PE
	 * n°33 dans l'image n°0859:
	 *
	 * x = "0.78"
	 *
	 */
	private final double xViewport;

	/**
	 * Coordonnées Y du PE dans une carte dans le viewer OSD. Exemple pour le PE
	 * n°33 dans l'image n°0859:
	 *
	 * y = "0.57"
	 *
	 */
	private final double yViewport;

	/**
	 * l'identifiant de l'image.
	 *
	 * Par exemple
	 *
	 * - pour la carte n° 14 l'identifiant est "0014"
	 *
	 * - pour la carte n° 10, il y a deux images avec respectivement l'identifiant
	 * "0010A" pour l'image correspondant à la partie gauche de la carte , "0010B"
	 * pour l'image correspondant à la partie droite de la carte.
	 *
	 */
	private final String mapIdentifier;

	/**
	 * numéro du point d'enquête
	 */
	public final int spIdentifier;

	private boolean hasResponse;
	
	private String rousselotPhoneticForm;
	
	public String complementaryInformation;

	private boolean hasInterpretation;
	
	private String author;
	
	private boolean isMultipleAuthors;
	
	private String color;

	public InventoryEntry(String inventoryEntry, double xViewport, double yViewport, boolean hasResponse, String rousselotPhoneticForm, String complementaryInformation,
			boolean hasInterpretation, String author, boolean isMultipleAuthors, String color) {
		this.inventoryEntry = inventoryEntry;
		this.xViewport = xViewport;
		this.yViewport = yViewport;
		this.mapIdentifier = getMapIdentifier();
		this.spIdentifier = getSpIdentifier();
		this.hasResponse = hasResponse;
		this.rousselotPhoneticForm = rousselotPhoneticForm;
		this.complementaryInformation = complementaryInformation;
		this.hasInterpretation = hasInterpretation;
		this.author = author;
		this.isMultipleAuthors = isMultipleAuthors;
		this.color = color;
	}

	public void setIsMultipleAuthors(boolean isMultipleAuthors) {
		this.isMultipleAuthors = isMultipleAuthors;
	}

	public boolean getHasResponse() {
		return hasResponse;
	}
	
	public String getRousselotPhoneticForm() {
		return rousselotPhoneticForm;
	}

	public String getComplementaryInformation() {
		return complementaryInformation;
	}
	
	public boolean hasInterpretation() {
		return hasInterpretation;
	}

	public void setHasInterpretation(boolean hasInterpretation) {
		this.hasInterpretation = hasInterpretation;
	}

	public void setAuthor(String author) {
		this.author = author;
	}

	public String getAuthor() {
		return author;
	}

	public boolean getIsMultipleAuthor() {
		return isMultipleAuthors;
	}

	public String getInventoryEntry() {
		return inventoryEntry;
	}

	public double getxViewport() {
		return xViewport;
	}

	public double getyViewport() {
		return yViewport;
	}

	public String getMapIdentifier() {
		return this.inventoryEntry.substring(inventoryEntry.length() - 4);
	}

	public int getSpIdentifier() {
		return Integer.parseInt(this.inventoryEntry.substring(52, inventoryEntry.length() - 8));
	}
	
	public void setColor(String color) {
		this.color = color;
	}

	public String getColor() {
		return color;
	}

}
