/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.eclatsgeoling.eclatsAppsBackEnd.classes;

/**
 *
 * @author seffarm
 */
public class EuropeanaResource {
    
    /**
     * Le lien ark vers la ressource Europeana en ligne
     */
    private final String ark;
    
    /**
     * l'identifiant de l'image. par exemple
     *
     * - pour la carte n° 14 l'identifiant est "0014"
     *
     * - pour la carte n° 10, il y a deux images avec respectivement
     * l'identifiant "0010A" pour l'image correspondant à la partie gauche de la
     * carte , "0010B" pour l'image correspondant à la partie droite de la
     * carte.
     *
     */
    private final String mapIdentifier;

    /**
     * L'utilisateur qui a enregistré la ressource à la carte
     */
    private final String username;
   
    /**
     * Le titre de la ressource Europeana en ligne
     */
    private final String title;
    /**
     * La description de la ressource
     */
    private final String dcDescription;

    /**
     * Le createur de la ressource
     */
    private final String dcCreator;

    /**
     * Le fournisseur de la ressource
     */
    private final String dataProvider;

    /**
     * Le pays d'où vient la ressource
     */
    private final String country;

    /**
     * L'image (si elle existe) associée à la ressource
     */
    private final String edmPreview;

    public EuropeanaResource(String ark, String mapIdentifier, String username, String title, String dcDescription, String dcCreator,
            String dataProvider, String country, String edmPreview) {
        this.ark = ark;
        this.mapIdentifier = mapIdentifier;
        this.username = username;
        this.title = title;
        this.dcDescription = dcDescription;
        this.dcCreator = dcCreator;
        this.dataProvider = dataProvider;
        this.country = country;
        this.edmPreview = edmPreview;
    }

    /**
     * @return the ark
     */
    public String getArk() {
        return ark;
    }
    
    /**
     * @return the mapIdentifier
     */
    public String getMapIdentifier() {
        return mapIdentifier;
    }

    /**
     * @return the user
     */
    public String getUsername() {
        return username;
    }
    
        /**
     * @return the ark
     */
    public String getTitle() {
        return title;
    }

    /**
     * @return the dcDescription
     */
    public String getDcDescription() {
        return dcDescription;
    }

    /**
     * @return the dcCreator
     */
    public String getDcCreator() {
        return dcCreator;
    }

    /**
     * @return the dataProvider
     */
    public String getDataProvider() {
        return dataProvider;
    }

    /**
     * @return the country
     */
    public String getCountry() {
        return country;
    }

    /**
     * @return the edmPreview
     */
    public String getEdmPreview() {
        return edmPreview;
    }
}
