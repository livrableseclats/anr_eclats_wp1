package fr.eclatsgeoling.eclatsAppsBackEnd.classes;

import java.util.ArrayList;
import java.util.List;

public class Lemma {

//	/**
//	 * Id du lemme
//	 */
//	private final String lemmaId;
    /**
     * Nature grammaticale du lemme (adverbe, verbe, nom commun...)
     */
    private final String grammaticalNature;

    /**
     * Genre du lemme (masculin ou féminin)
     */
    private final String gender;

    /**
     * Nombre du lemme (singulier ou pluriel)
     */
    private final String number;

    /**
     * Commentaires sur le lemme
     */
    private final String lemmaComments;

    /**
     * La liste de lemme de l'interprétation
     */
    private final List<Etymon> etymonsList;

    public Lemma(String grammaticalNature, String gender, String number, String lemmaComments) {
//	public Lemma(String lemmaId, String grammaticalNature, String gender, String number, String lemmaComments) {
//		this.lemmaId = lemmaId;
        this.grammaticalNature = grammaticalNature;
        this.gender = gender;
        this.number = number;
        this.lemmaComments = lemmaComments;
        this.etymonsList = new ArrayList<Etymon>();
    }

//	public String getLemmaId() {
//		return lemmaId;
//	}
    public String getGrammaticalNature() {
        return grammaticalNature;
    }

    public String getLemmaComments() {
        return lemmaComments;
    }

    public String getGender() {
        return gender;
    }

    public String getNumber() {
        return number;
    }

    public List<Etymon> getEtymonsList() {
        return etymonsList;
    }

    /**
     * @param etymon l'étymon à ajouter à la liste
     */
    public void addEtymon(Etymon etymon) {
        this.etymonsList.add(etymon);
    }

}
