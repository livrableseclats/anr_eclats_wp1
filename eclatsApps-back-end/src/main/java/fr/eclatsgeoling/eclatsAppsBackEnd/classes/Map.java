package fr.eclatsgeoling.eclatsAppsBackEnd.classes;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class Map {

    /**
     * l'identifiant de l'image. par exemple
     *
     * - pour la carte n° 14 l'identifiant est "0014"
     *
     * - pour la carte n° 10, il y a deux images avec respectivement
     * l'identifiant "0010A" pour l'image correspondant à la partie gauche de la
     * carte , "0010B" pour l'image correspondant à la partie droite de la
     * carte.
     *
     */
    private final String mapIdentifier;

    /**
     * index de l'image dans la liste des images ordonnées par numéro de carte
     */
    private final int mapIndex;

    /**
     * intitulé de la carte à laquelle est associée cette image
     */
    private final String mapTitle;

    /**
     * largeur (en pixels) de l'image
     */
    private final int width;

    /**
     * hauteur (en pixels) de l'image
     */
    private final int height;

    /**
     * titre contextualisé de la carte
     */
    private final String contextualizedTitle;

    /**
     * numéro du volume où se trouve la carte
     */
    private final String booklet;

    /**
     * url de l'image tiff de la carte
     */
    private final String mapTiffFormat;

    /**
     * url vers le descripteur XML des images tuilées (Deppe Zoom composer) de la carte
     */
    private final String imageLink;

    /**
     * La liste des commentaires utilisateurs associés à la carte
     */
    private final List<MapComment> userCommentList;
    
    /**
     * La liste des liens ARK associées aux TitleElements de la carte
     */
    private final HashMap<String, String> bnfLinks;

    /**
     * @param mapIdentifier l'identifiant (le numéro) de la carte
     * @param mapIndex index de l'image dans la liste des images ordonnées par numéro de carte
     * @param mapTitle titre de la carte
     * @param mapTiffFormat url de l'image tiff contenant la carte
     * @param imageLink url vers le descripteur XML des images tuilées (Deppe Zoom composer) de l'image de la carte
     * @param width largeur (en pixels) de l'image contenant la carte
     * @param height hauteur (en pixels) de l'image contenant la carte
     * @param contextualizedTitle titre contextualisé de la carte
     * @param booklet
     */
    public Map(String mapIdentifier, int mapIndex, String mapTitle, String mapTiffFormat, String imageLink, int width, int height, String contextualizedTitle,
            String booklet) {
        this.mapIdentifier = mapIdentifier;
        this.mapIndex = mapIndex;
        this.mapTitle = mapTitle;
        this.mapTiffFormat = mapTiffFormat;
        this.imageLink = imageLink;
        this.width = width;
        this.height = height;
        this.contextualizedTitle = contextualizedTitle;
        this.booklet = booklet;
        this.userCommentList = new ArrayList<>();
        this.bnfLinks = new HashMap<>();
    }

    public int getMapIndex() {
        return mapIndex;
    }

    public String getMapIdentifier() {
        return this.mapIdentifier;
    }

    public String getMapTitle() {
        return this.mapTitle;
    }

    /**
     * renvoie le nom du fichier image TIFF correspondant à l'image
     *
     * @return "CarteALF0014.tif" pour l'image d'identifiant "0014"
     */
    public String getMapTiffFormat() {
        // Mise en forme de la variable à partir de imageId (ajout d'une majuscule et du
        // .tif)
        return this.mapTiffFormat;
    }

    /**
     * retourne l'identifiant interne de la carte, c'est à dire le numéro sur 4
     * chiffres avec des 0 en tête (padding). Par exemple :
     *
     * carte n° 14 --> 0014 
     * 
     * carte n° 10A --> 0010A 
     * 
     * carte n° 451A --> 0451A 
     * 
     * carte n° 1370B --> 1370B
     * 
     * carte n° 1514AB --> 1514AB
     *
     * @return
     */
    public String getMapId() {
        if (this.mapIdentifier.charAt(0) == 'I') {
            switch (this.mapIdentifier) {
                case "I":
                    return "000I";
                case "II":
                    return "00II";
                case "III":
                    return "0III";
            }
        }
        // effectue le padding des 0 devant l'identifiant de la carte pour que le
        // numméro de la carte soit sur 4 digits
        int offset = 0;
        if (this.mapIdentifier.endsWith("AB")) {
            offset = 2;
        } else if (this.mapIdentifier.endsWith("A") || this.mapIdentifier.endsWith("B")) {
            offset = 1;
        }
        return ("0000" + this.mapIdentifier).substring(this.mapIdentifier.length() - offset);
    }

    /**
     * @return largeur de l'image
     */
    public int getWidth() {
        return this.width;
    }

    /**
     * @return hauteur de l'image
     */
    public int getHeight() {
        return this.height;
    }

    public String getBooklet() {
        return booklet;
    }

    public String getContextualizedTitle() {
        return this.contextualizedTitle;
    }

    /**
     * @return le nom (sans suffixe) du fichier image
     */
    public String getName() {
        return "CarteALF" + getMapId();
    }

    public String getImageLink() {
        return this.imageLink;
    }
    
    public HashMap<String, String> getBnfLinks(){
		return this.bnfLinks;    	
    }

    public void addUserComment(MapComment userComment) {
        this.userCommentList.add(userComment);
    }
    
    public void addBNFLink(String word, String ark) {
    	this.bnfLinks.put(word, ark);    	
    }

}
