package fr.eclatsgeoling.eclatsAppsBackEnd.classes;

import java.util.ArrayList;
import java.util.List;

public class Interpretation {

	/**
	 * Id de l'interprétation.
	 */
	private final String interpretationId;

	/**
	 * Réponse lémmatisée, forme graphique de référence, données en alphabet latin,
	 * des réponses dialectales.
	 */
	private final String lemmatizedResponse;

	/**
	 * Catégorie de motif spécifique. Exemple: aspect, couleur...
	 */
	private final String ground;

	/**
	 * Motif spécifique: Trait saillant ou propriété remarquable, partagés par un
	 * ensemble de référents et à laquelle renvoie explicitement la dénomination de
	 * la classe de ces référents au moment de la création lexicale. Exemple: forme
	 * de la feuille
	 */
	private final String specificGround;

	/**
	 * Désignant: façon dont une communauté linguistique rtaduit le motif en mots
	 */
	private final String designating;

	/**
	 * La liste de lemme de l'interprétation
	 */
	private final List<Lemma> lemmasList;

	/**
	 * Type de la réponse (phrase, lemme, syntagme)
	 */
	private final String responseType;

	/**
	 * Commentaire de l'utilisateur qui a créé l'interprétation
	 */
	private final String interpretationComment;

	/**
	 * Date de création de l'interprétation
	 */
	private final long interpretationDate;

	/**
	 * Carte associée à l'interprétation
	 */
	private final String mapIdentifier;

	/**
	 * Le nom de l'utilisateur qui a créé l'interprétation
	 */
	private final User user;

	/**
	 * Une liste de commentaires associés à l'interprétation de l'utilisateur
	 */
	private final List<MapComment> userCommentList;
	
	/**
	 * Niveau de confidentialité de l'interprétation
	 */
	private String confidentiality;
	
	/**
	 * La liste des réponses phonétiques en rousselot Gilléron issue de la propagation de l'interprétation
	 */
	private List<Response> associatedResponses;

	public Interpretation(String interpretationId, String lemmatizedResponse,
			String ground, String specificGround, String designating, String reponseType, String interpretationComment, long interpretationDate,
			String mapIdentifier, User user, String confidentiality) {
		this.interpretationId = interpretationId;
		this.lemmatizedResponse = lemmatizedResponse;
		this.ground = ground;
		this.specificGround = specificGround;
		this.designating = designating;
		this.lemmasList = new ArrayList<>();
		this.responseType = reponseType;
		this.interpretationComment = interpretationComment;
		this.interpretationDate = interpretationDate;
		this.mapIdentifier = mapIdentifier;
		this.user = user;
		this.confidentiality = confidentiality;
		this.userCommentList = new ArrayList<>();
		this.associatedResponses = new ArrayList<>();
	}

	public String getDesignating() {
		return designating;
	}

	public List<MapComment> getUserCommentList() {
		return userCommentList;
	}

	public long getInterpretationDate() {
		return interpretationDate;
	}

	public String getMapIdentifier() {
		return mapIdentifier;
	}

	/**
	 * @param lemma <Lemma> Un lemme à ajouter à la liste des lemmes qui corresponde
	 * à une réponse
	 */
	public void addLemma(Lemma lemma) {
		this.lemmasList.add(lemma);
	}

	public void addUserComment(MapComment userComment) {
		this.userCommentList.add(userComment);
	}
	
	public void addAssociatedResponses(ArrayList<Response> associatedResponsesList) {
		this.associatedResponses = associatedResponsesList;
	}
	
	public List<Response> getAssociatedResponses(){
		return associatedResponses;
	}

	public String getInterpretationId() {
		return interpretationId;
	}

	public String getLemmatizedResponse() {
		return lemmatizedResponse;
	}

	public String getGround() {
		return ground;
	}

	public String getSpecificGround() {
		return specificGround;
	}

	public List<Lemma> getLemmasList() {
		return lemmasList;
	}

	public String getResponseType() {
		return responseType;
	}

	public String getInterpretationComment() {
		return interpretationComment;
	}

	public User getUser() {
		return user;
	}
	
	public String getConfidentiality() {
		return confidentiality;
	}

    public void setConfidentiality(String confidentiality) {
        this.confidentiality = confidentiality;
    }

        
}
