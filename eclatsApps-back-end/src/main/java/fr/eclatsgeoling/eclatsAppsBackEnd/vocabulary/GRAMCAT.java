package fr.eclatsgeoling.eclatsAppsBackEnd.vocabulary;

import org.eclipse.rdf4j.model.Namespace;
import org.eclipse.rdf4j.model.impl.SimpleNamespace;

/**
 * Vocabulary constants for the ECLATS Grammatical Categories vocabulary
 *
 * @author Philippe Genoud - LIG Steamer - Université Grenoble Aples
 */
public class GRAMCAT {

    /**
     * The namespace: http://purl.org/fr/eclats/ontologies/catgram#
     */
    public static final String NAMESPACE = "http://purl.org/fr/eclats/ontologies/catgram#";

    /**
     * The recommended prefix for the SKOS namespace: "skos"
     */
    public static final String PREFIX = "gramcat";

    /**
     * An immutable {@link Namespace} constant that represents the SKOS
     * namespace.
     */
    public static final Namespace NS = new SimpleNamespace(PREFIX, NAMESPACE);

}
