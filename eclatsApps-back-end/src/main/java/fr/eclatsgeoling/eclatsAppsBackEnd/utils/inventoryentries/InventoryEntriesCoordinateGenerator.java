package fr.eclatsgeoling.eclatsAppsBackEnd.utils.inventoryentries;

import fr.eclatsgeoling.eclatsAppsBackEnd.classes.InventoryPoint;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.ArrayList;
import java.util.List;
import org.springframework.util.ResourceUtils;

/**
 * Génère par interpolation les coordonnées viewport des PE pour une carte
 * donnée à partir des données d'une carte de référence pour laquelle on a les
 * données complètes (la carte n° 0378), et des données ViewPort de 4 points sur
 * la carte à calculer (Point 297 (nord) , Point 796 (est), Point 987 (sud) et
 * Point 493 (ouest).
 *
 * @author Philippe Genoud - LIG Steamer - Université Grenoble Aples
 */
public class InventoryEntriesCoordinateGenerator {

    // ids des ponts de références
    private static final int POINT_OUEST = 493;
    private static final int POINT_EST = 987;
    private static final int POINT_NORD = 297;
    private static final int POINT_SUD = 796;

    /**
     * le fichier qui contient les points de référence surlesquels on va
     * s'aligner
     */
    private static final String REF_FILE_NAME = "Image0378Data.csv";

    private static final int IP_PREFIX_LENGTH = "http://purl.org/fr/eclats/resource/surveyPoint".length();

    /**
     * le numero de la carte que l'on veut évaluer
     */
    private final String mapNum;  // ex: "0321" carte 0321 Coquelicot

    private InventoryPoint ptOuestCible;
    private InventoryPoint ptEstCible;
    private InventoryPoint ptNordCible;
    private InventoryPoint ptSudCible;

    public InventoryEntriesCoordinateGenerator(String mapNum,
            InventoryPoint ptOuestCible, InventoryPoint ptEstCible,
            InventoryPoint ptNordCible, InventoryPoint ptSudCible) {
        this.mapNum = mapNum;
        this.ptOuestCible = ptOuestCible;
        this.ptEstCible = ptEstCible;
        this.ptNordCible = ptNordCible;
        this.ptSudCible = ptSudCible;
    }

    public InventoryEntriesCoordinateGenerator(String mapNum, List<InventoryPoint> referencesPoint) {
        this.mapNum = mapNum;
        for (InventoryPoint pt : referencesPoint) {
            switch (pt.getId()) {
                case POINT_OUEST:
                    this.ptOuestCible = pt;
                    break;
                case POINT_EST:
                    this.ptEstCible = pt;
                    break;
                case POINT_NORD:
                    this.ptNordCible = pt;
                    break;
                case POINT_SUD:
                    this.ptSudCible = pt;
                    break;
                default:
                    throw new IllegalArgumentException("Point de référence (n° " + pt.getId() + ") ne correspond pas à l'un des points attendus");
            }
        }
    }

    public List<InventoryPoint> generate() throws FileNotFoundException, IOException {

        // charge les Entrées d'inventaire de référence
        List<InventoryPoint> refInventoryPoints = loadReferenceInventoryPoints(REF_FILE_NAME);

        // recherche des points de référence pour alignement
        InventoryPoint ptOuestRef = find(refInventoryPoints, POINT_OUEST);
        InventoryPoint ptEstRef = find(refInventoryPoints, POINT_EST);
        InventoryPoint ptNordRef = find(refInventoryPoints, POINT_NORD);
        InventoryPoint ptSudRef = find(refInventoryPoints, POINT_SUD);

        // calcul des fonctions d'interpolation pour la carte
        LinearPointInterpolator nordOuestInterpolator = new LinearPointInterpolator(ptOuestRef, ptOuestCible, ptNordRef, ptNordCible);
        LinearPointInterpolator nordEstInterpolator = new LinearPointInterpolator(ptNordRef, ptNordCible, ptEstRef, ptEstCible);
        LinearPointInterpolator sudEstInterpolator = new LinearPointInterpolator(ptSudRef, ptSudCible, ptEstRef, ptEstCible);
        LinearPointInterpolator sudOuestInterpolator = new LinearPointInterpolator(ptOuestRef, ptOuestCible, ptSudRef, ptSudCible);
        // calcule par interpolation des coordonnées des points d'inventaire sur la carte mapNum
        List<InventoryPoint> newInventoryPoints = new ArrayList<>();
        for (InventoryPoint pt : refInventoryPoints) {
            //  InventoryPoint newPt = new InventoryPoint(pt.getNoLigne(), xInterpolator.interpolate(pt.getX()), yInterpolator.interpolate(pt.getY()));
            InventoryPoint newPt = new InventoryPoint(pt.getId(), pt.getxViewport(), pt.getyViewport());
            LinearPointInterpolator interpolator = null;
            if (newPt.getxViewport() <= ptNordRef.getxViewport()) {
                if (newPt.getyViewport() <= ptOuestRef.getyViewport()) {
                    interpolator = nordOuestInterpolator;
                } else {
                    interpolator = sudOuestInterpolator;
                }
            } else {
                if (newPt.getyViewport() <= ptOuestRef.getyViewport()) {
                    interpolator = nordEstInterpolator;
                } else {
                    interpolator = sudEstInterpolator;
                }
            }
            interpolator.interpolate(newPt);
            newInventoryPoints.add(newPt);
        }
        return newInventoryPoints;

    }

    /**
     * cherche dans une liste de points d'inventaire celui correspondant à un
     * numéro donné.
     *
     * @param inventoryPoints la liste des points d'inventaires.
     * @param num le numéro de point recherché.
     * @return le point d'inventaire de numéro num ou null si il n'existe pas
     * dans la liste.
     */
    private static InventoryPoint find(List<InventoryPoint> inventoryPoints, int num) {
        for (InventoryPoint pt : inventoryPoints) {
            if (pt.getId() == num) {
                return pt;
            }
        }
        return null;
    }

    /**
     *
     * @param fileName
     * @return
     * @throws FileNotFoundException
     * @throws IOException
     */
    private List<InventoryPoint> loadReferenceInventoryPoints(String fileName) throws FileNotFoundException, IOException {
        File file = ResourceUtils.getFile("classpath:" + REF_FILE_NAME);
        try (BufferedReader br = new BufferedReader(new InputStreamReader(
                new FileInputStream(file), "UTF-8"));) {
            String line;
            List<InventoryPoint> res = new ArrayList<>();
            br.readLine(); // on saute la première ligne qui contient les noms de colonnes
            int noLigne = 1;
            while ((line = br.readLine()) != null) {
                if (line.startsWith("#")) {
                    // on la saute
                } else {
                    String[] tokens = line.split(",");
                    res.add(new InventoryPoint(
                            Integer.parseInt(tokens[1].substring(IP_PREFIX_LENGTH)),
                            Double.parseDouble(tokens[2]),
                            Double.parseDouble(tokens[3]))
                    );
                }
                noLigne++;
            }
            return res;
        }
    }
}
