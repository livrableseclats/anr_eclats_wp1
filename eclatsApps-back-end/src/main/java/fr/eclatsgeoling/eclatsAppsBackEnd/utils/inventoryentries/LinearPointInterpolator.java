package fr.eclatsgeoling.eclatsAppsBackEnd.utils.inventoryentries;

import fr.eclatsgeoling.eclatsAppsBackEnd.classes.InventoryPoint;

/**
 *
 * @author Philippe Genoud - LIG Steamer - Université Grenoble Aples
 */
public class LinearPointInterpolator {

    private LinearInterpolator xInterpolator ;
    private LinearInterpolator yInterpolator ;
    
    public LinearPointInterpolator(InventoryPoint p1ref, InventoryPoint p1Cible, InventoryPoint p2ref, InventoryPoint p2Cible ) {
        this.xInterpolator = new LinearInterpolator(p1ref.getxViewport(), p1Cible.getxViewport(), p2ref.getxViewport(), p2Cible.getxViewport());
        this.yInterpolator = new LinearInterpolator(p1ref.getyViewport(), p1Cible.getyViewport(), p2ref.getyViewport(), p2Cible.getyViewport());
    }
    
    InventoryPoint interpolate(InventoryPoint pt) {
        pt.setxViewport(xInterpolator.interpolate(pt.getxViewport()));
        pt.setyViewport(yInterpolator.interpolate(pt.getyViewport()));
        return pt;
    }

}
