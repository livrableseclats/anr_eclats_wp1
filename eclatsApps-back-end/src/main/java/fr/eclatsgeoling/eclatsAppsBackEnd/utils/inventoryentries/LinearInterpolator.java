package fr.eclatsgeoling.eclatsAppsBackEnd.utils.inventoryentries;

/**
 * effectue interpolation linéaire.
 * @author Philippe Genoud - LIG Steamer - Université Grenoble Aples
 */
public class LinearInterpolator {
    
    private final double xa;
    private final double ya;
    
    private final double coeff;

    public LinearInterpolator(double xa, double ya, double xb, double yb) {
        this.xa = xa;
        this.ya = ya;
        this.coeff = (yb - ya) / (xb - xa);
    }
    
    public double interpolate(double x) {
        return ya + (x - xa) * coeff;
    }
    
}
