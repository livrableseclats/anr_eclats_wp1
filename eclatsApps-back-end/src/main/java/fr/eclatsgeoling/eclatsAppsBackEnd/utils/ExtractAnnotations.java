/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package fr.eclatsgeoling.eclatsAppsBackEnd.utils;

import fr.eclatsgeoling.eclatsAppsBackEnd.classes.Etymon;
import fr.eclatsgeoling.eclatsAppsBackEnd.classes.Interpretation;
import fr.eclatsgeoling.eclatsAppsBackEnd.classes.Lemma;
import fr.eclatsgeoling.eclatsAppsBackEnd.dao.InterpretationDAO;

import java.io.BufferedWriter;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.util.Iterator;
import java.util.List;
import java.util.Map;
import org.eclipse.rdf4j.model.vocabulary.RDF;

/**
 * ATTENTION : pour que cela marche sur steamerlod version 4.1 de cartodialect
 * il faut changer l'URI du repository dans Queries et mettre dans Queries.txt
 * l'attribut isConfidential comme OPTIONAL dans la requête
 * FIND_AN_INTERPRETATION
 * 
 * @author Philippe Genoud - LIG Steamer - Université Grenoble Aples
 */
public class ExtractAnnotations {

	public static final String PREFIX = "" + "@prefix eclats: <http://purl.org/fr/eclats/resource/> .\n"
			+ "@prefix eclatsonto: <http://purl.org/fr/eclats/ontology#> .\n";

	private static final String HAS_RESPONSE_LINK = "<%s> eclatsonto:hasInterpretation <%s>.\n";

	private static final String OUTPUT_FILE_NAME = "/data/anotationsCarole859.ttl";

	public static void main(String[] args) throws IOException {
		Map<String, List<String>> interpURIsAndResponsesMap = InterpretationDAO
				.findInterpretationsAndResponses("http://purl.org/fr/eclats/resource/map0859", "chauvinc");
		System.out.println(interpURIsAndResponsesMap.keySet().size());

		try (BufferedWriter bw = new BufferedWriter(new OutputStreamWriter(
				new FileOutputStream(new File(System.getProperty("user.dir") + OUTPUT_FILE_NAME)), "UTF8"));) {
			System.out.println("");
			bw.write(PREFIX);

			for (String interpURI : interpURIsAndResponsesMap.keySet()) {
				Interpretation interp = InterpretationDAO.findAnInterpretation(interpURI);
				interp.setConfidentiality("confidentielle");
				// géneration du turtle pour l'interpretation"
				bw.write(generateTurtle(interp));
				// generation des liens entre l'interpetation et les réponses
				for (String repUri : interpURIsAndResponsesMap.get(interpURI)) {
					bw.write(String.format(HAS_RESPONSE_LINK, repUri, interpURI));
				}
				bw.write("\n");
			}
		}
	}

	private static String generateTurtle(Interpretation interp) throws IOException {

		StringBuilder res = new StringBuilder();
		res.append("<").append(interp.getInterpretationId()).append("> a eclatsonto:Interpretation; \n")
				.append("a eclatsonto:Interpretation ;\n").append("\";\n").append("eclatsonto:ground \"")
				.append(interp.getGround()).append("\";\n").append("eclatsonto:specificGround \"")
				.append(interp.getSpecificGround()).append("\";\n").append("eclatsonto:designating \"")
				.append(interp.getDesignating()).append("\";\n").append("eclatsonto:interpretationDate \"")
				.append(interp.getInterpretationDate()).append("\";\n").append("eclatsonto:responseType \"")
				.append(interp.getResponseType()).append("\";\n").append("eclatsonto:lemmatizedResponse \"")
				.append(interp.getLemmatizedResponse()).append("\";\n").append("eclatsonto:interpretationComment \"")
				.append(interp.getInterpretationComment()).append("\";\n").append("eclatsonto:username \"")
				.append(interp.getUser()).append("\";\n").append("eclatsonto:confidentiality \"")
				.append(interp.getConfidentiality()).append("eclatsonto:lemmas (\n");

		Iterator<Lemma> lemmaIterator = interp.getLemmasList().iterator();

		// Parcours de la liste des lemmes
		for (Lemma lemma : interp.getLemmasList()) {

			lemmaIterator.next();

			res.append(" [ \n" + "     a eclatsonto:Lemma;\n" + "  eclatsonto:grammaticalNature \""
					+ lemma.getGrammaticalNature() + "\";\n" + "  eclatsonto:gender \"" + lemma.getGender() + "\";\n"
					+ "  eclatsonto:number \"" + lemma.getNumber() + "\";\n" + "  eclatsonto:lemmaComments \""
					+ lemma.getLemmaComments() + "\";\n");
			res.append("  eclatsonto:etymons (\n");

			List<Etymon> etymonList = lemma.getEtymonsList();
			Iterator<Etymon> etymonIterator = etymonList.iterator();

			// Parcours de la liste des étymon associés au lemme
			for (Etymon etymon : etymonList) {

				etymonIterator.next();
				res.append(" [ \n" + "     a eclatsonto:Etymon;\n" + "  eclatsonto:fewEtymon \"" + etymon.getFewEtymon()
						+ "\";\n" + "  eclatsonto:fewEtymonLanguage \"" + etymon.getFewEtymonLanguage() + "\";\n"
						+ "  eclatsonto:fewEtymonSignified \"" + etymon.getFewEtymonSignified() + "\";\n"
						+ "  eclatsonto:fewEtymonReference \"" + etymon.getFewEtymonReference() + "\";\n"
						+ "  eclatsonto:etymonComment \"" + etymon.getEtymonComment() + "\";\n"
						+ "  eclatsonto:uncertainty \"" + etymon.getUncertainty());

				// Si ce n'est pas la dernière itération
				if (etymonIterator.hasNext()) {
					res.append("\";\n]\n");
				} else {
					res.append("\";\n]);");
					if (lemmaIterator.hasNext()) {
						res.append("\n]\n");
					} else {
						res.append("\n]).");
					}
				}
			}
		}
		res.append("\n\n");
		return res.toString();

	}
}
