package fr.eclatsgeoling.eclatsAppsBackEnd.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.QueryLanguage;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.query.Update;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.eclatsgeoling.eclatsAppsBackEnd.classes.Etymon;
import fr.eclatsgeoling.eclatsAppsBackEnd.classes.Interpretation;
import fr.eclatsgeoling.eclatsAppsBackEnd.classes.Lemma;
import fr.eclatsgeoling.eclatsAppsBackEnd.classes.User;

/**
 * DAO regroupant les méthodes peremttant de gérer les interprétations
 *
 * @author Philippe Genoud - LIG Steamer - Université Grenoble Aples
 */
public class InterpretationDAO {
	private static final Logger LOGGER = LoggerFactory.getLogger(InterpretationDAO.class);

	// TODO utiliser les classes Vocabulary
	private static final String PREFIXES = "PREFIX eclatsonto: <http://purl.org/fr/eclats/ontology#>\n"
			+ "PREFIX rdf: <http://www.w3.org/1999/02/22-rdf-syntax-ns#>\n";

	private static final String INTERP_LINKS = "?reps eclatsonto:hasInterpretation <%s>.\n";

	private static final String INTERPRETATION = "<%s> \n" + "                  a eclatsonto:Interpretation;\n"
			+ "                  eclatsonto:author ?author;\n"
			+ "                  eclatsonto:confidentiality ?confidentiality;\n"
			+ "                  eclatsonto:interpretationDate ?interpretationDate;\n"
			+ "                  eclatsonto:lemmatizedResponse ?lemmatizedResponse;\n"
			+ "					 eclatsonto:ground ?ground;\n"
			+ "					 eclatsonto:specificGround ?specificGround;\n"
			+ "					 eclatsonto:designating ?designating;\n"
			+ "                  eclatsonto:interpretationComment ?interpretationComment;\n"
			+ "                  eclatsonto:responseType ?responseType;\n"
			+ "					 eclatsonto:lemmas ?consLemmas1.\n";

	private static final String LEMMA = "?lemmaX a eclatsonto:Lemma;\n"
			+ "       eclatsonto:grammaticalNature ?grammaticalNature_X;\n" + "       eclatsonto:gender ?gender_X;\n"
			+ "       eclatsonto:number ?number_X ;\n" + "       eclatsonto:lemmaComments ?lemmaComments_X;\n"
			+ "       eclatsonto:etymons ?consEtymonsLemX_1.\n";

	private static final String ETYMON = "?etymonX_Y a eclatsonto:Etymon;\n"
			+ "          eclatsonto:fewEtymon ?fewEtymon_X_Y;\n"
			+ "          eclatsonto:fewEtymonLanguage ?fewEtymonLanguage_X_Y;\n"
			+ "          eclatsonto:fewEtymonSignified ?fewEtymonSignified_X_Y;\n"
			+ "          eclatsonto:fewEtymonReference ?fewEtymonReference_X_Y;\n"
			+ "          eclatsonto:etymonComment ?etymonComment_X_Y;\n"
			+ "          eclatsonto:uncertainty ?uncertainty_X_Y.\n";

	/**
	 * génère le graph pattern SPARQL correspondant à la liste rdf des lemmes d'une
	 * interpretation.
	 *
	 * @param nbLemmas le nombre de lemmes de l'interprétation
	 *
	 * @return la chaine correspondant à la liste des lemmes. Par exemple si
	 *         nbLemmas vaut 3 on aura:
	 * 
	 *         <pre>
	 *     ?consLemmas1 rdf:first ?lemma1;
	 *                  rdf:rest ?consLemmas2.
	 *     ?consLemmas2 rdf:first ?lemma2;
	 *                  rdf:rest  ?consLemmas3.
	 *     ?consLemmas3 rdf:first ?lemma3;
	 *                  rdf:rest  rdf:nil.
	 *         </pre>
	 */
	private static StringBuilder generateLemmasList(int nbLemmas) {
		StringBuilder res = new StringBuilder("?consLemmas1 rdf:first ?lemma1;\n");
		for (int i = 2; i <= nbLemmas; i++) {
			res.append("rdf:rest ?consLemmas").append(i).append(".\n").append("?consLemmas").append(i)
					.append(" rdf:first ?lemma").append(i).append(";\n");
		}
		res.append("rdf:rest  rdf:nil.\n");
		return res;
	}

	/**
	 * génère le graph pattern SPARQL correspondant à la liste rdf des etymons d'un
	 * lemme donné.
	 *
	 * @param lemmaNumber le numéro (rang) du lemme dans l'interprétation
	 * @param nbEtymons   le nombre d'étymons associé à ce lemme
	 *
	 * @return la chaine correspondant à la liste des etymons. Par exemple si
	 *         lemmaNumber vaut 2 et que l'on a 3 étymons on aura:
	 * 
	 *         <pre>
	 *
	 *     ?consEtymonsLem2_1 rdf:first ?etymon2_1;
	 *                  rdf:rest ?consEtymonsLem2_2.
	 *     ?consEtymonsLem2_2 rdf:first ?etymon2_2;
	 *                  rdf:rest  ?consEtymonsLem2_3.
	 *     ?consEtymonsLem2_3 rdf:first ?etymon2_3;
	 *                  rdf:rest  rdf:nil.
	 *         </pre>
	 */
	private static StringBuilder generateEtymonsList(int lemmaNumber, int nbEtymons) {
		StringBuilder res = new StringBuilder("?consEtymonsLem").append(lemmaNumber).append("_1 rdf:first ?etymon")
				.append(lemmaNumber).append("_1;\n");
		for (int i = 2; i <= nbEtymons; i++) {
			res.append("rdf:rest ?consEtymonsLem").append(lemmaNumber).append("_").append(i).append(".\n")
					.append("?consEtymonsLem").append(lemmaNumber).append("_").append(i).append(" rdf:first ?etymon")
					.append(lemmaNumber).append("_").append(i).append(";\n");
		}
		res.append("rdf:rest  rdf:nil.\n");
		return res;
	}

	/**
	 * Génère le graph pattern SPARQL correspondant à tous les triplets des lemmes
	 * d'une interpretation.
	 *
	 * @param nbLemmas  le nombre de lemmes dans l'interprétation
	 * @param nbEtymons le nombre d'étymons associé à chaque lemme.
	 *                  <code>nbEtymons[i]</code> est le nombre d'étymons du lemme
	 *                  <code>i + 1</code>.
	 *
	 * @return la chaîne correspondant aux triplets des lemmes, c'est à dire :
	 *
	 *         1. La liste des lemmes
	 *
	 *         2. Pour chaque lemme
	 *
	 *         - les triplets correspondant aux données propres (attributs) du lemme
	 *
	 *         - la liste rdf de ses etymons
	 *
	 *         - les triplets correspondant aux données propres (attributs) de
	 *         chaque étymon
	 *
	 *         Par exemple pour une interprétation ayant deux lemmes, le premier
	 *         ayant un étymon et le deuxième trois etymons on aura la chaîne :
	 *
	 *         <pre>
	 * ?consLemmas1 rdf:first ?lemma1;
	 * rdf:rest ?consLemmas2.
	 * ?consLemmas2 rdf:first ?lemma2;
	 * rdf:rest  ?rdf:nil.
	 *
	 * ?lemma1 a eclatsonto:Lemma;
	 *         eclatsonto:grammaticalNature ?grammaticalNature_1;
	 *         eclatsonto:lemmaId ?lemmaId_1;
	 *         eclatsonto:gender ?gender_1;
	 *         eclatsonto:number ?number_1 ;
	 *         eclatsonto:lemmaComments ?lemmaComments_1;
	 *         eclatsonto:etymons ?consEtymonsLem1_1.
	 *
	 * ?consEtymonsLem1_1 rdf:first ?etymon1_1;
	 *                    rdf:rest rdf:nil.
	 *
	 * ?etymon1_1 a eclatsonto:Etymon;
	 *            eclatsonto:fewEtymon ?fewEtymon_1_1;
	 *            eclatsonto:fewEtymonLanguage ?fewEtymonLanguage_1_1;
	 *            eclatsonto:fewEtymonSignified ?fewEtymonSignified_1_1;
	 *            eclatsonto:fewEtymonReference ?fewEtymonReference_1_1;
	 *            eclatsonto:etymonComment ?etymonComment_1_1;
	 *            eclatsonto:uncertainty ?uncertainty_1_1.
	 *
	 * ?lemma2 a eclatsonto:Lemma;
	 *         eclatsonto:grammaticalNature ?grammaticalNature_2;
	 *         eclatsonto:lemmaId ?lemmaId_2;
	 *         eclatsonto:gender ?gender_2;
	 *         eclatsonto:number ?number_2 ;
	 *         eclatsonto:lemmaComments ?lemmaComments_2;
	 *         eclatsonto:etymons ?consEtymonsLem2_1.
	 *
	 * ?consEtymonsLem2_1 rdf:first ?etymon2_1;
	 *                    rdf:rest ?consEtymonsLem2_2.
	 * ?consEtymonsLem2_2 rdf:first ?etymon2_2;
	 *                    rdf:rest  rdf:nil.
	 *
	 * ?etymon2_1 a eclatsonto:Etymon;
	 *            eclatsonto:fewEtymon ?fewEtymon_2_1;
	 *            eclatsonto:fewEtymonLanguage ?fewEtymonLanguage_2_1;
	 *            eclatsonto:fewEtymonSignified ?fewEtymonSignified_2_1;
	 *            eclatsonto:fewEtymonReference ?fewEtymonReference_2_1;
	 *            eclatsonto:etymonComment ?etymonComment_2_1;
	 *            eclatsonto:uncertainty ?uncertainty_2_1.
	 *
	 * ?etymon2_2 a eclatsonto:Etymon;
	 *            eclatsonto:fewEtymon ?fewEtymon_2_2;
	 *            eclatsonto:fewEtymonLanguage ?fewEtymonLanguage_2_2;
	 *            eclatsonto:fewEtymonSignified ?fewEtymonSignified_2_2;
	 *            eclatsonto:fewEtymonReference ?fewEtymonReference_2_2;
	 *            eclatsonto:etymonComment ?etymonComment_2_2;
	 *            eclatsonto:uncertainty ?uncertainty_2_2.
	 *
	 *         </pre>
	 *
	 */
	private static StringBuilder generateLemmas(int nbLemmas, int[] nbEtymons) {
		StringBuilder res = generateLemmasList(nbLemmas);
		for (int i = 1; i <= nbLemmas; i++) {
			res.append(LEMMA.replace("X", "" + i));
			res.append(generateEtymonsList(i, nbEtymons[i - 1]));
			for (int j = 1; j <= nbEtymons[i - 1]; j++) {
				res.append(ETYMON.replace("X", "" + i).replace("Y", "" + j));
			}
		}
		return res;
	}

	/**
	 * Génère le graph pattern SPARQL correspondant à tous les triplets des lemmes
	 * d'une interpretation.
	 *
	 * @param interp l'interprétation
	 *
	 * @return la chaîne correspondant aux triplets de l'interpretation, c'est à
	 *         dire :
	 *
	 *         1. les triplets correspondant aux données propres (attributs) de
	 *         l'interprétation
	 *
	 *         2. La liste des lemmes
	 *
	 *         3. Pour chaque lemme
	 *
	 *         - les triplets correspondant aux données propres (attributs) du lemme
	 *
	 *         - la liste rdf de ses etymons
	 *
	 *         - les triplets correspondant aux données propres (attributs) de
	 *         chaque étymon
	 *
	 *         Par exemple pour une interprétation ayant comme URI
	 *         <code>interpURI</code> et constituée de deux lemmes, le premier ayant
	 *         un étymon et le deuxième trois etymons on aura la chaîne :
	 *
	 *         <pre>
	 * <interpURI>  a eclatsonto:Interpretation;
	 *              eclatsonto:author ?author;
	 *              eclatsonto:confidentiality ?confidentiality;
	 *              eclatsonto:interpretationDate ?interpretationDate;
	 *              eclatsonto:lemmatizedResponse ?lemmatizedResponse;
	 *              eclatsonto:ground ?ground;
	 *              eclatsonto:specificGround ?specificGround;
	 *              eclatsonto:designating ?designating;
	 *              eclatsonto:interpretationComment ?interpretationComment;
	 *              eclatsonto:responseType ?responseType;
	 *              eclatsonto:lemmas ?consLemmas1.
	 *
	 * ?consLemmas1 rdf:first ?lemma1;
	 *              rdf:rest ?consLemmas2.
	 * ?consLemmas2 rdf:first ?lemma2;
	 *              rdf:rest  ?rdf:nil.
	 *
	 * ?lemma1 a eclatsonto:Lemma;
	 *         eclatsonto:grammaticalNature ?grammaticalNature_1;
	 *         eclatsonto:lemmaId ?lemmaId_1;
	 *         eclatsonto:gender ?gender_1;
	 *         eclatsonto:number ?number_1 ;
	 *         eclatsonto:lemmaComments ?lemmaComments_1;
	 *         eclatsonto:etymons ?consEtymonsLem1_1.
	 *
	 * ?consEtymonsLem1_1 rdf:first ?etymon1_1;
	 *                    rdf:rest rdf:nil.
	 *
	 * ?etymon1_1 a eclatsonto:Etymon;
	 *            eclatsonto:fewEtymon ?fewEtymon_1_1;
	 *            eclatsonto:fewEtymonLanguage ?fewEtymonLanguage_1_1;
	 *            eclatsonto:fewEtymonSignified ?fewEtymonSignified_1_1;
	 *            eclatsonto:fewEtymonReference ?fewEtymonReference_1_1;
	 *            eclatsonto:etymonComment ?etymonComment_1_1;
	 *            eclatsonto:uncertainty ?uncertainty_1_1.
	 *
	 * ?lemma2 a eclatsonto:Lemma;
	 *         eclatsonto:grammaticalNature ?grammaticalNature_2;
	 *         eclatsonto:lemmaId ?lemmaId_2;
	 *         eclatsonto:gender ?gender_2;
	 *         eclatsonto:number ?number_2 ;
	 *         eclatsonto:lemmaComments ?lemmaComments_2;
	 *         eclatsonto:etymons ?consEtymonsLem2_1.
	 *
	 * ?consEtymonsLem2_1 rdf:first ?etymon2_1;
	 *                    rdf:rest ?consEtymonsLem2_2.
	 * ?consEtymonsLem2_2 rdf:first ?etymon2_2;
	 *                    rdf:rest  rdf:nil.
	 *
	 * ?etymon2_1 a eclatsonto:Etymon;
	 *            eclatsonto:fewEtymon ?fewEtymon_2_1;
	 *            eclatsonto:fewEtymonLanguage ?fewEtymonLanguage_2_1;
	 *            eclatsonto:fewEtymonSignified ?fewEtymonSignified_2_1;
	 *            eclatsonto:fewEtymonReference ?fewEtymonReference_2_1;
	 *            eclatsonto:etymonComment ?etymonComment_2_1;
	 *            eclatsonto:uncertainty ?uncertainty_2_1.
	 *
	 * ?etymon2_2 a eclatsonto:Etymon;
	 *            eclatsonto:fewEtymon ?fewEtymon_2_2;
	 *            eclatsonto:fewEtymonLanguage ?fewEtymonLanguage_2_2;
	 *            eclatsonto:fewEtymonSignified ?fewEtymonSignified_2_2;
	 *            eclatsonto:fewEtymonReference ?fewEtymonReference_2_2;
	 *            eclatsonto:etymonComment ?etymonComment_2_2;
	 *            eclatsonto:uncertainty ?uncertainty_2_2.
	 *
	 *         </pre>
	 */
	private static StringBuilder generateInterp(Interpretation interp) {
		StringBuilder interpGraphPattern = new StringBuilder();
		// calcule le nombre de lemmes de l'interprétation
		int nbLemmas = interp.getLemmasList().size();
		// pour chaque lemme calcule le nombre d'étymons
		int[] nbEtymons = new int[nbLemmas];
		for (int i = 0; i < nbLemmas; i++) {
			nbEtymons[i] = interp.getLemmasList().get(i).getEtymonsList().size();
		}
		// les triplets correspondant aux données propres (attributs) de
		// l'interprétation
		interpGraphPattern.append(String.format(INTERPRETATION, interp.getInterpretationId()))
				// les triplets correspondant au lemmes et à leurs etymons
				.append(generateLemmas(nbLemmas, nbEtymons));
		return interpGraphPattern;
	}

	/**
	 * génère la requête SPARQL permettant de supprimer tous les triplets associés à
	 * une interprétation.
	 *
	 * @param interp l'interprétation que l'on veut supprimer
	 * @return la requête SPARQL. Elle est de la forme
	 *
	 *         <pre>
	 *         DELETE { les triplets correspondants aux liens hasInterpretation à
	 *         supprimer les triplets correspondant aux données de l'interprétation
	 *         et de ses composants } WHERE { les triplets correspondants aux liens
	 *         hasInterpretation à supprimer les triplets correspondant aux données
	 *         de l'interprétation et de ses composants }
	 */
	private static StringBuilder generateSPARQL_DeleteQuery(Interpretation interp) {
		StringBuilder interpGraphPattern = generateInterp(interp);
		String interpLinks = String.format(INTERP_LINKS, interp.getInterpretationId());

		StringBuilder res = new StringBuilder(PREFIXES);
		res.append("DELETE {\n");
		res.append(interpLinks);
		res.append(interpGraphPattern);
		res.append("}\n WHERE {\n");
		res.append(interpLinks);
		res.append(interpGraphPattern);
		res.append("\n}\n");
		return res;
	}

	/**
	 * Construit un objet Interpretation collectant toutes les données d'une
	 * interprétation ainsi que les données de ses composants (lemmes et etymons)
	 *
	 * @param interpretationURI l'URI de l'interprétation
	 *
	 * @return l'objet contenant toutes les données de l'interprétation
	 */
	public static Interpretation findAnInterpretation(String interpretationURI) {

		// -----------------------------
		// Déclaration des variables
		// ------------------------------
		String prevLemmaId = null;
		Interpretation interpretation = null;
		Lemma lemma = null;
		boolean newInterp = true;
		// -----------------------------
		// Connection au repository
		// ------------------------------
		try (RepositoryConnection connection = Queries.REPOSITORY.getConnection()) {

			ValueFactory ResourceFactory = connection.getValueFactory();

			// Préparation de la requête SPARQL qui permet de récupérer une interprétation
			// donnée
			TupleQuery tupleQuery = connection.prepareTupleQuery(QueryLanguage.SPARQL,
					Queries.getQuery("FIND_AN_INTERPRETATION"));

			// Création de l'URI pour l'entrée d'inventaire concernée
			tupleQuery.setBinding("interpretationURI", ResourceFactory.createIRI(interpretationURI));

			LOGGER.info("\n ---------------------- Query FIND_AN_INTERPRETATION -------------------------- \n"
					+ " SetBinding inventoryEntry : " + interpretationURI + "\n"
					+ Queries.getQuery("FIND_AN_INTERPRETATION"));

			// exécution de la requête SPARQL
			try (TupleQueryResult rs = tupleQuery.evaluate()) {

				while (rs.hasNext()) {
					BindingSet bindingSet = rs.next();

					// --------------------------------------------------------------
					// Récupération des données (attributs) propre à l'interprétation
					// --------------------------------------------------------------
					if (newInterp) {
						interpretation = new Interpretation(interpretationURI,
								bindingSet.getValue("lemmatizedResponse").stringValue(),
								bindingSet.getValue("ground").stringValue(),
								bindingSet.getValue("specificGround").stringValue(),
								bindingSet.getValue("designating").stringValue(),
								bindingSet.getValue("responseType").stringValue(),
								bindingSet.getValue("interpretationComment").stringValue(),
								Long.parseLong(bindingSet.getValue("interpretationDate").stringValue()), "", // bindingSet.getValue("mapIdentifier").stringValue(),
								new User(bindingSet.getValue("author").stringValue(),
										bindingSet.getValue("organization").stringValue()),
								bindingSet.getValue("confidentiality").stringValue());
						newInterp = false;
					}

					// -----------------------------
					// Gestion du lemme
					// ------------------------------
					// Si c'est un nouveau lemme
					if (prevLemmaId == null || !bindingSet.getValue("elt1").stringValue().equals(prevLemmaId)) {
						lemma = new Lemma(bindingSet.getValue("grammaticalNature").stringValue(),
								bindingSet.getValue("gender").stringValue(),
								bindingSet.getValue("number").stringValue(),
								bindingSet.getValue("lemmaComments").stringValue());
						// Ajout du lemme à l'interprétation
						interpretation.addLemma(lemma);
						// on garde l'id du lemme si le lemmes a plusieurs etymons les lignes suivantes
						// du
						// binding set porteront sur le même lemme qu'il ne faudra pas recréer.
						prevLemmaId = bindingSet.getValue("elt1").stringValue();
					}

					// -----------------------------
					// Gestion de l'étymon
					// ------------------------------
					lemma.addEtymon(new Etymon(bindingSet.getValue("fewEtymon").stringValue(),
							bindingSet.getValue("fewEtymonLanguage").stringValue(),
							bindingSet.getValue("fewEtymonSignified").stringValue(),
							bindingSet.getValue("fewEtymonReference").stringValue(),
							bindingSet.getValue("etymonComment").stringValue(),
							bindingSet.getValue("uncertainty").stringValue()));

				}
			}

			return interpretation;
		}
	}

	// ---------------------------------------------------------------------------------------------
	// ---------------------------------------------------------------------------------------------
	// SUPPRESSION D'INTERPRETATION
	// ---------------------------------------------------------------------------------------------
	// ---------------------------------------------------------------------------------------------
	/**
	 * Supprime tous les triplets associés à une interprétation
	 *
	 * @param interpretationId l'Id de l'interprétation à supprimer
	 */
	public static void deleteInterpretation(String interpretationId) {

		Interpretation interpretation = findAnInterpretation(interpretationId);
		String sparqlQuery = generateSPARQL_DeleteQuery(interpretation).toString();

		LOGGER.info("\n ---------------------- SPARQL généré -------------------------- \n" 
				+ sparqlQuery);

		try (RepositoryConnection connection = Queries.REPOSITORY.getConnection()) {
			// Préparation de la requête SPARQL
			Update updateQuery = connection.prepareUpdate(QueryLanguage.SPARQL, sparqlQuery);
			updateQuery.execute();
			// TODO gérer UpdateExecutionException
		}
	}

	/**
	 * trouve toutes les interpretations produites par un utilisateur donné pour une
	 * carte donnée, et indique pour chaque interprétation les réponses auxquelles
	 * l'utilisateur à propagé cette interprétation.
	 * 
	 * @param mapURI l'URI de la carte
	 * @param author l'identifiant de l'utilisateur
	 * 
	 * @return un Map dont les clés sont les URI des interpretations et les valeurs
	 *         la liste des URIs des réponses auxquelles cette interpretation a été
	 *         associée.
	 */
	public static Map<String, List<String>> findInterpretationsAndResponses(String mapURI, String currentUser) {

		Map<String, List<String>> res = new HashMap<>();

		// -----------------------------
		// Connection au repository
		// ------------------------------
		try (RepositoryConnection connection = Queries.REPOSITORY.getConnection()) {
			ValueFactory ResourceFactory = connection.getValueFactory();

			System.out.println("Query \n" + Queries.getQuery("FIND_INTERPRETATION_URIS"));
			// Préparation de la requête SPARQL
			TupleQuery tupleQuery = connection.prepareTupleQuery(QueryLanguage.SPARQL,
					Queries.getQuery("FIND_INTERPRETATION_URIS"));

			LOGGER.info("\n ---------------------- Query FIND_INTERPRETATION_URIS -------------------------- \n"
					+ " SetBinding mapURI : " + ResourceFactory.createIRI(mapURI) + "\n" + " SetBinding author : "
					+ ResourceFactory.createIRI(currentUser) + "\n" + Queries.getQuery("FIND_INTERPRETATION_URIS"));

			tupleQuery.setBinding("mapURI", ResourceFactory.createIRI(mapURI));
			tupleQuery.setBinding("author", ResourceFactory.createLiteral(currentUser));

			String prevInterURI = null;

			try (TupleQueryResult rs = tupleQuery.evaluate()) {

				while (rs.hasNext()) {
					BindingSet bindingSet = rs.next();
					String interpURI = bindingSet.getValue("interpretationURI").stringValue();

					if (!interpURI.equals(prevInterURI)) {
						res.put(interpURI, new ArrayList<>());
						prevInterURI = interpURI;
					}
					String responseURI = bindingSet.getValue("responseURI").stringValue();
					res.get(interpURI).add(responseURI);
				}

			}
			return res;
		}

	}
}
