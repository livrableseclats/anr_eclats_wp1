package fr.eclatsgeoling.eclatsAppsBackEnd.dao;

import fr.eclatsgeoling.eclatsAppsBackEnd.classes.InventoryPoint;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.charset.StandardCharsets;
import java.util.List;
import java.util.UUID;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.rio.RDFFormat;

/**
 * DAO permettant d'enregister les coordonnées des points d'inventaire dans le
 * triple store, sans réponse phonétique.
 *
 * @author Philippe Genoud - LIG Steamer - Université Grenoble Aples
 */
public class InventoryPointDAO {

    public static final String PREFIX = ""
            + "@prefix : <http://purl.org/fr/eclats/graph#> .\n"
            + "@prefix eclats: <http://purl.org/fr/eclats/resource/> .\n"
            + "@prefix eclatsonto: <http://purl.org/fr/eclats/ontology#> .\n"
            + "\n";

    private static final String RDF_STRING = ""
            + "  eclats:map%s eclatsonto:hasInventoryEntry eclats:inventoryEntry_SP%s_map%s.\n"
            + "  eclats:inventoryEntry_SP%s_map%s a eclatsonto:InventoryEntry;\n"
            + "              eclatsonto:forSurveyPoint eclats:surveyPoint%s;\n"
            + "              eclatsonto:xViewport \"%s\"^^xsd:double;\n"
            + "              eclatsonto:yViewport \"%s\"^^xsd:double;\n"
            + "              eclatsonto:hasResponse eclats:%s.\n"
            + "	 eclats:%s a eclatsonto:Response;\n"
            + "              eclatsonto:complementaryInformation \"\";\n\n"
            + "              eclatsonto:rousselotPhoneticForm \"%s\".\n\n";

    /**
     * renvoie la chaine RDF en turtle correspondant aux triplets
     * définissant un point d'inventaire.
     *
     * @param mapNum le numéro de la carte
     * @param pt le point d'inventaire
     * 
     * @return Les triplets (en Turtle) pour le point d'inventaire pt.
     */
    private static String createTurtle(String mapNum, InventoryPoint pt) {
        String responseId = UUID.randomUUID().toString();
        return String.format(RDF_STRING, mapNum, pt.getId(), mapNum,
                pt.getId(), mapNum, pt.getId(), pt.getxViewport(), pt.getyViewport(),
                responseId, responseId, "forme phonétique non encore renseignée");
    }

    /**
     * Rajoute dans le triplestore ECLATS, les triplets représentant les entrées
     * d'inventaires (avec leurs coordonnées image (viewport)) sans leur réponse
     * phonétique.
     *
     * @param mapId l'identifiant de la carte
     * @param inventoryPointsList la liste des Points d'inventaires à créer.
     */
    public static void createInventoryPoints(String mapId, List<InventoryPoint> inventoryPointsList) throws IOException {
        
        StringBuilder sb = new StringBuilder(PREFIX);
        sb.append (":ALF_newInventries {\n"); // pour le graphe nommé 
        for (InventoryPoint pt : inventoryPointsList) {
            sb.append(createTurtle(mapId, pt));
        }
        sb.append("\n}\n"); // ferme le graphe nommé
        // conversion de la chaine en un inputStream
        InputStream inputStream = new ByteArrayInputStream(sb.toString().getBytes(StandardCharsets.UTF_8));
        RepositoryConnection connection = null;
        try {
            connection = Queries.REPOSITORY.getConnection();
            connection.begin();   // début d'une transcation
            connection.add(inputStream, "http://purl.org/fr/eclats/graph#", RDFFormat.TRIG);
            connection.commit(); // commiter la transcation provoque la persistence des données
        }
        catch (IOException ex) {
            if (connection != null) {
                connection.rollback(); // annule la transaction
            }
        }
    }

}
