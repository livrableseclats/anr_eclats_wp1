package fr.eclatsgeoling.eclatsAppsBackEnd.dao;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.UUID;
import java.util.Map.Entry;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Literal;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.QueryLanguage;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.util.Repositories;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.eclatsgeoling.eclatsAppsBackEnd.classes.InventoryEntry;

public class InventoryEntryDAO {
	private static final Logger LOGGER = LoggerFactory.getLogger(InventoryEntryDAO.class);

	static String GREY = "rgb(44, 62, 80)";
	static String VIOLET = "rgb(111, 30, 81)";
	static String BLUE = "blue";

	
	/**
	 * Envoie la requête qui permet de récupérer les entrées d'inventaires contenues dans une
	 * carte, la liste est utilisée pour le mode transcription phonétique
	 * @param mapId la carte courante
	 * @return la liste de toutes les entrées d'inventaires
	 */	
	public static ArrayList<InventoryEntry> findAllInventoryEntries(String mapId) {
		ArrayList<InventoryEntry> inventoryEntryList = new ArrayList<>();
		try (RepositoryConnection connection = Queries.REPOSITORY.getConnection()) {
			ValueFactory ResourceFactory = connection.getValueFactory();
			TupleQuery tupleQuery = connection.prepareTupleQuery(QueryLanguage.SPARQL,
					Queries.getQuery("ALL_INVENTORYENTRIES"));
			tupleQuery.setBinding("mapId", ResourceFactory.createIRI("http://purl.org/fr/eclats/resource/map" + mapId));

			LOGGER.info("\n ---------------------- Query ALL_INVENTORYENTRIES -------------------------- \n"
					+ "SetBinding mapId : http://purl.org/fr/eclats/resource/map" + mapId + "\n"
					+ Queries.getQuery("ALL_INVENTORYENTRIES"));

			try (TupleQueryResult rs = tupleQuery.evaluate()) {
				while (rs.hasNext()) {
					BindingSet bindingSet = rs.next();
					inventoryEntryList.add(new InventoryEntry(bindingSet.getValue("inventoryEntry").stringValue(), 0, 0,
							false, bindingSet.getValue("rousselotPhoneticForm").stringValue(), bindingSet.getValue("complementaryInformation").stringValue(), true, "", false, BLUE));
				}	
			}
		}
		return inventoryEntryList;
	}
	/**
	 * Fonction qui permet de récupérer les entrées d'inventaires contenues dans une
	 * carte pour l'affichage des disques d3 dans le viewer
	 *
	 * @param mapId       l'id de la carte pour laquelle il faut trouver les entrées
	 *                    d'inventaires
	 * @param currentUser le nom de l'utilisateur courant
	 * @return un ArrayList d'entrées d'inventaires
	 */
	public static ArrayList<InventoryEntry> findAllInventoryEntriesForD3(String mapId, String currentUser) {
		// Initialisation de la liste qui contient les PE dans l'image courante.
		ArrayList<InventoryEntry> inventoryEntryList = new ArrayList<>();
		// Liste des id d'entrée d'inventaires
		ArrayList<String> inventoryEntryIdList = new ArrayList<>();
		// Connection vers le repository
		try (RepositoryConnection connection = Queries.REPOSITORY.getConnection()) {
			ValueFactory ResourceFactory = connection.getValueFactory();
			// Préparation de la requête SPARQL
			TupleQuery tupleQuery = connection.prepareTupleQuery(QueryLanguage.SPARQL,
					Queries.getQuery("ALL_INVENTORYENTRIES_FOR_D3"));
			// Création de l'URI pour l'image courante
			tupleQuery.setBinding("mapId", ResourceFactory.createIRI("http://purl.org/fr/eclats/resource/map" + mapId));

			LOGGER.info("\n ---------------------- Query ALL_INVENTORYENTRIES_FOR_D3 -------------------------- \n"
					+ "SetBinding mapId : http://purl.org/fr/eclats/resource/map" + mapId + "\n"
					+ Queries.getQuery("ALL_INVENTORYENTRIES_FOR_D3"));

			// exécution de la requête
			try (TupleQueryResult rs = tupleQuery.evaluate()) {
				while (rs.hasNext()) {
					BindingSet bindingSet = rs.next();

					boolean hasResponse = hasResponse(bindingSet.getValue("rousselotPhoneticForm").stringValue());
					String rousselotPhoneticForm = bindingSet.getValue("rousselotPhoneticForm").stringValue();
					String complementaryInformation = bindingSet.getValue("complementaryInformation").stringValue();
					double xViewport = Double.parseDouble(bindingSet.getValue("xViewport").stringValue());
					double yViewport = Double.parseDouble(bindingSet.getValue("yViewport").stringValue());
					String inventoryEntryId = bindingSet.getValue("inventoryEntry").stringValue();

					int index = inventoryEntryIdList.indexOf(inventoryEntryId);

					if (index < 0) {

						// la liste ne contient pas déjà l'entrée d'inventaire, alors on l'ajoute
						// Mise à jour de la liste contenant les id des entrées d'inventaires déjà
						// présentes
						inventoryEntryIdList.add(inventoryEntryId);
						if (bindingSet.hasBinding("interpretation")) {
							String bindingUsername = bindingSet.getValue("author").stringValue().substring(35);
							if (bindingUsername.equals(currentUser)) {
								inventoryEntryList.add(new InventoryEntry(inventoryEntryId, xViewport, yViewport,
										hasResponse, rousselotPhoneticForm, complementaryInformation, true, bindingUsername, false, BLUE));
							} else {
								inventoryEntryList.add(new InventoryEntry(inventoryEntryId, xViewport, yViewport,
										hasResponse, rousselotPhoneticForm, complementaryInformation, true, bindingUsername, false, VIOLET));
							}
						} else {
							inventoryEntryList.add(new InventoryEntry(inventoryEntryId, xViewport, yViewport,
									hasResponse, rousselotPhoneticForm, complementaryInformation, false, "noAuthor", false, GREY));
						}

					} else {
						// La liste contient déjà l'entrée d'inventaire
						InventoryEntry ie = inventoryEntryList.get(index);

						if (bindingSet.hasBinding("interpretation")) {
							String bindingUsername = bindingSet.getValue("author").stringValue().substring(35);
							// cas 1 celle enregistrée n'a pas d'interp
							if (!ie.hasInterpretation()) {
								// il faut changer l'auteur
								ie.setHasInterpretation(true);
								ie.setAuthor(bindingUsername);
								ie.setColor(VIOLET);
							} else {
								// cas 2 celle enregistrée a déjà une inter
								if (ie.getAuthor().equals(currentUser) && !bindingUsername.equals(currentUser)) {
									// je suis dejà auteur et il y en a un nouveau
									ie.setIsMultipleAuthors(true);
									ie.setColor(BLUE);
								}
								if (!ie.getAuthor().equals(currentUser) && !bindingUsername.equals(ie.getAuthor())) {
									ie.setIsMultipleAuthors(true);
									ie.setColor(VIOLET);
									if (bindingUsername.equals(currentUser)) {
										ie.setColor(BLUE);
										ie.setAuthor(currentUser);
									}
								}

							}

						}

					}

				}

			}
			connection.close();
		}
		return inventoryEntryList;

	}

	public static boolean hasResponse(String response) {
		boolean hasResponse;
		if (response.equals("forme phonétique non encore renseignée")) {
			hasResponse = false;
		} else {
			hasResponse = true;
		}
		return hasResponse;
	}

	// --------------------------------------------------------------------------------
	// Partie création d'entrées d'inventaire (sera obsolète quand les données sur
	// les point d'enquêtes (coordonnées dans l'image, rousselotPhoneticForm) seront
	// récupérées.
	// --------------------------------------------------------------------------------
	/**
	 * Fonction qui permet d'ajouter une entrée d'inventaire et ses réponses
	 * phonétiques associées à un point dans une carte
	 *
	 * @param inventoryEntryForm le formulaire d'ajout d'entrée d'inventaire
	 *                           comportant les coordonnées de l'entrée d'inventaire
	 *                           et les réponses phonétiques qui lui sont associées
	 */
	public static void addInventoryEntry(LinkedHashMap<String, Object> inventoryEntryForm) {

		// La liste des formes phonétiques associées à l'entrée d'inventaire à créer
		ArrayList<String> rousselotPhoneticFormList = new ArrayList<>();

		// Hashmap comportant les réponses phonétiques en alphabet Rousselot Gilléron ou
		// API (dépend de si l'utilisateur possède un clavier Gilléron)
		@SuppressWarnings("unchecked")
		LinkedHashMap<String, LinkedHashMap<String, String>> rousselotPhoneticFormHashMap = (LinkedHashMap<String, LinkedHashMap<String, String>>) inventoryEntryForm
				.get("rousselotPhoneticForm");
		// Parcours des réponses phonétiques
		for (Entry<String, LinkedHashMap<String, String>> entryComponent : rousselotPhoneticFormHashMap.entrySet()) {
			LinkedHashMap<String, String> valueComponent = entryComponent.getValue();
			// Ajout des réponse dans une liste
			rousselotPhoneticFormList.add(valueComponent.get("rousselotPhoneticForm"));
		}

		// -----------------------------
		// Création de l'objet inventoryEntry
		// ------------------------------
		InventoryEntry newInventoryEntry = new InventoryEntry(inventoryEntryForm.get("inventoryEntry").toString(),
				Double.parseDouble(inventoryEntryForm.get("xViewport").toString()),
				Double.parseDouble(inventoryEntryForm.get("yViewport").toString()), true, "", "", false, "noAuthor", false,
				GREY);

		// -----------------------------
		// Appel de la fonction updateRepository(param)
		// pour ajouter l'entrée d'inventaire au triplestore
		// ------------------------------
		updateRepository(newInventoryEntry, rousselotPhoneticFormList);

	}

	/**
	 * Fonction qui permet d'ajouter au repository l'entrée d'inventaire en
	 * paramètre ainsi que les réponses associées à l'entrée d'inventaire
	 *
	 * @param newInventoryEntry         l'entrée d'inventaire à ajouter
	 * @param rousselotPhoneticFormList la liste des réponses associées à l'entrée
	 *                                  d'inventaire
	 */
	private static void updateRepository(InventoryEntry newInventoryEntry,
			ArrayList<String> rousselotPhoneticFormList) {

		// -----------------------------
		// Déclaration des préfixes et IRI
		// ------------------------------
		String eclatsonto = "http://purl.org/fr/eclats/ontology";
		String eclats = "http://purl.org/fr/eclats/resource/";

		ValueFactory vf = SimpleValueFactory.getInstance();

		IRI inventoryEntryIRI = vf.createIRI(newInventoryEntry.getInventoryEntry());
		IRI spIdentifierIRI = vf.createIRI(eclats + "surveyPoint" + newInventoryEntry.getSpIdentifier());
		IRI mapIRI = vf.createIRI(eclats + "map" + newInventoryEntry.getMapIdentifier());

		// -----------------------------
		// Connection au repository
		// ------------------------------
		Repositories.consume(Queries.REPOSITORY, (connection) -> {

			// -----------------------------
			// Carte -> Entrée d'inventaire
			// ------------------------------
			connection.add(vf.createStatement(mapIRI, RDF.TYPE, vf.createIRI(eclatsonto + "#Map")));
			connection.add(
					vf.createStatement(mapIRI, vf.createIRI(eclatsonto + "#hasInventoryEntry"), inventoryEntryIRI));

			// -----------------------------
			// Entrée d'inventaire -> SurveyPoint
			// ------------------------------
			connection
					.add(vf.createStatement(inventoryEntryIRI, RDF.TYPE, vf.createIRI(eclatsonto + "#InventoryEntry")));
			connection.add(vf.createStatement(inventoryEntryIRI, vf.createIRI(eclatsonto + "#forSurveyPoint"),
					spIdentifierIRI));
			connection.add(vf.createStatement(inventoryEntryIRI, vf.createIRI(eclatsonto + "#xViewport"),
					vf.createLiteral(newInventoryEntry.getxViewport())));
			connection.add(vf.createStatement(inventoryEntryIRI, vf.createIRI(eclatsonto + "#yViewport"),
					vf.createLiteral(newInventoryEntry.getyViewport())));

			// -----------------------------
			// Entrée d'inventaire -> Réponse
			// ------------------------------
			// Itérateur sur l'ArrayList comportant les réponses phonétiques
			Iterator<String> rousselotPhoneticFormListIterator = rousselotPhoneticFormList.iterator();
			// Tant qu'il y a une réponse phonétique, création d'une réponse et de sa forme
			// phonétique correspondante.
			while (rousselotPhoneticFormListIterator.hasNext()) {
				// Création de l'IRI id de la réponse courante ainsi que le Litéral
				// correspondant à la forme phonétique courante
				Literal rousselotPhoneticFormLiteral = vf.createLiteral(rousselotPhoneticFormListIterator.next());
				IRI responseIdIRI = vf.createIRI(eclats + UUID.randomUUID().toString());
				// Ajout des triplets au graphe
				connection.add(vf.createStatement(inventoryEntryIRI, vf.createIRI(eclatsonto + "#hasResponse"),
						responseIdIRI));
				connection.add(vf.createStatement(responseIdIRI, RDF.TYPE, vf.createIRI(eclatsonto + "#Response")));
				connection.add(vf.createStatement(responseIdIRI, vf.createIRI(eclatsonto + "#rousselotPhoneticForm"),
						rousselotPhoneticFormLiteral));
			}
		});
	}

	/**
	 * Fonction appelée lorsque l'utilisateur souhaite visualiser la localisation d'un point d'enquête dans la "Carte N° I :  NOMS FRANÇAIS DES LOCALITÉS"
	 * Retourne l'entrée d'inventaire correspondant à l'id passé en paramètre
	 */
	public static InventoryEntry findInventoryEntry(String inventoryEntry) {

		InventoryEntry inventry = null;
		// -----------------------------
		// Connection au repository
		// ------------------------------
		try (RepositoryConnection connection = Queries.REPOSITORY.getConnection()) {
			ValueFactory vf = SimpleValueFactory.getInstance();

			TupleQuery tupleQuery = connection.prepareTupleQuery(QueryLanguage.SPARQL,
					Queries.getQuery("FIND_INVENTORYENTRY"));

			// Insertion de l'URI correspondant à la carte à la requête SPARQL
			tupleQuery.setBinding("inventoryEntry", vf.createIRI(inventoryEntry));

			LOGGER.info("\n ---------------------- Query FIND_INVENTORYENTRY -------------------------- \n"
					+ "SetBinding inventoryEntry : " + inventoryEntry + "\n"
					+ Queries.getQuery("FIND_INVENTORYENTRY"));
			
			// Exécution de la requête
			// exécution de la requête
			try (TupleQueryResult rs = tupleQuery.evaluate()) {
				while (rs.hasNext()) {

					BindingSet bindingSet = rs.next();
					inventry = new InventoryEntry(bindingSet.getValue("inventoryEntry").stringValue(),
							Double.parseDouble(bindingSet.getValue("xViewport").stringValue()),
							Double.parseDouble(bindingSet.getValue("yViewport").stringValue()), true, "", "", true, "", false,
							"");
				}
			}
		}

		return inventry;
	}

	public static ArrayList<InventoryEntry> findRelatedInventoryEntries(String interpretationId) {

		// Initialisation de la liste qui contient les entrées d'inventaires associées à
		// l'interprétation
		ArrayList<InventoryEntry> inventoryEntryList = new ArrayList<>();

		try (RepositoryConnection connection = Queries.REPOSITORY.getConnection()) {
			ValueFactory ResourceFactory = connection.getValueFactory();
			// Préparation de la requête SPARQL
			TupleQuery tupleQuery = connection.prepareTupleQuery(QueryLanguage.SPARQL,
					Queries.getQuery("FIND_RELATED_INVENTORYENTRY"));

			tupleQuery.setBinding("interpretationId", ResourceFactory.createIRI(interpretationId));

			LOGGER.info("\n ---------------------- Query FIND_RELATED_INVENTORYENTRY -------------------------- \n"
					+ "SetBinding interpretationId : " + interpretationId + "\n"
					+ Queries.getQuery("FIND_RELATED_INVENTORYENTRY"));

			// exécution de la requête
			try (TupleQueryResult rs = tupleQuery.evaluate()) {
				while (rs.hasNext()) {

					BindingSet bindingSet = rs.next();
					inventoryEntryList.add(new InventoryEntry(bindingSet.getValue("inventoryEntry").stringValue(),
							Double.parseDouble(bindingSet.getValue("xViewport").stringValue()),
							Double.parseDouble(bindingSet.getValue("yViewport").stringValue()), true, "", "", true, "", false,
							""));
				}
			}
		}
		return inventoryEntryList;

	}

}
