package fr.eclatsgeoling.eclatsAppsBackEnd.dao;

import java.io.IOException;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.UUID;

import org.apache.commons.lang3.StringUtils;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.QueryLanguage;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class ExportInterpretationsListDAO {
	private static final Logger LOGGER = LoggerFactory.getLogger(ExportInterpretationsListDAO.class);

	static int ETYMONS_COLUMNS = 6;
	static int LEMMAS_COLUMNS = 4;

	/**
	 * création des headers des colonnes
	 *
	 * @param maxNumberOfLemmas
	 * @return
	 */
	public static StringBuilder createColumnsHeaders(int maxNumberOfLemmas, int maxNumberOfEtymons) {
		StringBuilder columnsHeaders = new StringBuilder();
		columnsHeaders
				.append("random_id;utilisateur;date;num_point_enquête;latitude;longitude;pays;localisation;gillieron;"
						+ "reponse_lemmatisee;motif;motif_specifique;designant;type_reponse;commentaires");

		for (int lemmaIndex = 1; lemmaIndex <= maxNumberOfLemmas; lemmaIndex++) {

			columnsHeaders.append("; lemme" + lemmaIndex + "_nature_grammaticale;" + "lemme" + lemmaIndex + "_genre;"
					+ "lemme" + lemmaIndex + "_nombre;" + "lemme" + lemmaIndex + "_commentaires");

			for (int etymonIndex = 1; etymonIndex <= maxNumberOfEtymons; etymonIndex++) {
				columnsHeaders.append(";lemme" + lemmaIndex + "_étymon" + etymonIndex + ";" + "lemme" + lemmaIndex
						+ "_étymonLangage" + etymonIndex + ";" + "lemme" + lemmaIndex + "_étymonSignifié" + etymonIndex
						+ ";" + "lemme" + lemmaIndex + "_fewEtymonRéférence" + etymonIndex + ";" + "lemme" + lemmaIndex
						+ "_étymonCommentaires" + etymonIndex + ";" + "lemme" + lemmaIndex + "_étymonIncertitude"
						+ etymonIndex);
			}
		}

		// Saut de ligne
		columnsHeaders.append("\n");

		return columnsHeaders;

	}

	/**
	 * Cette fonction permet à l'utilisateur d'exporter les données d'interprétation
	 * d'une carte ALF.
	 *
	 * @param mapId l'id de la carte pour laquelle il faut exporter les données
	 * @return un fichier csv comportant les données de la carte
	 * @throws IOException
	 */
	public static String exportAllInterpretationsInMap(String mapId, String currentUser) {

		// -----------------------------
		// Recherche du nombre de lemmes maximum dans les interprétation associées à
		// la carte
		// ------------------------------
		int maxNumberOfLemmas = findMaxCountOfLemmasInMap(mapId);
		int maxNumberOfEtymons = findMaxCountOfEtymonsInMap(mapId);
		// ------------------------------
		// Déclaration des variables
		// ------------------------------
		StringBuilder str = createColumnsHeaders(maxNumberOfLemmas, maxNumberOfEtymons); // str contient la ligne d'en
																							// tête

		DateFormat df = new SimpleDateFormat("dd/MM/yy_HH:mm");

		String previousRousselotPhoneticForm = null;
		String previousSpIdentifier = " ";
		String previousinterpretationId = " ";
		String previousLemma = " ";

		// Variables utilisées remplir les cellules vides dans le cas où une
		// interprétation contient moins de composants que le nombre maximum de
		// composants rencontrés dans l'export
		int currentNumberOfLemmas = 0;
		int currentNumberOfEtymon = 0;
		// -----------------------------
		// Connection au repository
		// ------------------------------

		try (RepositoryConnection connection = Queries.REPOSITORY.getConnection()) {
			ValueFactory ResourceFactory = connection.getValueFactory();

			// Préparation de la requête SPARQL permettant de lister des interprétations
			TupleQuery tupleQuery = connection.prepareTupleQuery(QueryLanguage.SPARQL,
					Queries.getQuery("FIND_ALL_INTERPRETATIONS"));

			// Insertion de l'URI correspondant à la carte dans la requête SPARQL
			tupleQuery.setBinding("mapId", ResourceFactory.createIRI("http://purl.org/fr/eclats/resource/map" + mapId));

			LOGGER.info("\n ---------------------- Query FIND_ALL_INTERPRETATIONS -------------------------- \n"
					+ " SetBinding mapId : " + mapId + "\n"
					+ Queries.getQuery("FIND_ALL_INTERPRETATIONS"));

			// exécution de la requête
			try (TupleQueryResult rs = tupleQuery.evaluate()) {
				while (rs.hasNext()) {
					BindingSet bindingSet = rs.next();
					// Interprétation publique ou interprétation appartenant à l'utilisateur ou utilisateur connecté et confidentialité restreinte
					if (bindingSet.getValue("confidentiality").stringValue().equals("publique")
							|| currentUser.equals(bindingSet.getValue("author").stringValue().substring(35))
							|| (currentUser != "" && bindingSet.getValue("confidentiality").stringValue().equals("restreinte"))) {

						long dateBinding = Long.parseLong(bindingSet.getValue("interpretationDate").stringValue());

						// Cas de la première interprétation ou d'une interprétation différente de la
						// précédente itération
						if (previousRousselotPhoneticForm == null
								|| !previousSpIdentifier.equals(bindingSet.getValue("spIdentifier").stringValue())
								|| !previousinterpretationId
										.equals(bindingSet.getValue("interpretationId").stringValue())
								|| !previousRousselotPhoneticForm
										.equals(bindingSet.getValue("rousselotPhoneticForm").stringValue())) {

							// Mise en forme de la fin d'une ligne dans le cas où ce n'est pas la première
							if (previousRousselotPhoneticForm != null) {

								// Nouvelle ligne
								// Si le nombre d'étymon du lemme précédent est inférieur au nombre max
								// d'étymons dans la carte ajout de ';'
								if (currentNumberOfEtymon < maxNumberOfEtymons) {
									str.append(StringUtils.repeat("; ",
											ETYMONS_COLUMNS * (maxNumberOfEtymons - (currentNumberOfEtymon))));
								}

								// Si le nombre de lemmes de l'interprétation précédente est inférieure au
								// nombre max de lemmes,
								// ajout de ';' manquant
								if (currentNumberOfLemmas < maxNumberOfLemmas) {
									str.append(StringUtils.repeat("; ",
											(LEMMAS_COLUMNS + ETYMONS_COLUMNS * maxNumberOfEtymons)
													* (maxNumberOfLemmas - currentNumberOfLemmas)));
								}

								str.append("\n");
								currentNumberOfEtymon = 0;
								currentNumberOfLemmas = 0;
							}

							str.append(UUID.randomUUID().toString()).append(";");
							// Concaténation de chaine à chaque résultat
							str.append(bindingSet.getValue("author").stringValue().substring(35) + ";" + df.format(dateBinding))
									.append(";");

							// -----------------------------------------------------
							// Informations sur le point d'enquête
							// -----------------------------------------------------
							str.append(bindingSet.getValue("spIdentifier").stringValue());
							str.append(";");
							str.append(bindingSet.getValue("latitude").stringValue());
							str.append(";");
							str.append(bindingSet.getValue("longitude").stringValue());
							str.append(";");
							str.append(bindingSet.getValue("country").stringValue());
							str.append(";");
							str.append(bindingSet.getValue("placeName").stringValue());
							str.append(";");

							// -----------------------------------------------------
							// Information sur l'interprétation globale
							// -----------------------------------------------------
							str.append("\"").append(bindingSet.getValue("rousselotPhoneticForm").stringValue())
									.append("\";").append("\"")
									.append(bindingSet.getValue("lemmatizedResponse").stringValue()).append("\";")
									.append("\"").append(bindingSet.getValue("ground").stringValue())
									.append("\";").append("\"")
									.append(bindingSet.getValue("specificGround").stringValue()).append("\";")
									.append("\"").append(bindingSet.getValue("designating").stringValue()).append("\";")
									.append("\"").append(bindingSet.getValue("responseType").stringValue())
									.append("\";").append("\"").append(bindingSet.getValue("interpretationComment").stringValue());

							// -----------------------------------------------------
							// Informations sur le lemme
							// -----------------------------------------------------
							str.append("\";").append("\"")
									.append(bindingSet.getValue("grammaticalNature").stringValue()).append("\";")
									.append("\"").append(bindingSet.getValue("gender").stringValue()).append("\";")
									.append("\"").append(bindingSet.getValue("number").stringValue()).append("\";")
									.append("\"").append(bindingSet.getValue("lemmaComments").stringValue())
									.append("\"");

							// Premier lemme terminé
							previousLemma = bindingSet.getValue("elt1").stringValue();

							// Initialisation du nombre de composant associé à l'interprétation
							currentNumberOfLemmas += 1;

							// -----------------------------------------------------
							// Informations sur l'étymon
							// -----------------------------------------------------
							if (previousLemma.equals(bindingSet.getValue("elt1").stringValue())) {

								str.append(";").append("\"").append(bindingSet.getValue("fewEtymon").stringValue())
										.append("\";").append("\"")
										.append(bindingSet.getValue("fewEtymonLanguage").stringValue()).append("\";")
										.append("\"").append(bindingSet.getValue("fewEtymonSignified").stringValue())
										.append("\";").append("\"")
										.append(bindingSet.getValue("fewEtymonReference").stringValue()).append("\";")
										.append("\"").append(bindingSet.getValue("etymonComment").stringValue())
										.append("\";").append("\"")
										.append(bindingSet.getValue("uncertainty").stringValue()).append("\"");
								currentNumberOfEtymon += 1;
							}

							previousRousselotPhoneticForm = bindingSet.getValue("rousselotPhoneticForm").stringValue();
							previousSpIdentifier = bindingSet.getValue("spIdentifier").stringValue();
							previousinterpretationId = bindingSet.getValue("interpretationId").stringValue();

						} // Cas d'une interprétation identique à la précédente itération -> plusieurs
							// lemmes ou plusieurs étymons
						else {
							// -----------------------------------------------------
							// Informations sur le lemme suivant de la même interprétation
							// -----------------------------------------------------

							// Si l'id du lemme courant est identique à l'id du lemme précédent, alors il y
							// a plusieurs étymon
							if (previousLemma.equals(bindingSet.getValue("elt1").stringValue())) {

								str.append(";").append("\"").append(bindingSet.getValue("fewEtymon").stringValue())
										.append("\";").append("\"")
										.append(bindingSet.getValue("fewEtymonLanguage").stringValue()).append("\";")
										.append("\"").append(bindingSet.getValue("fewEtymonSignified").stringValue())
										.append("\";").append("\"")
										.append(bindingSet.getValue("fewEtymonReference").stringValue()).append("\";")
										.append("\"").append(bindingSet.getValue("etymonComment").stringValue())
										.append("\";").append("\"")
										.append(bindingSet.getValue("uncertainty").stringValue()).append("\"");
								currentNumberOfEtymon += 1;

							} else {

								// Nouveau lemme
								if (!previousLemma.equals(bindingSet.getValue("elt1").stringValue())) {

									previousLemma = bindingSet.getValue("elt1").stringValue();
									currentNumberOfLemmas += 1;

									// Si le nombre d'étymon du lemme précédent est inférieur au nombre maximum
									// d'étymons dans la carte ajout de ';'
									if (currentNumberOfEtymon < maxNumberOfEtymons) {
										str.append(StringUtils.repeat("; ",
												ETYMONS_COLUMNS * (maxNumberOfEtymons - (currentNumberOfEtymon))));
									}

									currentNumberOfEtymon = 0;

									str.append(";").append("\"")
											.append(bindingSet.getValue("grammaticalNature").stringValue())
											.append("\";").append("\"")
											.append(bindingSet.getValue("gender").stringValue()).append("\";")
											.append("\"").append(bindingSet.getValue("number").stringValue())
											.append("\";").append("\"")
											.append(bindingSet.getValue("lemmaComments").stringValue()).append("\";")
											.append("\"").append(bindingSet.getValue("fewEtymon").stringValue())
											.append("\";").append("\"")
											.append(bindingSet.getValue("fewEtymonLanguage").stringValue())
											.append("\";").append("\"")
											.append(bindingSet.getValue("fewEtymonSignified").stringValue())
											.append("\";").append("\"")
											.append(bindingSet.getValue("fewEtymonReference").stringValue())
											.append("\";").append("\"")
											.append(bindingSet.getValue("etymonComment").stringValue()).append("\";")
											.append("\"").append(bindingSet.getValue("uncertainty").stringValue())
											.append("\"");
									currentNumberOfEtymon += 1;
								}
							}
						}
					}
				}
			}
			// Fin de la requête
			// Si le nombre d'étymon du lemme précédent est inférieur au nombre maximum
			// d'étymons dans la carte ajout de ';'
			if (currentNumberOfEtymon < maxNumberOfEtymons) {
				str.append(StringUtils.repeat("; ", ETYMONS_COLUMNS * (maxNumberOfEtymons - (currentNumberOfEtymon))));
			}

			// Si le nombre de lemmes de l'interprétation précédente est inférieure au
			// nombre max de lemmes,
			// ajout de ';' manquants pour les étymons et lemmes
			if (currentNumberOfLemmas < maxNumberOfLemmas) {
				str.append(StringUtils.repeat("; ", (LEMMAS_COLUMNS + ETYMONS_COLUMNS * maxNumberOfEtymons)
						* (maxNumberOfLemmas - currentNumberOfLemmas)));
			}
		}

		// ----------------------------------------------------------------
		// Mise en forme des caractères pour les différentes exceptions
		// mises en évidence dans l'import de fichiers CSV dans ShinyDialect
		// -----------------------------------------------------------------
		String formatData = str.toString().replaceAll("null|'", " ");
		return formatData;
	}

	/**
	 * Retourne le nombre maximum de lemmes associé aux interprétations contenues
	 * dans une carte. Permet de gérer les cases vides pour le csv qui sera intégré
	 * dans ShinyDialect.
	 *
	 * @param mapId identifiant de la carte pour laquelle l'utilisateur souhaite
	 *              exporter les données
	 * @return le nombre maximum de lemmes trouvé pour les interprétations contenues
	 *         dans une carte
	 */
	public static int findMaxCountOfLemmasInMap(String mapId) {
		try (RepositoryConnection connection = Queries.REPOSITORY.getConnection()) {
			ValueFactory ResourceFactory = connection.getValueFactory();

			// Préparation de la requête SPARQL permettant de compter le nombre de
			// composants de chaque interprétation
			TupleQuery tupleQuery = connection.prepareTupleQuery(QueryLanguage.SPARQL,
					Queries.getQuery("FIND_MAX_COUNT_OF_LEMMAS_IN_MAP"));

			// Insertion de l'URI correspondant à la carte dans la requête SPARQL
			tupleQuery.setBinding("mapId", ResourceFactory.createIRI("http://purl.org/fr/eclats/resource/map" + mapId));

			LOGGER.info("\n ---------------------- Query FIND_MAX_COUNT_OF_LEMMAS_IN_MAP -------------------------- \n"
					+ " SetBinding mapId : http://purl.org/fr/eclats/resource/map" + mapId + "\n"
					+ Queries.getQuery("FIND_MAX_COUNT_OF_LEMMAS_IN_MAP"));
			
			try (TupleQueryResult rs = tupleQuery.evaluate()) {
				if (rs.hasNext()) {
					BindingSet bindingSet = rs.next();
					return Integer.parseInt(bindingSet.getValue("max").stringValue());
				} else {
					return 0;
				}
			}

		}
	}

	/**
	 * Retourne le nombre maximum d'étymons associé aux interprétations contenues
	 * dans une carte. Permet de gérer les cases vides pour le csv qui sera intégré
	 * dans ShinyDialect.
	 *
	 * @param mapId identifiant de la carte pour laquelle l'utilisateur souhaite
	 *              exporter les données
	 * @return le nombre maximum d'étymons trouvé pour les lemmes contenues dans une
	 *         carte
	 */
	public static int findMaxCountOfEtymonsInMap(String mapId) {
		try (RepositoryConnection connection = Queries.REPOSITORY.getConnection()) {
			ValueFactory ResourceFactory = connection.getValueFactory();

			// Préparation de la requête SPARQL permettant de compter le nombre de
			// composants de chaque interprétation
			TupleQuery tupleQuery = connection.prepareTupleQuery(QueryLanguage.SPARQL,
					Queries.getQuery("FIND_MAX_COUNT_OF_ETYMONS_IN_MAP"));

			// Insertion de l'URI correspondant à la carte dans la requête SPARQL
			tupleQuery.setBinding("mapId", ResourceFactory.createIRI("http://purl.org/fr/eclats/resource/map" + mapId));

			LOGGER.info("\n ---------------------- Query FIND_MAX_COUNT_OF_ETYMONS_IN_MAP -------------------------- \n"
					+ " SetBinding mapId : http://purl.org/fr/eclats/resource/map" + mapId + "\n"
					+ Queries.getQuery("FIND_MAX_COUNT_OF_ETYMONS_IN_MAP"));

			try (TupleQueryResult rs = tupleQuery.evaluate()) {
				if (rs.hasNext()) {
					BindingSet bindingSet = rs.next();
					return Integer.parseInt(bindingSet.getValue("max").stringValue());
				} else {
					return 0;
				}
			}

		}
	}

}
