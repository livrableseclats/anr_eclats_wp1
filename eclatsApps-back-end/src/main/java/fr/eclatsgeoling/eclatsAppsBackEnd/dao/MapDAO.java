package fr.eclatsgeoling.eclatsAppsBackEnd.dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.BooleanQuery;
import org.eclipse.rdf4j.query.QueryLanguage;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Service;

import fr.eclatsgeoling.eclatsAppsBackEnd.classes.Map;

@Service
public class MapDAO {
	private static final Logger LOGGER = LoggerFactory.getLogger(MapDAO.class);

	/**
	 * Fonction qui permet de récupérer la liste de toutes les cartes contenues dans
	 * le repository
	 * 
	 * @return la liste de toutes les cartes contenues dans le repository
	 */
	public static List<Map> findAllMaps() {
		try (RepositoryConnection connection = Queries.REPOSITORY.getConnection()) {
			// Préparation de la requête SPARQL qui permet de récupérer la liste des cartes
			TupleQuery tupleQuery = connection.prepareTupleQuery(QueryLanguage.SPARQL, Queries.getQuery("ALL_MAPS"));
			LOGGER.info("\n ---------------------- Query ALL_MAPS -------------------------- \n"
					+ Queries.getQuery("ALL_MAPS"));
			return executeQueryForMaps(tupleQuery);
		}
	}

	public static List<Map> executeQueryForMaps(TupleQuery tupleQuery) {
		List<Map> res = new ArrayList<>();
		boolean first = true;
		String mapIdentifier = "";
		String previousMapIdentifier = "";
		Map currentMap = null;

		// exécution de la requête
		try (TupleQueryResult rs = tupleQuery.evaluate()) {
			while (rs.hasNext()) {
				BindingSet bindingSet = rs.next();
				if (first || !(previousMapIdentifier.equals(bindingSet.getValue("mapIdentifier").stringValue()))) {
					if (first) {
						first = false;
					} else {
						// Ajout de la Map à la liste si on itère sur une nouvelle carte
						res.add(currentMap);
					}
					// Récupération des valeurs à partir des URI
					mapIdentifier = bindingSet.getValue("mapIdentifier").stringValue();
					// Création de l'objet Map
					currentMap = new Map(mapIdentifier, Integer.parseInt(bindingSet.getValue("mapIndex").stringValue()),
							bindingSet.getValue("title").stringValue(),
							bindingSet.getValue("mapTiffFormat").stringValue(),
							bindingSet.getValue("imageLink").stringValue(),
							Integer.parseInt(bindingSet.getValue("width").stringValue()),
							Integer.parseInt(bindingSet.getValue("height").stringValue()),
							bindingSet.getValue("contextualizedTitle").stringValue(),
							bindingSet.getValue("booklet").stringValue());
					previousMapIdentifier = mapIdentifier;
				}
				// Liens ARK reliant les titleElements de la carte courantes avec RAMEAU
				if (bindingSet.getValue("word") != null && bindingSet.getValue("ark") != null) {
					currentMap.addBNFLink(bindingSet.getValue("word").stringValue(),
							bindingSet.getValue("ark").stringValue());
				}
			}
			return res;
		}
	}
	
	/**
	 * Méthode qui permet de connaître le "statut" d'une carte:
	 * 
	 * Carte vierge: C'est une carte pour laquelle nous n'avons pas les entrées d'inventaires (si la requête HAS_MAP_INVENTRIES retourne TRUE )
	 * Carte transcriptable: c'est à dire sur laquelle on peut utiliser les outils CRUD pour les transcriptions phonétiques (si la requête MISS_MAP_PHONETIC_TRANSCRIPTIONS retourne TRUE)
	 * Carte annotable: c'est à dire sur laquelle on peut utiiser les outils CRUD pour les annotations (si la requête MISS_MAP_PHONETIC_TRANSCRIPTIONS retourne FALSE)
	 */
	public static HashMap<String, Boolean> getMapStatus(String mapIdentifier) {
		
		HashMap<String, Boolean> statusList = new HashMap<>();
		BooleanQuery booleanQuery;
		String mapIRI = "http://purl.org/fr/eclats/resource/map" + mapIdentifier;
		
		try (RepositoryConnection connection = Queries.REPOSITORY.getConnection()) {
			ValueFactory ResourceFactory = connection.getValueFactory();
			
			booleanQuery = connection.prepareBooleanQuery(QueryLanguage.SPARQL, Queries.getQuery("HAS_MAP_INVENTRIES"));
			LOGGER.info("\n ---------------------- Query HAS_MAP_INVENTRIES -------------------------- \n"
					+ Queries.getQuery("HAS_MAP_INVENTRIES"));
			booleanQuery.setBinding("map", ResourceFactory.createIRI(mapIRI));
			boolean hasInventries = booleanQuery.evaluate();
			
			booleanQuery = connection.prepareBooleanQuery(QueryLanguage.SPARQL, Queries.getQuery("HAS_MAP_INTERPRETATIONS"));
			LOGGER.info("\n ---------------------- Query HAS_MAP_INTERPRETATIONS -------------------------- \n"
					+ Queries.getQuery("HAS_MAP_INTERPRETATIONS"));
			booleanQuery.setBinding("map", ResourceFactory.createIRI(mapIRI));
			boolean hasInterpretations = booleanQuery.evaluate();
			
			booleanQuery = connection.prepareBooleanQuery(QueryLanguage.SPARQL, Queries.getQuery("MISS_MAP_PHONETIC_TRANSCRIPTIONS"));
			LOGGER.info("\n ---------------------- Query MISS_MAP_PHONETIC_TRANSCRIPTIONS -------------------------- \n"
					+ Queries.getQuery("MISS_MAP_PHONETIC_TRANSCRIPTIONS"));
			booleanQuery.setBinding("map", ResourceFactory.createIRI(mapIRI));
			boolean missPhoneticTranscriptions = booleanQuery.evaluate();

			statusList.put("hasInventries", hasInventries);
			statusList.put("hasInterpretations", hasInterpretations);
			statusList.put("missPhoneticTranscriptions", missPhoneticTranscriptions);

			return statusList;
		}
	}
}
