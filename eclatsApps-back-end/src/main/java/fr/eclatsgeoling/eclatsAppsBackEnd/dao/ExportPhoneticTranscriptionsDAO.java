package fr.eclatsgeoling.eclatsAppsBackEnd.dao;

import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.QueryLanguage;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class ExportPhoneticTranscriptionsDAO {
	private static final Logger LOGGER = LoggerFactory.getLogger(ExportInterpretationsListDAO.class);

	public static String exportAllPhoneticTranscriptionInMap(String mapId) {
		StringBuilder str = new StringBuilder();
		String previousSpIdentifier = " ";

		// En têtes colonnes
		str.append(
				"num_point_enquete;latitude;longitude;pays;localisation;rep1_transcription_phonetique;rep1_info_complementaire;rep2_transcription_phonetique;rep2_info_complementaire;rep3_transcription_phonetique;rep3_info_complementaire");

		try (RepositoryConnection connection = Queries.REPOSITORY.getConnection()) {
			ValueFactory ResourceFactory = connection.getValueFactory();

			TupleQuery tupleQuery = connection.prepareTupleQuery(QueryLanguage.SPARQL,
					Queries.getQuery("EXPORT_ALL_PHONETIC_TRANSCRIPTION_IN_MAP"));

			// Insertion de l'URI correspondant à la carte dans la requête SPARQL
			tupleQuery.setBinding("mapId", ResourceFactory.createIRI("http://purl.org/fr/eclats/resource/map" + mapId));

			LOGGER.info(
					"\n ---------------------- Query EXPORT_ALL_PHONETIC_TRANSCRIPTION_IN_MAP -------------------------- \n"
							+ " SetBinding mapId : " + mapId + "\n"
							+ Queries.getQuery("EXPORT_ALL_PHONETIC_TRANSCRIPTION_IN_MAP"));

			// exécution de la requête
			try (TupleQueryResult rs = tupleQuery.evaluate()) {
				while (rs.hasNext()) {

					BindingSet bindingSet = rs.next();
					if (!(previousSpIdentifier.equals(bindingSet.getValue("spIdentifier").stringValue()))) {
						str.append("\n");
						str.append(bindingSet.getValue("spIdentifier").stringValue()).append(";");
						str.append(bindingSet.getValue("latitude").stringValue()).append(";");
						str.append(bindingSet.getValue("longitude").stringValue()).append(";");
						str.append(bindingSet.getValue("country").stringValue()).append(";");
						str.append(bindingSet.getValue("placeName").stringValue()).append(";");
						if (bindingSet.getValue("rousselotPhoneticForm").stringValue()
								.equals("forme phonétique non encore renseignée")) {
							str.append("non renseignée").append(";");
						} else {
							str.append(bindingSet.getValue("rousselotPhoneticForm").stringValue()).append(";");
						}
						str.append(bindingSet.getValue("complementaryInformation").stringValue());
					} else {
						str.append(";");
						str.append(bindingSet.getValue("rousselotPhoneticForm").stringValue()).append(";");
						str.append(bindingSet.getValue("complementaryInformation").stringValue());
					}
					previousSpIdentifier = bindingSet.getValue("spIdentifier").stringValue();
				}
			}
		}
		return str.toString();
	}
}
