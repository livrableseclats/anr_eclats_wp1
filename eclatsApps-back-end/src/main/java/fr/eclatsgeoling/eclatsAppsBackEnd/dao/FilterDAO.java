package fr.eclatsgeoling.eclatsAppsBackEnd.dao;

import java.util.ArrayList;
import java.util.Collections;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.QueryLanguage;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


public class FilterDAO {
	private static final Logger LOGGER = LoggerFactory.getLogger(FilterDAO.class);

	/**
	 * Fonction qui permet de récupérer la liste des différentes valeurs associées aux attributs des interprétations publiques 
	 * OU appartenant à l'utilisateur connecté déjà renseignées
	 * @param username le nom de l'utilisateur connecté
	 * @return une liste de valeurs
	 */
	public static ArrayList<ArrayList<String>> findAllAttributes(String username) {
		
        // -----------------------------
        // Déclaration des variables
        // ------------------------------
		
        String eclats = "http://purl.org/fr/eclats/resource/";
        ValueFactory vf = SimpleValueFactory.getInstance();
		IRI currentUser = vf.createIRI(eclats + username);

		// La liste contenant les listes associées au attributs d'une interprétation
		ArrayList<ArrayList<String>> res = new ArrayList<ArrayList<String>>();
		// Les listes comportant les valeurs pour chaque attribut
		ArrayList<String> allLemmatizedResponses = new ArrayList<String>();
		ArrayList<String> allDesignating = new ArrayList<String>();
		ArrayList<String> allGrounds = new ArrayList<String>();
		ArrayList<String> allSpecificGrounds = new ArrayList<String>();
		ArrayList<String> allFewEtymon = new ArrayList<String>();
		ArrayList<String> allFewEtymonSignified = new ArrayList<String>();
		ArrayList<String> allFewEtymonReference = new ArrayList<String>();
		
		try (RepositoryConnection connection = Queries.REPOSITORY.getConnection()) {
			// Préparation de la requête SPARQL permettant de lister des interprétations
			TupleQuery tupleQuery = connection.prepareTupleQuery(QueryLanguage.SPARQL, Queries.getQuery("ALL_ATTRIBUTES"));
			// Binding du nom de l'utilisateur courant
            tupleQuery.setBinding("currentUser", currentUser);

			LOGGER.info("\n ---------------------- Query ALL_ATTRIBUTES -------------------------- \n"
					+ " SetBinding currentUser : " + currentUser + "\n"
					+ Queries.getQuery("ALL_ATTRIBUTES"));
			
			// exécution de la requête
			try (TupleQueryResult rs = tupleQuery.evaluate()) {
				while (rs.hasNext()) {
					BindingSet bindingSet = rs.next();
					if (!bindingSet.getValue("lemmatizedResponse").stringValue().equals("") &&
						!allLemmatizedResponses.contains(bindingSet.getValue("lemmatizedResponse").stringValue())) {
						allLemmatizedResponses.add(bindingSet.getValue("lemmatizedResponse").stringValue());
					}
					if (!bindingSet.getValue("designating").stringValue().equals("") &&
						!allDesignating.contains(bindingSet.getValue("designating").stringValue())) {
						allDesignating.add(bindingSet.getValue("designating").stringValue());
					}
					if (!bindingSet.getValue("ground").stringValue().equals("") &&
						!allGrounds.contains(bindingSet.getValue("ground").stringValue())) {
						allGrounds.add(bindingSet.getValue("ground").stringValue());
					}
					if (!bindingSet.getValue("specificGround").stringValue().equals("") &&
						!allSpecificGrounds.contains(bindingSet.getValue("specificGround").stringValue())) {
						allSpecificGrounds.add(bindingSet.getValue("specificGround").stringValue());
					}
					if (!bindingSet.getValue("fewEtymon").stringValue().equals("") &&
						!allFewEtymon.contains(bindingSet.getValue("fewEtymon").stringValue())) {
						allFewEtymon.add(bindingSet.getValue("fewEtymon").stringValue());
						}
					if (!bindingSet.getValue("fewEtymonSignified").stringValue().equals("") &&
						!allFewEtymonSignified.contains(bindingSet.getValue("fewEtymonSignified").stringValue())) {
						allFewEtymonSignified.add(bindingSet.getValue("fewEtymonSignified").stringValue());
						}
					if (!bindingSet.getValue("fewEtymonReference").stringValue().equals("") &&
						!allFewEtymonReference.contains(bindingSet.getValue("fewEtymonReference").stringValue())) {
						allFewEtymonReference.add(bindingSet.getValue("fewEtymonReference").stringValue());
						}
				}
			}
		}
		
        // -----------------------------
        // Ajout des tableaux de valeurs dans le tableau 'général'
        // ------------------------------
		Collections.sort(allLemmatizedResponses);
		Collections.sort(allDesignating);
		Collections.sort(allGrounds);
		Collections.sort(allSpecificGrounds);
		Collections.sort(allFewEtymon);
		Collections.sort(allFewEtymonSignified);
		Collections.sort(allFewEtymonReference);
		
		res.add(allLemmatizedResponses);		
		res.add(allDesignating);
		res.add(allGrounds);
		res.add(allSpecificGrounds);
		res.add(allFewEtymon);
		res.add(allFewEtymonSignified);
		res.add(allFewEtymonReference);
		
		return res;
	}
}
