package fr.eclatsgeoling.eclatsAppsBackEnd.dao;

import java.util.ArrayList;
import java.util.List;

import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.QueryLanguage;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.eclatsgeoling.eclatsAppsBackEnd.classes.SurveyPoint;

public class SurveyPointDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(SurveyPointDAO.class);

    /**
     * Fonction qui permet de récupérer la liste de toutes lespoints d'enquête
     * contenues dans le repository
     *
     * @return la liste de toutes les cartes contenues dans le repository
     */
    public static List<SurveyPoint> findAllSurveyPoints() {

        // -----------------------------
        // Déclaration des variables
        // ------------------------------
        List<SurveyPoint> res = new ArrayList<>();
        int previousSpIdentifier = 0;
        boolean first = true;
        SurveyPoint currentSurveyPoint = null;
        // -----------------------------
        // Connection au repository
        // ------------------------------
        try (RepositoryConnection connection = Queries.REPOSITORY.getConnection()) {
            // Préparation de la requête SPARQL qui permet de récupérer la liste de tous les
            // points d'enquête
            TupleQuery tupleQuery = connection.prepareTupleQuery(QueryLanguage.SPARQL,
                    Queries.getQuery("FIND_ALL_SURVEYPOINTS"));
            LOGGER.info("\n ---------------------- Query FIND_ALL_SURVEYPOINTS -------------------------- \n"
                    + Queries.getQuery("FIND_ALL_SURVEYPOINTS"));

            // exécution de la requête
            try (TupleQueryResult rs = tupleQuery.evaluate()) {
                while (rs.hasNext()) {
                    // Each result is represented by a BindingSet, which corresponds to a result row
                    BindingSet bindingSet = rs.next();

                    if (first || previousSpIdentifier != Integer.parseInt(bindingSet.getValue("spIdentifier").stringValue())) {
                        if (first) {
                            first = false;
                        } else {
                            res.add(currentSurveyPoint);
                        }
                        currentSurveyPoint = new SurveyPoint(
                                Integer.parseInt(bindingSet.getValue("spIdentifier").stringValue()),
                                bindingSet.getValue("placeName").stringValue(),
                                (bindingSet.getValue("dbPediaLink") == null) ? "pas de lien DBpedia"
                                : bindingSet.getValue("dbPediaLink").stringValue(),
                                (bindingSet.getValue("bnfLink") == null) ? "pas de lien BNF"
                                : bindingSet.getValue("bnfLink").stringValue(),
                                (bindingSet.getValue("gallicaNoteBookLink") == null) ? "pas de Carnet"
                                : bindingSet.getValue("gallicaNoteBookLink").stringValue(),
                                bindingSet.getValue("country").stringValue(),
                                bindingSet.getValue("latitude").stringValue(),
                                bindingSet.getValue("longitude").stringValue(),
                                bindingSet.getValue("dateEnquete").stringValue(),
                                Integer.parseInt(bindingSet.getValue("nbTemoins").stringValue()),
                                bindingSet.getValue("sexeT1").stringValue(),
                                bindingSet.getValue("ageT1").stringValue());
                        currentSurveyPoint.addInfosTemoins(bindingSet.getValue("temoin1").stringValue());
                        previousSpIdentifier = Integer.parseInt(bindingSet.getValue("spIdentifier").stringValue());

                    }
                    if (bindingSet.getValue("temoin2") != null) {
                        currentSurveyPoint.addInfosTemoins(bindingSet.getValue("temoin2").stringValue());
                    }
                    if (bindingSet.getValue("temoin3") != null) {
                        currentSurveyPoint.addInfosTemoins(bindingSet.getValue("temoin3").stringValue());
                    }
                    if (bindingSet.getValue("temoin4") != null) {
                        currentSurveyPoint.addInfosTemoins(bindingSet.getValue("temoin4").stringValue());
                    }
                }
            }
        }
        return res;
    }

    /**
     * Fonction qui permet de retrouver un point d'enquête à partir de son
     * identifiant.
     *
     * @param spIndentifier l'identifiant du point d'enquête à renvoyer
     * @return le Point d'enquête à retrouver avec tous ses attributs
     */
    public static SurveyPoint findSurveyPoint(String spIndentifier) {

        // -----------------------------
        // Déclaration des variables
        // ------------------------------
        SurveyPoint surveyPoint = null;

        // -----------------------------
        // Connection au repository
        // ------------------------------
        try (RepositoryConnection connection = Queries.REPOSITORY.getConnection()) {
            ValueFactory vf = SimpleValueFactory.getInstance();
            // Préparation de la requête SPARQL qui permet de trouver un point d'enquête
            TupleQuery tupleQuery = connection.prepareTupleQuery(QueryLanguage.SPARQL,
                    Queries.getQuery("FIND_SURVEYPOINT"));

            // Insertion de l'URI correspondant au point d'enquête dans la requête SPARQL
            tupleQuery.setBinding("surveyPoint",
                    vf.createIRI("http://purl.org/fr/eclats/resource/surveyPoint" + spIndentifier));

            LOGGER.info("\n ---------------------- Query FIND_SURVEYPOINTS -------------------------- \n"
                    + " SetBinding surveyPoint : http://purl.org/fr/eclats/resource/surveyPoint" + spIndentifier + "\n"
                    + Queries.getQuery("FIND_SURVEYPOINT"));

            try (TupleQueryResult rs = tupleQuery.evaluate()) {
                while (rs.hasNext()) {
                    BindingSet bindingSet = rs.next();
                    if (bindingSet == null) {
                        surveyPoint = null;
                    } else {
                        surveyPoint = new SurveyPoint(Integer.parseInt(spIndentifier),
                                bindingSet.getValue("placeName").stringValue(),
                                (bindingSet.getValue("dbPediaLink") == null) ? "pas de lien DBpedia"
                                : bindingSet.getValue("dbPediaLink").stringValue(),
                                (bindingSet.getValue("bnfLink") == null) ? "pas de lien BNF"
                                : bindingSet.getValue("bnfLink").stringValue(),
                                (bindingSet.getValue("gallicaNoteBookLink") == null) ? "pas de Carnet"
                                : bindingSet.getValue("gallicaNoteBookLink").stringValue(),
                                bindingSet.getValue("country").stringValue(),
                                bindingSet.getValue("latitude").stringValue(),
                                bindingSet.getValue("longitude").stringValue(),
                                bindingSet.getValue("dateEnquete").stringValue(),
                                Integer.parseInt(bindingSet.getValue("nbTemoins").stringValue()),
                                bindingSet.getValue("sexeT1").stringValue(),
                                bindingSet.getValue("ageT1").stringValue());
                    }
                }
            }
        }
        return surveyPoint;
    }

    public static String getSurveyPointsCoords() {
        StringBuilder res = new StringBuilder("{\"type\":\"FeatureCollection\",\"features\":[");
        String featureString = "{\"type\":\"Feature\",\"id\":\"%s\"," + "\"geometry\":" + "{\"type\":\"Point\","
                + "\"coordinates\":[%s,%s]" + "}," + "\"properties\": {" + "\"spIdentifier\": %s,"
                + "\"placeName\": \"%s\"," + "\"country\": \"%s\"" + "}," + "\"geometry_name\":\"the_geom\"" + "}";
        try (RepositoryConnection connection = Queries.REPOSITORY.getConnection()) {
            // Préparation de la requête SPARQL qui permet de récupérer la liste de tous les
            // points d'enquête
            TupleQuery tupleQuery = connection.prepareTupleQuery(QueryLanguage.SPARQL,
                    Queries.getQuery("FIND_ALL_SURVEYPOINTS"));

            LOGGER.info("\n ---------------------- Query FIND_ALL_SURVEYPOINTS -------------------------- \n"
                    + Queries.getQuery("FIND_ALL_SURVEYPOINTS"));

            boolean firstFeature = true;
            // exécution de la requête
            try (TupleQueryResult rs = tupleQuery.evaluate()) {
                while (rs.hasNext()) {
                    // Each result is represented by a BindingSet, which corresponds to a result row
                    BindingSet bindingSet = rs.next();
                    // Récupération des valeurs à partir des URI
                    int spIdentifier = Integer.parseInt(bindingSet.getValue("spIdentifier").stringValue());
                    String placeName = bindingSet.getValue("placeName").stringValue();
                    String country = bindingSet.getValue("country").stringValue();
                    String latitude = bindingSet.getValue("latitude").stringValue();
                    String longitude = bindingSet.getValue("longitude").stringValue();
                    if (!firstFeature) {
                        res.append(",");
                    }
                    res.append(String.format(featureString, spIdentifier, longitude, latitude, spIdentifier, placeName,
                            country));
                    firstFeature = false;

                }
            }
        }
        res.append("],\"crs\":{\"type\":\"name\",\"properties\":{\"name\":\"urn:ogc:def:crs:EPSG::4326\"}}}");
        return res.toString();

    }
}
