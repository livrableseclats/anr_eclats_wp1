package fr.eclatsgeoling.eclatsAppsBackEnd.dao;

import fr.eclatsgeoling.eclatsAppsBackEnd.classes.EuropeanaResource;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.QueryLanguage;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.util.Repositories;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author seffarm
 */
public class EuropeanaResourcesDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(EuropeanaResourcesDAO.class);

    /**
     * Ajoute la ressource Europeana au tripleStore
     *
     * @param europeanaResourceForm les éléments caractérisant la ressource
     * Europeana a ajouter au tripleStore
     */
    public static void addEuropeanaResourceForMap(LinkedHashMap<String, String> europeanaResourceForm) {
        EuropeanaResource newEuropeanaResource = new EuropeanaResource(
                europeanaResourceForm.get("ark"),
                europeanaResourceForm.get("mapIdentifier"),
                europeanaResourceForm.get("username"),
                europeanaResourceForm.get("title"),
                europeanaResourceForm.get("dcDescription"),
                europeanaResourceForm.get("dcCreator"),
                europeanaResourceForm.get("dataProvider"),
                europeanaResourceForm.get("country"),
                europeanaResourceForm.get("edmPreview"));

        ValueFactory vf = SimpleValueFactory.getInstance();

        // Définition des préfixes
        String eclats = "http://purl.org/fr/eclats/resource/";
        String eclatsonto = "http://purl.org/fr/eclats/ontology#";
        IRI context = vf.createIRI("http://purl.org/fr/eclats/graph#europeanaResources");
        IRI ark = vf.createIRI(newEuropeanaResource.getArk());
        Repositories.consume(Queries.REPOSITORY, (connection) -> {

            // -----------------------------
            // Connection au repository
            // ------------------------------
            connection.add(vf.createStatement(ark, RDF.TYPE, vf.createIRI(eclatsonto + "EuropeanaResource")), context);

            connection.add(vf.createStatement(vf.createIRI(eclats + "map" + getMapId(newEuropeanaResource.getMapIdentifier())),
                    vf.createIRI(eclatsonto + "hasEuropeanaResource"), ark), context);

            connection.add(vf.createStatement(ark, vf.createIRI(eclatsonto + "registeredBy"),
                    vf.createIRI(eclats + europeanaResourceForm.get("username"))), context);
            connection.add(vf.createStatement(ark, vf.createIRI(eclatsonto + "title"),
                    vf.createLiteral(europeanaResourceForm.get("title"))), context);
            connection.add(vf.createStatement(ark, vf.createIRI(eclatsonto + "dcDescription"),
                    vf.createLiteral(europeanaResourceForm.get("dcDescription"))), context);
            connection.add(vf.createStatement(ark, vf.createIRI(eclatsonto + "dcCreator"),
                    vf.createLiteral(europeanaResourceForm.get("dcCreator"))), context);
            connection.add(vf.createStatement(ark, vf.createIRI(eclatsonto + "dataProvider"),
                    vf.createLiteral(europeanaResourceForm.get("dataProvider"))), context);
            connection.add(vf.createStatement(ark, vf.createIRI(eclatsonto + "country"),
                    vf.createLiteral(europeanaResourceForm.get("country"))), context);
            connection.add(vf.createStatement(ark, vf.createIRI(eclatsonto + "edmPreview"),
                    vf.createLiteral(europeanaResourceForm.get("edmPreview"))), context);
        });
    }

    /**
     * Recherche la liste des ressources Europeana associées à la carte dont
     * l'identifiant est passé en paramètre
     *
     * @param mapIdentifier l'identifiant de la carte à laquelle il faut
     * récupérer les ressources Europeana
     * @return une liste de ressources Europeana associées à la carte dont
     * l'idientiant est passé en paramètre
     */
    public static List<EuropeanaResource> findEuropeanaResourcesForMap(String mapIdentifier) {
        List<EuropeanaResource> europeanaResourcesList = new ArrayList<>();

        // -----------------------------
        // Connection au repository
        // ------------------------------
        try (RepositoryConnection connection = Queries.REPOSITORY.getConnection()) {

            ValueFactory ResourceFactory = connection.getValueFactory();
            TupleQuery tupleQuery = connection.prepareTupleQuery(QueryLanguage.SPARQL,
                    Queries.getQuery("FIND_EUROPEANARESOURCES"));
            tupleQuery.setBinding("map", ResourceFactory.createIRI("http://purl.org/fr/eclats/resource/map" + getMapId(mapIdentifier)));

            LOGGER.info("\n ---------------------- Query FIND_EUROPEANARESOURCES -------------------------- \n"
                    + " SetBinding map : http://purl.org/fr/eclats/resource/map" + getMapId(mapIdentifier) + "\n"
                    + Queries.getQuery("FIND_EUROPEANARESOURCES"));
            // exécution de la requête SPARQL
            try (TupleQueryResult rs = tupleQuery.evaluate()) {

                while (rs.hasNext()) {
                    BindingSet bindingSet = rs.next();

                    EuropeanaResource europeanaResource = new EuropeanaResource(
                            bindingSet.getValue("ark").stringValue(),
                            bindingSet.getValue("mapIdentifier").stringValue(),
                            bindingSet.getValue("username").stringValue().substring(35),
                            bindingSet.getValue("title").stringValue(),
                            bindingSet.getValue("dcDescription").stringValue(),
                            bindingSet.getValue("dcCreator").stringValue(),
                            bindingSet.getValue("dataProvider").stringValue(),
                            bindingSet.getValue("country").stringValue(),
                            bindingSet.getValue("edmPreview").stringValue());

                    europeanaResourcesList.add(europeanaResource);
                }
            }
        }
        return europeanaResourcesList;
    }

    /**
     * Permet de formatter l'identifiant de la carte pour créer l'IRI
     * correspondant à la carte Exemple: si mapIdenfier = "1" => renvoie "0001"
     *
     * @param mapIdentifier
     * @return
     */
    public static String getMapId(String mapIdentifier) {
        if (mapIdentifier.charAt(0) == 'I') {
            switch (mapIdentifier) {
                case "I":
                    return "000I";
                case "II":
                    return "00II";
                case "III":
                    return "0III";
            }
        }
        // effectue le padding des 0 devant l'identifiant de la carte pour que le
        // numméro de la carte soit sur 4 digits
        int offset = 0;
        if (mapIdentifier.endsWith("AB")) {
            offset = 2;
        } else if (mapIdentifier.endsWith("A") || mapIdentifier.endsWith("B")) {
            offset = 1;
        }
        return ("0000" + mapIdentifier).substring(mapIdentifier.length() - offset);
    }

}
