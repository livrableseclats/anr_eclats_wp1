package fr.eclatsgeoling.eclatsAppsBackEnd.dao;

import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.UUID;
import java.util.Map.Entry;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.Model;
import org.eclipse.rdf4j.model.Statement;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.QueryLanguage;
import org.eclipse.rdf4j.query.QueryResults;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.RepositoryResult;
import org.eclipse.rdf4j.repository.util.Repositories;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.eclatsgeoling.eclatsAppsBackEnd.classes.MapComment;
import fr.eclatsgeoling.eclatsAppsBackEnd.classes.User;

public class MapCommentDAO {
	private static final Logger LOGGER = LoggerFactory.getLogger(MapCommentDAO.class);

	/**
	 * Fonction qui permet d'ajouter un commentaire à une carte
	 * @param mapCommentForm le formulaire renseigné par l'utilisateur dans la partie client de l'application
	 */
	public static void addMapComment(LinkedHashMap<String, String> mapCommentForm) {

		// -----------------------------
		// Déclaration des variables
		// ------------------------------
		String author = null;
		String mapComment = null;
		String mapId = null;

		// -----------------------------
		// Assignation des valeurs à partir du formulaire
		// ------------------------------

		for (Entry<String, String> entry : mapCommentForm.entrySet()) {

			if (entry.getKey().equals("author")) {
				author = entry.getValue();
			}

			if (entry.getKey().equals("mapComment")) {
				mapComment = entry.getValue();
			}

			if (entry.getKey().equals("mapId")) {
				mapId = entry.getValue();
			}

		}

		updateRepository(mapId, author, mapComment);
	}

	/**
	 * Mise à jour du graphe à partir des données du formulaire renseigné par l'utilisateur
	 * @param mapId
	 * @param author
	 * @param mapComment
	 */
	public static void updateRepository(String mapId, String author, String mapComment) {
		ValueFactory vf = SimpleValueFactory.getInstance();

		// Définition des préfixes
		String eclats = "http://purl.org/fr/eclats/resource/";
		String eclatsonto = "http://purl.org/fr/eclats/ontology#";
		IRI context = vf.createIRI("http://purl.org/fr/eclats/graph#ALF_interpretations");

		// Création d'un random Id pour le commentaire à ajouter
		IRI mapCommentId = vf.createIRI(eclats + UUID.randomUUID().toString());

		// -----------------------------
		// Connection au repository
		// ------------------------------

		Repositories.consume(Queries.REPOSITORY, (connection) -> {

			// -----------------------------
			// Association au type MapComment et à la carte
			// ------------------------------

			connection.add(vf.createStatement(mapCommentId, RDF.TYPE, vf.createIRI(eclatsonto + "MapComment")), context);

			connection.add(vf.createStatement(vf.createIRI(eclats + "map" + mapId),
					vf.createIRI(eclatsonto + "hasMapComment"), mapCommentId), context);

			// -----------------------------
			// Association à l'utilisateur, ajout d'une date et enregistrement du
			// commentaire
			// ------------------------------

			connection.add(vf.createStatement(mapCommentId, vf.createIRI(eclatsonto + "author"),
					vf.createIRI(eclats + author)), context);

			connection.add(vf.createStatement(mapCommentId, vf.createIRI(eclatsonto + "mapCommentDate"),
					vf.createLiteral(System.currentTimeMillis())), context);

			connection.add(vf.createStatement(mapCommentId, vf.createIRI(eclatsonto + "mapComment"),
					vf.createLiteral(mapComment)), context);

		});

	}

	/**
	 * Fonction qui permet de récupérer la liste des commentaires associés à une carte
	 * @param mapId l'id de la carte
	 * @return une liste de commentaires
	 */
	public static ArrayList<MapComment> findAllMapComment(String mapId) {

		ArrayList<MapComment> mapCommentList = new ArrayList<>();

		// -----------------------------
		// Connection au repository
		// ------------------------------

		try (RepositoryConnection connection = Queries.REPOSITORY.getConnection()) {

			ValueFactory ResourceFactory = connection.getValueFactory();

			TupleQuery tupleQuery = connection.prepareTupleQuery(QueryLanguage.SPARQL,
					Queries.getQuery("FIND_ALL_MAPCOMMENTS"));
			tupleQuery.setBinding("mapId", ResourceFactory.createIRI("http://purl.org/fr/eclats/resource/map" + mapId));

			LOGGER.info("\n ---------------------- Query FIND_ALL_MAPCOMMENTS -------------------------- \n"
					+ " SetBinding mapId : http://purl.org/fr/eclats/resource/map" + mapId + "\n"
					+ Queries.getQuery("FIND_ALL_MAPCOMMENTS"));

			// exécution de la requête SPARQL
			try (TupleQueryResult rs = tupleQuery.evaluate()) {

				while (rs.hasNext()) {
					BindingSet bindingSet = rs.next();

					MapComment mapComment = new MapComment(bindingSet.getValue("mapCommentId").stringValue(),
							Long.parseLong(bindingSet.getValue("mapCommentDate").stringValue()),
							new User(bindingSet.getValue("author").stringValue().substring(35), 
									bindingSet.getValue("organization").stringValue()),
							bindingSet.getValue("mapComment").stringValue());

					mapCommentList.add(mapComment);

				}
			}
		}

		return mapCommentList;
	}

	/**
	 * Fonction qui permet de supprimer une interprétation assopciée à une réponse dans une carte 
	 * @param interpretationId l'id de l'interprétation à supprimer
	 */
	public static void deleteMapComment(String mapCommentId) {
		// -----------------------------
		// Connection au repository
		// ------------------------------
		try (RepositoryConnection connection = Queries.REPOSITORY.getConnection()) {

			// Cas d'une suppression ou d'une édition
			ValueFactory vf = connection.getValueFactory();

			// Suppression des triplets associés au commentaire
			connection.remove(vf.createIRI(mapCommentId), null, null);

			// Suppression prédicat entre la carte et le commentaire
			RepositoryResult<Statement> statements = connection.getStatements(null, null, vf.createIRI(mapCommentId));
			Model modelToRemove = QueryResults.asModel(statements);
			connection.remove(modelToRemove);

		}
	}

	public static MapComment findMapCommentToEdit(String mapCommentIdToEdit) {

		MapComment mapComment = null;

		// -----------------------------
		// Connection au repository
		// ------------------------------

		try (RepositoryConnection connection = Queries.REPOSITORY.getConnection()) {
			
			ValueFactory vf = connection.getValueFactory();

			TupleQuery tupleQuery = connection.prepareTupleQuery(QueryLanguage.SPARQL,
					Queries.getQuery("FIND_MAPCOMMENT"));
			
			tupleQuery.setBinding("mapCommentId", vf.createIRI(mapCommentIdToEdit));

			LOGGER.info("\n ---------------------- Query FIND_MAPCOMMENT -------------------------- \n"
					+ " SetBinding mapCommentId :" + mapCommentIdToEdit + "\n"
					+ Queries.getQuery("FIND_MAPCOMMENT"));

			// exécution de la requête SPARQL
			try (TupleQueryResult rs = tupleQuery.evaluate()) {

				while (rs.hasNext()) {
					BindingSet bindingSet = rs.next();

					mapComment = new MapComment(bindingSet.getValue("mapCommentId").stringValue(),
							Long.parseLong(bindingSet.getValue("mapCommentDate").stringValue()),
							new User(bindingSet.getValue("author").stringValue().substring(35), 
									bindingSet.getValue("organization").stringValue().substring(35)),
							bindingSet.getValue("mapComment").stringValue());

				}
			}

		}

		return mapComment;
	}

}
