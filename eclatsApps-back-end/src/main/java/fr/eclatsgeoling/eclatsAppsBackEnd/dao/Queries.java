package fr.eclatsgeoling.eclatsAppsBackEnd.dao;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.HashMap;
import java.util.Map;
import org.eclipse.rdf4j.repository.http.HTTPRepository;
import org.springframework.util.ResourceUtils;

public class Queries {

    /**
     * Chemin vers le repository
     */
    public static final HTTPRepository REPOSITORY = new HTTPRepository("http://localhost:7200/repositories/ECLATS5"); 
    private static Map<String, String> queriesMap = new HashMap<>();

    static {
        REPOSITORY.initialize();
        REPOSITORY.setUsernameAndPassword("eclats5", "eclats5$@*");

        try {
            File file = ResourceUtils.getFile("classpath:queries.txt");
//                        BufferedReader br = new BufferedReader(
//                new FileReader(file));
			BufferedReader br = new BufferedReader(new InputStreamReader(
                new FileInputStream(file), "UTF-8"));
            String line;
            boolean queryIdLine = true; // true si la ligne est une ligne significative
            // et est la première ligne d'une query , c'est à dire son identifiant

            String queryId = null ;
            StringBuilder query = null;
            while ((line = br.readLine()) != null) {
                line = line.trim();
                if (! line.isEmpty() && !line.startsWith("#")) {
                    // pas un commentaire
                    if (line.startsWith("--")) {
                        // fin de la query
                        queriesMap.put(queryId, query.toString());
                        queryIdLine = true;
                    } else {
                        if (queryIdLine) {
                            queryId = line;
                            query = new StringBuilder();
                            queryIdLine = false;
                        } else {
                            query.append(line).append("\n");
                        }
                    }
                }
            }
        } catch (IOException e) {
            System.out.println("error chargement fichier");
        }
    }
    
    public static String getQuery(String queryId) {
        return queriesMap.get(queryId);
    }

}
