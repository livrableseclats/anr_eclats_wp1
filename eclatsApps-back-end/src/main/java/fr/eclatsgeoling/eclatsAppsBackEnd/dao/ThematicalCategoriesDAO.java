package fr.eclatsgeoling.eclatsAppsBackEnd.dao;

import java.io.IOException;
import java.util.List;

import org.eclipse.rdf4j.query.QueryLanguage;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.eclatsgeoling.eclatsAppsBackEnd.classes.Map;

public class ThematicalCategoriesDAO {
	private static final Logger LOGGER = LoggerFactory.getLogger(MapDAO.class);

	/**
	 * Méthode permettant de récupérer la liste des cartes associées aux thèmes
	 * passés en paramètre
	 * 
	 * @param thematicCat la liste des thèmes renseignés par l'utilisateur
	 * @return la liste des cartes associées aux thèmes passés en paramètre
	 */
	public static List<Map> findMapsWithThematicCat(String thematicCat)
			throws JsonParseException, JsonMappingException, IOException {
		// Conversion de la chaine de charactères
		TypeReference<List<String>> type = new TypeReference<List<String>>() {
		};
		List<String> formattedThematicCat = new ObjectMapper().readValue(thematicCat, type);
		String query = Queries.getQuery("FIND_MAPS_WITH_THEMATIC_CAT");
		StringBuilder replaceValuesForQueryString = new StringBuilder("");

		for (int i = 0; i < formattedThematicCat.size(); i++) {
			replaceValuesForQueryString.append("(<http://purl.org/fr/eclats/ontologies/themes#");
			replaceValuesForQueryString.append(formattedThematicCat.get(i));
			replaceValuesForQueryString.append(">)\n");
		}

		query = String.format(query, replaceValuesForQueryString);

		try (RepositoryConnection connection = Queries.REPOSITORY.getConnection()) {

			LOGGER.info("\n ---------------------- Query FIND_MAPS_WITH_THEMATIC_CAT -------------------------- \n"
					+ query);

			return MapDAO.executeQueryForMaps(connection.prepareTupleQuery(QueryLanguage.SPARQL, query));
		}
	}

}
