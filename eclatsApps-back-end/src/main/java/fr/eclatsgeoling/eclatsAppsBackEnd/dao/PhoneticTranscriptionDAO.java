package fr.eclatsgeoling.eclatsAppsBackEnd.dao;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;
import java.util.List;
import java.util.UUID;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.QueryLanguage;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.query.Update;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class PhoneticTranscriptionDAO {
	private static final Logger LOGGER = LoggerFactory.getLogger(MapDAO.class);

	/**
	 * Permet d'ajouter une transcription phonétiques à une liste d'entrées
	 * d'inventaires
	 * 
	 * @param params contient l'id de la carte, les ids des entrées d'inventaire et
	 *               la transcription phonétique à associer
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	public static void addPhoneticTranscription(String mapId, String phoneticTranscription, String phoneticTranscriptionInfo, String circleList)
			throws JsonParseException, JsonMappingException, IOException {
		TypeReference<List<LinkedHashMap<String, String>>> type = new TypeReference<List<LinkedHashMap<String, String>>>() {
		};
		List<LinkedHashMap<String, String>> newCircleList = new ObjectMapper().readValue(circleList, type);
		List<LinkedHashMap<String, String>> replacePhoneticTranscriptionList = new ArrayList<LinkedHashMap<String, String>>();
		List<LinkedHashMap<String, String>> addPhoneticTranscriptionList = new ArrayList<LinkedHashMap<String, String>>();
		for (int i = 0; i < newCircleList.size(); i++) {
			System.out.println(newCircleList.get(i));
			if (newCircleList.get(i).get("replace").equals("true")) {
				replacePhoneticTranscriptionList.add(newCircleList.get(i));
			} else {
				addPhoneticTranscriptionList.add(newCircleList.get(i));
			}
		}
		if (replacePhoneticTranscriptionList.size() != 0) {
			executeReplace(mapId, phoneticTranscription, phoneticTranscriptionInfo, replacePhoneticTranscriptionList);
		}
		if (addPhoneticTranscriptionList.size() != 0) {
			executeAdd(mapId, phoneticTranscription, phoneticTranscriptionInfo, addPhoneticTranscriptionList);
		}
	}

	public static void executeAdd(String mapId, String phoneticTranscription, String phoneticTranscriptionInfo, List<LinkedHashMap<String, String>> circleList) {
		String eclats = "http://purl.org/fr/eclats/resource/";
		String eclatsonto = "http://purl.org/fr/eclats/ontology#";

		try (RepositoryConnection connection = Queries.REPOSITORY.getConnection()) {
			ValueFactory ResourceFactory = connection.getValueFactory();
			IRI phoneticTranscriptionContext = ResourceFactory.createIRI("http://purl.org/fr/eclats/graph#usersPhoneticTranscriptions");
			IRI responseContext = ResourceFactory.createIRI("http://purl.org/fr/eclats/graph#ALF_newInventries");
			// Parcours de la liste des points, création des IRI correspondant aux entrées d'inventaires
			// et aux UUID des nouvelles réponses phonétiques
			for (int i = 0; i < circleList.size(); i++) {
				IRI inventoryEntryIdIRI = ResourceFactory
						.createIRI("http://purl.org/fr/eclats/resource/inventoryEntry_SP" + circleList.get(i).get("id")
								+ "_map" + mapId);
				IRI responseIdIRI = ResourceFactory.createIRI(eclats + UUID.randomUUID().toString());
				connection.add(ResourceFactory.createStatement(inventoryEntryIdIRI, ResourceFactory.createIRI(eclatsonto + "hasResponse"), responseIdIRI, responseContext));
				connection.add(ResourceFactory.createStatement(responseIdIRI, RDF.TYPE, ResourceFactory.createIRI(eclatsonto + "Response"), responseContext));
				connection.add(ResourceFactory.createStatement(responseIdIRI, ResourceFactory.createIRI(eclatsonto + "rousselotPhoneticForm"),
						ResourceFactory.createLiteral(phoneticTranscription), phoneticTranscriptionContext));
				connection.add(ResourceFactory.createStatement(responseIdIRI, ResourceFactory.createIRI(eclatsonto + "complementaryInformation"),
						ResourceFactory.createLiteral(phoneticTranscriptionInfo), phoneticTranscriptionContext));

			}
		}
	}

	public static void executeReplace(String mapId, String phoneticTranscription, String phoneticTranscriptionInfo, List<LinkedHashMap<String, String>> circleList) {
		try (RepositoryConnection connection = Queries.REPOSITORY.getConnection()) {
			LOGGER.info("\n -------------------- Query REPLACE_PHONETIC_TRANSCRIPTION ------------------------ \n" + Queries.getQuery("REPLACE_PHONETIC_TRANSCRIPTION"));
			// Préparation de la requête SPARQL
			Update updateQuery = connection.prepareUpdate(QueryLanguage.SPARQL, Queries.getQuery("REPLACE_PHONETIC_TRANSCRIPTION"));
			ValueFactory ResourceFactory = connection.getValueFactory();
			
			for (int i = 0; i < circleList.size(); i++) {
			updateQuery.setBinding("inventoryEntry",
					ResourceFactory.createIRI("http://purl.org/fr/eclats/resource/inventoryEntry_SP"
							+ circleList.get(i).get("id") + "_map" + mapId));		
			updateQuery.setBinding("phoneticTranscriptionToReplace",
					ResourceFactory.createLiteral(circleList.get(i).get("initialPhoneticTranscription")));
			updateQuery.setBinding("complementaryInformationToReplace",
					ResourceFactory.createLiteral(circleList.get(i).get("initialComplementaryInformation")));
			updateQuery.setBinding("newPhoneticTranscription",
					ResourceFactory.createLiteral(phoneticTranscription));
			updateQuery.setBinding("newComplementaryInformation", 
					ResourceFactory.createLiteral(phoneticTranscriptionInfo));
			updateQuery.execute();
			}			
		}
	}
	
	/**
	 * Supprime la transcription phonétique associées aux entrées d'inventaires contenues dans la liste des disques
	 * @param mapId
	 * @param phoneticTranscriptionToDelete
	 * @param circleList
	 * @throws JsonParseException
	 * @throws JsonMappingException
	 * @throws IOException
	 */
	public static void deletePhoneticTranscription(String mapId, String phoneticTranscriptionToDelete, String complementaryInformationToDelete, String circleList) throws JsonParseException, JsonMappingException, IOException {
		TypeReference<List<LinkedHashMap<String, String>>> type = new TypeReference<List<LinkedHashMap<String, String>>>() {
		};
		List<LinkedHashMap<String, String>> newCircleList = new ObjectMapper().readValue(circleList, type);
		
		try (RepositoryConnection connection = Queries.REPOSITORY.getConnection()) {
			LOGGER.info("\n -------------------- Query GET_COUNT_OF_RESPONSES ------------------------ \n" + Queries.getQuery("GET_COUNT_OF_RESPONSES"));
			// Préparation de la requête SPARQL
			TupleQuery tupleQuery = connection.prepareTupleQuery(QueryLanguage.SPARQL, Queries.getQuery("GET_COUNT_OF_RESPONSES"));
			ValueFactory ResourceFactory = connection.getValueFactory();
			
			for (int i = 0; i < newCircleList.size(); i++) {
			tupleQuery.setBinding("inventoryEntry", ResourceFactory.createIRI("http://purl.org/fr/eclats/resource/inventoryEntry_SP"
					+ newCircleList.get(i).get("id") + "_map" + mapId));
			try (TupleQueryResult rs = tupleQuery.evaluate()) {
				while (rs.hasNext()) {
					BindingSet bindingSet = rs.next();
					if(Integer.parseInt(bindingSet.getValue("count").stringValue()) > 1 ) {
						Update updateQuery = connection.prepareUpdate(QueryLanguage.SPARQL, Queries.getQuery("DELETE_PHONETIC_TRANSCRIPTION"));
						updateQuery.setBinding("inventoryEntry",
								ResourceFactory.createIRI("http://purl.org/fr/eclats/resource/inventoryEntry_SP"
										+ newCircleList.get(i).get("id") + "_map" + mapId));		
						updateQuery.setBinding("phoneticTranscriptionDelete",
								ResourceFactory.createLiteral(phoneticTranscriptionToDelete));
						updateQuery.setBinding("complementaryInformationToDelete",
								ResourceFactory.createLiteral(complementaryInformationToDelete));
						updateQuery.execute();
						
					} else {						
						Update updateQuery = connection.prepareUpdate(QueryLanguage.SPARQL, Queries.getQuery("REPLACE_PHONETIC_TRANSCRIPTION"));
						updateQuery.setBinding("inventoryEntry",
								ResourceFactory.createIRI("http://purl.org/fr/eclats/resource/inventoryEntry_SP"
										+ newCircleList.get(i).get("id") + "_map" + mapId));		
						updateQuery.setBinding("phoneticTranscriptionToReplace",
								ResourceFactory.createLiteral(phoneticTranscriptionToDelete));
						updateQuery.setBinding("newPhoneticTranscription",
								ResourceFactory.createLiteral("forme phonétique non encore renseignée"));
						updateQuery.setBinding("complementaryInformationToReplace",
								ResourceFactory.createLiteral(complementaryInformationToDelete));
						updateQuery.setBinding("newComplementaryInformation", 
								ResourceFactory.createLiteral(""));
						updateQuery.execute();
						
						}
					}
				}
			}
		}			
	}
}
