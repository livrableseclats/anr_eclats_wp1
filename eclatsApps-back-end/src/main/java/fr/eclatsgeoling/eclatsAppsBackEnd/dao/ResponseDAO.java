package fr.eclatsgeoling.eclatsAppsBackEnd.dao;

import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedHashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.UUID;

import org.eclipse.rdf4j.model.IRI;
import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.model.impl.SimpleValueFactory;
import org.eclipse.rdf4j.model.vocabulary.RDF;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.QueryLanguage;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.eclipse.rdf4j.repository.RepositoryException;
import org.eclipse.rdf4j.repository.util.Repositories;
import org.eclipse.rdf4j.rio.RDFFormat;
import org.eclipse.rdf4j.rio.RDFParseException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import fr.eclatsgeoling.eclatsAppsBackEnd.classes.Etymon;
import fr.eclatsgeoling.eclatsAppsBackEnd.classes.Interpretation;
import fr.eclatsgeoling.eclatsAppsBackEnd.classes.Lemma;
import fr.eclatsgeoling.eclatsAppsBackEnd.classes.Response;
import fr.eclatsgeoling.eclatsAppsBackEnd.classes.User;


public class ResponseDAO {
	private static final Logger LOGGER = LoggerFactory.getLogger(ResponseDAO.class);

	// ---------------------------------------------------------------------------------------------
	// CONSULTATION D'INTERPRETATION
	// ---------------------------------------------------------------------------------------------
	/**
	 * Fonction qui permet de trouver toutes les réposnes et interprétations
	 * associées à une entrée d'inventaire Elle est appelée au moment où
	 * l'utilisateur souhaite consulter les annotations d'un point d'enquête
	 *
	 * @param inventoryEntry l'entrée d'inventaire cliquée dans le visualiseur
	 * @return une liste contenant la ou les réponses associées à l'entrée
	 *         d'inventaire et les interprétations qui lui sont associées
	 */
	public static ArrayList<Response> findAllInterpretations(String inventoryEntry) {

		// -----------------------------
		// Déclaration des variables
		// ------------------------------
		ArrayList<Response> responseList = new ArrayList<>();
		String prevResponseId = null;
		String prevInterpretationId = null;
		String prevLemmaId = null;
		Response response = null;
		Interpretation interpretation = null;
		Lemma lemma = null;
		boolean newRep = false;
		boolean newInterp = false;

		// -----------------------------
		// Connection au repository
		// ------------------------------
		try (RepositoryConnection connection = Queries.REPOSITORY.getConnection()) {

			ValueFactory ResourceFactory = connection.getValueFactory();

			// Préparation de la requête SPARQL qui permet de récupérer toutes les
			// interprétations
			TupleQuery tupleQuery = connection.prepareTupleQuery(QueryLanguage.SPARQL,
					Queries.getQuery("FIND_ALL_INTERPRETATIONS"));

			// Création de l'URI pour l'entrée d'inventaire concernée
			tupleQuery.setBinding("inventoryEntry", ResourceFactory.createIRI(inventoryEntry));

			LOGGER.info("\n ---------------------- Query FIND_ALL_INTERPRETATIONS -------------------------- \n"
					+ "SetBinding inventoryEntry : " + inventoryEntry + "\n"
					+ Queries.getQuery("FIND_ALL_INTERPRETATIONS"));

			// exécution de la requête SPARQL
			try (TupleQueryResult rs = tupleQuery.evaluate()) {

				while (rs.hasNext()) {
					BindingSet bindingSet = rs.next();
					String responseId = bindingSet.getValue("responseId").stringValue();
					String rousselotPhoneticForm = bindingSet.getValue("rousselotPhoneticForm").stringValue();
					// Si c'est une nouvelle réponse
					if (prevResponseId == null || !responseId.equals(prevResponseId)) {
						// "0" pour l'attribut spIdentifier car pas nécessaire ici.
						response = new Response(responseId, rousselotPhoneticForm, bindingSet.getValue("complementaryInformation").stringValue(), "0");
						prevResponseId = responseId;
						newRep = true;
					}

					// -----------------------------
					// Gestion de l'interprétation
					// ------------------------------
					// Si c'est une nouvelle interprétation
					if (prevInterpretationId == null
							|| !bindingSet.getValue("interpretationId").stringValue().equals(prevInterpretationId)) {

						interpretation = new Interpretation(bindingSet.getValue("interpretationId").stringValue(),
								bindingSet.getValue("lemmatizedResponse").stringValue(),
								bindingSet.getValue("ground").stringValue(),
								bindingSet.getValue("specificGround").stringValue(),
								bindingSet.getValue("designating").stringValue(),
								bindingSet.getValue("responseType").stringValue(),
								bindingSet.getValue("interpretationComment").stringValue(),
								Long.parseLong(bindingSet.getValue("interpretationDate").stringValue()),
								bindingSet.getValue("mapIdentifier").stringValue(),
								new User(bindingSet.getValue("author").stringValue().substring(35),
										bindingSet.getValue("organization").stringValue()),
								bindingSet.getValue("confidentiality").stringValue());

						interpretation.addAssociatedResponses(findAssociatedResponses(
								bindingSet.getValue("interpretationId").stringValue(), rousselotPhoneticForm));

						prevInterpretationId = bindingSet.getValue("interpretationId").stringValue();

						newInterp = true;
					}

					// -----------------------------
					// Gestion du lemme
					// ------------------------------
					// Si c'est un nouveau lemme
					if (prevLemmaId == null || !bindingSet.getValue("elt1").stringValue().equals(prevLemmaId)) {
						System.out.println("**** elt1 : " + bindingSet.getValue("elt1"));
						lemma = new Lemma(bindingSet.getValue("grammaticalNature").stringValue(),
								bindingSet.getValue("gender").stringValue(),
								bindingSet.getValue("number").stringValue(),
								bindingSet.getValue("lemmaComments").stringValue());
						// Ajout du lemme à l'interprétation
						interpretation.addLemma(lemma);
						// on garde l'id du lemme si le lemmes a plusieurs etymons les lignes suivantes
						// du
						// binding set porteront sur le même lemme qu'il ne faudra pas recréer.
						prevLemmaId = bindingSet.getValue("elt1").stringValue();

					}

					// -----------------------------
					// Gestion de l'étymon
					// ------------------------------
					lemma.addEtymon(new Etymon(bindingSet.getValue("fewEtymon").stringValue(),
							bindingSet.getValue("fewEtymonLanguage").stringValue(),
							bindingSet.getValue("fewEtymonSignified").stringValue(),
							bindingSet.getValue("fewEtymonReference").stringValue(),
							bindingSet.getValue("etymonComment").stringValue(),
							bindingSet.getValue("uncertainty").stringValue()));

					// -----------------------------
					// Ajout de l'interprétation à la réposne courante
					// ------------------------------
					// Si c'est c'est une nouvelle interprétation ou qu'on est dans le cas du fil
					// d'actualité
					if (newInterp) {
						response.addInterpretation(interpretation);
						newInterp = false;
					}

					// -----------------------------
					// Ajout de la réponse à la liste
					// ------------------------------
					if (newRep) {
						responseList.add(response);
						newRep = false;
					}
				}
			}

			return responseList;
		}
	}

	// ---------------------------------------------------------------------------------------------
	// ---------------------------------------------------------------------------------------------
	// CREATION D'INTERPRETATION
	// ---------------------------------------------------------------------------------------------
	// ---------------------------------------------------------------------------------------------
	/**
	 * Fonction qui permet de récupérer la liste des réponses associées à une entrée
	 * d'inventaire. Cette fonction est appelée notamment pour la consutation
	 * d'interprétations
	 *
	 * @param inventoryEntry l'entrée d'inventaire cliquée par l'utilisateur
	 * @return la liste des réponses associées à l'entrée d'inventaire.
	 */
	public static ArrayList<Response> findAllResponses(String inventoryEntry) {

		// -----------------------------
		// Déclaration des variables
		// ------------------------------
		// Initialisation de la liste qui contient les responses associées à une entrée
		// d'inventaire
		ArrayList<Response> responseList = new ArrayList<>();

		// -----------------------------
		// Connection au repository
		// ------------------------------
		try (RepositoryConnection connection = Queries.REPOSITORY.getConnection()) {
			ValueFactory ResourceFactory = connection.getValueFactory();
			TupleQuery tupleQuery = connection.prepareTupleQuery(QueryLanguage.SPARQL,
					Queries.getQuery("FIND_ALL_RESPONSES"));
			// Création de l'URI pour l'entrée d'inventaire concernée
			tupleQuery.setBinding("inventoryEntry", ResourceFactory.createIRI(inventoryEntry));

			LOGGER.info("\n ---------------------- Query FIND_ALL_RESPONSES -------------------------- \n"
					+ "SetBinding inventoryEntry : " + inventoryEntry + "\n"
					+ Queries.getQuery("FIND_ALL_RESPONSES"));

			// exécution de la requête
			try (TupleQueryResult rs = tupleQuery.evaluate()) {

				while (rs.hasNext()) {
					BindingSet bindingSet = rs.next();
					// "0" pour l'attribut spIdentifier car pas nécessaire ici.
					responseList.add(new Response(bindingSet.getValue("responseId").stringValue(), 
							bindingSet.getValue("rousselotPhoneticForm").stringValue(), bindingSet.getValue("complementaryInformation").stringValue(), "0"));
				}
			}
		}
		return responseList;
	}

	/**
	 * Requête qui permet d'ajouter une interprétation à une entrée d'inventaire et
	 * de la diffuser à toutes les entrées d'inventaires dont une réponse associée
	 * contient la même forme phonétique en alphabet Rousselot
	 *
	 * @param response le formulaire renseigné par l'utilisateur dans la partie
	 *                 client de l'application
	 */
	@SuppressWarnings("unchecked")
	public static void addInterpretation(LinkedHashMap<String, Object> creationForm) {

		// -----------------------------
		// Connection au repository
		// ------------------------------
		Repositories.consume(Queries.REPOSITORY, (connection) -> {

			// -----------------------------
			// Déclaration des variables
			// ------------------------------
			Interpretation newInterpretation = null;
			Lemma newLemma = null;

			// Booleen qui permet de savoir s'il s'agit de la première réponse à laquelle on
			// a associé l'interprétation. Permet de ne créer qu'une fois les lemmes et
			// étymons dans la mise à jour du repository (update repository)
			boolean first = true;

			for (Entry<String, Object> entry : creationForm.entrySet()) {

				LinkedHashMap<String, Object> value = (LinkedHashMap<String, Object>) entry.getValue();
				String invEntry = "" + value.get("inventoryEntry");
				String mapIdentifier = "" + value.get("mapId");
				String rousselotPhoneticForm = "" + value.get("rousselotPhoneticForm");

				// -----------------------------
				// Recherche de toutes les réponses contenues dans la carte
				// ------------------------------
				ArrayList<Response> listResponsesInMap = findAllResponsesInMap(mapIdentifier);

				// -----------------------------
				// Gestion de l'interprétation
				// ------------------------------

				newInterpretation = new Interpretation(UUID.randomUUID().toString(),
						"" + value.get("lemmatizedResponse"),
						"" + value.get("ground"), "" + value.get("specificGround"), "" + value.get("designating"),
						"" + value.get("responseType"), "" + value.get("interpretationComment"),
						System.currentTimeMillis(), mapIdentifier, new User(value.get("author").toString(), ""),
						"" + value.get("confidentiality"));

				// -----------------------------
				// Gestion des lemmes
				// ------------------------------

				ArrayList<Object> lemmaHashMap = (ArrayList<Object>) value.get("lemmasList");
				Iterator<Object> lemmaIter = lemmaHashMap.iterator();

				while (lemmaIter.hasNext()) {

					LinkedHashMap<String, Object> currentLemme = (LinkedHashMap<String, Object>) lemmaIter.next();
					newLemma = new Lemma("" + currentLemme.get("grammaticalNature"), "" + currentLemme.get("gender"),
							"" + currentLemme.get("number"), "" + currentLemme.get("lemmaComments"));

					// -----------------------------
					// Gestion des étymons
					// ------------------------------
					Iterator<Object> etymonIter = ((ArrayList<Object>) currentLemme.get("etymonsList")).iterator();

					while (etymonIter.hasNext()) {
						LinkedHashMap<String, String> currentEtymon = (LinkedHashMap<String, String>) etymonIter.next();
						newLemma.addEtymon(new Etymon("" + currentEtymon.get("fewEtymon"),
								"" + currentEtymon.get("fewEtymonLanguage"),
								"" + currentEtymon.get("fewEtymonSignified"),
								"" + currentEtymon.get("fewEtymonReference"), "" + currentEtymon.get("etymonComment"),
								"" + currentEtymon.get("uncertainty")));
					}
				}

				// Ajout du lemme à la réponse précédemment créée
				newInterpretation.addLemma(newLemma);

				// -----------------------------
				// Recherche des réponses identiques pour lesquelles il faut propager
				// l'interprétation
				// ------------------------------
				if (value.get("spreading").toString().equals("true")) {
					// Parcours de toutes les réponses contenues dans la carte courante
					for (int i = 0; i < listResponsesInMap.size(); i++) {
						Response currentResponse = listResponsesInMap.get(i);
						// si la phonétique en alphabet rousselot phonétique de la réponse est identique
						// à celle à laquelle l'utilisateur souhaite ajouter une interprétation alors on
						// lui ajoute l'interprétation
						if (currentResponse.getRousselotPhoneticForm().equals(rousselotPhoneticForm)) {
							currentResponse.addInterpretation(newInterpretation);
							// Mise à jour du repository
							updateRepository(currentResponse, invEntry, first, connection);
							first = false;
						}
					}

					// -----------------------------
					// Recherche des réponses initialement contenue dans la propagation de départ (à
					// la création) (propagation aux réponses identiques et à celle sélectionnées
					// par levenshtein)
					// CETTE PARTIE N'EST NÉCESSAIRE QUE DANS LE CAS D'UNE EDITION D'INTERPRÉTATION
					// ------------------------------
					if (value.get("associatedResponses") != null) {
						ArrayList<Object> associatedResponses = (ArrayList<Object>) value.get("associatedResponses");
						for (int j = 0; j < associatedResponses.size(); j++) {
							LinkedHashMap<String, String> response = (LinkedHashMap<String, String>) associatedResponses.get(j);
							String rousselotPhoneticForm1 = null;
							String spIdentifier = null;
							String responseId = null;
							String complementaryInformation = null;
							for (Map.Entry<String, String> entry1 : response.entrySet()) {
								if (entry1.getKey().equals("rousselotPhoneticForm")) {
									rousselotPhoneticForm1 = (String) entry1.getValue();
								}
								if (entry1.getKey().equals("spIdentifier")) {
									spIdentifier = (String) entry1.getValue();
								}
								if (entry1.getKey().equals("spIdentifier")) {
									spIdentifier = (String) entry1.getValue();
								}
								if (entry1.getKey().equals("complementaryInformation")) {
									complementaryInformation = (String) entry1.getValue();
								}
								if (entry1.getKey().equals("responseId")) {
									responseId = (String) entry1.getValue();
								}
							}
							Response currentAssociatedResponse = new Response(responseId, rousselotPhoneticForm1, complementaryInformation,
									spIdentifier);
							// Parcours des réponses contenues dans la carte
							for (int i = 0; i < listResponsesInMap.size(); i++) {
								Response currentResponse = listResponsesInMap.get(i);
								// Si les deux formes phonétiques correspondent, alors ajout de l'interprétation
								// à la réponse
								if (currentResponse.getRousselotPhoneticForm()
										.equals(currentAssociatedResponse.getRousselotPhoneticForm())) {
									currentResponse.addInterpretation(newInterpretation);
									if (first) {
										// Mise à jour du repository
										updateRepository(currentResponse, invEntry, first, connection);
										first = false;
									} else {
										// Mise à jour du repository
										updateRepository(currentResponse, invEntry, false, connection);
									}
								}
							}

						}
					}
				} else {
					// Cas où 'utilisateur ne souhaite pas propager l'interprétation aux réponses
					// identiques
					ArrayList<Response> responsesForInventry = findAllResponses(invEntry);
					Response currentResponse = null;
					for (int i = 0; i < responsesForInventry.size(); i++) {
						if (responsesForInventry.get(i).getRousselotPhoneticForm().equals(rousselotPhoneticForm)) {
							currentResponse = responsesForInventry.get(i);
						}
					}
					currentResponse.addInterpretation(newInterpretation);
					// Mise à jour du repository
					updateRepository(currentResponse, invEntry, first, connection);
					first = false;
				}
				// -----------------------------
				// Recherche des réponses phonétiquement proches pour lesquelles il faut
				// propager l'interprétation
				// ------------------------------
				if (value.get("levenshteinList") != null) {
					ArrayList<String> levenshteinList = (ArrayList<String>) value.get("levenshteinList");

					// Parcours de la liste des réponses phonétiquement proches que l'utilisateur a
					// coché
					for (int j = 0; j < levenshteinList.size(); j++) {
						// Parcours des réponses contenues dans la carte
						for (int i = 0; i < listResponsesInMap.size(); i++) {

							rousselotPhoneticForm = levenshteinList.get(j);
							Response currentResponse = listResponsesInMap.get(i);

							// Si les deux formes phonétiques correspondent, alors ajout de l'interprétation
							// à la réponse
							if (currentResponse.getRousselotPhoneticForm().equals(rousselotPhoneticForm)) {
								currentResponse.addInterpretation(newInterpretation);

								// Mise à jour du repository
								updateRepository(currentResponse, invEntry, false, connection);

							}
						}
					}
				}
			}
		});

	}

	/**
	 * Fontion qui permet de récupérer la liste des réponses contenues dans une
	 * carte Elle est appelée lors de la diffusion d'une interprétation aux réponses
	 * d'une carte dont la forme phonétique en alphabet Rousselot est identique
	 *
	 * @param mapId l'id de la carte courante
	 * @return la liste des réponses contenues dans une carte
	 */
	public static ArrayList<Response> findAllResponsesInMap(String mapId) {

		// -----------------------------
		// Déclaration des variables
		// ------------------------------
		// Initialisation de la liste qui contient les responses associées à une entrée
		// d'inventaire
		ArrayList<Response> responseList = new ArrayList<>();

		// -----------------------------
		// Connection au repository
		// ------------------------------
		try (RepositoryConnection connection = Queries.REPOSITORY.getConnection()) {
			ValueFactory ResourceFactory = connection.getValueFactory();
			// Préparation de la requête SPARQL
			TupleQuery tupleQuery = connection.prepareTupleQuery(QueryLanguage.SPARQL,
					Queries.getQuery("FIND_ALL_RESPONSES_IN_MAP"));
			// Création de l'URI pour l'entrée d'inventaire concernée
			tupleQuery.setBinding("mapId", ResourceFactory.createIRI("http://purl.org/fr/eclats/resource/map" + mapId));

			LOGGER.info("\n ---------------------- Query FIND_ALL_RESPONSES_IN_MAP -------------------------- \n"
					+ "SetBinding mapId : http://purl.org/fr/eclats/resource/map" + mapId + "\n"
					+ Queries.getQuery("FIND_ALL_RESPONSES_IN_MAP"));

			// exécution de la requête
			try (TupleQueryResult rs = tupleQuery.evaluate()) {

				while (rs.hasNext()) {
					BindingSet bindingSet = rs.next();
					responseList.add(new Response(bindingSet.getValue("responseId").stringValue(), bindingSet.getValue("rousselotPhoneticForm").stringValue(),
							bindingSet.getValue("complementaryInformation").stringValue(), bindingSet.getValue("spIdentifier").stringValue()));
				}
			}
		}
		return responseList;
	}

	// ---------------------------------------------------------------------------------------------
	// ---------------------------------------------------------------------------------------------
	// UPDATE REPOSITORY
	// ---------------------------------------------------------------------------------------------
	// ---------------------------------------------------------------------------------------------
	/**
	 * Ajoute une interprétation à une réponse correspondant à une entrée
	 * d'inventaire.
	 *
	 * @param response   la réponse à ajouter à l'entrée d'inventaire
	 * @param invEntry   l'entrée d'inventaire
	 * @param first      le booléen permettant de savoir si on est sur la première
	 *                   itération ou non
	 * @param connection la connexion courante au repository
	 * @throws RepositoryException
	 */
	private static void updateRepository(Response response, String invEntry, boolean first,
			RepositoryConnection connection) throws RepositoryException {

		// -----------------------------
		// Déclaration des variables
		// ------------------------------
		ValueFactory vf = SimpleValueFactory.getInstance();
		// Définition des préfixes
		String eclats = "http://purl.org/fr/eclats/resource/";
		String eclatsonto = "http://purl.org/fr/eclats/ontology#";
		IRI context = vf.createIRI("http://purl.org/fr/eclats/graph#ALF_interpretations");
		IRI responseIdIRI = vf.createIRI(eclats + response.getResponseId());

		List<Interpretation> interpretationList = response.getInterpretationList();
		Interpretation interpretation = interpretationList.get(interpretationList.size() - 1);
		IRI interpretationIdIRI = vf.createIRI(eclats + interpretation.getInterpretationId());

		// -----------------------------
		// Gestion des interprétations
		// ------------------------------
		connection.add(vf.createStatement(responseIdIRI, vf.createIRI(eclatsonto + "hasInterpretation"),
				interpretationIdIRI, context));

		// -----------------------------
		// Déclaration des lemmes
		// ------------------------------
		// Booleen qui permet de savoir s'il s'agit de la première réponse à laquelle on
		// associé l'interprétation, permet de ne créer qu'une fois les lemmes et
		// l'interprétation dans la mise à jour du repository
		if (first) {

			connection.add(vf.createStatement(interpretationIdIRI, RDF.TYPE,
					vf.createIRI(eclatsonto + "Interpretation"), context));			
			connection.add(vf.createStatement(interpretationIdIRI, vf.createIRI(eclatsonto + "ground"),
					vf.createLiteral(interpretation.getGround())), context);
			connection.add(vf.createStatement(interpretationIdIRI, vf.createIRI(eclatsonto + "specificGround"),
					vf.createLiteral(interpretation.getSpecificGround())), context);
			connection.add(vf.createStatement(interpretationIdIRI, vf.createIRI(eclatsonto + "designating"),
					vf.createLiteral(interpretation.getDesignating())), context);
			connection.add(vf.createStatement(interpretationIdIRI, vf.createIRI(eclatsonto + "interpretationDate"),
					vf.createLiteral(interpretation.getInterpretationDate())), context);
			connection.add(vf.createStatement(interpretationIdIRI, vf.createIRI(eclatsonto + "responseType"),
					vf.createLiteral(interpretation.getResponseType())), context);
			connection.add(vf.createStatement(interpretationIdIRI, vf.createIRI(eclatsonto + "lemmatizedResponse"),
					vf.createLiteral(interpretation.getLemmatizedResponse())), context);
			connection.add(vf.createStatement(interpretationIdIRI, vf.createIRI(eclatsonto + "interpretationComment"),
					vf.createLiteral(interpretation.getInterpretationComment())), context);
			connection.add(vf.createStatement(interpretationIdIRI, vf.createIRI(eclatsonto + "author"),
					vf.createIRI(eclats + interpretation.getUser().getUsername())), context);
			connection.add(vf.createStatement(interpretationIdIRI, vf.createIRI(eclatsonto + "confidentiality"),
					vf.createLiteral(interpretation.getConfidentiality())), context);
			// La liste des composants associés à la réponse
			List<Lemma> lemmaList = interpretation.getLemmasList();
			Iterator<Lemma> lemmaIterator = lemmaList.iterator();

			// Définition des préfixes
			String prefixes = "@prefix eclats: <http://purl.org/fr/eclats/resource/>.\n"
					+ "@prefix eclatsonto: <http://purl.org/fr/eclats/ontology#>.\n" + "eclats:"
					+ interpretation.getInterpretationId() + " a eclatsonto:Interpretation; \n"
					+ "eclatsonto:lemmas (\n";

			StringBuffer lemmasTtl = new StringBuffer();

			// Parcours de la liste des lemmes
			for (Lemma lemma : lemmaList) {

				lemmaIterator.next();

				lemmasTtl.append(" [ \n" + "     a eclatsonto:Lemma;\n" + "  eclatsonto:grammaticalNature \""
						+ lemma.getGrammaticalNature() + "\";\n" + "  eclatsonto:gender \"" + lemma.getGender()
						+ "\";\n" + "  eclatsonto:number \"" + lemma.getNumber() + "\";\n"
						+ "  eclatsonto:lemmaComments \"" + lemma.getLemmaComments() + "\";\n");
				lemmasTtl.append("  eclatsonto:etymons (\n");

				List<Etymon> etymonList = lemma.getEtymonsList();
				Iterator<Etymon> etymonIterator = etymonList.iterator();

				// Parcours de la liste des étymon associés au lemme
				for (Etymon etymon : etymonList) {

					etymonIterator.next();
					lemmasTtl.append(" [ \n" + "     a eclatsonto:Etymon;\n" + "  eclatsonto:fewEtymon \""
							+ etymon.getFewEtymon() + "\";\n" + "  eclatsonto:fewEtymonLanguage \""
							+ etymon.getFewEtymonLanguage() + "\";\n" + "  eclatsonto:fewEtymonSignified \""
							+ etymon.getFewEtymonSignified() + "\";\n" + "  eclatsonto:fewEtymonReference \""
							+ etymon.getFewEtymonReference() + "\";\n" + "  eclatsonto:etymonComment \""
							+ etymon.getEtymonComment() + "\";\n" + "  eclatsonto:uncertainty \""
							+ etymon.getUncertainty());

					// Si ce n'est pas la dernière itération
					if (etymonIterator.hasNext()) {
						lemmasTtl.append("\";\n]\n");
					} else {
						lemmasTtl.append("\";\n]);");
						if (lemmaIterator.hasNext()) {
							lemmasTtl.append("\n]\n");
						} else {
							lemmasTtl.append("\n]).");
						}
					}
				}
			}

			try {

				// -----------------------------
				// Ajout de la liste de composants au repository
				// ------------------------------
				System.out.println(prefixes + lemmasTtl);

				connection.add(new ByteArrayInputStream((prefixes + lemmasTtl).getBytes()), "", RDFFormat.TURTLE,
						context);
			} catch (RDFParseException | RepositoryException | IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
				System.out.println(prefixes + lemmasTtl);

			}
		}

	}

	//////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////
	//////////////////////////////////////////////////////////////////////////////////////////////
	/**
	 * Requête qui permet de trouver les formes phonétiques proches de celle
	 * renseignée en paramètre de la fonction
	 *
	 * @param rousselotPhoneticForm_mapId la forme phonétique à comparer et l'id de
	 *                                    la carte
	 * @return Une liste de formes phonétiques qui sont phonétiquement proches
	 */
	public static HashMap<String, Integer[]> findLevenshteinCorrespondance(String rousselotPhoneticForm, String mapId,
			String levenshteinDistance, String author) {

		// -----------------------------
		// Déclaration des vaiables
		// ------------------------------
		// Instanciation du hashmap qui va comporter les différentes formes
		// phonétiques correspondantes et la distance de lenvenshtein associée
		HashMap<String, Integer[]> hmap = new HashMap<String, Integer[]>();
		CharSequence charSeqRousselotPhoneticForm = rousselotPhoneticForm;

		// -----------------------------
		// Connection au repository
		// ------------------------------
		try (RepositoryConnection connection = Queries.REPOSITORY.getConnection()) {
			ValueFactory ResourceFactory = connection.getValueFactory();
			// Préparation de la requête SPARQL qui permet de récupérer la liste des
			// réponses contenues dans une carte
			TupleQuery tupleQuery = connection.prepareTupleQuery(QueryLanguage.SPARQL,
					Queries.getQuery("FIND_COUNT_OF_IDENTICAL_RESPONSES_FOR_LEVENSTHEIN"));

			// Insertion de l'URI correspondant à l'id de la carte dans la requête SPARQL
			tupleQuery.setBinding("mapId", ResourceFactory.createIRI("http://purl.org/fr/eclats/resource/map" + mapId));
			tupleQuery.setBinding("author", ResourceFactory.createIRI("http://purl.org/fr/eclats/resource/" + author));

			LOGGER.info("\n ---------------------- Query FIND_COUNT_OF_IDENTICAL_RESPONSES_FOR_LEVENSTHEIN -------------------------- \n"
					+ "SetBinding mapId : http://purl.org/fr/eclats/resource/map" + mapId + "\n"
					+ "SetBinding author : http://purl.org/fr/eclats/resource/" + author + "\n"
					+ Queries.getQuery("FIND_COUNT_OF_IDENTICAL_RESPONSES_FOR_LEVENSTHEIN"));

			// exécution de la requête SPARQL
			try (TupleQueryResult rs = tupleQuery.evaluate()) {

				while (rs.hasNext()) {
					BindingSet bindingSet = rs.next();

					CharSequence rousselotPhoneticFormToCompare = bindingSet.getValue("rousselotPhoneticForm")
							.stringValue();
					// Si la forme phonétique n'est pas la même à celle passée en paramètre de la
					// requête et qu'elle a un indice de levenshtein inférieur à l'indice renseigné
					// en
					// paramètre de la requête et que l'ArrayList ne comporte pas déjà la forme
					// phonétique, alors on l'ajoute.
					if (!rousselotPhoneticFormToCompare.toString().equals(charSeqRousselotPhoneticForm)
							&& Response.levenshteinDistance(rousselotPhoneticFormToCompare,
									charSeqRousselotPhoneticForm) <= Integer.parseInt(levenshteinDistance)) {

						Integer[] levenshtein_count = new Integer[] {
								Response.levenshteinDistance(rousselotPhoneticFormToCompare,
										charSeqRousselotPhoneticForm),
								Integer.parseInt(bindingSet.getValue("count").stringValue()) };
						hmap.put(bindingSet.getValue("rousselotPhoneticForm").stringValue(), levenshtein_count);

					}
				}
			}
		}
		// Tri du Hashmap
		return sortByValue(hmap);
	}

	/**
	 * Fonction qui retourne un Hashmap trié par valeur
	 *
	 * @param hm le hashmap a trier
	 * @return le hashmap trié
	 */
	public static HashMap<String, Integer[]> sortByValue(HashMap<String, Integer[]> hm) {
		// Create a list from elements of HashMap
		List<Map.Entry<String, Integer[]>> list = new LinkedList<Map.Entry<String, Integer[]>>(hm.entrySet());

		// Sort the list
		Collections.sort(list, new Comparator<Map.Entry<String, Integer[]>>() {
			public int compare(Map.Entry<String, Integer[]> o1, Map.Entry<String, Integer[]> o2) {
				return (o1.getValue()[0]).compareTo(o2.getValue()[0]);
			}
		});

		// put data from sorted list to hashmap
		HashMap<String, Integer[]> temp = new LinkedHashMap<String, Integer[]>();
		for (Map.Entry<String, Integer[]> aa : list) {
			temp.put(aa.getKey(), aa.getValue());
		}
		return temp;
	}

	/**
	 * Fonction qui retourne le nombre de réponse identiques dans une carte en
	 * particulier
	 *
	 * @param mapId_rousselotPhoneticForm le paramètre de la requête HTTP qui
	 *                                    contient l'id de la carte courante et la
	 *                                    réponse phonétique.
	 * @return un entier correspondant au nombre de réponse identique dans la carte
	 */
	public static int findCountOfIdenticalResponses(String mapId, String rousselotPhoneticForm) {

		// -----------------------------
		// Déclaration des variables
		// ------------------------------
		int count = 0;

		// -----------------------------
		// Connection au repository
		// ------------------------------
		try (RepositoryConnection connection = Queries.REPOSITORY.getConnection()) {
			ValueFactory ResourceFactory = connection.getValueFactory();
			// Préparation de la requête SPARQL qui permet de récupérer le nombre de
			// réponses dont la
			// forme phonétique est identique
			TupleQuery tupleQuery = connection.prepareTupleQuery(QueryLanguage.SPARQL,
					Queries.getQuery("FIND_COUNT_OF_IDENTICAL_RESPONSES"));

			// Insertion de l'URI correspondant à l'id de la carte dans la requête SPARQL
			tupleQuery.setBinding("mapId", ResourceFactory.createIRI("http://purl.org/fr/eclats/resource/map" + mapId));
			tupleQuery.setBinding("rousselotPhoneticForm", ResourceFactory.createLiteral(rousselotPhoneticForm));

			LOGGER.info("\n ---------------------- Query FIND_COUNT_OF_IDENTICAL_RESPONSES -------------------------- \n"
					+ "SetBinding mapId : http://purl.org/fr/eclats/resource/map" + mapId + "\n"
					+ "SetBinding rousselotPhoneticForm : " + rousselotPhoneticForm + "\n"
					+ Queries.getQuery("FIND_COUNT_OF_IDENTICAL_RESPONSES"));
			
			// exécution de la requête SPARQL
			try (TupleQueryResult rs = tupleQuery.evaluate()) {
				while (rs.hasNext()) {
					BindingSet bindingSet = rs.next();
					count = Integer.parseInt(bindingSet.getValue("count").stringValue()) - 1;
				}
			}
		}
		return count;
	}

	// Les réponses phonétiques proches associées à l'interprétation au moment de la
	// création d'un interprétation. Ce sont les réponses issues de leveshtein qui
	// ont été cochées par l'utilisateur
	public static ArrayList<Response> findAssociatedResponses(String interpretationId, String rousselotPhoneticForm) {
		// -----------------------------
		// Déclaration des variables
		// ------------------------------

		ArrayList<Response> associatedResponses = new ArrayList<Response>();
		ArrayList<String> rousselotPhoneticFormList = new ArrayList<String>();

		// -----------------------------
		// Connection au repository
		// ------------------------------
		try (RepositoryConnection connection = Queries.REPOSITORY.getConnection()) {
			ValueFactory ResourceFactory = connection.getValueFactory();

			TupleQuery tupleQuery = connection.prepareTupleQuery(QueryLanguage.SPARQL,
					Queries.getQuery("FIND_ASSOCIATED_RESPONSES"));

			LOGGER.info("\n ---------------------- Query FIND_ASSOCIATED_RESPONSES -------------------------- \n"
					+ "SetBinding interpretationId : "  + interpretationId + "\n"
					+ Queries.getQuery("FIND_ASSOCIATED_RESPONSES"));			

			// Insertion de l'URI correspondant à l'id de la carte dans la requête SPARQL
			tupleQuery.setBinding("interpretationId", ResourceFactory.createIRI(interpretationId));

			try (TupleQueryResult rs = tupleQuery.evaluate()) {
				while (rs.hasNext()) {
					BindingSet bindingSet = rs.next();
					String phoneticForm = bindingSet.getValue("rousselotPhoneticForm").stringValue();
					// Si la forme phonétique courante n'est pas déjà dans la liste des formes
					// phonétiques parcourues
					// Et qu'elle est différente de la forme phonétique de l'entrée d'inventaire
					// Alors ajout de la réponse dans la liste des réponses associées à
					// l'interprétation
					if (!rousselotPhoneticFormList.contains(phoneticForm)
							&& !phoneticForm.equals(rousselotPhoneticForm)) {

						rousselotPhoneticFormList.add(bindingSet.getValue("rousselotPhoneticForm").stringValue());
						associatedResponses
								.add(new Response(bindingSet.getValue("responseId").stringValue(), phoneticForm, bindingSet.getValue("complementaryInformation").stringValue(), "0"));

					}
				}
			}
		}
		return associatedResponses;
	}

//---------------- CODE MORT -----------------//
//	/**
//	 * Fonction permettant d'ajouter ou de remplacer (si c'est une réponse
//	 * provisoire) une réponse phonétique associées à une entrée d'inventaire.
//	 * 
//	 * @param inventoryEntryId l'entrée d'inventaire
//	 * @param responseForm     le formulaire comportant la réponse phonétique et les
//	 *                         autres entrées d'inventaires à associer à la réponse
//	 */
//	@SuppressWarnings("unchecked")
//	public static void addPhoneticResponse(LinkedHashMap<String, Object> responseForm) {
//		ArrayList<String> inventoryEntryList = new ArrayList<String>();
//		String mapId = "";
//		// -----------------------------
//		// Parcours du formulaire => Récupère les entrées d'inventaires et l'id de la
//		// carte
//		// ------------------------------
//		for (Entry<String, Object> entry : responseForm.entrySet()) {
//			if (entry.getKey().equals("inventoryEntriesId")) {
//				inventoryEntryList = (ArrayList<String>) entry.getValue();
//			}
//			if (entry.getKey().equals("mapId")) {
//				mapId = entry.getValue().toString();
//			}
//		}
//		// -----------------------------
//		// Connection au repository
//		// ------------------------------
//		try (RepositoryConnection connection = Queries.REPOSITORY.getConnection()) {
//			ValueFactory vf = connection.getValueFactory();
//
//			// Définition des préfixes
//			String eclats = "http://purl.org/fr/eclats/resource/";
//			String eclatsonto = "http://purl.org/fr/eclats/ontology#";
//			IRI context = vf.createIRI("http://purl.org/fr/eclats/graph#USER_PHONETIC_RESPONSES");
//
//			// -----------------------------
//			// Parcours du formulaire
//			// ------------------------------
//			for (Entry<String, Object> entry : responseForm.entrySet()) {
//				if (entry.getKey().equals("response")) {
//					// -----------------------------
//					// Parcours des entrées d'inventaires
//					// ------------------------------
//					for (int i = 0; i < inventoryEntryList.size(); i++) {
//						IRI inventoryEntryIdIRI = vf.createIRI("http://purl.org/fr/eclats/resource/inventoryEntry_SP"
//								+ inventoryEntryList.get(i) + "_map" + mapId);
//						// Récupère la réponse phonétique associée à l'entrée d'inventaire pour vérifier
//						// si c'est une réponse provisoire ou non
//						// Réponse provisoire: "forme phonétique non encore renseignée"
//						RepositoryResult<Statement> st = connection.getStatements(inventoryEntryIdIRI,
//								vf.createIRI(eclatsonto + "hasResponse"), null);
//						RepositoryResult<Statement> st2 = connection.getStatements(
//								vf.createIRI(st.next().getObject().toString()),
//								vf.createIRI(eclatsonto + "rousselotPhoneticForm"), null);
//						String phoneticResponseToReplace = st2.next().getObject().stringValue();
//						// S'il s'agit d'une réponse phonétique provisoire, alors elle est supprimée
//						if (phoneticResponseToReplace.equals("forme phonétique non encore renseignée")) {
//							connection.remove(inventoryEntryIdIRI, vf.createIRI(eclatsonto + "hasResponse"), null);
//						}
//
//						String rousselotPhoneticForm = entry.getValue().toString();
//						IRI responseIdIRI = vf.createIRI(eclats + UUID.randomUUID().toString());
//						connection.add(vf.createStatement(inventoryEntryIdIRI, vf.createIRI(eclatsonto + "hasResponse"),
//								responseIdIRI, context));
//						connection.add(vf.createStatement(responseIdIRI, RDF.TYPE,
//								vf.createIRI(eclatsonto + "Response"), context));
//						connection.add(
//								vf.createStatement(responseIdIRI, vf.createIRI(eclatsonto + "rousselotPhoneticForm"),
//										vf.createLiteral(rousselotPhoneticForm), context));
//
//					}
//				}
//			}
//		}
//		;
//	}
}
