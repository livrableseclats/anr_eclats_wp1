package fr.eclatsgeoling.eclatsAppsBackEnd.dao;

import java.io.IOException;
import java.util.LinkedHashMap;
import java.util.List;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

public class ExportMapListDAO {

    /**
     * Permet la mise en forme des cartes issues des recherches de cartes par
     * intitulé, numéro, ou par catégories grammaticales pour l'export de ces
     * données en format csv
     *
     * @param mapsList la liste des cartes issue de la recherche
     * @return Retourne une liste de cartes de type string
     * @throws com.fasterxml.jackson.core.JsonParseException
     * @throws com.fasterxml.jackson.databind.JsonMappingException
     */
    @SuppressWarnings({"unchecked", "rawtypes"})
    public static String exportMapSearchResult(String mapsList)
            throws JsonParseException, JsonMappingException, IOException {
        TypeReference<LinkedHashMap<String, List>> type = new TypeReference<LinkedHashMap<String, List>>() {
        };
        LinkedHashMap<String, List> formattedMapsList = new ObjectMapper().readValue(mapsList, type);
        StringBuilder str = new StringBuilder();
        str.append("numCarte;intitule;intitule_contextualise \n");
        formattedMapsList.entrySet().stream().map((entry) -> (List<LinkedHashMap<String, String>>) entry.getValue()).forEachOrdered((currentMap) -> {
            for (int i = 0; i < currentMap.size(); i++) {

                str.append(currentMap.get(i).get("mapIdentifier"))
                        .append(";")
                        .append(currentMap.get(i).get("mapTitle"))
                        .append(";")
                        .append(currentMap.get(i).get("contextualizedTitle"))
                        .append("\n");
            }
        });

        return str.toString();

    }
}
