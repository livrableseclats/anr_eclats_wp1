package fr.eclatsgeoling.eclatsAppsBackEnd.dao;

import java.io.IOException;
import java.util.ArrayList;
import java.util.LinkedHashMap;

import org.eclipse.rdf4j.model.ValueFactory;
import org.eclipse.rdf4j.query.BindingSet;
import org.eclipse.rdf4j.query.QueryLanguage;
import org.eclipse.rdf4j.query.TupleQuery;
import org.eclipse.rdf4j.query.TupleQueryResult;
import org.eclipse.rdf4j.repository.RepositoryConnection;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.fasterxml.jackson.core.JsonParseException;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.JsonMappingException;
import com.fasterxml.jackson.databind.ObjectMapper;

import fr.eclatsgeoling.eclatsAppsBackEnd.classes.Map;
import fr.eclatsgeoling.eclatsAppsBackEnd.vocabulary.GRAMCAT;
import java.util.List;
import java.util.Map.Entry;

public class GrammaticalCategoriesDAO {
	private static final Logger LOGGER = LoggerFactory.getLogger(GrammaticalCategoriesDAO.class);

	/**
	 * Permet de récupérer la liste des propriétés associées à une catégorie
	 * grammaticale
	 * 
	 * @param concept la catégorie grammaticale (concept SKOS) sélectionnée par
	 *                l'utilisateur
	 * @return une String contenant les différentes propriétés correspondant à la
	 *         catégorie grammaticale en paramètre
	 */
	public static StringBuilder findProps(String concept) {
		boolean first = true;

		StringBuilder result = new StringBuilder();
		List<String> props = new ArrayList<>();

		try (RepositoryConnection connection = Queries.REPOSITORY.getConnection()) {
			TupleQuery tupleQuery = connection.prepareTupleQuery(QueryLanguage.SPARQL, Queries.getQuery("FIND_PROPS"));
			ValueFactory ResourceFactory = connection.getValueFactory();
			tupleQuery.setBinding("concept", ResourceFactory.createIRI(GRAMCAT.NAMESPACE + concept));

			LOGGER.info("\n ---------------------- Query FIND_PROPS -------------------------- \n"
					+ " SetBinding concept : " + GRAMCAT.NAMESPACE + concept + "\n" + Queries.getQuery("FIND_PROPS"));

			try (TupleQueryResult rs = tupleQuery.evaluate()) {
				while (rs.hasNext()) {
					BindingSet bindingSet = rs.next();

					// Fermeture d'une propriété (exemple: si on a déjà parcouru toutes les valeurs
					// de genre
					if (!first && !(props.contains(bindingSet.getValue("prop").stringValue()))) {
						result.deleteCharAt(result.length() - 1);
						result.append("] \n");
						result.append("}, \n");
					}
					// Première ligne
					if (first) {
						result.append("[");
						first = false;
					}
					// Nouvelle propriété
					if (!(props.contains(bindingSet.getValue("prop").stringValue()))) {

						props.add(bindingSet.getValue("prop").stringValue());
						props.add(bindingSet.getValue("value").stringValue());

						result.append("{ \n");
						result.append("\"id\" : \"").append(bindingSet.getValue("prop").stringValue()).append("\", \n");
						result.append("\"label\" : \"").append(bindingSet.getValue("propPrefLabel").stringValue())
								.append("\", \n");
						result.append("\"children\" : [{ \n");
						result.append("\"id\" : \"").append(bindingSet.getValue("prop").stringValue()).append("_")
								.append(bindingSet.getValue("value").stringValue()).append("\", \n");
						result.append("\"label\" : \"").append(bindingSet.getValue("valuePrefLabel").stringValue())
								.append("\" \n");
						result.append("}, \n");
					}

					// La propriété existe déjà
					if (!(props.contains(bindingSet.getValue("value").stringValue()))) {
						result.append("{\n");
						result.append("\"id\" : \"").append(bindingSet.getValue("prop").stringValue()).append("_")
								.append(bindingSet.getValue("value").stringValue()).append("\", \n");
						result.append("\"label\" : \"").append(bindingSet.getValue("valuePrefLabel").stringValue())
								.append("\" \n");
						result.append("},");
					}
					if (!(rs.hasNext())) {
						// Fin du parcours des résultats de la requête
						result.deleteCharAt(result.length() - 1);
						result.append("]\n");
						result.append("}]\n");
					}
				}
			}
		}

		if (result.length() == 0) {
			result.append("[]");
		}

		return result;
	}

	/**
	 * Permet de récupéréer une liste de cartes dont l'intitulé est filtré par
	 * catégorie grammaticale et par propriété.
	 * 
	 * @param selectedProperties la catégorie grammaticale et les propriétés
	 *                           sélectionnées par l'utilisatuer
	 * @return une liste de cartes dont l'intitulé contient un mot dont la catégorie
	 *         grammaticale et les propriétés correspondent avec celles passées en
	 *         paramètre
	 * @throws IOException
	 * @throws JsonMappingException
	 * @throws JsonParseException
	 */
	@SuppressWarnings({ "rawtypes", "unchecked" })
	public static List<Map> findMapsWithGramCat(String selectedProperties)
			throws JsonParseException, JsonMappingException, IOException {
		// Conversion de la chaine de charactères
		TypeReference<LinkedHashMap<String, List>> type = new TypeReference<LinkedHashMap<String, List>>() {
		};
		LinkedHashMap<String, List> formattedSelectedProperties = new ObjectMapper().readValue(selectedProperties,
				type);

		String concept = null;
		String query = Queries.getQuery("FIND_MAPS_WITH_GRAM_CAT");
		StringBuilder propertiesForQueryString = new StringBuilder("");

		for (Entry<String, List> entry : formattedSelectedProperties.entrySet()) {
			LinkedHashMap<String, Object> test = (LinkedHashMap<String, Object>) entry.getValue().get(0);
			for (Entry<String, Object> entry1 : test.entrySet()) {
				if (entry1.getKey().equals("concept")) {
					concept = (String) "<http://purl.org/fr/eclats/ontologies/catgram#" + entry1.getValue() + ">";
				}
				if (entry1.getKey().contentEquals("properties")) {
					List<String> properties = (List<String>) entry1.getValue();
					for (int i = 0; i < properties.size(); i++) {
						String predicat = properties.get(i).split("_")[0];
						String object = properties.get(i).split("_")[1];
						propertiesForQueryString.append("?titleElt <").append(predicat).append("> <").append(object)
								.append(">.");
					}
				}
			}
		}
		query = String.format(query, concept, propertiesForQueryString);

		try (RepositoryConnection connection = Queries.REPOSITORY.getConnection()) {

			LOGGER.info("\n ---------------------- Query CATGRAM -------------------------- \n" + query);

			return MapDAO.executeQueryForMaps(connection.prepareTupleQuery(QueryLanguage.SPARQL, query));
		}
	}

	/**
	 * @param selectedProperties la catégorie grammaticale et les propriétés
	 *                           sélectionnées par l'utilisatuer
	 * @return une liste de cartes dont l'intitulé contient des mots dont la
	 *         catégorie grammaticale correspond aux concepts en paramètres
	 * @throws com.fasterxml.jackson.core.JsonParseException
	 * @throws com.fasterxml.jackson.databind.JsonMappingException
	 */
	@SuppressWarnings({ "unchecked", "rawtypes" })
	public static List<Map> findMapsWithGramCatWithOperator(String selectedProperties)
			throws JsonParseException, JsonMappingException, IOException {
		TypeReference<LinkedHashMap<String, List>> type = new TypeReference<LinkedHashMap<String, List>>() {
		};
		LinkedHashMap<String, List> formattedSelectedProperties = new ObjectMapper().readValue(selectedProperties,
				type);

		List listOfParams = new ArrayList<>();
		String query = "";
		boolean first = true;
		String predicat;
		String object;
		StringBuilder propertiesForQueryString = new StringBuilder("");

		for (Entry<String, List> entry : formattedSelectedProperties.entrySet()) {
			List params = entry.getValue();
			for (int j = 0; j < params.size(); j++) {
				// Le paramètre courant contenant un concept et ses propriétés
				LinkedHashMap<String, Object> currentParam = (LinkedHashMap<String, Object>) params.get(j);

				for (Entry<String, Object> entry1 : currentParam.entrySet()) {
					// Enregistrement de l'opérateur pour appeler la requête SPARQL correspondante
					if (entry1.getKey().equals("operator")) {
						if (entry1.getValue().equals("ET")) {
							query = Queries.getQuery("FIND_MAPS_WITH_GRAM_CAT_AND");
						} else {
							query = Queries.getQuery("FIND_MAPS_WITH_GRAM_CAT_OR");
						}
					}
					// Enregistrement des concepts
					if (entry1.getKey().equals("concept")) {
						listOfParams
								.add("<http://purl.org/fr/eclats/ontologies/catgram#" + entry1.getValue() + ">. \n");
					}

					// Enregistrement des propriétés
					if (entry1.getKey().contentEquals("properties")) {
						List<String> properties = (List<String>) entry1.getValue();
						for (int i = 0; i < properties.size(); i++) {
							predicat = properties.get(i).split("_")[0];
							object = properties.get(i).split("_")[1];
							propertiesForQueryString.append("?titleElt <").append(predicat).append("> <").append(object)
									.append(">. \n");
						}
						// Premier concept
						if (first) {
							listOfParams.set(0, listOfParams.get(0) + propertiesForQueryString.toString());
							first = false;
							// Second concept
						} else {
							listOfParams.set(1, listOfParams.get(1) + propertiesForQueryString.toString());
						}
					}
				}
			}
		}
		query = String.format(query, listOfParams.get(0), listOfParams.get(1));
		try (RepositoryConnection connection = Queries.REPOSITORY.getConnection()) {
			
			LOGGER.info("\n ---------------------- Query CATGRAM -------------------------- \n" + query);
			
			return MapDAO.executeQueryForMaps(connection.prepareTupleQuery(QueryLanguage.SPARQL, query));
		}
	}
}