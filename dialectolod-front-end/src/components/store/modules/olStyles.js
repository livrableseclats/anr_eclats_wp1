import { Style, Stroke, Fill, Text } from 'ol/style'
import { Point } from 'ol/geom';
import { DEVICE_PIXEL_RATIO } from 'ol/has';
import CircleStyle from 'ol/style/Circle';

const getters = {
  // -------------------
  // CanvasPattern => retourne un pattern hachuré
  // ------------------
  getPattern: state => (color) => {
    let canvas = document.createElement('canvas')
    let canvasContext = canvas.getContext('2d')
    let pixelRatio = DEVICE_PIXEL_RATIO

    let pattern = (function() {
      canvas.width = 8 * pixelRatio;
      canvas.height = 8 * pixelRatio;
      // white background
      canvasContext.fillStyle = 'white';
      canvasContext.fillRect(0, 0, canvas.width, canvas.height);
      // circle
      canvasContext.fillStyle = color;
      canvasContext.beginPath();
      canvasContext.arc(4 * pixelRatio, 4 * pixelRatio, 3 * pixelRatio, 0, 2 * Math.PI);
      canvasContext.fill();
      return canvasContext.createPattern(canvas, 'repeat');
    }())
    return pattern
  },
}

const actions = {
  // -------------------
  // Style des couches shiny
  // ------------------
  setShinyStyle(context, feature) {
    return feature.setStyle(new Style({
      fill: new Fill({
        color: feature.get('color')
      })
    }))
  },

  // -------------------
  // Style random
  // ------------------
  setRandomStyle(context, feature) {
    return feature.setStyle(new Style({
      fill: new Fill({
        color: 'rgba(' + Math.floor(Math.random() * 256) + ', ' + Math.floor(Math.random() * 256) + ',' + Math.floor(Math.random() * 256) + ')'
      })
    }))
  },

  // -------------------
  // Style monochrome
  // ------------------
  setMonochromeStyle(context, feature) {
    console.log('mono')
    return feature.setStyle(new Style({
      fill: new Fill({
        color: 'white'
      }),
      stroke: new Stroke({
        width: 0.5
      })
    }))
  },

  // -------------------
  // Style hachuré
  // ------------------
  setStackedStyle(context, feature) {
    return feature.setStyle(new Style({
      fill: new Fill({
        color: context.getters.getPattern(feature.get('color'))
      })
    }))
  },


  // -------------------
  // Styles des features de type points
  // Style défini par l'utilisateur lorsqu'il a sélectionné/isolé
  // Une feature (voir composant OptionsLayerView)
  // ------------------
  setPointsStrokeAndColor(context, data) {
    data.feature.setStyle(
      new Style({
        image: new CircleStyle({
          radius: 5,
          fill: new Fill({
            color: data.color
          }),
          stroke: new Stroke({
            width: data.strokeWidth
          })
        })
      })
    )
  },
  // -------------------
  // Styles des features de type polygons
  // Style défini par l'utilisateur lorsqu'il a sélectionné/isolé
  // Une feature (voir composant OptionsLayerView)
  // ------------------
  setPolygonsStrokeAndColor(context, data) {
    data.feature.setStyle(new Style({
      fill: new Fill({
        color: data.feature.getStyle().getFill().getColor() instanceof CanvasPattern ? context.getters.getPattern(data.color) : data.color
      }),
      stroke: new Stroke({
        width: data.strokeWidth
      })
    }))
  },
  // -------------------
  // Affiche les labels pour les couches shiny
  // (voir composant FeaturesView)
  // ------------------
  setLabels(context, feature) {
    return feature.setStyle(new Style({
      fill: feature.getStyle().getFill(),
      text: new Text({
        text: feature.get('modality'),
        font: 'bold 10px "Open Sans", "Arial Unicode MS", "sans-serif"',
        placement: 'point',
        fill: new Fill({
          color: 'black'
        })
      })
    }))
  },
  // -------------------
  // Rétabli le style par défaut d'un couche après avoir affiché les labels
  // ------------------
  setDefaultStyle(context, feature) {
    return feature.setStyle(new Style({
      fill: feature.getStyle().getFill()
    }))
  },

  // -------------------
  // Style appliqué à une feature au clic de l'utilisateur, lorsqu'il consulte la popup contenant les différents attributs
  // ------------------
  setSelectedFeatureStyle(context, data) {
    if (!(data.feature instanceof Point)) {
      data.feature.setStyle(new Style({
        fill: new Fill({
          color: data.feature.getStyle().getFill().getColor() instanceof CanvasPattern ? context.getters.getPattern(data.color) : data.color
        }),
        stroke: new Stroke({
          width: data.strokeWidth,
          color: 'yellow'
        })
      }))
    }
  }
}

export let olStyles = {
  actions: actions,
  getters: getters,
};