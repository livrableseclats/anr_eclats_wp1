import Vue from 'vue'
import Vuex from 'vuex'

import OSM, { ATTRIBUTION } from 'ol/source/OSM.js'
import VectorSource from 'ol/source/Vector'
import GeoJSON from 'ol/format/GeoJSON'
import Map from 'ol/Map'
import View from 'ol/View'
import { fromLonLat } from 'ol/proj'
import VectorLayer from 'ol/layer/Vector'
import { Tile as TileLayer } from 'ol/layer.js'
import { Style, Fill } from 'ol/style'
import axios from 'axios'
import convert from 'xml-js'
import { olStyles } from '@components/store/modules/olStyles.js'

Vue.use(Vuex)

// La constante comporte les différentes couches disponibles depuis des ressources openSource en ligne
// ou encore les couches disponible via geoserver
const state = {
  url_geoserver: 'http://localhost:8081/geoserver/',
  //url_geoserver: 'http://lig-tdcge.imag.fr/geoserver/',
  olmap: null,
  // La liste des layers afffichées sur la carte
  olmapLayers: [],
  // Liste des fonds de carte disponibles
  backgroundLayers: [{
      layerName: 'ESRI_WHITE_MapLayer',
      title: 'Épuré (blanc)',
      visible: true,
      value: new TileLayer({
        source: new OSM({
          attributions: [
            'Tiles &copy; Esri &mdash; Esri, DeLorme, NAVTEQ',
            ATTRIBUTION
          ],
          url: 'https://server.arcgisonline.com/ArcGIS/rest/services/Canvas/World_Light_Gray_Base/MapServer/tile/{z}/{y}/{x}'
        })
      })
    },
    {
      layerName: 'ESRI_NatGeoWorldMap',
      title: 'Régions naturelles',
      visible: false,
      value: new TileLayer({
        source: new OSM({
          attributions: [
            'Tiles &copy; Esri &mdash; National Geographic, Esri, DeLorme, NAVTEQ, UNEP-WCMC, USGS, NASA, ESA, METI, NRCAN, GEBCO, NOAA, iPC',
            ATTRIBUTION
          ],
          url: 'https://server.arcgisonline.com/ArcGIS/rest/services/NatGeo_World_Map/MapServer/tile/{z}/{y}/{x}'
        })
      })
    },

    {
      layerName: 'ESRI_RasterMap',
      title: 'Satellite',
      visible: false,
      value: new TileLayer({
        source: new OSM({
          attributions: [
            'Tiles &copy; Esri &mdash; Source: Esri, i-cubed, USDA, USGS, AEX, GeoEye, Getmapping, Aerogrid, IGN, IGP, UPR-EGP, and the GIS User Community',
            ATTRIBUTION
          ],
          url: 'https://server.arcgisonline.com/ArcGIS/rest/services/World_Imagery/MapServer/tile/{z}/{y}/{x}'
        })
      })
    },
    {
      layerName: 'ESRI_TOPO_MapLayer',
      visible: false,
      title: 'Topographie',
      value: new TileLayer({
        source: new OSM({
          attributions: [
            'Tiles &copy; Esri &mdash; Esri, DeLorme, NAVTEQ, TomTom, Intermap, iPC, USGS, FAO, NPS, NRCAN, GeoBase, Kadaster NL, Ordnance Survey, Esri Japan, METI, Esri China (Hong Kong), and the GIS User Community',
            ATTRIBUTION
          ],
          url: 'https://server.arcgisonline.com/ArcGIS/rest/services/World_Topo_Map/MapServer/tile/{z}/{y}/{x}'
        })
      })
    }
  ],
  capabilities: {},
  jsonLayers: [
    { id: 'commons', label: 'généralités', children: [] },
    { id: 'eclats', label: 'ressources linguistiques', children: [] },
    { id: 'oldMaps', label: 'cartes anciennes', children: [] },
    { id: 'mapWrapper', label: 'ressources mapWrapper', children: [] },
    { id: 'SANDRE', label: 'ressources SANDRE', children: [] },
    { id: 'BRGM', label: 'ressources BRGM', children: [] }
  ]
}

// ---------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------
// GETTERS
// ---------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------

const getters = {
  getUrlGeoserver: state => { return state.url_geoserver },
  // Retourne la carte openlayer
  getOlmap: state => { return state.olmap },
  // Retourne le tableau correspondant aux couches actuellement contenues dans la carte openlayer
  getOlmapLayers: state => { return state.olmapLayers.filter(olmapLayers => olmapLayers) },
  // Retourne un array comportant les différents fonds de cartes contenues dans le store
  getBackgroundLayers: state => { return state.backgroundLayers.filter(backgroundLayer => backgroundLayer) },
  // Retourne un fond de carte dont le titre est équivalent à celui passé en paramètre
  getBackgroundLayerByTitle: state => (title) => { return state.backgroundLayers.find(backgroundLayer => backgroundLayer.title === title) },
  // Retourne les features associées à une couche passée en paramètres (permet d'afficher la liste
  // des features et la couleur associée)
  getFeaturesInLayer: state => (layer) => {
    let featuresArray = []
    layer.getSource().on('change', function(evt) {
      let source = evt.target
      if (source.getState() === 'ready') {
        let features = evt.target.getFeatures()
        for (let i = 0; i < features.length; i++) {
          if (!featuresArray.includes(features[i])) {
            featuresArray.push(features[i])
          }
        }
      }
    })

    // Cas où le bandeau a été refermé, il faut recharger les features
    // Car la source ne passe jamais dans le cas 'change' ... A AMELIORER
    if (featuresArray.length === 0 && layer.getSource().getState() === 'ready') {
      let features = layer.getSource().getFeatures()
      for (let i = 0; i < features.length; i++) {
        if (!featuresArray.includes(features[i])) {
          featuresArray.push(features[i])
        }
      }
    }
    return featuresArray
  },
  // Retourne les données complètes de la requête 'getCapabilities' adressée à geoserver au chargement de l'application
  // En format json
  getCapabilities: state => { return state.capabilities },
  // Retourne les données relatives aux couches 'Layer' en format json contenues dans le résultat de la requête
  // 'getCapabilities' adressée à geoserver au chargement de l'application
  getJsonLayers: state => { return state.jsonLayers },
  // Retourne le titre du layer passé en paramètre
  getJsonLayerTitleById: state => (id) => {
    for (let i = 0; i < state.jsonLayers.length; i++) {
      for (let j = 0; j < state.jsonLayers[i].children.length; j++) {
        if (state.jsonLayers[i].children[j].id === id) {
          return state.jsonLayers[i].children[j].label
        }
      }
    }

  },
}

const mutations = {
  // PROVISOIRE : SAUVEGARDE DE TOUTES LES LAYERS EN FORMAT JSON DANS UN TABLEAU
  // POUR VOIR LEUR ATTRIBUTS
  GET_CAPABILITIES(state, jsonFile) {
    state.capabilities = jsonFile
    for (let i = 0; i < state.capabilities.elements[1].elements[1].elements[3].elements.length; i++) {
      if (state.capabilities.elements[1].elements[1].elements[3].elements[i].name === "Layer") {
        let newElmt = {
          id: state.capabilities.elements[1].elements[1].elements[3].elements[i].elements[0].elements[0].text,
          label: state.capabilities.elements[1].elements[1].elements[3].elements[i].elements[1].elements[0].text,
          jsonLayer: state.capabilities.elements[1].elements[1].elements[3].elements[i]
        }
        if (newElmt.id.includes('commons')) { state.jsonLayers[0].children.push(newElmt) }
        if (newElmt.id.includes('eclats')) { state.jsonLayers[1].children.push(newElmt) }
        if (newElmt.id.includes('oldMaps')) { state.jsonLayers[2].children.push(newElmt) }
        if (newElmt.id.includes('mapWrapper')) { state.jsonLayers[3].children.push(newElmt) }
        if (newElmt.id.includes('SANDRE')) { state.jsonLayers[4].children.push(newElmt) }
        if (newElmt.id.includes('BRGM')) { state.jsonLayers[5].children.push(newElmt) }
      }
    }

    // Tri des tables contenant les layers dans l'odre alphabétique
    for (let i = 0; i < state.jsonLayers.length; i++) {
      state.jsonLayers[i].children.sort(function(layer1, layer2) {
        if (layer1.label < layer2.label) //sort string ascending
          return -1
        if (layer1.label > layer2.label)
          return 1
        return 0
      })
    }

  },
  // Génère la carte openLayer
  SET_OLMAP(state, map) { state.olmap = map },
  // Ajoute la couche au tableau de layer
  ADD_LAYER(state, layer) {
    state.olmapLayers.push(layer)
    if (state.olmap) {
      state.olmap.addLayer(layer)
    }
  },
  // Supprime la couche en paramètre du tableau de layer
  REMOVE_LAYER(state, layer) {
    state.olmapLayers.splice(state.olmapLayers.indexOf(layer), 1)
    state.olmap.removeLayer(layer)
  },
  // Supprime toutes les couches du tableau de layer expecté le fond de carte et rafraichi
  REMOVE_ALL_LAYERS(state) {
    state.olmapLayers.splice(1)
    state.olmap.getLayers().getArray().splice(1, state.olmap.getLayers().getArray().length)
    state.olmap.renderSync()
  },
  // Création et ajout d'une nouvelle couche vectorielle contenant la feature sélectionnée par l'utilisateur
  // Seulement si la feature n'a pas déjà été sélectionnée
  SET_SELECTED_FEATURE(state, params) {
    let exists = false;
    for (let i = 0; i < state.olmapLayers.length; i++) {
      if (state.olmapLayers[i].get('title') === ('feature_' + params.title)) {
        exists = true
      }
    }

    if (!exists) {
      let newVectorLayer
      // ------------------- COUCHE VECTORIELLE ------------------ //
      if (params.isShinyLayer) {
        let newVectorSource = new VectorSource()
        newVectorSource.addFeature(params.feature)
        newVectorLayer = new VectorLayer({
          title: 'feature_' + params.feature.get('modality'),
          source: newVectorSource,
          style: new Style({
            fill: new Fill({
              color: params.feature.get('color')
            })
          })
        })
      }
      // ------------- COUCHE IMAGE ------------- //
      if (!params.isShinyLayer) {
        newVectorLayer = new VectorLayer({
          title: 'feature_' + params.feature[0].values_.modality,
          points: params.feature[0].values_.spIdentifi ? params.feature[0].values_.spIdentifi : null,
          source: new VectorSource({
            features: params.feature
          }),
        })
      }
      // Ajout de la couche vectorielle contenant la feature sélectionnée à la map
      state.olmap.addLayer(newVectorLayer)
      newVectorLayer.setZIndex(state.olmapLayers.length + 1)
      state.olmapLayers.push(newVectorLayer)
    }
  }
}

const actions = {
  // Requête permettant de récupérer les différentes couches disponibles sur geoserver
  getCapabilities(context) {
    return axios.get(state.url_geoserver + 'wms?service=wms&version=1.1.1&request=GetCapabilities').then(response => {
      context.commit('GET_CAPABILITIES', JSON.parse(convert.xml2json(response.data)))
    })
  },
  // Instanciation de la carte openlayer
  setOlMap(context) {
    context.commit('SET_OLMAP',
      new Map({
        target: 'map',
        layers: state.olmapLayers,
        view: new View({ center: fromLonLat([3, 46]), zoom: 6 })
      })
    )
  },
  // Ajout d'une couche dans la liste des couches du visualiseur OL
  addLayer(context, layer) {
    return context.commit('ADD_LAYER', layer)
  },
  // Supprime une couche
  removeLayer(context, layer) {
    return context.commit('REMOVE_LAYER', layer)
  },
  removeAllLayers(context) {
    return context.commit('REMOVE_ALL_LAYERS')
  },
  // Créer une couche à partir de la feature sélectionnée par l'utilisateur
  setSelectedFeature(context, params) {
    return context.commit('SET_SELECTED_FEATURE', params)
  }
}

// ---------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------
// INITIALISATION DU STORE ET EXPORT
// ---------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------

// Création du store à partir des state
let store = new Vuex.Store({
  state: state,
  getters: getters,
  mutations: mutations,
  actions: actions,
  modules: {
    olStyles
  }
})

// Export du store
export default store