# Lancement EclatsApps

Instructions pour le lancement des applications EclatsApps

## Récupération du repository

``` bash
# SSH
git clone git@gricad-gitlab.univ-grenoble-alpes.fr:ECLATS/eclatsApps.git
# HTTPS
git clone https://gricad-gitlab.univ-grenoble-alpes.fr/ECLATS/eclatsApps.git
# branch developpement
git checkout developpement
```

Les applications Cartodialect et DialectoLOD partagent le même backend "eclatsApps-back-end"

## [GraphDB](http://graphdb.ontotext.com/)

#### Lancer graphDB sur LINUX:

``` bash
# Aller dans le dossier d'installation de graphDB
cd graphdb-free-8.6.1/bin
# Lancement
./graphdb -d -p ~/graphdbPID
```

L'option -p permet de stocker dans le fichier ~\graphdbPID l'identifiant du processus graphdb. Tuer ce processus permet d'arrêter proprement l'application (sans risque de corrompre les repositories). Par exemple, si graphdbID contient le numéro de process 12354

``` bash
# Arrêter l'application
kill 12354
```

#### Lancer graphDB sur WINDOWS:

Double click sur le logo de l'application 

## Back-end - [SpringBoot](https://spring.io/projects/spring-boot)

``` bash
# répertoire back-end
cd eclatsApps/eclatsApps-back-end

# lance le back-end sur localhost:8080
mvn spring-boot:run
```
*Nécessite [Maven](https://maven.apache.org/)*

Ou directement dans un IDE tel Nebeans ou Eclipse

## Cartodialect Front-end - [VueJs](https://fr.vuejs.org/)

``` bash
# répertoire front-end
cd eclatsApps/cartodialect-front-end

# installer les dépendences
npm install

# lance le front-end sur localhost:8081
npm run dev
```

*Nécessite [nodeJs](https://nodejs.org/en/)*

## DialectoLOD Front-end - [VueJs](https://fr.vuejs.org/)

``` bash
# répertoire front-end
cd eclatsApps/dialectolod-front-end

# installer les dépendences
npm install

# lance le front-end sur localhost:8081
npm run dev
```

*Nécessite [nodeJs](https://nodejs.org/en/)*