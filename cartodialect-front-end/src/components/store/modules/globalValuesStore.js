const state = {
/**
    * La liste des types de réponses.
    * A mettre à jour une fois que Carole aura donné les informations nécessaires
    */
  typesList: [
    'Phrase',
    'Syntagme',
    'Lemme',
    '',
  ],

  /**
 * La liste des types de composants.
 * A mettre à jour une fois que Carole aura donné les informations nécessaires
 */
  grammaticalNatureList: [
    '',
    'Adjectif',
    'Adverbe',
    'Conjonction de coordination',
    'Conjonction de subordination',
    'Déterminant',
    'Nom commun',
    'Nom propre',
    'Préposition',
    'Verbe',
    'Pronom personnel',
    'Pronom démonstratif',
    'Pronom indéfini',
    'Pronom interrogatif',
    'Pronom relatif'
  ],

  /**
   * La liste des langages d'origine d'un etymon
   */
  languageEtymonList: [
    '',
    'Latin',
    'Arabe',
    'Anglais',
    'Ancien allemand',
    'Ancien haut allemand',
    'Ancien francique',
    'Ancien français',
    'Ancien germanique',
    'Ancien picard',
    'Ancien provençal',
    'Basque',
    'Breton',
    'Flamand',
    'Francique',
    'Gascon',
    'Gaulois',
    'Grec',
    'Hébreu',
    'Moyen français',
    'Néerlandais',
    'Parsi ou Langue persanne',
    'Turc',
    'Origine inconnue',
    'Autre'
  ],

  /**
   * Liste des choix concernant le niveau de confiance d'une annotation
   */
  uncertaintyList: [
     'Certain',
    'Incertain',
    'Très incertain',
    ''
  ],

  /**
   * Liste des genres que l'utlisateur peut associer à un etymon
   */
  genderList: [
    'Féminin',
    'Masculin',
    ''
  ],

  /**
   * Liste des genres que l'utlisateur peut associer à un etymon
   */
  numberList: [
    'Singulier',
    'Pluriel',
    '',
  ],

  /**
   * Informations complémentaires à une transcription phonétique (masculin, féminin, pluriel)
   */
  phoneticTranscriptionComplementaryInfoList: [
    'm',
    'f',
    'mp',
    'fp',
    'p',
    '',
  ],

  confidentialityList: [
    'confidentielle',
    'restreinte',
    'publique'
  ]
}

const getters = {
  getTypesList: state => { return state.typesList },
  getGrammaticalNatureList: state => { return state.grammaticalNatureList },
  getLanguageEtymonList: state => { return state.languageEtymonList },
  getUncertaintyList: state => { return state.uncertaintyList },
  getGenderList: state => { return state.genderList },
  getNumberList: state => { return state.numberList },
  getPhoneticTranscriptionComplementaryInfoList: state => { return state.phoneticTranscriptionComplementaryInfoList },
  getConfidentialityList: state => { return state.confidentialityList }
}


export let globalValuesStore = {
  state: state,
  getters: getters
}
