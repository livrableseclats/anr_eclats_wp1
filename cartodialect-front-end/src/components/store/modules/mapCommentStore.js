import axios from 'axios'

const state = {
  mapCommentsList: []
}

const getters = {
  getMapCommentsList: state => { return state.mapCommentsList }
}

const mutations = {
  SET_MAP_COMMENTS_LIST(state, data) { state.mapCommentsList = data }
}

const actions = {
  // Récupère la liste des commentaires associées à la carte en paramètre
  getMapCommentsList(context, mapId) {
    return axios.get(context.rootState.url_serveur + '/findAllMapComment', {
      params: { mapId: mapId }
    }).then(response => { context.commit('SET_MAP_COMMENTS_LIST', response.data) })
  },
  // Ajoute un commentaire de carte à la carte passée en paramètre du formulaire mapCommentForm
  addMapComment(context, mapCommentForm) {
    return axios.post(context.rootState.url_serveur + '/addMapComment',
      mapCommentForm, {
        auth: {
          username: context.rootGetters.getCurrentUser.username,
          password: context.rootGetters.getCurrentUser.password
        },
      }).then(response => { context.dispatch('getMapCommentsList', newMapComment.mapId)
    }).catch(err => err);
  },
  // Supprime le commentaire de carte dont l'id est mapCommentId
  deleteMapComment(context, mapCommentId) {
    return axios.delete(context.rootState.url_serveur + '/deleteMapComment', {
      params: { mapCommentId: mapCommentId },
      auth: {
        username: context.rootGetters.getCurrentUser.username,
        password: context.rootGetters.getCurrentUser.password
      }
    }).then(response => { context.dispatch('getMapCommentsList', newMapComment.mapId)
   }).catch(err => err);
  }
}

export let mapCommentStore = {
  state: state,
  getters: getters,
  mutations: mutations,
  actions: actions
}