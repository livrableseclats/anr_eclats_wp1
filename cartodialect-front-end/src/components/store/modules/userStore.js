import axios from 'axios'

const state = {
  currentUser: {
    username: '',
    password: '',
    organization: ''
  },
  isAuthenticated: false
}

const getters = {
  getCurrentUser: state => { return state.currentUser },
  getIsAuthenticated: state => { return state.isAuthenticated }
}

const mutations = {
  UPDTATE_CURRENT_USER(state, data) {
    if (data != null) {
      state.currentUser.username = data.username
      state.currentUser.password = data.password
      state.isAuthenticated = true
    } else {
      state.currentUser.username = ''
      state.currentUser.password = ''
      state.isAuthenticated = false
    }
  }
}


const actions = {
  login(context, data) {
    axios.get(context.rootState.url_serveur + '/login', {
        auth: {
          username: data.username,
          password: data.password
        }
      })
      .then((result) => {
        if (result.data.name) {
          return context.dispatch('setCurrentUser', data)
        }
      })
      .catch((error) => {
        alert('Identifiant ou mot de passe erroné')
      })
  },
  setCurrentUser(context, data) {
    context.commit('UPDTATE_CURRENT_USER', data)
  },
  logOut(context) {
    context.commit('UPDTATE_CURRENT_USER', null)
  }
}

export let userStore = {
  state: state,
  getters: getters,
  mutations: mutations,
  actions: actions
}