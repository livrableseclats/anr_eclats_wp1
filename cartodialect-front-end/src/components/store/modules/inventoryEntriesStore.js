import axios from 'axios'
import * as d3 from 'd3'

const state = {
  // La couche svg générée avec openseadron-svg-overlay
  overlay: null,
  // La liste des entrées d'inventaires associées à la carte courante pour l'affichage des disques D3
  inventoryEntriesList: [],
  // Carte courante
  currentMap: null,
  // Point d'enquête cliqué sur la carte courante
  currentInventoryEntry: null,
  // La liste des entrées d'inventaire ayant la même forme phonétique que l'entrée d'inventaire cliquée @currentInventoryEntry
  invEntrySameRousselotPhoneticFormList: [],
  // L'entrée d'inventaire sur laquelle il faut zoomer dans la "Carte N° I :  NOMS FRANÇAIS DES LOCALITÉS"
  inventoryEntryToZoom: null,
  // Le disque D3 sur lequel l'utilisateur a cliqué
  currentCircle: null,
  // Chaine de caractère, Correspond au mode 'interprétation' ou au mode 'transcription' phonétique
  // Permet d'envoyer des requêtes ou de gérer les disques D3 en fonction du mode
  mode: null,
  // Booléen, True si l'utilisateur est en mode interprétation OU en mode transcription phonétique, sinon false 
  // Permet principalement de gérer les boutons de la toolbar 
  isModeActivated: false,
  // La liste de toutes les entrées d'inventaire (utile pour le mode transcription phonétique)
  allInventoryEntries: []
}

const getters = {
  getOverlay: state => { return state.overlay },
  getInventoryEntriesList: state => { return state.inventoryEntriesList },
  getCurrentMap: state => { return state.currentMap },
  getCurrentCircle: state => { return state.currentCircle },
  getInventoryEntryBySpIdentifier: state => (spIdentifier) => { return state.inventoryEntriesList.find(list => list.spIdentifier == spIdentifier) },
  getCurrentInventoryEntry: state => { return state.currentInventoryEntry },
  getInvEntrySameRousselotPhoneticFormList: state => { return state.invEntrySameRousselotPhoneticFormList },
  getInventoryEntryToZoom: state => { return state.inventoryEntryToZoom },
  getMode: state => { return state.mode },
  getIsModeActivated: state => { return state.isModeActivated },
  getAllInventoryEntries: state => {return state.allInventoryEntries }
}

const mutations = {
  SET_OVERLAY(state, overlay) { state.overlay = overlay },
  SET_ALL_INVENTORYENTRIES(state, data) { state.inventoryEntriesList = data },
  UPDATE_CURRENT_MAP(state, mapId) { 
    state.currentMap = mapId 
    state.invEntrySameRousselotPhoneticFormList = []
  },
  UPDATE_CURRENT_CIRCLE(state, currentCircle) { state.currentCircle = currentCircle },
  UPDATE_CURRENT_INVENTORYENTRY_BY_SPIDENTIFIER(state, spIdentifier) {
    state.currentInventoryEntry = state.inventoryEntriesList.find(inventry => inventry.spIdentifier == spIdentifier)
  },
  UPDATE_CURRENT_INVENTORYENTRY(state, inventoryEntry) { state.currentInventoryEntry = state.inventoryEntriesList.find(inventry => inventry == inventoryEntry) },
  UPDATE_CURRENT_INVENTORYENTRY_TO_ZOOM(state, inventoryEntry) { state.inventoryEntryToZoom = inventoryEntry },
  UPDATE_INVENTRY_SAME_PHONETIC_RESPONSE(state, phoneticTranscription) {
    console.log('UPDATE_INVENTRY_SAME_PHONETIC_RESPONSE mise à jour de la liste des entrées d\'inventaires ayant la même réponse phonétique')
    state.invEntrySameRousselotPhoneticFormList = []
    for (let i = 0; i < state.allInventoryEntries.length; i++) {
      if (state.allInventoryEntries[i].rousselotPhoneticForm === phoneticTranscription) {
        state.invEntrySameRousselotPhoneticFormList.push(state.allInventoryEntries[i])
      }
    }
  },
  UPDATE_MODE(state, mode) { state.mode = mode },
  UPDATE_MODE_ACTIVATION(state, isModeActivated) { state.isModeActivated = isModeActivated },
  UPDATE_ALL_INVENTORYENTRIES(state, allInventoryEntries) {
    state.allInventoryEntries = allInventoryEntries
  }
}

const actions = {

  // ---------------------------------------------------------------------------------------------
  // ENTREES D'INVENTAIRES
  // ---------------------------------------------------------------------------------------------

  // Requête HTTP pour récupérer la liste des entrées d'inventaires et affiche les disques d3 sur l'overlay
  getAllInventoryEntriesForD3(context, data) {
    context.commit('UPDATE_MODE', data.mode)
    context.commit('UPDATE_CURRENT_MAP', data.mapId)
    d3.selectAll('circle').remove()
    return new Promise((resolve) => {
      axios.get(context.rootState.url_serveur + '/findAllInventoryEntriesForD3', {
        params: {
          mapId: data.mapId,
          username: context.rootGetters.getCurrentUser.username
        }
      }).then(response => {
        context.commit('SET_ALL_INVENTORYENTRIES', response.data)
        // Une fois les données chargées, génération des disques d3
        if (response.data) {
          resolve(context.dispatch('displayCircle', data.mode))
        }
      })
    })
  },
  // Requête pour récupérer toutes les entrées d'inventaires
  findAllInventoryEntries(context, mapId) {
    axios.get(context.rootState.url_serveur + '/findAllInventoryEntries', {
      params: { mapId: mapId, }
    }).then(response => { context.commit('UPDATE_ALL_INVENTORYENTRIES', response.data) })
  },
 
  // Fonction appelée lorsque l'utilisateur souhaite visualiser la localisation d'un point d'enquête dans la "Carte N° I :  NOMS FRANÇAIS DES LOCALITÉS"
  // Recherche de l'entrée d'inventaire correspondante
  findInventoryEntry(context, inventoryEntry) {
    axios.get(context.rootState.url_serveur + '/findInventoryEntry', {
      params: { inventoryEntry: inventoryEntry }
    }).then(response => { context.commit('UPDATE_CURRENT_INVENTORYENTRY_TO_ZOOM', response.data) })
  },
  // Met à jour l'entrée d'inventaire cliquée 
  updateCurrentInventoryEntry(context, inventoryEntry) { context.commit('UPDATE_CURRENT_INVENTORYENTRY', inventoryEntry) },

  // ---------------------------------------------------------------------------------------------
  // D3
  // ---------------------------------------------------------------------------------------------
  
  // Création de la couche svg overlay sur le viewer
  setOverlay(context, overlay) { context.commit('SET_OVERLAY', overlay) },

  // Affichage des disques (couleurs par défaut des interprétations) dans la couche svg
  displayCircle(context, mode) {
      d3.selectAll('circle').remove()
      for (let i = 0; i < state.inventoryEntriesList.length; i++) {
      let rousselotPhoneticForm = state.inventoryEntriesList[i].rousselotPhoneticForm
      let d3Circle = d3.select(state.overlay.node()).append('circle')
        .attr('cx', state.inventoryEntriesList[i].xViewport)
        .attr('fill-opacity', 0.3)
        .attr('cy', state.inventoryEntriesList[i].yViewport)
        .attr('r', 0.006)
        .attr('id', state.inventoryEntriesList[i].spIdentifier)
        .attr('inventoryEntryId', state.inventoryEntriesList[i].inventoryEntry)
        .attr('rousselotPhoneticForm', state.inventoryEntriesList[i].rousselotPhoneticForm)
        .attr('complementaryInformation', state.inventoryEntriesList[i].complementaryInformation)
        .style('fill', evt => {
          if (mode === "interpretations") {
            return state.inventoryEntriesList[i].color
          } else {
            return (rousselotPhoneticForm === "forme phonétique non encore renseignée" ? "rgb(44, 62, 80)" : "rgb(111, 30, 81)")
          }
        })
        .style('stroke', evt => {
          if (state.inventoryEntriesList[i].isMultipleAuthor) {
            return d3.rgb(111, 30, 81);
          } else {
            return 'none';
          }
        })
        .style('stroke-width', evt => {
          if (state.inventoryEntriesList[i].isMultipleAuthor) {
            return 0.002;
          } else {
            return 'none';
          }
        })        
      // Clic sur un PE, si l'utilisateur effectue un "shift-click" en mode "phoneticTranscription" alors appel
      // de l'action située dans le store inventoryEntryPhoneticTranscriptionStore 
      state.overlay.onClick(d3Circle.node(), evt => {
        // Shift clic en mode transcription phonétique
        if(state.mode === "phoneticTranscription" && evt.shift && context.rootGetters.getIsPhoneticTranscriptionEditionMode) {
          // Cas où le point contient déjà une transcription phonétique alors ouverture de la fenêtre modale sinon ajout à la liste des disques cliqués
          if(d3Circle.node().attributes.rousselotPhoneticForm.value != "forme phonétique non encore renseignée" 
              && !(context.rootGetters.getIsDeleteMode)
              && !(context.rootGetters.getCircleList.find(circle => circle.id == +d3Circle.node().id))) {
            context.commit('UPDATE_SHOWMODAL', d3Circle)
          } else {
            if(!(d3Circle.node().attributes.rousselotPhoneticForm.value === "forme phonétique non encore renseignée" && context.rootGetters.getIsDeleteMode)) {
            context.dispatch('addShiftClickedCircle', { d3Circle: d3Circle, replace: true })
            }
          }    
        }
        // Simple clic
        else if (!evt.shift) {
          context.dispatch('clickedCircle', d3Circle)
        }
      })
    }
  },

  // Disque cliqué, mise à jour de l'entrée d'inventaire courante, du disque d3 correspondant et des ei dont les réponses phonétiques 
  // sont identiques. En fonction du mode, gestion des couleurs et/ou interprétations
  // appel de la fonction de mise à jour des disques en fonction du mode.
  clickedCircle(context, d3Circle){
    console.log('--------------- Clicked Circle ---------------')
    if(d3Circle.node() != state.currentCircle) {
      context.commit('UPDATE_CURRENT_CIRCLE', d3Circle.node())
      context.commit('UPDATE_CURRENT_INVENTORYENTRY_BY_SPIDENTIFIER', d3.select(state.currentCircle).attr('id'))
      context.dispatch('getResponses', state.currentInventoryEntry.inventoryEntry)
      if(state.mode === 'phoneticTranscription') {
      context.dispatch('restaureCirclesForPhoneticTranscriptionMode', state.invEntrySameRousselotPhoneticFormList)
    } else {
        context.dispatch('getAllInterpretations', state.currentInventoryEntry.inventoryEntry)
      }
    }
    context.commit('UPDATE_INVENTRY_SAME_PHONETIC_RESPONSE', state.currentInventoryEntry.rousselotPhoneticForm)
    context.dispatch('manageSelectedCircle')
    console.log('------------------------------')
  },

  // Réinitialise le diamètre et l'opacité de tous les points
  // Si un point a été cliqué, augmente le diamètre et l'opacité du PE cliqué
  // et modification de l'opacité des entrées d'inventaires ayant la même réponse phonétique que l'entrée d'inventaire cliquée
  manageSelectedCircle() {
  console.log('manageSelectedCircle: Réinitialise le diamètre et l`\'opacité de tous les points, le cercle cliqué en vert et plus gros')
  d3.selectAll('circle').attr('r', 0.006).attr('fill-opacity', 0.3)
    if (state.currentCircle != null) {
      if(state.mode === 'phoneticTranscription') {
        d3.select(state.currentCircle).attr('r', 0.01).attr('fill-opacity', 0.5).style('fill', 'green')
      } else {
        d3.select(state.currentCircle).attr('r', 0.01).attr('fill-opacity', 0.5)
       }
      for (let i = 0; i < state.invEntrySameRousselotPhoneticFormList.length; i++) {
        d3.select("[id='" + state.invEntrySameRousselotPhoneticFormList[i].spIdentifier + "']").attr('fill-opacity', 0.5)
        if(state.mode === 'phoneticTranscription' && state.currentCircle.attributes.rousselotPhoneticForm.value != "forme phonétique non encore renseignée") {
          d3.select("[id='" + state.invEntrySameRousselotPhoneticFormList[i].spIdentifier + "']").style('fill', 'green')
        }
      }
    } 
  },
  // Changement de la couleur des entrées d'inventaires concernées par l'interprétation consultée
  manageCircleForInterpretationConsultation(context, relatedInventoryEntries) {
    d3.selectAll('circle').remove();
    context.dispatch('displayCircle', 'interpretations')
    for (let i = 0; i < relatedInventoryEntries.length; i++) {
      d3.select("[id='" + relatedInventoryEntries[i].spIdentifier + "']")
        .style('fill', 'green')
        .style('stroke', 'none')        
    }
    // Permet de gérer le rayon et l'opacité du point cliqué
    setTimeout(() => { d3.select("[id='" + state.currentCircle.id + "']").attr('r', 0.01).attr('fill-opacity', 0.5) }, 10);
  },
  // Fermeture du bandeau latéral pour les interprétations ou pour les transcriptions phonétiques
  closeSideBar(context, mode) {
    if(mode === 'phoneticTranscription') {
    context.commit('UPDATE_ISDELETEMODE', false)
    context.commit('UPDATE_ISPHONETICTRANSCRIPTIONEDITIONMODE', false)
    context.commit('RESET_RESPONSESLIST')
    }
    context.dispatch('updateCurrentInventoryEntry', null)
    context.dispatch('manageSelectedCircle')
    context.dispatch('displayCircle', mode)
  }
}


export let inventoryEntriesStore = {
  state: state,
  getters: getters,
  mutations: mutations,
  actions: actions
}