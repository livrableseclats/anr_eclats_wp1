import axios from 'axios'

const state = {
  // Url vers europeana
  url_europeana: 'https://www.europeana.eu/api/v2/search.json?',
  // Europeana API Key
  europeanaApiKey: 'nbc6yxWZt',
  // OFFSET (max 100)
  numberOfRecords: 100,
  // La réponse de la requête envoyée à europeana
  europeanaResponse: [],
  // Les items contenus dans la réponse europeana
  europeanaItems: [],
  // La liste des ressources Europeana associée à la carte courante du visualiseur
  europanaResourcesList: []
}

const getters = {
  getEuropeanaItems: state => { return state.europeanaItems },
  getEuropeanaResponse: state => { return state.europeanaResponse },
  getEuropanaResourcesList: state => { return state.europanaResourcesList }
}

const mutations = {
  SET_EUROPEANA_RESULT_DATA(state, data) {
    state.europeanaResponse = data
    state.europeanaItems = data.items
  },
  RESET_EUROPEANA_DATA(state) {
    state.europanaResourcesList = []
    state.europeanaResponse = []
    state.europeanaItems = []
  },
  SET_EUROPEANA_RESOURCES_FOR_MAP(state, data) { state.europanaResourcesList = data }
}

const actions = {
  // Envoie de la requête permettant de récupérer les ressources Europeana selon les mots clés renseignés par l'utilisateur
  findEuropeanaResults(context, keyWords) {
    axios.get(state.url_europeana + 'query=' + keyWords + '&wskey=' + state.europeanaApiKey + '&rows=' + state.numberOfRecords)
      .then(response => { context.commit('SET_EUROPEANA_RESULT_DATA', response.data) })
  },
  // Restaure les tableaux contenant les résultats de la requête lancée à Europeana
  resetEuropeanaData(context) { context.commit('RESET_EUROPEANA_DATA') },
  // Envoi de la requête permettant d'associer une ressource Europeana à une carte
  addEuropeanaResourceForMap(context, europeanaResource) {
    return axios.post(context.rootState.url_serveur + '/addEuropeanaResourceForMap',
      europeanaResource, {
        auth: {
          username: context.rootGetters.getCurrentUser.username,
          password: context.rootGetters.getCurrentUser.password
        },
      }).catch(err => err);
  },
  // Envoie la requête permettant de récupérer la liste des ressources Europeana associées à la carte
  findEuropeanaResourcesForMap(context, mapIdentifier) {
    context.commit('RESET_EUROPEANA_DATA')
    return axios.get(context.rootState.url_serveur + '/findEuropeanaResourcesForMap', {
      params: {
        mapIdentifier: mapIdentifier
      }
    }).then(response => { context.commit('SET_EUROPEANA_RESOURCES_FOR_MAP', response.data) })
  }
}

export let europeanaStore = {
  state: state,
  getters: getters,
  mutations: mutations,
  actions: actions
}
