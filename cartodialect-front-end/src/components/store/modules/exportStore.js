import axios from 'axios'

const actions = {
  // Export des interprétations de la carte passée en paramètre 
  // (toutes les interprétations publiques, et les interprétations confidentielles créées par l'utilisateur courant s'il est connecté)
  exportInterpretations(context, mapId) {
    return axios.get(context.rootState.url_serveur + '/exportAllInterpretationsInMap', {
      params: {
        mapId: mapId,
        username: context.rootGetters.getCurrentUser.username
      }
    }).then(response => { context.dispatch('downloadFile', { body: response.data, namedRequest: 'interpretations_carte' + mapId + '.csv' }) })
  },
  // Export de la liste des transcriptions phonétiques associées aux entrées d'inventaire de la carte passée en paramètre
  exportPhoneticTranscription(context, mapId) {
    return axios.get(context.rootState.url_serveur + '/exportAllPhoneticTranscriptionInMap', {
      params: { mapId: mapId }
    }).then(response => { context.dispatch('downloadFile', { body: response.data, namedRequest: 'transcriptions_phonétiques_carte' + mapId + '.csv' }) })
  },
  // Requête permettant de mettre en forme la liste des cartes issues
  // SOIT de la recherche grammaticale en cours
  // SOIT de la recherche de cartes par intitulé (bootstrapTable)
  // pour les exporter en format csv
  exportMapSearchResult(context, data) {
    axios.post(context.rootState.url_serveur + '/exportMapSearchResult', { mapList: data.bootstrapTable })
      .then(response => { context.dispatch('downloadFile', { body: response.data, namedRequest: 'liste_cartes.csv' }) })
  },
  // Création d'un blob qui représente l'objet (fichier csv) avec la string  dans le corps de la requête
  // crée une chaîne contenant une URL représentant l’objet passé en paramètre. La durée de vie de l’URL est liée au
  // document de la fenêtre depuis laquelle elle a été créée. La nouvelle URL d’objet représente l’objet File ou Blob spécifié.
  downloadFile(context, data) {
    const blob = new Blob([data.body], { type: 'text/csv' });
    const url = window.URL.createObjectURL(blob);
    const a = document.createElement('a');
    a.href = url;
    a.download = 'export_' + data.namedRequest;
    document.body.appendChild(a);
    a.click();
    document.body.removeChild(a);
  }
}

export let exportStore = {
  actions: actions
}
