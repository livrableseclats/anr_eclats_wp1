import axios from 'axios'

const state = {
  // La liste des cartes dont le titre est inclu dans une thématique choisie par l'utilisateur
  mapsWithThematicCatList: [],
}

const getters = {
  getMapsWithThematicCatList: state => { return state.mapsWithThematicCatList }
}

const mutations = {
SET_MAPS_WITH_THEMATIC_CAT_LIST(state, data) { state.mapsWithThematicCatList = data }
}

const actions = {
  // Requête HTTP pour récupérer la liste des cartes associées aux catégories thématiques sélectionnées par l'utilisateur
  findMapsWithThematicCat(context, data) {
    axios.get(context.rootState.url_serveur + '/findMapsWithThematicCat', { params: { thematicCat: JSON.stringify(data) } })
    .then(response => { context.commit('SET_MAPS_WITH_THEMATIC_CAT_LIST', response.data) })
  }
}

export let thematicCatStore = {
  state: state,
  getters: getters,
  mutations: mutations,
  actions: actions
}
