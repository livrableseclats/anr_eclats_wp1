import axios from 'axios'
import * as d3 from 'd3'

const state = {
    selectedReferencePoint: 297,
    referencePoints: [
        { id: 297, label: 'Point d\'enquête n°297 (NORD)', xViewport: null, yViewport: null },
        { id: 987, label: 'Point d\'enquête n°987 (EST)', xViewport: null, yViewport: null },
        { id: 796, label: 'Point d\'enquête n°796 (SUD)', xViewport: null, yViewport: null },
        { id: 493, label: 'Point d\'enquête n°493 (OUEST)', xViewport: null, yViewport: null }
    ],
    tempInventries: []
}

const getters = {
    getSelectedReferencePoint: state => { return state.selectedReferencePoint },
    getReferencePoints: state => { return state.referencePoints },
    getTempInventries: state => { return state.tempInventries }
}

const mutations = {
    SET_SELECTED_REFERENCE_POINT(state, selectedReferencePoint) { state.selectedReferencePoint = selectedReferencePoint },
    SET_REFPOINT_COORDS(state, coords){
        state.referencePoints.find(surveyPoint => surveyPoint.id == state.selectedReferencePoint).xViewport = coords.x
        state.referencePoints.find(surveyPoint => surveyPoint.id == state.selectedReferencePoint).yViewport = coords.y
    },
    SET_TEMP_INVENTRIES(state, data) { state.tempInventries = data },
    // Rétablit l'état d'origine des variables du store
    RESET_DATA(state) {
        state.selectedReferencePoint = 297
        state.referencePoints = [
            { id: 297, label: 'Point d\'enquête n°297 (NORD)', xViewport: null, yViewport: null },
            { id: 987, label: 'Point d\'enquête n°987 (EST)', xViewport: null, yViewport: null },
            { id: 796, label: 'Point d\'enquête n°796 (SUD)', xViewport: null, yViewport: null },
            { id: 493, label: 'Point d\'enquête n°493 (OUEST)', xViewport: null, yViewport: null }
        ]
        state.tempInventries = []
    }
}

const actions = {
    // Gestion de l'affichage des quatre points de référence
    displayRefPointCircle(context, coords){
    // Suppression du disque précédent s'il s'agit d'un point de référence déjà renseigné 
    if(d3.select("[id='" + context.rootGetters.getSelectedReferencePoint + "']").node() != null) {
       d3.select("[id='" + context.rootGetters.getSelectedReferencePoint + "']").node().remove()
    }
    // Création du nouveau disque
    context.commit('SET_REFPOINT_COORDS', coords)
    d3.select(context.rootGetters.getOverlay.node()).append('circle')
        .attr('cx', coords.x)
        .attr('fill-opacity', 0.5)
        .attr('cy', coords.y)
        .attr('r', 0.006)
        .attr('id', context.rootGetters.getSelectedReferencePoint)
        .style('fill', 'red')
    // Passage au point suivant
    if(context.state.selectedReferencePoint != 493) {
        context.commit('SET_SELECTED_REFERENCE_POINT', 
        context.state.referencePoints[context.state.referencePoints.indexOf
            (context.state.referencePoints.find(surveyPoint => surveyPoint.id == state.selectedReferencePoint)) + 1].id)
        }
    },
    // Envoi de la requête permettant de récupérer la liste des entrées d'inventaires générées par l'algorithme
    // de génération d'entrées d'inventaires
    generateInventries(context) {
        return new Promise((resolve) => {
            axios.get(context.rootState.url_serveur + '/generateInventries', { 
                params: { 
                    mapId: context.getters.getMapByIndex(context.rootGetters.getIndex).mapId, 
                    referencePoints: JSON.stringify(state.referencePoints) 
                },
                auth: {
                      username: context.rootGetters.getCurrentUser.username,
                      password: context.rootGetters.getCurrentUser.password
                    }
        }).then(response => {
            context.commit('SET_TEMP_INVENTRIES', response.data)
                if (response.data) {
                    resolve(context.dispatch('displayTempInventries', response.data))
                }
            })
        })
    },
    // Ajoute les entrées d'inventaires générées au graphe
    addGenerateInventries(context){
        return  new Promise((resolve) => { 
            axios.post(context.rootState.url_serveur + '/generateInventries', null, { 
                params: {
                    mapId: context.getters.getMapByIndex(context.rootGetters.getIndex).mapId, 
                    referencePoints: JSON.stringify(state.referencePoints) 
                },
                auth: {
                      username: context.rootGetters.getCurrentUser.username,
                      password: context.rootGetters.getCurrentUser.password
                    }
        }).then(response => { 
            context.dispatch('resetInventryGenerator')
                if (response.status === 200) {
                    resolve(context.dispatch('updateIndex', context.rootGetters.getIndex))
                }
            })
        })
    },
    // Affiche les entrées d'inventaires temporaires 
    displayTempInventries(context, tempInventries){
        d3.selectAll('circle').remove()
        for(let i = 0; i < tempInventries.length; i++){
            d3.select(context.rootGetters.getOverlay.node()).append('circle')
            .attr('cx', tempInventries[i].xViewport)
            .attr('fill-opacity', 0.5)
            .attr('cy', tempInventries[i].yViewport)
            .attr('r', 0.006)
            .attr('id', tempInventries[i].id)
            .style('fill', 'red') 
        }
    },
    // Supprimer les disques d3 et rétablit l'état d'origine des variables du store
    resetInventryGenerator(context) {
        d3.selectAll('circle').remove()
        context.commit('RESET_DATA')
    }
}

export let inventryGeneratorStore = {
  state: state,
  getters: getters,
  mutations: mutations,
  actions: actions
}
