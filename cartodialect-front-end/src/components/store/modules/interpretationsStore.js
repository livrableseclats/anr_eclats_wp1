import axios from 'axios'

const state = {
  // La liste des réponses associées à l'entrée d'inventaire cliquée
  responsesList: [],
  // La liste des interprétations associées à l'entrée d'inventaire cliquée
  interpretationsList: [],
  // Le nombre de réponse identiques à celle sélectionnée pour la création d'une interprétation
  countOfIdenticalResponses: null,
  // La liste des réponses phonétiquement proche selon l'indice de lenvenshtein
  levenshteinList: [],
}

const getters = {
  getResponsesList: state => { return state.responsesList },
  getInterpretationsList: state => { return state.interpretationsList },
  getCountOfIdenticalResponses: state => { return state.countOfIdenticalResponses },
  getLevenshteinList: state => { return state.levenshteinList }
}

const mutations = {
  SET_RESPONSES(state, data) { state.responsesList = data },
  RESET_RESPONSESLIST(state) { state.responsesList = [] },
  SET_ALL_INTERPRETATIONS(state, data) { state.interpretationsList = data },
  UPDATE_COUNT_OF_IDENTICAL_RESPONSES(state, data) { state.countOfIdenticalResponses = data },
  SET_LEVENSHTEIN_LIST(state, data) {
    state.levenshteinList = []
    state.levenshteinList = data
  }
}

const actions = {
  // Charge la liste des réponses phonétique associées à une entrée d'inventaire
  getResponses(context, inventoryEntry) {
   return axios.get(context.rootState.url_serveur + '/findAllResponses', {
      params: { inventoryEntry: inventoryEntry }
    }).then(response => { context.commit('SET_RESPONSES', response.data) })
  },
  // Requête HTTP pour récupérer la liste des interprétations associées à l'entrée d'inventaire cliquée
  getAllInterpretations(context, inventoryEntry) {
    return axios.get(context.rootState.url_serveur + '/findAllInterpretations', {
      params: { inventoryEntry: inventoryEntry }
    }).then(response => { context.commit('SET_ALL_INTERPRETATIONS', response.data) })
  },
  // Envoi du formulaire d'ajout d'interprétation
  addInterpretation(context, interpretation) {
    return axios.post(context.rootState.url_serveur + '/addInterpretation', {
      params: interpretation
    }, {
      auth: {
        username: context.rootGetters.getCurrentUser.username,
        password: context.rootGetters.getCurrentUser.password
      },
    }).then(response => {
      context.dispatch('reloadInventy', interpretation.mapId)
    }).catch(err => err);
  },
  // Envoi de la requête permettant la suppression d'une interprétation
  deleteInterpretation(context, data) {
    return axios.delete(context.rootState.url_serveur + '/deleteInterpretation', {
      params: {interpretationId:data.interpretation.interpretationId},
      auth: {
        username: context.rootGetters.getCurrentUser.username,
        password: context.rootGetters.getCurrentUser.password
      },
    }).then(response => {
      if (!data.isEdition) {
        context.dispatch('reloadInventy', data.mapId)
      }
    })
  },
  // Envoi du formulaire correspondant à l'interprétation éditée
  editInterpretation(context, data) {
    context.dispatch('deleteInterpretation', { interpretation: data.interpretationToEdit, mapId: data.mapId, isEdition: true })
      .then(response => { context.dispatch('addInterpretation', data.interpretationToEdit) })
  },
  // Recherche des entrées d'inventaires concernée par l'interprétation consultée
  findRelatedInventoryEntries(context, interpretationId) {
    return axios.get(context.rootState.url_serveur + '/findRelatedInventoryEntries', {
      params: { interpretationId: interpretationId }
    }).then(response => { context.dispatch('manageCircleForInterpretationConsultation', response.data) })
  },
  // Recharge la couche svg des entrées d'inventaires (d3Circles)
  reloadInventy(context, mapId) {
    context.dispatch('getAllInventoryEntriesForD3', { mapId: mapId, mode: 'interpretations' }),
      context.dispatch('updateCurrentInventoryEntry', null)
  },
  // Renvoie le nombre de réponses phonétiquement identiques à celle passée en paramètre
  findCountOfIdenticalResponses(context, data) {
    return axios.get(context.rootState.url_serveur + '/findCountOfIdenticalResponses', {
      params: {
        mapId: data.mapId,
        rousselotPhoneticForm: data.rousselotPhoneticForm
      }
    }).then(response => { context.commit('UPDATE_COUNT_OF_IDENTICAL_RESPONSES', response.data) })
  },
  // Renvoie la liste des réponses phonétiquement proche (selon levenshtein) ainsi que leur occurence dans la carte
  findLevenshteinCorrespondance(context, data) {
    return axios.get(context.rootState.url_serveur + '/findLevenshteinCorrespondance', {
      params: {
        mapId: data.mapId,
        rousselotPhoneticForm: data.rousselotPhoneticForm,
        levenshteinDistance: data.levenshteinDistance,
        author: data.author
      }
    }).then(response => { context.commit('SET_LEVENSHTEIN_LIST', response.data) })
  }
}

export let interpretationsStore = {
  state: state,
  getters: getters,
  mutations: mutations,
  actions: actions
}