import axios from 'axios'

const state = {
  // La liste des cartes dont le titre comprend une catégorie grammaticale choisie par l'utilisateur
  mapsWithGramCatList: [],
  props: []
}

const getters = {
  getMapsWithGramCatList: state => { return state.mapsWithGramCatList },
  getProps: state => { return state.props }
}

const mutations = {
  SET_MAPS_WITH_CAT_GRAM_LIST(state, data) { state.mapsWithGramCatList = data },
  // Supprime et remplace la liste des propriétés
  SET_PROPS(state, data) {
    state.props.splice(data.index, 1)
    state.props.push(JSON.parse(JSON.stringify(data.response)))
  },
  RESET_PROPS(state) { state.props = [] },
  RESET_MAPS_WITH_CAT_GRAM_LIST(state) { state.mapsWithGramCatList = [] }
}

const actions = {
  // Requête HTTP pour récupérer la liste des propriétés associées à la catégorie grammaticale sélectionnée par l'utilisateur
  // L'attribut index permet de gérer le tableau contenant la liste des propriétés associée à chaque catgram
  findProps(context, data) {
    axios.get(context.rootState.url_serveur + '/findProps', { params: { concept: data.concept } })
      .then(response => { context.commit('SET_PROPS', { response: response.data, index: data.index }) })
  },
  // Requête HTTP pour récupérer la liste des cartes dont le titre comprend une catégorie grammaticale choisie par l'utilisateur
  findMapsWithGramCat(context, data) {
    axios.get(context.rootState.url_serveur + '/findMapsWithGramCat', { params: { selectedProperties: JSON.stringify(data) } })
      .then(response => { context.commit('SET_MAPS_WITH_CAT_GRAM_LIST', response.data) })
    context.commit('RESET_PROPS')
  },
  // Requête HTTP pour récupérer la liste des cartes dont le titre comprend plusieurs catégories grammaticale ainsi qu'un opérateur (ET/OU)
  findMapsWithGramCatWithOperator(context, data) {
    axios.get(context.rootState.url_serveur + '/findMapsWithGramCatWithOperator', { params: { selectedProperties: JSON.stringify(data) } })
      .then(response => { context.commit('SET_MAPS_WITH_CAT_GRAM_LIST', response.data) })
    context.commit('RESET_PROPS')
  },
  resetMapsWithGramCatList(context) {
    context.commit('RESET_MAPS_WITH_CAT_GRAM_LIST')
  }
}

export let catgramStore = {
  state: state,
  getters: getters,
  mutations: mutations,
  actions: actions
}