import axios from 'axios'
import * as d3 from 'd3'

const state = {
  // Booléen, Permet de savoir si l'utilisateur est en mode consultation ou édition (création, édition ou suppression) de transcriptions phonétiques, 
  // c'est à dire en train de créer éditer ou supprimer une transcription phonétique
  isPhoneticTranscriptionEditionMode: false,
  // Permet de savoir si on est en mode suppression de TP
  isDeleteMode: false,
  // Pour la transcription phonétique, La liste des disques cliqués ainsi que les disques ayant la même transcription phonétique que le premier point cliqué
  circleList: [{ id: null, initialPhoneticTranscription: '', replace: false }],
  // Le booléen permettant d'ouvrir la fenêtre modale gérant l'ajout ou le remplacement d'une transcription phonétique d'un PE qui en contient déjà une
  showModal: false,
  // Le disque shift cliqué contenant déjà une transcription phonétique
  modalCircle: null,
  // La valeur de l'input du clavier API
  apiKeyboardValue: ''
}

const getters = {
    getIsPhoneticTranscriptionEditionMode: state => { return state.isPhoneticTranscriptionEditionMode },
    getCircleList: state => { return state.circleList },
    getShowModal: state => { return state.showModal },
    getModalCircle: state => { return state.modalCircle },
    getIsDeleteMode: state => { return state.isDeleteMode },
    getApiKeyboardValue: state => { return state.apiKeyboardValue }
}

const mutations = {
    UPDATE_API_KEYBOARD_VALUE(state, data) { state.apiKeyboardValue = data }, 
    UPDATE_ISPHONETICTRANSCRIPTIONEDITIONMODE(state, isPhoneticTranscriptionEditionMode) { state.isPhoneticTranscriptionEditionMode = isPhoneticTranscriptionEditionMode },
    UPDATE_ISDELETEMODE(state, isDeleteMode) { state.isDeleteMode = isDeleteMode},
    // Ajoute le disque dans la liste des disque shift-cliqués s'il n'est pas déjà dans la liste et lui attribue la couleur verte
    // Si le disque existe déjà et qu'il est différent du premier disque cliqué (currentCircle), alors il est supprimé de la liste et retrouve sa couleur grise
    UPDATE_CIRCLE_LIST(state, data) {
      console.log('UPDATE_CIRCLE_LIST')
      let currentCircle = { 
        id: data.shiftClickedCircle.id , 
        initialPhoneticTranscription: data.shiftClickedCircle.attributes.rousselotPhoneticForm.value, 
        initialComplementaryInformation: data.shiftClickedCircle.attributes.complementaryInformation.value,
        replace: data.replace 
      }
      let alreadyExists = false      
      // Cas où le disque a déjà été sélectionné et qu'il est différent l'entrée d'inventaire courante => on l'enlève de la sélection
      if(data.shiftClickedCircle != state.currentCircle) {
        for(let i = 0; i < state.circleList.length; i++) {
          if(state.circleList[i].id == +currentCircle.id) {
            console.log('UPDATE_CIRCLE_LIST suppression du disque cliqué à la circleList et restauration de sa couleur par défaut')
            state.circleList.splice(i, 1)
            alreadyExists = true
          }
        }
        d3.select("[id='" + currentCircle.id + "']").style('fill', evt => {
          return currentCircle.initialPhoneticTranscription === "forme phonétique non encore renseignée"  ? "rgb(44, 62, 80)" : "rgb(111, 30, 81)"
        })
      }
      // Le disque n'existe pas dans la liste
      if(!alreadyExists) {
        console.log('UPDATE_CIRCLE_LIST ajout du disque courant à la circleList et coloration')
        state.circleList.push(currentCircle)
        d3.select("[id='" + currentCircle.id + "']").style('fill', 'green')
      }
      return state.circleList
    },
    // Ici on récupère la liste des disques correspondants aux entrées d'inventaires ayant la même réponse phonétique que le disque cliqué
    // Utile pour la consultation, édition de transcription phonétique.
    UPDATE_CIRCLE_LIST_FOR_CONSULTATION(state, list){
      console.log('UPDATE_CIRCLE_LIST_FOR_CONSULTATION', list)
      state.circleList = []
      for(let i = 0; i < list.length; i++) {
           state.circleList.push({ id : list[i].spIdentifier, 
            initialPhoneticTranscription: list[i].rousselotPhoneticForm,
            initialComplementaryInformation: list[i].complementaryInformation,
            replace: true })
      }
    },
    // Ré-initialise la liste des disques shif-cliqués. Ajoute le nouveau disque cliqué (d3Circle, le disque courant) au nouveau tableau
    RESET_CIRCLE_LIST(state, data) {  
      console.log('RESET_CIRCLE_LIST')
       state.circleList = []
       state.circleList.push({ 
        id : data.d3Circle.id, 
        initialPhoneticTranscription: data.d3Circle.attributes.rousselotPhoneticForm.value,
        initialComplementaryInformation: data.d3Circle.attributes.complementaryInformation.value,
        replace: data.replace })
        d3.select("[id='" + data.d3Circle.id + "']").style('fill', 'green')
      },
    UPDATE_SHOWMODAL(state, d3Circle) {
        if(state.showModal) {
          state.modalCircle = null
        } else {
          state.modalCircle = d3Circle
        }
        return state.showModal = !state.showModal
    }
}
const actions = {
    // Disque shif-cliqué, en mode édition de transcriptions phonétiques
    addShiftClickedCircle(context, data) { return context.commit('UPDATE_CIRCLE_LIST', { shiftClickedCircle: data.d3Circle.node(), replace: data.replace })},
    // ré-initialise la liste des disques cliqués
    resetCircleList(context, replace) { return context.commit('RESET_CIRCLE_LIST', { d3Circle: context.rootGetters.getCurrentCircle, replace: replace })},
    // Restaure les valeurs associées à la fenêtre modale utilisée pour savoir si l{utilisateur souhaite
    // remplacer ou ajouter une transcription phonétique à une transcription existante
    resetModal(context) { return context.commit('UPDATE_SHOWMODAL', null) },
    // Réinitialise les disques
    // Met à jour la liste 'invEntrySameRousselotPhoneticFormList' des entrées d'inventaires correspondant à la nouvelle forme phonétique et 
    // Met à jour la liste 'circleList'
    // recolore les disques correspondant à cette nouvelle liste
    reloadData(context, data) {
      console.log('--------------- reload Data ---------------')
      context.dispatch('restaureCirclesForPhoneticTranscriptionMode', data.invEntrySameRousselotPhoneticFormList)
      context.commit('UPDATE_INVENTRY_SAME_PHONETIC_RESPONSE', data.phoneticTranscription)
      context.commit('UPDATE_CIRCLE_LIST_FOR_CONSULTATION', context.rootGetters.getInvEntrySameRousselotPhoneticFormList)
      context.dispatch('manageSelectedCircle')
      console.log('-----------------------------------------')
    },
    // rétablissement des couleurs par défaut des points shif-cliqués et des entrées d'inventaires ayant la même réponse phonétique que le premier point cliqué
    restaureCirclesForPhoneticTranscriptionMode(context, list) {
      console.log('restaureCirclesForPhoneticTranscriptionMode')
      if(context.rootGetters.getCurrentInventoryEntry != null) {
        d3.select("[id='" + context.rootGetters.getCurrentInventoryEntry.spIdentifier + "']").style('fill', evt => {
          return (context.rootGetters.getCurrentInventoryEntry.rousselotPhoneticForm === "forme phonétique non encore renseignée" ? "rgb(44, 62, 80)" : "rgb(111, 30, 81)")
        })
      } 
      if(state.circleList.length > 0) {
        for(let i = 0; i < state.circleList.length; i++) {
        d3.select("[id='" +  state.circleList[i].id + "']").style('fill', evt => {
          return state.circleList[i].initialPhoneticTranscription === "forme phonétique non encore renseignée"  ? "rgb(44, 62, 80)" : "rgb(111, 30, 81)"
          })
        }
      }
      for (let i = 0; i < list.length; i++) {
          d3.select("[id='" + list[i].spIdentifier + "']").style('fill', evt => {
            return (list[i].rousselotPhoneticForm === "forme phonétique non encore renseignée" ? "rgb(44, 62, 80)" : "rgb(111, 30, 81)")
        }).attr('fill-opacity', evt => {
          return (list[i].spIdentifier != context.rootGetters.getCurrentInventoryEntry.spIdentifier ? 0.3 : 0.6)
        })
      }
    },
    // Ajoute/remplace la transcription phonétiques aux entrées d'inventaires sélectionnées
    addPhoneticTranscription(context, data) {
    console.log('addPhoneticTranscription circleList: ', state.circleList)
    console.log('addPhoneticTranscription TP: ', data.phoneticTranscription)
    console.log('addPhoneticTranscription Info: ', data.phoneticTranscriptionInfo)

    return axios.post(context.rootState.url_serveur + '/addPhoneticTranscription', null, {
            params: {
                mapId: context.rootGetters.getCurrentMap,
                phoneticTranscription: data.phoneticTranscription,
                phoneticTranscriptionInfo: data.phoneticTranscriptionInfo != null ? data.phoneticTranscriptionInfo : '' ,
                circleList: JSON.stringify(state.circleList),
            },
            auth: {
                username: context.rootGetters.getCurrentUser.username,
                password: context.rootGetters.getCurrentUser.password
            },
        }).then(response => {
            context.dispatch('updateCurrentInventoryEntry', null)
            context.dispatch('findAllInventoryEntries', context.rootGetters.getCurrentMap)
            context.dispatch('updateIndex', context.rootGetters.getIndex)
            context.dispatch('getAllInventoryEntriesForD3', { mapId: context.rootGetters.getCurrentMap, mode: 'phoneticTranscription' })
        })
    },
    // Supprime la transcription phonétique passée en paramètre pour les entrées d'inventaires contenues dans la liste des disques
    deletePhoneticTranscription(context, data) {
      console.log('deletePhoneticTranscription', state.circleList)
      return axios.post(context.rootState.url_serveur + '/deletePhoneticTranscription', null, {
        params: {
            mapId: context.rootGetters.getCurrentMap,
            phoneticTranscriptionToDelete: data.phoneticTranscriptionToDelete,
            complementaryInfoToDelete: data.complementaryInfoToDelete,
            circleList: JSON.stringify(state.circleList),
        },
        auth: {
            username: context.rootGetters.getCurrentUser.username,
            password: context.rootGetters.getCurrentUser.password
        },
    }).then(response => {
        context.dispatch('updateCurrentInventoryEntry', null)
        context.dispatch('findAllInventoryEntries', context.rootGetters.getCurrentMap)
        context.dispatch('updateIndex', context.rootGetters.getIndex)
        context.dispatch('getAllInventoryEntriesForD3', { mapId: context.rootGetters.getCurrentMap, mode: 'phoneticTranscription' })
        state.circleList = []
    })
    }
  }

export let phoneticTranscriptionsStore = {
    state: state,
    getters: getters,
    mutations: mutations,
    actions: actions
}