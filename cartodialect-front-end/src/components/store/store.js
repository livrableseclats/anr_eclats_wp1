import Vue from 'vue'
import Vuex from 'vuex'
import axios from 'axios'
import { inventoryEntriesStore } from '@components/store/modules/inventoryEntriesStore.js'
import { phoneticTranscriptionsStore } from '@components/store/modules/phoneticTranscriptionsStore.js'
import { interpretationsStore } from '@components/store/modules/interpretationsStore.js'
import { globalValuesStore } from '@components/store/modules/globalValuesStore.js'
import { userStore } from '@components/store/modules/userStore.js'
import { mapCommentStore } from '@components/store/modules/mapCommentStore.js'
import { catgramStore } from '@components/store/modules/catgramStore.js'
import { thematicCatStore } from '@components/store/modules/thematicCatStore.js'
import { exportStore } from '@components/store/modules/exportStore.js'
import { europeanaStore } from '@components/store/modules/europeanaStore.js'
import { inventryGeneratorStore } from '@components/store/modules/inventryGeneratorStore.js'

import jsonPackage from '../../../package.json'

Vue.use(Vuex)

// ---------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------
// STATE
// ---------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------


const state = {
  // Url vers le serveur
  url_serveur: 'http://localhost:8080/eclats5AppsBackEnd',
  // Version de l'application
  currentVersion: jsonPackage.version,
  // Liste des cartes de l'ALF
  mapList: [],
  // la liste des cartes 'épurée' de l'ALF pour la recherche de cartes par intitulé/identifiant
  mapListForTable: [],
  // La liste des url vers les tuiles des cartes de l'ALF
  tileSources: [],
  // L'index correspondant à la carte actuellement affichée dans le visualiseur
  index: 0,
  // La liste des poitns d'enquête de l'ALF
  surveyPointsList: [],
  // Booléen permettant de checker si les données des cartes de l'ALF ont été chargées dans le store
  hasLoadedData: false,
  // Les booléens permettant de connaitre le statut d'une carte
  hasInventries: false,
  missPhoneticTranscriptions: false,
  hasInterpretations: false
}

// ---------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------
// GETTERS
// ---------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------

const getters = {
  getUrlServeur: state => { return state.url_serveur },
  getCurrentVersion: state => { return state.currentVersion },
  // Retourne le tableau contenant la liste des cartes de l'ALF
  getMapList: state => { return state.mapList },
  // Retourne le tableau 'épuré' de la liste des cartes de l'ALF pour la recherche de cartes par intitulé/identifiant
  getMapListForTable: state => { return state.mapListForTable },
  // Retourne la carte d'index mapIndex
  getMapByIndex: state => (mapIndex) => { return state.mapList.find(map => map.mapIndex == mapIndex) },
  // Retourne le tableau contenant la liste des cartes de l'ALF
  getSurveyPointsList: state => { return state.surveyPointsList },
  // Retourne la liste des url vers les tuiles des cartes ALF
  getTileSources: state => { return state.tileSources },
  // Retourne l'index correspondant à la carte courante affichée dans le visualiseur
  getIndex: state => { return state.index },
  // Retourne le point d'enquête d'identifiant spIdentifier
  getSurveyPoint: state => (spIdentifier) => { return state.surveyPointsList.find(surveyPoint => surveyPoint.spIdentifier == spIdentifier) },
  getHasInventries: state => { return state.hasInventries },
  getMissPhoneticTranscriptions: state => { return state.missPhoneticTranscriptions },
  getHasInterpretations: state => { return state.hasInterpretations }
  }

// ---------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------
// MUTATIONS
// ---------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------

const mutations = {
  // Enregistre la liste des cartes de l'ALF
  SET_MAP_LIST(state, data) {
    state.mapList = data
    for (let i = 0; i < state.mapList.length; i++) {
      state.mapListForTable.push({
        'mapIdentifier': state.mapList[i].mapIdentifier,
        'mapTitle': state.mapList[i].mapTitle,
        'contextualizedTitle': state.mapList[i].contextualizedTitle,
        'bnfLinks': state.mapList[i].bnfLinks,
        'imageLink': state.mapList[i].imageLink,
        'mapTiffFormat': state.mapList[i].mapTiffFormat,
      })
      // Tableau comportant les liens vers les tuiles pour le visualiseur
      state.tileSources.push(state.mapList[i].imageLink)
    }
    state.hasLoadedData = true
  },
  SET_SURVEYPOINTS_LIST(state, data) { state.surveyPointsList = data },
  UPDATE_INDEX(state, index) { state.index = index },
  UPDATE_MAP_STATUS(state, data) {
    state.hasInventries = data.hasInventries
    state.missPhoneticTranscriptions = data.missPhoneticTranscriptions
    state.hasInterpretations = data.hasInterpretations
  }
}

const actions = {
  // Requête HTTP pour récupérer la liste des cartes de l'ALF
  getAllMaps(context) {
    return axios.get(state.url_serveur + '/findAllMaps')
      .then(response => (context.commit('SET_MAP_LIST', response.data)))
  },
  // Requête HTTP pour récupérer la liste des points d'enquête
  getAllSurveyPoints(context) {
    return axios.get(state.url_serveur + '/findAllSurveyPoints')
      .then(response => (context.commit('SET_SURVEYPOINTS_LIST', response.data)))
  },
  // Mise à jour de l'index correspondant à la carte courante dans le visualiseur
  updateIndex(context, index) {
    axios.get(state.url_serveur + '/getMapStatus', { params: { mapIdentifier: context.getters.getMapByIndex(index).mapId }})
      .then(response => (context.commit('UPDATE_MAP_STATUS', response.data)))
    context.commit("UPDATE_MODE", null)  
    return context.commit('UPDATE_INDEX', index)
  }
}

// ---------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------
// INITIALISATION DU STORE ET EXPORT
// ---------------------------------------------------------------------------------------------
// ---------------------------------------------------------------------------------------------

// Création du store à partir des state
let store = new Vuex.Store({
  state: state,
  getters: getters,
  mutations: mutations,
  actions: actions,
  modules: {
    inventoryEntriesStore,
    phoneticTranscriptionsStore,
    interpretationsStore,
    globalValuesStore,
    userStore,
    mapCommentStore,
    catgramStore,
    thematicCatStore,
    exportStore,
    europeanaStore,
    inventryGeneratorStore
  }
})

// Export du store
export default store
