// The Vue build version to load with the `import` command
// (runtime-only or standalone) has been set in webpack.base.conf with an alias.
import Vue from 'vue'
import Vuex from 'vuex'
import BootstrapVue from 'bootstrap-vue'
import 'bootstrap/dist/css/bootstrap.css'
import 'bootstrap-vue/dist/bootstrap-vue.css'
import VueRouter from 'vue-router'
import VueSidebarMenu from 'vue-sidebar-menu'
import "vue-sidebar-menu/src/scss/vue-sidebar-menu.scss";
import VueSlideoutPanel from 'vue2-slideout-panel';
import moment from 'moment'
import VModal from 'vue-js-modal'

import App from './App'
import Home from './components/layout/Home'
import SeadragonViewer from './components/commons/SeadragonViewer'
import Search from './components/search/Search'
import store from './components/store/store.js'

Vue.config.productionTip = false
Vue.use(BootstrapVue)
Vue.use(VueRouter)
Vue.use(Vuex)
Vue.use(VueSidebarMenu)
Vue.use(VueSlideoutPanel)
Vue.use(VModal)
Vue.filter('formatDate', function(value) {
  if (value) {
    return moment(String(value)).format('DD-MM-YYYY')
  }
});

const routes = [
  { path: '/', component: Home },
  { path: '/recherche', component: Search },
  { path: '/visualiseur', component: SeadragonViewer }
]
const router = new VueRouter({
  routes // raccourci pour `routes: routes`
})

/* eslint-disable no-new */
new Vue({
  router,
  store,
  el: '#app',
  components: { App },
  template: '<App/>'
})
