# ANR ECLATS Livrables lot 1



Ce projet Gitlab contient le code source des applications Cartodialect et DialectoLOD développées dans le cadre du projet ANR ECLATS (Lots 1 et 4). Ces applications s'intègrent au projet ANR ECLATS qui vise à fournir un outillage numérique innovant permettant le stockage, l’enrichissement sémantique, la visualisation des contenus géolinguistiques mais aussi la création de cartes interprétatives à partir des données extraites de l'ALF (Altas Linguistique de la France).



# Cartodialect

![image-20200220113539634](https://zupimages.net/up/20/09/fk2g.png)



L’application **Cartodialect** (http://cartodialect.imag.fr/) permet de visualiser et explorer les données cartographiques extraites de l’ALF, mais également d’annoter les réponses phonétiques associées à un point d’enquête dans une carte. Basée sur les technologies du web des données (linked data), les données sont réprésentés sour la forme d'un graphe de connaissances et stockées dans un triple store (RDF). L'application cartodialect offre une interface web qui permet de visualiser les cartes ALF en haute definition, d'effectuer des recherches avancées dans le corpuse de cartes (1920 cartes) et d'annoter les cartes.



-----------------------



 # DialectoLOD

![image-20200220113957396](https://zupimages.net/up/20/09/w3d6.png)

L’application **DialectoLOD** (http://lig-tdcge.imag.fr/steamer/eclats/dialectoLOD/) garantit la contextualisation et l’analyse des phénomènes géolinguistiques par la superposition de différentes couches linguistiques et thématiques publiées selon les spécifications WMS et WFS élaborées par l’OGC.



# Auteurs



Philippe GENOUD, Maeva SEFFAR 